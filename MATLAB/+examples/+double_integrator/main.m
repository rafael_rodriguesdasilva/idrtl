Ts = 1;
A = [1,Ts;0,1];
B = [Ts^2/2;Ts];
Au = [-1;1];
bu = [2,2]';
Ax = [-eye(2);eye(2)];
bx = [10,10,1.85,2]';
x0 = [1,-5.5]';

idRTL.init

sys = idRTL.LinearSystem(A,B,[],Ax,bx,Au,bu);
x1 = sys.x(1);
x2 = sys.x(2);

collide = idRTL.Or(idRTL.Proposition(-10 <= x1 & x1 <= -5 & -10 <= x2 & x2 <= -5),...
              idRTL.Proposition(-5 <= x1 & x1 <= 1.85 & -4 <= x2 & x2 <= -3));
target = idRTL.Proposition(-0.5 <= x1 & x1 <= 0.5 & -0.5 <= x2 & x2 <= 0.5);
roi = idRTL.Or(idRTL.Proposition(-6 <= x1 & x1 <= -5 & 1 <= x2 & x2 <= 2),...
              idRTL.Proposition(-5 <= x1 & x1 <= -4 & -3 <= x2 & x2 <= -2));

phi = idRTL.And(idRTL.Until(~collide,target),idRTL.Until(~target,roi));

N = 1;
elapsedtime=zeros(N,1);
for i=1:N
    tic
    [M,s,x,u] = idRTL.solve(0.001,0.0001,phi,sys,x0);
    elapsedtime(i) = toc;
end
disp(['Elapsed time is ',num2str(mean(elapsedtime)),' +/-',num2str(std(elapsedtime)),' seconds.']);
idRTL.kripke.plot_partition(M,0);
hold on;
plot(x(:,1),x(:,2),'b*--');

