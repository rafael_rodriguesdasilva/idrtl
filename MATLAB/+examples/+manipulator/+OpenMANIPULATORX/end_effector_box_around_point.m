function out = end_effector_box_around_point(p,dx,dy,sys)
%BOX_AROUND_POINT Summary of this function goes here
    x = sys.x(:);
    pe = examples.manipulator.OpenMANIPULATORX.x_to_pe(x);

    out = examples.manipulator.OpenMANIPULATORX.pred_to_prop(...
                              p(1)-dx <= pe(1) & pe(1) <= p(1)+dx & ...
                              p(2)-dy <= pe(2) & pe(2) <= p(2)+dy, sys);
end

