function o = obstacle(Ao,bo,sys,S)
%OBSTACLE Summary of this function goes here
%   Detailed explanation goes here
 
    x = sys.x(:);
    pj3 = examples.manipulator.OpenMANIPULATORX.x_to_pj3(x);
    pj4 = examples.manipulator.OpenMANIPULATORX.x_to_pj4(x);
    pe = examples.manipulator.OpenMANIPULATORX.x_to_pe(x);
    
    o = examples.manipulator.OpenMANIPULATORX.pred_to_prop(Ao*pj3<=bo,sys);
    o = idRTL.Or(o,examples.manipulator.OpenMANIPULATORX.pred_to_prop(Ao*pj4<=bo,sys));
    o = idRTL.Or(o,examples.manipulator.OpenMANIPULATORX.pred_to_prop(Ao*pe<=bo,sys));
    if S>2
        L = linspace(0,1,S);
        L(end) = [];
        L(1) = [];        
        for l=L
            o = idRTL.Or(o,examples.manipulator.OpenMANIPULATORX.pred_to_prop(...
                                                    Ao*(l*pj3+(1-l)*pj4)<=bo,sys));
            o = idRTL.Or(o,examples.manipulator.OpenMANIPULATORX.pred_to_prop(...
                                                     Ao*(l*pj4+(1-l)*pe)<=bo,sys));
        end
    end
end

