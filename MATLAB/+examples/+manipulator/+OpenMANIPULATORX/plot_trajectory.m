function plot_trajectory(x)
%PLOT_TRAJECTORY Summary of this function goes here
    hold on;
    pj2 = examples.manipulator.OpenMANIPULATORX.pj2';
    pj3 = examples.manipulator.OpenMANIPULATORX.x_to_pj3(x')';
    pj4 = examples.manipulator.OpenMANIPULATORX.x_to_pj4(x')';
    pe = examples.manipulator.OpenMANIPULATORX.x_to_pe(x')';
    plot(pj3(:,1),pj3(:,2),'b:*');
    plot(pj4(:,1),pj4(:,2),'b:*');
    plot( pe(:,1), pe(:,2),'b:*');
    for i=1:size(x,1)
        plot([  pj2(1),pj3(i,1)],[  pj2(2),pj3(i,2)],'b-','linewidth',3);
        plot([pj3(i,1),pj4(i,1)],[pj3(i,2),pj4(i,2)],'b-','linewidth',3);
        plot([pj4(i,1), pe(i,1)],[pj4(i,2), pe(i,2)],'b-','linewidth',3);
    end
    axis equal
    hold off;
end

