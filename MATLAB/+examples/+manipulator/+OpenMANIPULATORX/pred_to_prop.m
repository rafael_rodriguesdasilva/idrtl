function out = pred_to_prop(pi,sys)
%P_TO_PROP Summary of this function goes here
%   Detailed explanation goes here
    [A,b] = predicates2polytope(pi);
    out = idRTL.Proposition(A*sys.x(:)<=b);
end

