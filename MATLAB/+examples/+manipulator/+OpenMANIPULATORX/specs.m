function specs = specs()
%SPECS OpenMANIPULATOR-X specifications
    % dj<a><b> : distance between joint a and b
    specs.dj12 = 0.077; %m
    specs.dj23 = 0.130; %m
    specs.dj34 = 0.124; %m
    specs.dj4e = 0.126; %m
    specs.Vmax = 0.020; %m/s
    specs.dth = wrapTo2Pi(acos(0.128/0.130));
end

