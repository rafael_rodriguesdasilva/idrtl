function q = x_to_q(x)
%X_TO_THE Summary of this function goes here
%   Detailed explanation goes here

    K = size(x,2);
    thj2p = wrapTo2Pi(atan2(x(2,:),x(1,:)));
    thj3p = wrapTo2Pi(atan2(x(4,:),x(3,:)));
    thj4p = wrapTo2Pi(atan2(x(6,:),x(5,:)));
    dth = wrapTo2Pi(examples.manipulator.OpenMANIPULATORX.specs().dth);
        
    thj1 = zeros(1,K);
    thj2 = wrapToPi(wrapTo2Pi(pi/2-thj2p)-dth);
    thj3 = -wrapToPi(thj3p+thj2);
    thj4 = wrapToPi(thj3p-thj4p);
    e = zeros(1,K);
    q = [thj1', thj2', thj3', thj4', e']';
end

