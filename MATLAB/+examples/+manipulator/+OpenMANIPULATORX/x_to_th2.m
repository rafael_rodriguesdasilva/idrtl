function th2 = x_to_th2(x)
%X_TO_THE Summary of this function goes here
%   Detailed explanation goes here
    th2 = wrapTo2Pi(atan2(x(2,:),x(1,:)));
    th2 = wrapTo2Pi(th2-examples.manipulator.OpenMANIPULATORX.specs().dth);
    th2 = wrapToPi(pi/2-th2);
end

