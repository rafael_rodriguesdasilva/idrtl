function th3 = x_to_th3(x)
%X_TO_THE Summary of this function goes here
%   Detailed explanation goes here
    th3 = wrapToPi(wrapTo2Pi(atan2(x(4,:),x(3,:)))+...
        examples.manipulator.OpenMANIPULATORX.specs().dth);
end

