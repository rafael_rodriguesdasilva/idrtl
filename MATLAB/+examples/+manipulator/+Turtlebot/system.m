function sys = system(Ts)
%SYSTEM Summary of this function goes here

    specs = examples.manipulator.OpenMANIPULATORX.specs;
    dj23 = specs.dj23;
    dj34 = specs.dj34;
    dj4e = specs.dj4e;

    Axq_j12 = cell(4,1);
    bxq_j12 = cell(4,1);
    [Axq_j12{1},bxq_j12{1}] = utils.poly_containing_arc([0,0],dj23,pi,pi-pi/4);
    [Axq_j12{2},bxq_j12{2}] = utils.poly_containing_arc([0,0],dj23,pi-pi/4,pi-pi/2);
    [Axq_j12{3},bxq_j12{3}] = utils.poly_containing_arc([0,0],dj23,pi-pi/2,pi-3*pi/4);
    [Axq_j12{4},bxq_j12{4}] = utils.poly_containing_arc([0,0],dj23,pi-3*pi/4,0);

    Axq_j23 = cell(5,1);
    bxq_j23 = cell(5,1);
    [Axq_j23{1},bxq_j23{1}] = utils.poly_containing_arc([0,0],dj34,pi,pi-pi/4);
    [Axq_j23{2},bxq_j23{2}] = utils.poly_containing_arc([0,0],dj34,pi-pi/4,pi-pi/2);
    [Axq_j23{3},bxq_j23{3}] = utils.poly_containing_arc([0,0],dj34,pi-pi/2,pi-3*pi/4);
    [Axq_j23{4},bxq_j23{4}] = utils.poly_containing_arc([0,0],dj34,pi-3*pi/4,0);
    [Axq_j23{5},bxq_j23{5}] = utils.poly_containing_arc([0,0],dj34,0,-pi/4);

    Axq_j3e = cell(6,1);
    bxq_j3e = cell(6,1);
    [Axq_j3e{1},bxq_j3e{1}] = utils.poly_containing_arc([0,0],dj4e, 7*pi/8,9*pi/8);
    [Axq_j3e{2},bxq_j3e{2}] = utils.poly_containing_arc([0,0],dj4e, 5*pi/8,7*pi/8);
    [Axq_j3e{3},bxq_j3e{3}] = utils.poly_containing_arc([0,0],dj4e, 3*pi/8,5*pi/8);
    [Axq_j3e{4},bxq_j3e{4}] = utils.poly_containing_arc([0,0],dj4e,   pi/8,3*pi/8);
    [Axq_j3e{5},bxq_j3e{5}] = utils.poly_containing_arc([0,0],dj4e,  -pi/8, pi/8);
    [Axq_j3e{6},bxq_j3e{6}] = utils.poly_containing_arc([0,0],dj4e,-3*pi/8,-pi/8);

    n = 6;
    m = n;
    [Au,bu] = utils.box(-specs.Vmax*ones(1,m),specs.Vmax*ones(1,m));
    A = eye(n);
    B = eye(n,m)*Ts;
    c = zeros(n,1);

    % create a discrete-time linear hybrid system 
    sys = idRTL.LinearHybridSystem(n,m);


    for i=1:4
        for j=1:3
            for k=1:2
                ii = i;
                jj = i-1+j;
                kk = i-1+j-1+k;
                if jj<=5
                    Axq = blkdiag(Axq_j12{ii},Axq_j23{jj},Axq_j3e{kk});
                    bxq =        [bxq_j12{ii};bxq_j23{jj};bxq_j3e{kk}];
                    sys = add_mode(sys,A,B,c,Au,bu,Axq,bxq);
                end
            end
        end
    end
    

end

