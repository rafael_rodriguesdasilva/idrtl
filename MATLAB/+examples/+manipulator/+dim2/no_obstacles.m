close all;

Ts = 0.1;%s


% initialize internal variables
idRTL.init

% create a discrete-time linear hybrid system 
sys = examples.manipulator.OpenMANIPULATORX.system(Ts);

% p0 = [x0,z0]: is the initial position where x0 is the initial horizontal 
% and z0 is the initial vertical (height) position
specs = examples.manipulator.OpenMANIPULATORX.specs;
p0j23 = [         0,specs.dj23]'; 
p0j34 = [specs.dj34,         0]'; 
p0j4e = [specs.dj4e,         0]'; 
x0 = [p0j23;p0j34;p0j4e];
q0 = max(sys.get_modes_of(x0));


p_target = [0.339,0.095];
dx = 0.01;
dy = 0.01;
target = examples.manipulator.OpenMANIPULATORX.end_effector_box_around_point(...
    p_target,dx,dy,sys);

phi = idRTL.Eventually(target);

tic
M = idRTL.abstract(phi,sys,x0,q0);
toc
tic
[s,x,u] = idRTL.check(0.001,0.0001,M,phi);
toc

figure;
examples.manipulator.OpenMANIPULATORX.plot_trajectory(x);
hold on;

q = examples.manipulator.OpenMANIPULATORX.x_to_q(x');

if ~robotics.ros.internal.Global.isNodeActive
    rosinit
end
th2pub = rospublisher('/open_manipulator/joint2_position/command','std_msgs/Float64');
th3pub = rospublisher('/open_manipulator/joint3_position/command','std_msgs/Float64');
th4pub = rospublisher('/open_manipulator/joint4_position/command','std_msgs/Float64');
th2msg = rosmessage(th2pub);
th3msg = rosmessage(th3pub);
th4msg = rosmessage(th4pub);
q_0 = examples.manipulator.OpenMANIPULATORX.x_to_q(x0);
th2msg.Data = q_0(2);
th3msg.Data = q_0(3);
th4msg.Data = q_0(4);
send(th2pub,th2msg);
send(th3pub,th3msg);
send(th4pub,th4msg);

for k=1:size(x,1)
    tic
    th2msg.Data = q(2,k);
    th3msg.Data = q(3,k);
    th4msg.Data = q(4,k);
    send(th2pub,th2msg);
    send(th3pub,th3msg);
    send(th4pub,th4msg);
    elapsedtime = toc;
    assert(Ts>=elapsedtime);
    pause(Ts-elapsedtime);
end

