close all;

Ts = 0.5;%s
S = 2;

% initialize internal variables
idRTL.init

% create a discrete-time linear hybrid system 
sys = examples.manipulator.OpenMANIPULATORX.system(Ts);

% p0 = [x0,z0]: is the initial position where x0 is the initial horizontal 
% and z0 is the initial vertical (height) position
specs = examples.manipulator.OpenMANIPULATORX.specs;
p0j23 = [         0,specs.dj23]'; 
p0j34 = [specs.dj34,         0]'; 
p0j4e = [specs.dj4e,         0]'; 
x0 = [p0j23;p0j34;p0j4e];
q0 = max(sys.get_modes_of(x0));

p_target = [0.339,0.095];
dx = 0.01;
dy = 0.01;
target = examples.manipulator.OpenMANIPULATORX.end_effector_box_around_point(...
    p_target,dx,dy,sys);

[Ao1,bo1] = utils.box([0.254,0.165],[0.400,0.195]);
unsafe = examples.manipulator.OpenMANIPULATORX.obstacle(Ao1,bo1,sys,S);

phi = idRTL.Until(~unsafe,target);

tic
M = idRTL.abstract(phi,sys,x0,q0);
toc
tic
[s,x,u] = idRTL.check(0.001,0.0001,M,phi);
toc

figure;
examples.manipulator.OpenMANIPULATORX.plot_trajectory(x);
hold on;
plot(idRTL.polytope.polyshape(Ao1,bo1),'linewidth',3,'facecolor','k');
hold off;
