close all;

% requires to execute
% roscore
% roslaunch open_manipulator_gazebo open_manipulator_gazebo.launch

Ts = 0.5;%s
S = 2;

% initialize internal variables
idRTL.init

% create a discrete-time linear hybrid system 
sys = examples.manipulator.OpenMANIPULATORX.system(Ts);

% p0 = [x0,z0]: is the initial position where x0 is the initial horizontal 
% and z0 is the initial vertical (height) position
specs = examples.manipulator.OpenMANIPULATORX.specs;
p0j23 = [         0,specs.dj23]'; 
p0j34 = [specs.dj34,         0]'; 
p0j4e = [specs.dj4e,         0]'; 
x0 = [p0j23;p0j34;p0j4e];
q0 = max(sys.get_modes_of(x0));

p_target = [0.389,0.140];
dx = 0.01;
dy = 0.01;
target = examples.manipulator.OpenMANIPULATORX.end_effector_box_around_point(...
    p_target,dx,dy,sys);

p_home = [0.240,0.207];
dx = 0.01;
dy = 0.01;
home = examples.manipulator.OpenMANIPULATORX.end_effector_box_around_point(...
    p_home,dx,dy,sys);


[Ao1,bo1] = utils.box([0.250,0.145],[0.400,0.215]);
unsafe1 = examples.manipulator.OpenMANIPULATORX.obstacle(Ao1,bo1,sys,S);
[Ao2,bo2] = utils.box([0.250,0.000],[0.400,0.110]);
unsafe2 = examples.manipulator.OpenMANIPULATORX.obstacle(Ao2,bo2,sys,S);
unsafe = idRTL.Or(unsafe1,unsafe2);

phi = idRTL.Until(~unsafe,idRTL.And(target,idRTL.Until(~unsafe,home)));

tic
M = idRTL.abstract(phi,sys,x0,q0);
toc
tic
[s,x,u] = idRTL.check(0.001,0.0001,M,phi);
toc

figure;
examples.manipulator.OpenMANIPULATORX.plot_trajectory(x);
hold on;
plot(idRTL.polytope.polyshape(Ao1,bo1),'linewidth',3,'facecolor','k');
plot(idRTL.polytope.polyshape(Ao2,bo2),'linewidth',3,'facecolor','k');
hold off;

dt = examples.manipulator.OpenMANIPULATORX.dt;
K = (Ts/dt)*size(x,1);
xx = [utils.interparc(K,utils.mk_distinct(x(:,1)),utils.mk_distinct(x(:,2))),...
      utils.interparc(K,utils.mk_distinct(x(:,3)),utils.mk_distinct(x(:,4))),...
      utils.interparc(K,utils.mk_distinct(x(:,5)),utils.mk_distinct(x(:,6)))];
q = examples.manipulator.OpenMANIPULATORX.x_to_q(xx');

if ~robotics.ros.internal.Global.isNodeActive
    rosinit
end
th2pub = rospublisher('/open_manipulator/joint2_position/command','std_msgs/Float64');
th3pub = rospublisher('/open_manipulator/joint3_position/command','std_msgs/Float64');
th4pub = rospublisher('/open_manipulator/joint4_position/command','std_msgs/Float64');
th2msg = rosmessage(th2pub);
th3msg = rosmessage(th3pub);
th4msg = rosmessage(th4pub);
q_0 = examples.manipulator.OpenMANIPULATORX.x_to_q(x0);
th2msg.Data = q_0(2);
th3msg.Data = q_0(3);
th4msg.Data = q_0(4);
send(th2pub,th2msg);
send(th3pub,th3msg);
send(th4pub,th4msg);

for k=1:size(q,2)
    tic
    th2msg.Data = q(2,k);
    th3msg.Data = q(3,k);
    th4msg.Data = q(4,k);
    send(th2pub,th2msg);
    send(th3pub,th3msg);
    send(th4pub,th4msg);
    elapsedtime = toc;
    assert(dt>=elapsedtime);
    pause(dt-elapsedtime);
end

