function plot(A,b,color,alpha)
%PLOT_POLY Summary of this function goes here
%   Detailed explanation goes here
    subplot(1,3,1);hold on;
    pgon = idRTL.polytope.polyshape(A,b,'xsection',[3,4,5,6]);
    plot(pgon,'FaceColor',color,'FaceAlpha',alpha,'LineWidth',3);        
    axis equal
    subplot(1,3,2);hold on;
    pgon = idRTL.polytope.polyshape(A,b,'xsection',[1,2,5,6]);
    plot(pgon,'FaceColor',color,'FaceAlpha',alpha,'LineWidth',3);
    axis equal
    subplot(1,3,3);hold on;
    pgon = idRTL.polytope.polyshape(A,b,'xsection',[1,2,3,4]);
    plot(pgon,'FaceColor',color,'FaceAlpha',alpha,'LineWidth',3);
    axis equal
end

