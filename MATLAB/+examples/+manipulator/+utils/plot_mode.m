function plot_mode(M)
%PLOT_PARTITION Summary of this function goes here
%   Detailed explanation goes here

    Q = [M.Q{:}];
    subplot(1,3,1);
    for q=Q
        hold on;
        idRTL.kripke.plot_partition(M,q,'xsection',[3,4,5,6],'nostates',true);
    end
    hold off;
    subplot(1,3,2);
    for q=Q
        hold on;
        idRTL.kripke.plot_partition(M,q,'xsection',[1,2,5,6],'nostates',true);
    end
    hold off;
    subplot(1,3,3);
    for q=Q
        hold on;
        idRTL.kripke.plot_partition(M,q,'xsection',[1,2,3,4],'nostates',true);
    end
    hold off;
    
end

