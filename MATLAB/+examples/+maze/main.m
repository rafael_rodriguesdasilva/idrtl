close all;
clear variables;

idRTL.init
npassages=4;
N=1;


nint = 2;
dim = 2;
x0= [1 ; 1 ; zeros(dim*(nint-1),1)];
xd_lb = -2*ones(1,dim*(nint-1));
xd_ub =  2*ones(1,dim*(nint-1));
x_lb = [0,0,xd_lb];
x_ub = [30,30,xd_ub];


Ts = 0.5;
[A,B] = utils.intdiffeq(dim,nint,Ts);
m = size(B,2);
[Au,bu] = utils.box(-0.5*ones(m,1),0.5*ones(m,1));
[Ax,bx] = utils.box(x_lb,x_ub);

sys = idRTL.LinearSystem(A,B,[],Ax,bx,Au,bu);


if npassages>=1
    obstacles1 = utils.assert_box(sys.x,[0,5,xd_lb],[2,30,xd_ub]);
    obstacles2 = utils.assert_box(sys.x,[4,0,xd_lb],[6,27.5,xd_ub]);
    unsafe = idRTL.Or(...
                        idRTL.Proposition(obstacles1),...
                        idRTL.Proposition(obstacles2));
end
if npassages>=2
    obstacles3 = utils.assert_box(sys.x,[8,8,xd_lb],[10,30,xd_ub]);
    obstacles4 = utils.assert_box(sys.x,[12,0,xd_lb],[14,24.5,xd_ub]);
    unsafe = idRTL.Or(unsafe,idRTL.Or(...
                        idRTL.Proposition(obstacles3),...
                        idRTL.Proposition(obstacles4)));
end
if npassages>=3
    obstacles5 = utils.assert_box(sys.x,[16,11,xd_lb],[18,30,xd_ub]);
    obstacles6 = utils.assert_box(sys.x,[20,0,xd_lb],[22,21.5,xd_ub]);
    unsafe = idRTL.Or(unsafe,idRTL.Or(...
                        idRTL.Proposition(obstacles5),...
                        idRTL.Proposition(obstacles6)));
end
if npassages>=4
    obstacles7 = utils.assert_box(sys.x,[24,14,xd_lb],[26,30,xd_ub]);
    obstacles8 = utils.assert_box(sys.x,[28,0,xd_lb],[30,18.5,xd_ub]);
    unsafe = idRTL.Or(unsafe,idRTL.Or(...
                        idRTL.Proposition(obstacles7),...
                        idRTL.Proposition(obstacles8)));
end

if (1<=npassages)&&(npassages<=4)
    phi = idRTL.Until(~unsafe,...
       idRTL.Proposition(utils.assert_box(sys.x,[28,18.5,xd_lb],[30,30,xd_ub])));
end


elapsed_times = zeros(N,1);

for i=1:N
    tic
    [M,s,x,u] = idRTL.solve(0.001,0.0001,phi,sys,x0);
    elapsed_times(i) = toc;
end
disp(['Elapsed time is ',num2str(mean(elapsed_times)),' +/-',num2str(std(elapsed_times)),' seconds.']);
idRTL.kripke.plot_partition(M,0);
hold on;
plot(x(:,1),x(:,2),'b*--');

