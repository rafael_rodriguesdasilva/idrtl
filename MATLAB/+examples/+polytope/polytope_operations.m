% Test polytope operations and compare computational performance with mpt2

% simple polytope box
dim = 50;
iscompare = 0;
A1 = [eye(dim);-eye(dim)];
b1 = [ones(dim,1);ones(dim,1)];
A2 = [eye(dim);-eye(dim)];
b2 = [2*ones(dim,1);zeros(dim,1)];
A3 = [eye(dim);-eye(dim)];
b3 = [1;2*ones(dim-1,1);zeros(dim,1)];
A4 = [eye(dim);-eye(dim)];
b4 = [2*ones(dim,1);-1;-ones(dim-1,1)];
if iscompare
    poly1 = polytope(A1,b1);
    poly2 = polytope(A2,b2);
    poly3 = polytope(A3,b3);
    poly4 = polytope(A4,b4);
end

disp('creation with reduntant polytope representation');
A_in = [A1;A2];
b_in = [b1;b2];

tic
[A_out,b_out] = idRTL.polytope.create(A_in,b_in);
rcheb = idRTL.polytope.chebyradius(A_out,b_out);
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

if iscompare
    tic
    poly = polytope(A_in,b_in);
    elapsedtime = toc;
    disp(['mpt2 elapsed time ',num2str(1000*elapsedtime),'ms.']);
end

disp('intersection');

tic
[Aand,band] = idRTL.polytope.and(A1,b1,A2,b2);
rcheb_and = idRTL.polytope.chebyradius(Aand,band);
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

if iscompare
    tic
    poly_and = poly1 & poly2;
    elapsedtime = toc;
    disp(['mpt2 elapsed time ',num2str(1000*elapsedtime),'ms.']);
end
    
disp('difference');

tic
[Adiff,bdiff] = idRTL.polytope.difference(A1,b1,A2,b2);
% for i=1:length(Adiff)
%     rcheb_diff(i) = idRTL.polytope.chebyradius(Adiff{i},bdiff{i});
% end
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

if iscompare
    tic
    regionand = regiondiff(poly1,poly2);
    elapsedtime = toc;
    disp(['mpt2 elapsed time ',num2str(1000*elapsedtime),'ms.']);
end

disp('or');

tic
[Aor,bor] = idRTL.polytope.or(A1,b1,A2,b2);
% for i=1:length(Adiff)
%     rcheb_diff(i) = idRTL.polytope.chebyradius(Aor{i},bor{i});
% end
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

if iscompare
    tic
    regionor = poly1 | poly2;
    elapsedtime = toc;
    disp(['mpt2 elapsed time ',num2str(1000*elapsedtime),'ms.']);
end

disp('isfulldim');

tic
isfulldim1 = idRTL.polytope.isfulldim(A1,b1);
% for i=1:length(Adiff)
%     rcheb_diff(i) = idRTL.polytope.chebyradius(Aor{i},bor{i});
% end
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

disp('isinside');

tic
isinside1 = idRTL.polytope.isinside(A1,b1,zeros(dim,1));
% for i=1:length(Adiff)
%     rcheb_diff(i) = idRTL.polytope.chebyradius(Aor{i},bor{i});
% end
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

disp('is_adjacent_to');

tic
is_adjacent_to1 = idRTL.polytope.is_adjacent_to(A3,b3,A4,b4);
% for i=1:length(Adiff)
%     rcheb_diff(i) = idRTL.polytope.chebyradius(Aor{i},bor{i});
% end
elapsedtime = toc;
disp(['idRTL elapsed time ',num2str(1000*elapsedtime),'ms.']);

