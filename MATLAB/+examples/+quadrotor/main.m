close all;
clear variables;

idRTL.init
scenarionum=10;
N=1;


Ts = 1;
smoothness = 1;
%       x,y,z,p
nint = [4,4,4,2];
dim = 4;
n = sum(nint);
m = length(nint);
[A,B] = utils.intdiffeq(dim,nint,Ts);
[Au,bu] = utils.box(-0.5*ones(m,1),0.5*ones(m,1));

x0 = [0.5;0.5;0.5;zeros(n-3,1)];

px_ub           = [6,7,8,9,10,11,12,13,14,15];
py_ub           = [4,4,4,4, 4, 4, 4, 4, 4, 4];
pz_ub           = [4,4,4,4, 4, 4, 4, 4, 4, 4];
px_ub = px_ub(scenarionum);
py_ub = py_ub(scenarionum);
pz_ub = pz_ub(scenarionum);
xd_ub =  smoothness*ones(1,n-dim);
xd_lb = -smoothness*ones(1,n-dim);
x_ub = [px_ub,py_ub,pz_ub, pi,xd_ub];
x_lb = [    0,    0,    0,-pi,xd_lb];

[Ax,bx] = utils.box(x_lb,x_ub);
sys = idRTL.LinearSystem(A,B,[],Ax,bx,Au,bu);

obstaclesXTag               = [2,4,6,8,10,12,14];
obstaclesZTag               = [1,3];

obsnb_x = length(obstaclesXTag(obstaclesXTag<=px_ub));
obsnb_z = length(obstaclesXTag(obstaclesZTag<=pz_ub));
obsnb = obsnb_x+obsnb_z;
obs = cell(obsnb,1);
for i = 1:obsnb_x
    for j = 1:obsnb_z
        obs{(j-1)*obsnb_x+i} = utils.assert_box(sys.x,...
                [obstaclesXTag(i)-1,    0,obstaclesZTag(j)-1,-pi,xd_lb],...
                [  obstaclesXTag(i),py_ub,  obstaclesZTag(j), pi,xd_ub]);
    end
end
unsafe = idRTL.Or(idRTL.Proposition(obs{1}),idRTL.Proposition(obs{2}));
for i = 3:obsnb
    unsafe = idRTL.Or(unsafe,idRTL.Proposition(obs{i}));
end
goal = idRTL.Proposition(utils.assert_box(sys.x,...
                [px_ub-1,py_ub-1,pz_ub-1,-pi,xd_lb],...
                [px_ub  ,py_ub  ,pz_ub  , pi,xd_ub]));
            
phi = idRTL.Until(~unsafe,goal);


elapsed_times = zeros(N,1);

for i=1:N
    tic
    [M,s,x,u] = idRTL.solve(0.001,0.0001,phi,sys,x0);
    elapsed_times(i) = toc;
end
disp(['Elapsed time is ',num2str(mean(elapsed_times)),' +/-',num2str(std(elapsed_times)),' seconds.']);
idRTL.kripke.plot3_partition(M,0);
hold on;
plot3(x(:,1),x(:,2),x(:,3),'b*--');

