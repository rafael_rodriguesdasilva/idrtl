clear variables
close all


Ts = 1;
A = [1,0;0,1];
B = [Ts,0;0,Ts];
Au = [-eye(2);eye(2)];
bu = [2,2,2,2]';
Ax = [-eye(2);eye(2)];
bx = [0,0,10,10]';
x0 = [5,1]';

idRTL.init

sys = idRTL.LinearSystem(A,B,[],Ax,bx,Au,bu);
x1 = sys.x(1);
x2 = sys.x(2);

collide = idRTL.Or(...
            idRTL.Or(idRTL.Proposition(2.5 <= x1 & x1 <= 7.5 & 2.5 <= x2 & x2 <= 7.5),...
                idRTL.Proposition(2.5 <= x1 & x1 <= 3.5 & 1 <= x2 & x2 <= 2.5)),...
            idRTL.Proposition(6.5 <= x1 & x1 <= 7.5 & 0 <= x2 & x2 <= 1.5));
goal = idRTL.Proposition(2.5 <= x1 & x1 <= 7.5 & 7.5 <= x2 & x2 <= 10);

phi = idRTL.Until(~collide,goal);

M = idRTL.kripke.create(x0,0,[],[],[],...
                                {0},{sys.A},{sys.B},{sys.c},...
                                {sys.Ax},{sys.bx},...
                                {sys.Au},{sys.bu});


idRTL.kripke.plot(M);
for i=1:length(phi.As)
    pause;
    M = idRTL.kripke.add_polytope(M,phi.As{i},phi.bs{i},phi.label{i});
    idRTL.kripke.plot(M);
end