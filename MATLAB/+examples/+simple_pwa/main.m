x0 = [1,0.2]';

Ts = 1;
n = 2;
m = 2;
A = eye(n);
B = Ts*eye(m);
[Au,bu] = utils.box([-0.1,-0.1],[0.2,0.2]);


idRTL.init

sys = idRTL.LinearHybridSystem(n,m);
q0 = 0;
cq = [0.1;0.2];
Axq = [ 1, 1;
        0,-1;
       -1, 0];
bxq = [4;-0.1;-2];
sys = add_mode(sys,A,B,cq,Au,bu,Axq,bxq);
cq = [0;0];
Axq = [-1, 1;
        0,-1;
        1, 0];
bxq = [0;-0.1;2];
if all(abs(Axq*x0-bxq)<=eps) 
    q0 = 1;
end
sys = add_mode(sys,A,B,cq,Au,bu,Axq,bxq);
x1 = sys.x(1);
x2 = sys.x(2);

safe = idRTL.Proposition(x2-x1<=0 & x1+x2<=4 & -x2 <= -0.1);
target = idRTL.Proposition(x1+x2<=4 & -x2 <= -0.1 & -x1 <= -3 & x1 <= 5);

phi = idRTL.Until(safe,target);

tic
[M,s,x,u] = idRTL.solve(0.001,0.00001,phi,sys,x0,q0);
toc

<<<<<<< HEAD
idRTL.kripke.plot_partition(M,0);
hold on;
idRTL.kripke.plot_partition(M,1);
hold on;
plot(x(:,1),x(:,2),'b*--');
=======
idRTL.kripke.plot_partition(M);
% hold on;
% plot(x(:,1),x(:,2),'b*--');
>>>>>>> parent of 3522422... BUGFIX: bug at idkripke_z3_state (number of state bits)

