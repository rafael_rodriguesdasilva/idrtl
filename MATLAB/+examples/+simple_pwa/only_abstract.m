clear variables
close all


x0 = [1,0.2]';


Ts = 1;
n = 2;
m = 2;
A = eye(n);
B = Ts*eye(m);
[Au,bu] = utils.box([-0.1,-0.1],[0.2,0.2]);

idRTL.init

sys = idRTL.LinearHybridSystem(n,m);
q0 = 0;
cq = [0.1;0.2];
Axq = [ 1, 1;
        0,-1;
       -1, 0];
bxq = [4;-0.1;-2];
sys = add_mode(sys,A,B,cq,Au,bu,Axq,bxq);
cq = [0;0];
Axq = [-1, 1;
        0,-1;
        1, 0];
bxq = [0;-0.1;2];
if all(abs(Axq*x0-bxq)<=eps) 
    q0 = 1;
end
sys = add_mode(sys,A,B,cq,Au,bu,Axq,bxq);
x1 = sys.x(1);
x2 = sys.x(2);

target = idRTL.Proposition(x1+x2<=4 & -x2 <= -0.1 & -x1 <= -3 & x1 <= 5);

phi = idRTL.Eventually(target);

M = idRTL.kripke.create(x0,q0,[],[],[],...
                                [],[],[],[],...
                                [],[],...
                                [],[]);


for i=1:length(sys.Axq)
    M = idRTL.kripke.add_mode(M,sys.Q{i},sys.Aq{i},sys.Bq{i},sys.cq{i},...
                                sys.Axq{i},sys.bxq{i},sys.Auq{i},sys.buq{i});
    idRTL.kripke.plot(M);
    pause;
end
for i=1:length(phi.As)
    M = idRTL.kripke.add_polytope(M,phi.As{i},phi.bs{i},phi.label{i});
    idRTL.kripke.plot(M);
    pause;
end