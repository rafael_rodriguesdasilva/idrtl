/*
 * and.c
 *
 *  Created on: Apr 13, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=4) {
    	mexPrintf("idSTL.bsc.check\n");
        mexErrMsgIdAndTxt( "MATLAB:reach:invalidNumInputs",
                "Four inputs required (delta,delta_progress,M,phi).");
    } else if(nlhs!=3) {
    	mexPrintf("idSTL.bsc.check\n");
        mexErrMsgIdAndTxt( "MATLAB:reach:maxlhs",
                "Three outputs required (s,x,u).");
    }
#define IN_delta	    	prhs[0]
#define IN_delta_progress	prhs[1]
#define IN_M		    	prhs[2]
#define IN_phi		    	prhs[3]
#define OUT_s		    plhs[0]
#define OUT_x		    plhs[1]
#define OUT_u		    plhs[2]


    /* code here */
    id_set_global_constants();

    idbool_t is_fair = FALSE;
    idkripke_t *p_M = idkripke_fromMXArray_structure(IN_M);
    idformula_t *p_phi = idformula_fromMXArray(IN_phi,p_M);
    double 	delta = Dbl_fromMXArray(IN_delta),
    		delta_progress = Dbl_fromMXArray(IN_delta_progress);
    idrobrun_t *p_solution;

    if (idRTL_MATLAB(delta, delta_progress, p_phi, &p_solution)) {
		mexPrintf("Solution found.\n");
		OUT_s 		= MXArray_from_drun(p_solution->mp_states_intarray,p_solution->mp_nsample_intarray,p_M);
		OUT_x     	= MXArray_from_dblvector(p_solution->mp_x,p_solution->m_length,p_solution->m_n);
		OUT_u     	= MXArray_from_dblvector(p_solution->mp_u,p_solution->m_length,p_solution->m_m);
		p_solution->destroy(p_solution);
   } else {
		mexPrintf("No solution exits.\n");
		OUT_s		= mxCreateDoubleMatrix(0,0,mxREAL);
		OUT_x		= mxCreateDoubleMatrix(0,0,mxREAL);
		OUT_u		= mxCreateDoubleMatrix(0,0,mxREAL);
    }


    p_M->destroy(p_M);
    p_phi->destroy(p_phi);

    id_destroy_global_constants();

    return;
}
