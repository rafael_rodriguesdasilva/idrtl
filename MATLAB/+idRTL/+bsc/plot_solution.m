function plot_solution(M,Mplan,X,U,fontsize)
%PLOT_SOLUTION Summary of this function goes here
%   Detailed explanation goes here
    if nargin==4
        fontsize = 12;
    elseif nargin~=5
        error('Invalid number of arguments.');
    end
    
    if (isempty(M.Aw)||isempty(M.bw))
        error("The workspace is empty.");
    end
    
    clf;
    
    W = polytope(M.Aw,M.bw);
    nstates = length(M.As);

    cmap = lines;
    cmap_labels = cell(nstates,1);
    states_in_plan = cellfun(@(x) str2double(x)+1,Mplan.label);

    Options.color='k';
    Options.xdimension = 2;
    Options.shade = 0;
    plot(W,Options);    hold on;

    cnt = 0;
    for i=1:nstates
        si = polytope(M.As{i},M.bs{i});
        V = extreme(si);
        x = (max(V(:,1))+min(V(:,1)))/2;
        y = (max(V(:,2))+min(V(:,2)))/2;
        if isempty(M.label{i})
            Options.color='k';
            Options.shade = 0.2;
            plot(si,Options);
        else
            ism = cellfun(@(x) strcmp(x,M.label{i}),cmap_labels);
            if any(ism)
                Options.color=cmap(ism,:);
            else
                Options.color=cmap(cnt+1,:);
                cmap_labels{cnt+1} = M.label{i};
                cnt = mod(cnt+1,size(cmap,1));
            end
            Options.shade = 0.5;
            plot(si,Options);
        end
        text(x,y,['s_{',num2str(i),'}'],'FontWeight','bold','FontSize',fontsize,'HorizontalAlignment','center');
        if ismember(i,states_in_plan)
            Options.color='r';
            Options.shade = 0.25;
            plot(si,Options);
        end
    end
    
    plot(M.x0(1),M.x0(2),'bp','MarkerFaceColor','b','MarkerSize',12);
    if ~isempty(X)
        plot(X(:,1),X(:,2),'r*-','linewidth',3);
    end
    hold off;
    axis equal
    set(gca,'FontSize',fontsize)


end

