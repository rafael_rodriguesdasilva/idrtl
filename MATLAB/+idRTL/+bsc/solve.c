/*
 * and.c
 *
 *  Created on: Apr 13, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=16) {
    	mexPrintf("idSTL.bsc.solve\n");
        mexErrMsgIdAndTxt( "MATLAB:bsc:invalidNumInputs",
                "Sixteen inputs required (delta,delta_progress,x0,q0,Aw,bw,As,bs,label,modes,phi,A,B,c,Au,bu).");
    } else if(nlhs!=4) {
    	mexPrintf("idSTL.bsc.solve\n");
        mexErrMsgIdAndTxt( "MATLAB:bsc:maxlhs",
                "Four outputs required (M,s,x,u).");
    }
#define IN_delta	    	prhs[0]
#define IN_delta_progress	prhs[1]
#define IN_x0		    	prhs[2]
#define IN_q0		    	prhs[3]
#define IN_phi		    	prhs[4]
#define IN_As		    	prhs[5]
#define IN_bs		    	prhs[6]
#define IN_labels	    	prhs[7]
#define IN_Q	    		prhs[8]
#define IN_A		    	prhs[9]
#define IN_B		    	prhs[10]
#define IN_c		    	prhs[11]
#define IN_Aw		    	prhs[12]
#define IN_bw		    	prhs[13]
#define IN_Au		    	prhs[14]
#define IN_bu		    	prhs[15]
#define OUT_M		    plhs[0]
#define OUT_s		    plhs[1]
#define OUT_x		    plhs[2]
#define OUT_u		    plhs[3]


    /* code here */
    id_set_global_constants();

    idbool_t is_fair = FALSE;
    idkripke_t *p_M = idkripke_fromMXArray(iddblmatrix_fromMXArray(IN_x0),int_fromMXArray(IN_q0),is_fair,
			IN_As,IN_bs,IN_labels,IN_Q,IN_A,IN_B,IN_c,IN_Aw,IN_bw,IN_Au,IN_bu);
    idformula_t *p_phi = idformula_fromMXArray(IN_phi,p_M);
    double 	delta = Dbl_fromMXArray(IN_delta),
    		delta_progress = Dbl_fromMXArray(IN_delta_progress);
    idrobrun_t *p_solution;

    OUT_M = MXArray_from_idkripke(p_M);
    if (idRTL_MATLAB(delta, delta_progress, p_phi, &p_solution)) {
		mexPrintf("Solution found.\n");
		OUT_s 		= MXArray_from_drun(p_solution->mp_states_intarray,p_solution->mp_nsample_intarray,p_M);
		OUT_x     	= MXArray_from_dblvector(p_solution->mp_x,p_solution->m_length,p_solution->m_n);
		OUT_u     	= MXArray_from_dblvector(p_solution->mp_u,p_solution->m_length,p_solution->m_m);
		p_solution->destroy(p_solution);
   } else {
		mexPrintf("No solution exits.\n");
		OUT_s		= mxCreateDoubleMatrix(0,0,mxREAL);
		OUT_x		= mxCreateDoubleMatrix(0,0,mxREAL);
		OUT_u		= mxCreateDoubleMatrix(0,0,mxREAL);
    }


    p_M->destroy(p_M);
    p_phi->destroy(p_phi);

    id_destroy_global_constants();

    return;
}
