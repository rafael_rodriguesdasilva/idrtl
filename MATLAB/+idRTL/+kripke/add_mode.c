/*
 * add_polytope.c
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=9) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:add_polytope:invalidNumInputs",
                "Nine inputs required.");
    } else if (nlhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:add_polytope:maxlhs",
                "One output required.");
    }
#define OUT_M           plhs[0]
#define IN_M		    prhs[0]
#define IN_Q 	    	prhs[1]
#define IN_A	 		prhs[2]
#define IN_B	 		prhs[3]
#define IN_c	 		prhs[4]
#define IN_Aw		    prhs[5]
#define IN_bw		    prhs[6]
#define IN_Au		    prhs[7]
#define IN_bu		    prhs[8]

     /* code here */
    id_set_global_constants();

    idbool_t is_fair = FALSE;
    idkripke_t *p_M = idkripke_fromMXArray_structure(IN_M);
    idpolytope_t 	*p_statedom = idpolytope_fromMXArray(IN_Aw,IN_bw),
    				*p_inputdom = idpolytope_fromMXArray(IN_Au,IN_bu);
    int q = idmode_fromMXArray(IN_Q);
    idlinear_sys_t *p_sys = idlinear_sys_fromMXArray(IN_A,IN_B,IN_c);
    p_M->add_mode(p_M,q,p_sys,p_statedom,p_inputdom);
    OUT_M = MXArray_from_idkripke(p_M);
    p_M->destroy(p_M);


    id_destroy_global_constants();

    return;

}
