/*
 * add_polytope.c
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=4) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:add_polytope:invalidNumInputs",
                "Four inputs required.");
    } else if (nlhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:add_polytope:maxlhs",
                "One output required.");
    }
#define OUT_M           plhs[0]
#define IN_M		    prhs[0]
#define IN_As		    prhs[1]
#define IN_bs		    prhs[2]
#define IN_label 	    prhs[3]

     /* code here */
    id_set_global_constants();

    idbool_t is_fair = FALSE;
    idkripke_t *p_M = idkripke_fromMXArray_structure(IN_M);
    idpolytope_t *p_poly = idpolytope_fromMXArray(IN_As,IN_bs);
    int label = idlabel_fromMXArray(IN_label);
    p_M->add_polytope(p_M,p_poly,label);
    OUT_M = MXArray_from_idkripke(p_M);
    p_M->destroy(p_M);


    id_destroy_global_constants();

    return;

}
