/*
 * create.c
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=13) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:create:invalidNumInputs",
                "Thirteen inputs required.");
    } else if (nlhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:kripke:create:maxlhs",
                "One output required.");
    }
#define OUT_M           plhs[0]
#define IN_x0		    prhs[0]
#define IN_q0		    prhs[1]
#define IN_As		    prhs[2]
#define IN_bs		    prhs[3]
#define IN_labels	    prhs[4]
#define IN_Q		    prhs[5]
#define IN_A	 		prhs[6]
#define IN_B	 		prhs[7]
#define IN_c	 		prhs[8]
#define IN_Aw		    prhs[9]
#define IN_bw		    prhs[10]
#define IN_Au		    prhs[11]
#define IN_bu		    prhs[12]

     /* code here */
    id_set_global_constants();

    idbool_t is_fair = FALSE;
    idkripke_t *p_M = idkripke_fromMXArray(iddblmatrix_fromMXArray(IN_x0),int_fromMXArray(IN_q0),is_fair,
			IN_As,IN_bs,IN_labels,IN_Q,IN_A,IN_B,IN_c,IN_Aw,IN_bw,IN_Au,IN_bu);
    OUT_M = MXArray_from_idkripke(p_M);
    p_M->destroy(p_M);


    id_destroy_global_constants();

    return;

}
