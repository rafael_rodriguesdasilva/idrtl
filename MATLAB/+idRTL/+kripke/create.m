% create.m Help file for idRTL.kripke.create MEX-file.
%  create.c - create a polytope
% 
%  The calling syntax is:
% 
% 		M = CREATE(x0,Aw,bw,As,bs,labels)
%   
%  This is a MEX-file for MATLAB.
