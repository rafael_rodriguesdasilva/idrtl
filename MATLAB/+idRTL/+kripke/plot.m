function plot(M,varargin)
%PLOT  draw a Kripke structure
% 
%       * M : Kripke structure
%       * 'nolabels',[T,F]  : whether we print the labels or not .
%       * 'fontesize',number: font size for labels.
%       * 'xsection',vector : through which states to cut the plot 
%                            (e.g. value=[4 5] for a cut along x4 and x5)
%                            default value = 3:size(A,2).
%       * 'xvalues',vector  : at which values to cut 
%                           (e.g. value=[0 10] will cut at x4=0, x5=10)
%                            default value = zeros(size(xsection)).

    carg = cellfun(@(x) ischar(x),varargin);
    varargin(carg) = lower(varargin(carg));
    Options = struct(varargin{:});
     if isfield(Options, 'fontsize') 
         fontsize = Options.fontsize;
     else
         fontsize = 12;
     end
   
    clf;
    subplot(1,2,1);    
    for i=1:length(M.Q)
        hold on;
        idRTL.kripke.plot_partition(M,M.Q{i},varargin{:});
    end
    hold off;

    subplot(1,2,2);
    idRTL.kripke.plot_kripke(M,fontsize);
end
