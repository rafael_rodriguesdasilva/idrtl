function plot3_partition(M,q,varargin)
%PLOT draw 2D partition of a Kripke structure
% 
%       * M : Kripke structure
%       * q : mode to plot
%       * 'fontesize',number: font size for labels.
%       * 'xsection',vector : through which states to cut the plot 
%                            (e.g. value=[4 5] for a cut along x4 and x5)
%                            default value = 3:size(A,2).
%       * 'xvalues',vector  : at which values to cut 
%                           (e.g. value=[0 10] will cut at x4=0, x5=10)
%                            default value = zeros(size(xsection)).

    nstates = length(M.As);
    dim = size(M.As{1},2);
    ind = find(cellfun(@(x) x==q,M.Q));
    
    if dim<3
        error('The system must be at least 3D.');
    end
    
    carg = cellfun(@(x) ischar(x),varargin);
    varargin(carg) = lower(varargin(carg));
    Options.orig = struct(varargin{:});
    Options.alphaShape = {'xsection',4:dim,'xvalues',zeros(size(4:dim))};
    Options.patch = {'LineWidth',3};
    Options.text = {'FontSize',12,'FontWeight','bold',...
                                        'HorizontalAlignment','center'};
    if isfield(Options.orig, 'xvalues') 
        Options.alphaShape{4} = Options.orig.xvalues;
    end
    x = 1;
    y = 2;
    z = 3;
    if isfield(Options.orig, 'xsection') 
        Options.alphaShape{2} = Options.orig.xsection;
        x = Options.orig.xsection(1);
        y = Options.orig.xsection(2);
        z = Options.orig.xsection(3);
    end
    if isfield(Options.orig, 'fontsize') 
        Options.text{2} = Options.orig.fontsize;
    end
    if isfield(Options.orig, 'nostates') 
        nostates = Options.orig.nostates;
    else
        nostates = true;
    end
    
    cmap = lines;
    cmap_labels = cell(nstates,1);
    
    shp = idRTL.polytope.alphaShape(M.Aw{ind},M.bw{ind},Options.alphaShape{:});
    plot(shp,'FaceAlpha',0,Options.patch{:});
    hold on;
    view(3);

    cnt = 0;
    for i=1:nstates
        shp = idRTL.polytope.alphaShape(M.As{i},M.bs{i},Options.alphaShape{:});
        if isempty(M.label{i})
            if ~nostates
                plot(shp,'FaceAlpha',0.05,Options.patch{:});
            end
        else
            ism = cellfun(@(x) strcmp(x,label2str(M.label{i})),cmap_labels);
            if any(ism)
                color=cmap(ism,:);
            else
                color=cmap(cnt+1,:);
                cmap_labels{cnt+1} = label2str(M.label{i});
                cnt = mod(cnt+1,size(cmap,1));
            end
            plot(shp,'FaceColor',color,'FaceAlpha',0.2,Options.patch{:});
        end
    end
    plot3(M.x0(x),M.x0(y),M.x0(z),'p','MarkerEdgeColor','b','MarkerFaceColor','b',...
        'MarkerSize',10);
    hold off;
    axis equal
    set(gca,'FontSize',12)
end

function out = label2str(label)
    if isempty(label)
        out = [];
    else
        out = mat2str(label);
    end
end