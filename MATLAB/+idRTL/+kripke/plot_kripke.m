function plot_kripke(M,fontsize)
%PLOT Summary of this function goes here
%   Detailed explanation goes here

    if nargin==1
        fontsize = 12;
    elseif nargin~=2
        error('Invalid number of arguments.');
    end
    
    nstates = length(M.As);
     
    cmap = lines;
    cmap_labels = cell(nstates,1);
    cmap_ind    = cell(nstates,1);
    cnt = 0;
    
    G = digraph(M.delta'+1);
    Nodes = cell(nstates,1);
    for i=1:nstates
        if isempty(M.label{i})
            idx = nstates;
        else
            ism = cellfun(@(x) strcmp(x,M.label{i}),cmap_labels);
            if any(ism)
                idx = find(ism);
            else
                cnt = mod(cnt+1,size(cmap,1));
                idx = cnt;
                cmap_labels{idx} = M.label{i};
            end
        end
        cmap_ind{idx} = [cmap_ind{idx},i];
        Nodes{i} = [newline,'s_{',num2str(i),'}',newline,M.label{i}];
    end
    if (0<M.s0)&&(M.s0<=nstates)
        s0_ = M.s0+1;
    else
        s0_ = 1;
    end
    h = plot(G,'ArrowSize',6,'EdgeColor','k','LineWidth',2,'MarkerSize',20,...
        'Layout','layered','Direction','right','Sources',s0_);
    highlight(h,cmap_ind{nstates},'NodeColor',[0.8,0.8,0.8]); 
    for i=1:cnt
        highlight(h,cmap_ind{i},'NodeColor',cmap(i,:)); 
    end
    h.NodeLabel = '';
    xd = get(h, 'XData');
    yd = get(h, 'YData');
    text(xd, yd, Nodes, 'FontSize',fontsize, 'FontWeight','bold', 'HorizontalAlignment','center', 'VerticalAlignment','middle')

    %axis image
    set(gca,'FontSize',fontsize)

end
