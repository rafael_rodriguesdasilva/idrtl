function plot_partition(M,q,varargin)
%PLOT draw 2D partition of a Kripke structure
% 
%       * M : Kripke structure
%       * q : mode to plot
%       * 'nostates',[T,F]  : whether we print the states name or not .
%       * 'nolabels',[T,F]  : whether we print the labels or not .
%       * 'fontesize',number: font size for labels.
%       * 'xsection',vector : through which states to cut the plot 
%                            (e.g. value=[4 5] for a cut along x4 and x5)
%                            default value = 3:size(A,2).
%       * 'xvalues',vector  : at which values to cut 
%                           (e.g. value=[0 10] will cut at x4=0, x5=10)
%                            default value = zeros(size(xsection)).

    nstates = length(M.As);
    dim = size(M.As{1},2);
    x0 = M.x0;
    ind = find(cellfun(@(x) x==q,M.Q));
    
    carg = cellfun(@(x) ischar(x),varargin);
    varargin(carg) = lower(varargin(carg));
    Options.orig = struct(varargin{:});
    Options.polyshape = {'xsection',3:dim,'xvalues',zeros(size(3:dim))};
    Options.patch = {'LineWidth',3};
    Options.text = {'FontSize',12,'FontWeight','bold',...
                                        'HorizontalAlignment','center'};
    if isfield(Options.orig, 'xvalues') 
        Options.polyshape{4} = Options.orig.xvalues;
    end
    if isfield(Options.orig, 'xsection') 
        Options.polyshape{2} = Options.orig.xsection;
        x0(Options.orig.xsection) = [];
    end
    if isfield(Options.orig, 'fontsize') 
        Options.text{2} = Options.orig.fontsize;
    end
    if isfield(Options.orig, 'nolabels') 
        nolabels = Options.orig.nolabels;
    else
        nolabels = false;
    end
    if isfield(Options.orig, 'nostates') 
        nostates = Options.orig.nostates;
    else
        nostates = false;
    end
    
    cmap = lines;
    cmap_labels = cell(nstates,1);
    
    pgonw = idRTL.polytope.polyshape(M.Aw{ind},M.bw{ind},Options.polyshape{:});
    plot(pgonw,'FaceAlpha',0,Options.patch{:});
    hold on;

    cnt = 0;
    for i=1:nstates
        if M.modes{i}==q 
            pgon = idRTL.polytope.polyshape(M.As{i},M.bs{i},Options.polyshape{:});
            [x,y] = centroid(pgon);
            if isempty(M.label{i})
                plot(pgon,'FaceAlpha',0,Options.patch{:});
                if nolabels && ~nostates
                    text(x,y,['s_{',num2str(i),'}'],Options.text{:});
                elseif ~nolabels && ~nostates
                    text(x,y,['s_{',num2str(i),'}',newline,label2str(M.label{i})],Options.text{:});
                end
            else
                ism = cellfun(@(x) strcmp(x,label2str(M.label{i})),cmap_labels);
                if any(ism)
                    color=cmap(ism,:);
                else
                    color=cmap(cnt+1,:);
                    cmap_labels{cnt+1} = label2str(M.label{i});
                    cnt = mod(cnt+1,size(cmap,1));
                end
                plot(pgon,'FaceColor',color,'FaceAlpha',0.5,Options.patch{:});
                if nolabels && ~nostates
                    text(x,y,['s_{',num2str(i),'}'],Options.text{:});
                elseif ~nolabels && nostates
                    text(x,y,label2str(M.label{i}),Options.text{:});
                elseif ~nolabels && ~nostates
                    text(x,y,['s_{',num2str(i),'}',newline,label2str(M.label{i})],Options.text{:});
                end
            end
        end
    end
    plot(x0(1),x0(2),'p','MarkerEdgeColor','b','MarkerFaceColor','b',...
        'MarkerSize',10);
    hold off;
    axis equal
    set(gca,'FontSize',12)
end

function out = label2str(label)
    if isempty(label)
        out = [];
    else
        out = mat2str(label);
    end
end