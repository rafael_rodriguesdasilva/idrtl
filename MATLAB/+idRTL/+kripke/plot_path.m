function plot_path(M,varargin)
%PLOT draw 2D partition of a Kripke structure
% 
%       * M : Kripke structure
%       * 'nostates',[T,F]  : whether we print the states name or not .
%       * 'nolabels',[T,F]  : whether we print the labels or not .
%       * 'fontesize',number: font size for labels.
%       * 'xsection',vector : through which states to cut the plot 
%                            (e.g. value=[4 5] for a cut along x4 and x5)
%                            default value = 3:size(A,2).
%       * 'xvalues',vector  : at which values to cut 
%                           (e.g. value=[0 10] will cut at x4=0, x5=10)
%                            default value = zeros(size(xsection)).

    nstates = length(M.As);
    dim = size(M.As{1},2);
    
    carg = cellfun(@(x) ischar(x),varargin);
    varargin(carg) = lower(varargin(carg));
    Options.orig = struct(varargin{:});
    Options.polyshape = {'xsection',3:dim,'xvalues',zeros(size(3:dim))};
    Options.patch = {'LineWidth',3};
    Options.text = {'FontSize',12,'FontWeight','bold',...
                                        'HorizontalAlignment','center'};
    if isfield(Options.orig, 'xvalues') 
        Options.polyshape{4} = Options.orig.xvalues;
    end
    if isfield(Options.orig, 'xsection') 
        Options.polyshape{2} = Options.orig.xsection;
    end
    if isfield(Options.orig, 'fontsize') 
        Options.text{2} = Options.orig.fontsize;
    end
    if isfield(Options.orig, 'nolabels') 
        nolabels = Options.orig.nolabels;
    else
        nolabels = false;
    end
    if isfield(Options.orig, 'nostates') 
        nostates = Options.orig.nostates;
    else
        nostates = false;
    end
    
    pgon = idRTL.polytope.polyshape(M.Aw,M.bw,Options.polyshape{:});
    plot(pgon,'FaceAlpha',0,Options.patch{:});
    hold on;

    for i=1:nstates
        pgon = idRTL.polytope.polyshape(M.As{i},M.bs{i},Options.polyshape{:});
        [x,y] = centroid(pgon);
        plot(pgon,'FaceColor','k','FaceAlpha',0.2,Options.patch{:});
        if isempty(M.label{i})
            if nolabels && ~nostates
                text(x,y,['s_{',num2str(i),'}'],Options.text{:});
            elseif ~nolabels && ~nostates
                text(x,y,['s_{',num2str(i),'}',newline,M.label{i}],Options.text{:});
            end
        else
            if nolabels && ~nostates
                text(x,y,['s_{',num2str(i),'}'],Options.text{:});
            elseif ~nolabels && nostates
                text(x,y,M.label{i},Options.text{:});
            elseif ~nolabels && ~nostates
                text(x,y,['s_{',num2str(i),'}',newline,M.label{i}],Options.text{:});
            end
        end
    end
    hold off;
    axis equal
    set(gca,'FontSize',12)
end

