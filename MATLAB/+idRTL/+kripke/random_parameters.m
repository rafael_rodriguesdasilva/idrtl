function [x0,Aw,bw,As,bs,labels] = random_parameters(dim,nsymbols,...
    ndisjunctions_persymbol, npredicates_perdisjunctions, xmax)

x0 = 2*xmax*rand(dim,1) - xmax;
Aw = [eye(dim);-eye(dim)];
bw = xmax*ones(2*dim,1);
As = {};
bs = {};
labels = {};
cnt = 0;
for i=1:nsymbols
    cnt = cnt+1;
    ndisjunctions = ndisjunctions_persymbol;
    for j=1:ndisjunctions
        npredicates = npredicates_perdisjunctions;
        Asi = Aw;
        bsi = bw;
        for k=1:npredicates
            pi = 2*xmax*rand(dim,1) - xmax;
            xcnt = 1;
            while((xcnt<=10)&&(any(Asi*pi>bsi))) 
                pi = 2*xmax*rand(dim,1) - xmax;
                xcnt = xcnt+1;
            end
            if (xcnt>10)
                break;
            end
            Asi(2*dim+k,:) = 2*rand(1,dim) - 1;
            bsi(2*dim+k) = Asi(2*dim+k,:)*pi;
        end
        [Asi,bsi] = idRTL.polytope.create(Asi,bsi);
        As{cnt} = Asi;
        bs{cnt} = bsi;
        labels{cnt} = char(96+i);
    end    
end
As = As(:);
bs = bs(:);
labels = labels(:);
end

