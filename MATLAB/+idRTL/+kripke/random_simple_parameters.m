function [x0,Aw,bw,As,bs,labels] = random_simple_parameters(dim,nsymbols,...
    ndisjunctions_persymbol, xmax)

if (length(ndisjunctions_persymbol)~=nsymbols) && ...
        (length(ndisjunctions_persymbol)~=1)
    error('invalid arguments.');
end

if length(ndisjunctions_persymbol)==1 
    ndisjunctions_persymbol = repmat(ndisjunctions_persymbol,nsymbols,1);
end

x0 = 2*xmax*rand(dim,1) - xmax;
Aw = [eye(dim);-eye(dim)];
bw = xmax*ones(2*dim,1);
As = {};
bs = {};
labels = {};
cnt = 0;
for i=1:nsymbols
    cnt = cnt+1;
    ndisjunctions = ndisjunctions_persymbol(i);
    for j=1:ndisjunctions
        Asi = Aw;
        bsi = bw;
        for k=unique(randi(2*dim,1,randi(2*dim)))
            bsi(k) = 2*xmax*rand-xmax;
        end
        As{cnt} = Asi;
        bs{cnt} = bsi;
        labels{cnt} = char(96+i);
    end    
end
As = As(:);
bs = bs(:);
labels = labels(:);
end

