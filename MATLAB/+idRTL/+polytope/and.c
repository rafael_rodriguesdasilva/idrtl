/*
 * and.c
 *
 *  Created on: Apr 13, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=4) {
        mexErrMsgIdAndTxt( "MATLAB:context:invalidNumInputs",
                "Four inputs required.");
    } else if(nlhs!=2) {
        mexErrMsgIdAndTxt( "MATLAB:context:maxlhs",
                "Two outputs required.");
    }
#define OUT_A           plhs[0]
#define OUT_b           plhs[1]
#define IN_A		    prhs[0]
#define IN_b		    prhs[1]
#define IN_lhs		    prhs[2]
#define IN_rhs		    prhs[3]


    if ((mxGetM(IN_A)<=0)||(mxGetN(IN_A)<=0)) {
    	mexErrMsgTxt("Matrix A cannot be empty!");
    } else if (mxGetM(IN_b)!=mxGetM(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector b must have same number of rows!");
    } else if (mxGetN(IN_b)!=1) {
    	mexErrMsgTxt("Vector b must be a column vector.");
    } else if (mxGetM(IN_lhs)<=0) {
    	mexErrMsgTxt("Matrix lhs cannot be empty.");
    } else if (mxGetN(IN_lhs)!=mxGetN(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector lhs must have same dimension!");
    } else if (mxGetM(IN_rhs)!=mxGetM(IN_lhs)) {
    	mexErrMsgTxt("Matrix lhs and vector rhs must have same number of rows!");
    } else if (mxGetN(IN_rhs)!=1) {
    	mexErrMsgTxt("Vector rhs must be a column vector.");
    }
    /* code here */
	id_set_global_constants();

    idpolytope_t *p_poly1, *p_poly2, *p_inter;
    idpredicate_t *p_pi;
    p_poly1 = idpolytope_fromMXArray(IN_A,IN_b);

    if (mxIsScalar(IN_rhs)) {
    	p_pi = id_mkLe(idsparse_fromMXArray(IN_lhs),Dbl_fromMXArray(IN_rhs));
    	p_inter = p_poly1->and_with_predicate(p_poly1,p_pi);
    	p_poly1->destroy(p_poly1);
    	p_pi->destroy(p_pi);
    } else {
    	p_poly2 = idpolytope_fromMXArray(IN_lhs,IN_rhs);
    	p_inter = p_poly1->and_with_polytope(p_poly1,p_poly2);
    	p_poly1->destroy(p_poly1);
    	p_poly2->destroy(p_poly2);
    }

    OUT_A = MXArray_fromGMPmat(p_inter->mp_A);
    OUT_b = MXArray_fromGMPmat(p_inter->mp_b);

    p_inter->destroy(p_inter);


    id_destroy_global_constants();

    return;

}
