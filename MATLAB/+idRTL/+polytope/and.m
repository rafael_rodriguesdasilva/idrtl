% and.m Help file for idSTL.polytope.and MEX-file.
%  and.c - computes the intersection between two polytopes
% 
%  The calling syntax is:
% 
% 		[A,b] = AND(A1,b1,A2,b2)
%   
%  A1,b1 : halfspace representation of a polytope, i.e., A1*x<=b1
%  A2,b2 : halfspace representation of a polytope, i.e., A2*x<=b2
%  A,b   : minimal halfspace representation of the intersection between
%          polytopes 1 and 2.
% 
%  This is a MEX-file for MATLAB.
