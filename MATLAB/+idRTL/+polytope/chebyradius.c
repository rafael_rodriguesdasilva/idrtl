/*
 * chebyradius.c
 *
 *  Created on: Apr 12, 2019
 *      Author: rafael
 */



#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=2) {
        mexErrMsgIdAndTxt( "MATLAB:context:invalidNumInputs",
                "Two inputs required.");
    } else if(nlhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:context:maxlhs",
                "One output required.");
    }
#define OUT_RCheby      plhs[0]
#define IN_A		    prhs[0]
#define IN_b		    prhs[1]

    idpolytope_t *p_poly;
    double rcheb;

    if ((mxGetM(IN_A)<=0)||(mxGetN(IN_A)<=0)) {
    	mexErrMsgTxt("Matrix A cannot be empty!");
    } else if (mxGetM(IN_b)!=mxGetM(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector b must have same number of rows!");
    } else if (mxGetN(IN_b)!=1) {
    	mexErrMsgTxt("Vector b must be a column vector.");
    }
    /* code here */
    id_set_global_constants();

    p_poly = idpolytope_fromMXArray(IN_A,IN_b);
    rcheb = p_poly->chebyradius(p_poly);

    OUT_RCheby = MXArray_fromDbl(&rcheb,1);

    p_poly->destroy(p_poly);

    id_destroy_global_constants();

    return;

}
