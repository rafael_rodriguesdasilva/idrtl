% chebyradius.m Help file for idSTL.polytope.chebyradius MEX-file.
%  chebyradius.c - computes the Chebychev radius of a polytope
% 
%  The calling syntax is:
% 
% 		RCheby = CHEBYRADIUS(A,b)
%   
%  A,b    : halfspace representation of a polytope, i.e., A1*x<=b1
%  RCheby : Chebychev radius of the polytope
% 
%  This is a MEX-file for MATLAB.
