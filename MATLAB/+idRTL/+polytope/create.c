/*
 * create.c
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=2) {
        mexErrMsgIdAndTxt( "MATLAB:context:invalidNumInputs",
                "Two inputs required.");
    } else if ((nlhs!=3)&&(nlhs!=2)&&(nlhs!=1)) {
        mexErrMsgIdAndTxt( "MATLAB:context:maxlhs",
                "Three,two or one outputs required.");
    }
#define OUT_A           plhs[0]
#define OUT_b           plhs[1]
#define OUT_V           plhs[2]
#define IN_A		    prhs[0]
#define IN_b		    prhs[1]

    idmatrix_t  *p_V;
    idpolytope_t *p_poly;

    if ((mxGetM(IN_A)<=0)||(mxGetN(IN_A)<=0)) {
    	mexErrMsgTxt("Matrix A cannot be empty!");
    } else if (mxGetM(IN_b)!=mxGetM(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector b must have same number of rows!");
    } else if (mxGetN(IN_b)!=1) {
    	mexErrMsgTxt("Vector b must be a column vector.");
    }
    /* code here */
    id_set_global_constants();

    p_poly = idpolytope_fromMXArray(IN_A,IN_b);


    if (nlhs==3) {
    	p_poly->get_vertices(p_poly,&p_V);
        OUT_A = MXArray_fromGMPmat(p_poly->mp_A);
        OUT_b = MXArray_fromGMPmat(p_poly->mp_b);
        OUT_V = MXArray_fromGMPmat(p_V);
        p_V->destroy(p_V);
    } else if (nlhs==2) {
        OUT_A = MXArray_fromGMPmat(p_poly->mp_A);
        OUT_b = MXArray_fromGMPmat(p_poly->mp_b);
    } else {
    	p_poly->get_vertices(p_poly,&p_V);
        OUT_A = MXArray_fromGMPmat(p_V);
        p_V->destroy(p_V);
    }

    p_poly->destroy(p_poly);


    id_destroy_global_constants();

    return;

}
