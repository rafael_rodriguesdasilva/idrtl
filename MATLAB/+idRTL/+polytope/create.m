% create.m Help file for idSTL.polytope.create MEX-file.
%  create.c - create a polytope
% 
%  The calling syntax is:
% 
% 		[A,b] = CREATE(Ain,bin)
%   
%  Ain,bin : halfspace representation of a polytope, i.e., A1*x<=b1
%  A,b   : minimal halfspace representation of the polytope
% 
%  This is a MEX-file for MATLAB.
