% difference.m Help file for idSTL.polytope.difference MEX-file.
%  difference.c - compute the difference between two polytopes
% 
%  The calling syntax is:
% 
% 		[A,b] = DIFFERENCE(A1,b1,A2,b2)
%   
%  A1,b1 : halfspace representation of a polytope, i.e., A1*x<=b1
%  A2,b2 : halfspace representation of a polytope, i.e., A2*x<=b2
%  A,b   : cell vector with a set of minimal halfspace representations 
%          of the polytopes forming the region polytope 1 minus polytope 2.
% 
%  This is a MEX-file for MATLAB.
