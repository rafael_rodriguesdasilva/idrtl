/*
 * is_adjacent_to.c
 *
 *  Created on: Apr 14, 2019
 *      Author: rafael
 */


#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=4) {
        mexErrMsgIdAndTxt( "MATLAB:context:invalidNumInputs",
                "Four inputs required.");
    } else if((nlhs!=1)&&(nlhs!=3)&&(nlhs!=5)) {
        mexErrMsgIdAndTxt( "MATLAB:context:maxlhs",
                "One, Three or Five outputs required.");
    }
#define OUT_isadj	    plhs[0]
#define OUT_adjfacet_i  plhs[1]
#define OUT_adjfacet_j  plhs[2]
#define OUT_A  			plhs[3]
#define OUT_b  			plhs[4]
#define IN_A		    prhs[0]
#define IN_b		    prhs[1]
#define IN_lhs		    prhs[2]
#define IN_rhs		    prhs[3]

    idpolytope_t *p_poly1, *p_poly2, *p_inter, *p_convex_or = NULL;
    idbool_t isadj;
    int adjfacet_i, adjfacet_j;

    if ((mxGetM(IN_A)<=0)||(mxGetN(IN_A)<=0)) {
    	mexErrMsgTxt("Matrix A cannot be empty!");
    } else if (mxGetM(IN_b)!=mxGetM(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector b must have same number of rows!");
    } else if (mxGetN(IN_b)!=1) {
    	mexErrMsgTxt("Vector b must be a column vector.");
    } else if (mxGetM(IN_lhs)<=0) {
    	mexErrMsgTxt("Matrix lhs cannot be empty.");
    } else if (mxGetN(IN_lhs)!=mxGetN(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector lhs must have same dimension!");
    } else if (mxGetM(IN_rhs)!=mxGetM(IN_lhs)) {
    	mexErrMsgTxt("Matrix lhs and vector rhs must have same number of rows!");
    } else if (mxGetN(IN_rhs)!=1) {
    	mexErrMsgTxt("Vector rhs must be a column vector.");
    }
    /* code here */
    id_set_global_constants();

    p_poly1 = idpolytope_fromMXArray(IN_A,IN_b);
    p_poly2 = idpolytope_fromMXArray(IN_lhs,IN_rhs);
    p_inter = p_poly1->and_with_polytope(p_poly1,p_poly2);
	isadj = p_poly1->is_adjacent_to(p_poly1,p_poly2,p_inter,&adjfacet_i,&adjfacet_j,&p_convex_or);
	p_poly1->destroy(p_poly1);
	p_poly2->destroy(p_poly2);
	p_inter->destroy(p_inter);

	if (isadj) {
		OUT_isadj = mxCreateDoubleScalar(1.0);
	} else {
		OUT_isadj = mxCreateDoubleScalar(0.0);
	}
	if ((nlhs == 3)||(nlhs == 5)) {
		OUT_adjfacet_i = mxCreateDoubleScalar((double)adjfacet_i);
		OUT_adjfacet_j = mxCreateDoubleScalar((double)adjfacet_j);
	}
	if (nlhs == 5) {
		if (p_convex_or==NULL) {
			OUT_A = MXArray_empty();
			OUT_b = MXArray_empty();
		} else {
			OUT_A = MXArray_fromGMPmat(p_convex_or->mp_A);
			OUT_b = MXArray_fromGMPmat(p_convex_or->mp_b);
		}
	}

	idpolytope_destroy(p_convex_or);

	id_destroy_global_constants();

    return;
}
