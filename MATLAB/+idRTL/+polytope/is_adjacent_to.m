% is_adjacent_to.m Help file for idSTL.polytope.is_adjacent_to MEX-file.
%  is_adjacent_to.c - checks if two polytopes are adjacent
% 
%  The calling syntax is:
% 
% 		[isadj,adj1,adj2] = IS_ADJACENT_TO(A1,b1,A2,b2)
%   
%  A1,b1     : halfspace representation of a polytope, i.e., A1*x<=b1
%  A2,b2     : halfspace representation of a polytope, i.e., A2*x<=b2
%  isadj     : true if and only if they are adjacent
%  adj1,adj2 : adjacent facet of polytope 1 and 2. 
% 
%  This is a MEX-file for MATLAB.
