% isfulldim.m Help file for idSTL.polytope.isfulldim MEX-file.
%  isfulldim.c - checks if a polytope is full-dimensional
% 
%  The calling syntax is:
% 
% 		isfull = ISFULLDIM(A,b)
%   
%  A,b    : halfspace representation of a polytope, i.e., A*x<=b
%  isfull : true if and only if the polytope is full dimensional.
% 
%  This is a MEX-file for MATLAB.
