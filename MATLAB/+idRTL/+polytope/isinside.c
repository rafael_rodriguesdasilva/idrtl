/*
 * isinside.c
 *
 *  Created on: Apr 12, 2019
 *      Author: rafael
 */



#include "../mxinterface.h"

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
/* variable declarations here */

    if(nrhs!=3) {
        mexErrMsgIdAndTxt( "MATLAB:context:invalidNumInputs",
                "Three inputs required.");
    } else if(nlhs!=1) {
        mexErrMsgIdAndTxt( "MATLAB:context:maxlhs",
                "One output required.");
    }
#define OUT_out        plhs[0]
#define IN_A		    prhs[0]
#define IN_b		    prhs[1]
#define IN_x		    prhs[2]

    if ((mxGetM(IN_A)<=0)||(mxGetN(IN_A)<=0)) {
    	mexErrMsgTxt("Matrix A cannot be empty!");
    } else if (mxGetM(IN_b)!=mxGetM(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector b must have same number of rows!");
    } else if (mxGetN(IN_b)!=1) {
    	mexErrMsgTxt("Vector b must be a column vector.");
    } else if (mxGetN(IN_x)!=1) {
    	mexErrMsgTxt("Vector x must be a column vector.");
    } else if (mxGetM(IN_x)!=mxGetN(IN_A)) {
    	mexErrMsgTxt("Matrix A and vector x must have same dimension!");
    }
    /* code here */
    id_set_global_constants();

    iddblmatrix_t *p_x = iddblmatrix_fromMXArray(IN_x);
    idpolytope_t *p_poly;
    int isinside;

	p_poly = idpolytope_fromMXArray(IN_A,IN_b);
    isinside = (int)p_poly->isinside_dblcolvector(p_poly,p_x);

    OUT_out = MXArray_fromInt(&isinside,1);

    p_poly->destroy(p_poly);
    p_x->destroy(p_x);

    id_destroy_global_constants();

    return;

}
