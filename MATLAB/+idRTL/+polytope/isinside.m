% isinside.m Help file for idSTL.polytope.isinside MEX-file.
%  isinside.c - checks if a point is inside a polytope
% 
%  The calling syntax is:
% 
% 		isin = ISINSIDE(A,b,x)
%   
%  A,b  : halfspace representation of a polytope, i.e., A*x<=b
%  x    : a point in the space of the polytope
%  isin : true if and only if the point is inside the polytope.
% 
%  This is a MEX-file for MATLAB.
