function pgon = polyshape(A,b,varargin)
%POLYSHAPE Overloads polyshape for 2D polygons
% 
%       * A,b : halfspace representation (A*x<=b)
%       * 'xsection',value: through which states to cut the plot 
%                           (e.g. value=[4 5] for a cut along x4 and x5)
%                           default value = 3:size(A,2).
%       * 'xvalues',value: at which values to cut 
%                          (e.g. value=[0 10] will cut at x4=0, x5=10)
%                           default value = zeros(size(xsection)).

    carg = cellfun(@(x) ischar(x),varargin);
    varargin(carg) = lower(varargin(carg));
    Options = struct(varargin{:});
    
    if isfield(Options, 'xvalues') 
        xvalues = reshape(Options.xvalues,length(Options.xvalues),1);
    else
        xvalues = [];
    end
    if isfield(Options, 'xsection') 
        xsection = reshape(Options.xsection,length(Options.xsection),1);
        if isempty(xvalues)
            xvalues = zeros(size(xsection));
        end
    else
        xsection = 3:size(A,2);
        if isempty(xvalues)
            xvalues = zeros(size(xsection));
        end
    end
    
    if size(A,2) > 2
        [A,b] = idRTL.polytope.project(A,b,xsection,xvalues);
    end
    
    [~,~,V] = idRTL.polytope.create(A,b);
%     V(:,1) = [];
    V = unique(round(-V,9),'rows');
    
    V = sortrows([atan2(V(:,2)-mean(V(:,2)),V(:,1)-mean(V(:,1))),V],1);
    pgon = polyshape(V(:,2:3));
end

