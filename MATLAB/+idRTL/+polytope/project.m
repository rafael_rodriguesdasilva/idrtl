function [A,b] = project(A,b,xsection,xvalues)
%PROJECT Summary of this function goes here
%   Detailed explanation goes here
    b = b - A(:,xsection)*xvalues;
    A(:,xsection) = [];
    b(all(abs(A)<=eps,2)) = [];
    A(all(abs(A)<=eps,2),:) = [];
end

