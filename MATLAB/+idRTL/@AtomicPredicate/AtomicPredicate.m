classdef AtomicPredicate
    %ATOMICPREDICATE mu >= 0
    
    properties (SetAccess = private)
        mu
    end
    
    methods
        function obj = AtomicPredicate(mu)
            %ATOMICPREDICATE Construct an instance of this class
            obj.mu = mu;
        end
    end
end

