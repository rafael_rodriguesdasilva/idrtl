function c = and(a,b)
%AND c = [a;b] == a AND b
    if ~isa(a,'idRTL.AtomicPredicate') || ~isvector(a) || size(a,2) ~= 1 || ...
            ~isa(b,'idRTL.AtomicPredicate') || ~isvector(b) || size(b,2) ~= 1
        error('Invalid arguments.');
    end

    c = [a;b];
    
end

