function [A,b] = predicates2polytope(p)
%PREDICATES2POLYTOPE p => A*x <= b

    if isa(p,'idRTL.AtomicPredicate') && isvector(p)
        len = length(p);
        A = [];
        b = zeros(len,1);
        for i=1:len
            if isempty(A) && ~isempty(p(i).mu.variables)
                n = p(i).mu.variables(1).dim;
                A = zeros(len,n);
            end
            if ~isempty(p(i).mu.variables)
                A(i,[p(i).mu.variables.idx]) = -p(i).mu.values;
            end
            b(i) = p(i).mu.cnst;
        end
    else
        error('Invalid arguments.');
    end
end

