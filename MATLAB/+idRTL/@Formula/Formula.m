classdef Formula
    %FORMULA Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        As
        bs
        label
        phi
        childs
        parent
        type
    end
    
    properties (Access = private)
        id
    end
    
    methods
        function obj = Formula(childs,type)
            %FORMULA Construct an instance of this class
            
            if ~isa(type,'idRTL.FormulaType')
                error('Invalid formula type.');
            end
            
            obj.id = idRTL.Formula.next_id();
            
            if type == idRTL.FormulaType.AtomicProposition
                if isa(childs,'idRTL.AtomicPredicate')&&isvector(childs)
                    [obj.As,obj.bs,obj.label,obj.phi] = ...
                        idRTL.Formula.predicates2atomicproposition(childs,...
                        idRTL.Formula.next_symbol(obj.id));
                    obj.type = type;
                else
                    error('Invalid arguments.');
                end                            
            elseif type == idRTL.FormulaType.And
                if length(childs)==2 && isa(childs(1),'idRTL.Formula') ...
                        && isa(childs(2),'idRTL.Formula')
                    [obj.As,obj.bs,obj.label,obj.phi,obj.childs] = ...
                             idRTL.Formula.formulas2and(childs(1),childs(2));
                    obj.childs(1).parent = obj;
                    obj.childs(2).parent = obj;
                    obj.type = type;
                else 
                        error('Invalid arguments.');
                end                 
             elseif type ==  idRTL.FormulaType.Or
                if length(childs)==2 && isa(childs(1),'idRTL.Formula') ...
                             && isa(childs(2),'idRTL.Formula')
                    [obj.As,obj.bs,obj.label,obj.phi,obj.childs,obj.type] = ...
                             idRTL.Formula.formulas2or(childs(1),childs(2));
                    obj.childs(1).parent = obj;
                    obj.childs(2).parent = obj;
                else 
                    error('Invalid arguments.');
                end                            
            elseif type ==  idRTL.FormulaType.Until || ...
                    type ==  idRTL.FormulaType.Release
                if length(childs)==2 && isa(childs(1),'idRTL.Formula') ...
                         && isa(childs(2),'idRTL.Formula')
                    [obj.As,obj.bs,obj.label,obj.phi,obj.childs] = ...
                                idRTL.Formula.get_formula(childs(1),childs(2),type);
                    obj.childs(1).parent = obj;
                    obj.childs(2).parent = obj;
                    obj.type = type;
                else 
                    error('Invalid arguments.');
                end
            elseif type ==  idRTL.FormulaType.Always || ...
                    type ==  idRTL.FormulaType.Eventually
                if length(childs)==1 && isa(childs(1),'idRTL.Formula')
                    nelem = length(childs.phi)+1;
                    obj.phi = childs.phi;
                    obj.phi(end).parents = nelem;
                    obj.phi(nelem).type =idRTL.Formula.type2id(type);
                    obj.phi(nelem).childs = length(childs.phi);
                    obj.phi(nelem).parents = [];
                    obj.phi(nelem).atom = [];
                    obj.phi(nelem).isneg = [];                    
                    obj.As = childs.As;
                    obj.bs = childs.bs;
                    obj.label = childs.label;
                    obj.childs = childs;
                    obj.childs.parent = obj;
                    obj.type = type;
                else 
                    error('Invalid arguments.');
                end
            else 
                error('Invalid arguments.');
            end
            
            [obj.As,obj.bs,obj.label] = ...
                idRTL.Formula.validate_partitions(obj.As,obj.bs,obj.label);
        end
        
    end
     
    methods (Static)
       function out = next_id(data)
           persistent Var;
           if nargin==1
               Var = abs(data);
               out = [];
           else
               if isempty(Var)
                   Var = 0;
                   out = Var;
               else
                   out = Var;
                   Var = Var+1;
               end
           end           
       end
       function out = next_symbol(id,data)
           persistent Var;
           if nargin==2
               if isempty(data)
                   Var = {};
                   out = [];
               else
                   row = find(cellfun(@(x) ismember(id,x),Var),1);
                   if ~isempty(row)
                       col = ismember(Var{row},id);
                       Var{row}(col) = [];
                   end
                   Var{data+1} = [Var{data+1},id];
                   out = [];
               end
           else
               row = find(cellfun(@isempty,Var),1);
               if isempty(row)
                   out = length(Var);
                   Var{out+1} = id;
               else
                   out = row-1;
                   Var{row} = id;
               end
           end           
       end
       function [As,bs,label] = validate_partitions(As,bs,label)
            todel = false(1,length(As));
            for i=1:length(As)
                A = As{i};
                b = bs{i};
                for j=1:size(A,1)
                    anorm = norm(A(j,:));
                    A(j,:) = A(j,:)/anorm;
                    b(j) = b(j)/anorm;
                end
                for j=1:i-1
                    A2 = As{j};
                    b2= bs{j};
                    if all(ismember([A,b],[A2,b2],'row')) && length(b)==length(b2)
                        todel(i) = true;
                        break;
                    end                   
                end
                As{i} = A;
                bs{i} = b;
            end
            As(todel) = [];
            bs(todel) = [];
            label(todel) = [];
        end
        function id = type2id(type)
            switch type
                case idRTL.FormulaType.AtomicProposition
                    id = 1;
                case idRTL.FormulaType.And
                    id = 2;
                case idRTL.FormulaType.Or
                    id = 3;
                case idRTL.FormulaType.Until
                    id = 4;
                case idRTL.FormulaType.Release
                    id = 5;
                case idRTL.FormulaType.Always
                    id = 6;
                case idRTL.FormulaType.Eventually
                    id = 7;
            end
        end
        function [As,bs,label,phi] = predicates2atomicproposition(p,l)
            [A,b] = predicates2polytope(p);
            As = {A};
            bs = {b};
            label = {l};
            phi.type =idRTL.Formula.type2id(idRTL.FormulaType.AtomicProposition);
            phi.childs = [];
            phi.parents = [];
            phi.atom = l;
            phi.isneg = 0;  
        end
        function [As,bs,label,phi,childs] = formulas2and(phi1,phi2)
            if phi1.type == idRTL.FormulaType.AtomicProposition && ...
                    phi2.type ~= idRTL.FormulaType.AtomicProposition
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi2,phi1,idRTL.FormulaType.And);
            elseif phi2.type == idRTL.FormulaType.AtomicProposition && ...
                    phi1.type ~= idRTL.FormulaType.AtomicProposition                
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi1,phi2,idRTL.FormulaType.And);
            elseif phi1.type ~= idRTL.FormulaType.AtomicProposition && ...
                    phi2.type ~= idRTL.FormulaType.AtomicProposition                
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi1,phi2,idRTL.FormulaType.And);
            else
                error('Invalid arguments.');
            end      
        end
        function phi = formulastruct_vertcat(phi1,phi2,type)
            len1 = length(phi1);
            len2 = length(phi2);
            for i=1:len2
                if ~isempty(phi2(i).childs)
                    for j=1:length(phi2(i).childs)
                        phi2(i).childs(j) = phi2(i).childs(j) + len1;
                    end
                end
                if ~isempty(phi2(i).parents)
                    for j=1:length(phi2(i).parents)
                        phi2(i).parents(j) = phi2(i).parents(j) + len1;
                    end
                end
            end
            nelem = len1+len2+1;
            phi1(end).parents = nelem;
            phi2(end).parents = nelem;
            phi = [phi1;phi2];
            phi(nelem).type =idRTL.Formula.type2id(type);
            phi(nelem).childs = [len1,len1+len2];
            phi(nelem).parents = [];
            phi(nelem).atom = [];
            phi(nelem).isneg = [];
        end
        
        function [As,bs,label,phi,childs] = get_formula(phi1,phi2,type)
            phi = idRTL.Formula.formulastruct_vertcat(phi1.phi,phi2.phi,type);
            As = [phi1.As;phi2.As];                
            bs = [phi1.bs;phi2.bs];                
            label = [phi1.label;phi2.label];
            childs = [phi1;phi2];               
        end
        
        function [As,bs,label,phi,childs,type] = formulas2or(phi1,phi2)
            type = idRTL.FormulaType.Or;
            if phi1.type == idRTL.FormulaType.AtomicProposition && ...
                    phi2.type == idRTL.FormulaType.AtomicProposition
                if (phi2.label{1} ~= phi1.label{1}) 
                    idRTL.Formula.next_symbol(phi2.id,phi1.label{1});
                    phi2.label(:) = phi1.label(1);
                end                
                [As,bs,label,phi,childs] = idRTL.Formula.or_propositions(phi1,phi2);
                type = idRTL.FormulaType.AtomicProposition;
            elseif phi1.type == idRTL.FormulaType.AtomicProposition && ...
                    phi2.type ~= idRTL.FormulaType.AtomicProposition
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi2,phi1,idRTL.FormulaType.Or);
            elseif phi2.type == idRTL.FormulaType.AtomicProposition && ...
                    phi1.type ~= idRTL.FormulaType.AtomicProposition                
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi1,phi2,idRTL.FormulaType.Or);
            elseif phi1.type ~= idRTL.FormulaType.AtomicProposition && ...
                    phi2.type ~= idRTL.FormulaType.AtomicProposition                
                [As,bs,label,phi,childs] = ...
                    idRTL.Formula.get_formula(phi1,phi2,idRTL.FormulaType.Or);
            else
                error('Invalid arguments.');
            end      
        end
        function [As,bs,label,phi,childs] = or_propositions(prop1,prop2)
            As = [prop1.As;prop2.As];
            bs = [prop1.bs;prop2.bs];
            label = num2cell(repmat(prop1.label{1},length(As),1));
            phi = prop1.phi;
            childs = [];
        end
    end
end

