function b = not(a)
%NOT 

    if a.type == idRTL.FormulaType.AtomicProposition
        b = a;
        b.phi.isneg = mod(b.phi.isneg+1,2);
    else
        error('Negation is only accepted for atomic propositions.');
    end
end

