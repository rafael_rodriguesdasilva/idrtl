classdef LinearFunction
    %LINEARFUNCTION A linear function    
    % = variables(1)*values(1) + ... + variables(len)*values(len) + cnst
    % where len = length(variables)
    
    properties (SetAccess = private)
        variables
        values
        cnst 
    end
    
    methods
        function obj = LinearFunction()
            %LINEARFUNCTION Construct an instance of this class
            obj.cnst = 0;
        end
    end
end

