function c = ge(a,b)
%LE c = a - b >= 0
    
    assert(all(size(a)==size(b)),...
        'The dimensions of A and B do not agree.');
    
    if isscalar(a)
        mu = a - b;
        c = idRTL.AtomicPredicate(mu);
    else
        c = [];
        for i=1:size(a,1)
            for j=1:size(a,2)
                if isempty(c)
                    c = a(i,j) >= b(i,j);
                else
                    c = c & a(i,j) >= b(i,j);
                end
            end
        end
    end

end

