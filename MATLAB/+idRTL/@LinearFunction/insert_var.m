function obj = insert_var(obj,value,variable)
%INSERT_VAR 

    if ~isscalar(value) || ~isscalar(variable)
        error('Only scalar arguments.');
    end
    
    if isempty(obj.variables)
        obj.variables = variable;
        obj.values = value;
    else
        obj.variables(end+1) = variable;
        obj.values(end+1) = value;
        
        [obj.variables,ind] = sort(obj.variables);
        obj.values = obj.values(ind);
    end    
end

