function c = le(a,b)
%LE c = b - a >= 0

    assert(all(size(a)==size(b)),...
        'The dimensions of A and B do not agree.');
    
    if isscalar(a)
        mu = b - a;
        c = idRTL.AtomicPredicate(mu);
    else
        c = [];
        for i=1:size(a,1)
            for j=1:size(a,2)
                if isempty(c)
                    c = a(i,j) <= b(i,j);
                else
                    c = c & a(i,j) <= b(i,j);
                end
            end
        end
    end
end

