function f = mtimes(a,b)
%MTIMES f = a * b

    assert((size(a,2)==size(b,1))||isscalar(a)||isscalar(b),...
        'The inner dimensions of A and B do not agree.');
    
    if isscalar(a) && isscalar(b)
        if isnumeric(a) && isa(b,'idRTL.LinearFunction')
            if abs(a)<eps
                f = 0;
            else
                f = b;
                for i=1:length(f.values)
                    f.values(i) = a*f.values(i);
                end
                f.cnst = a*f.cnst;
            end
        elseif isa(a,'idRTL.LinearFunction') && isnumeric(b)
            if abs(b)<eps
                f = 0;
            else
                f = a;
                for i=1:length(f.values)
                    f.values(i) = b*f.values(i);
                end
                f.cnst = b*f.cnst;
            end
        else
            error('Invalid type');
        end
    elseif isscalar(a)
        rows = size(b,1);
        cols = size(b,2);
        f(rows,cols) = idRTL.LinearFunction();
         for row=1:rows            
            for col=1:cols
                f(row,col) = a*b(row,col);
            end
         end
    elseif isscalar(b)
        rows = size(a,1);
        cols = size(a,2);
        f(rows,cols) = idRTL.LinearFunction();
         for row=1:rows            
            for col=1:cols
                f(row,col) = b*a(row,col);
            end
         end
   else
        rows = size(a,1);
        cols = size(b,2);
        f(rows,cols) = idRTL.LinearFunction();
        for row=1:rows            
            for col=1:cols
                for i=1:size(a,2)
                    f(row,col) = f(row,col) + a(row,i)*b(i,col);
                end
            end
        end
    end

    
    
end

