function f = plus(a,b)
%PLUS f = a + b

    if ~isscalar(a) || ~isscalar(b)
        assert(all(size(a)==size(b)),...
            'The dimensions of A and B do not agree.');
        [rows,cols] = size(a);
        f(rows,cols) = idRTL.LinearFunction();
        for i=1:size(a,1)
            for j=1:size(a,2)
                f(i,j) = a(i,j) + b(i,j);
            end
        end        
    else
        f = idRTL.LinearFunction();

        if isa(a,'idRTL.LinearFunction') && isa(b,'idRTL.LinearFunction')
            f.variables = a.variables;
            f.values = a.values;
            f = f.variables_vertcat(b.variables,b.values);
            f.cnst = a.cnst+b.cnst;
        elseif isa(a,'idRTL.LinearFunction') && isa(b,'idRTL.Variable')
            f.variables = a.variables;
            f.values = a.values;
            f = f.variables_vertcat(b,1);
            f.cnst = a.cnst;
        elseif isa(a,'idRTL.LinearFunction') &&isnumeric(b)
            f.variables = a.variables;
            f.values = a.values;
            f.cnst = a.cnst+b;
        elseif isa(b,'idRTL.LinearFunction') && isa(a,'idRTL.Variable')
            f.variables = b.variables;
            f.values = b.values;
            f = f.variables_vertcat(a,1);
            f.cnst = b.cnst;
        elseif isa(b,'idRTL.LinearFunction') &&isnumeric(a)
            f.variables = b.variables;
            f.values = b.values;
            f.cnst = b.cnst+a;
        else
            error('Invalid type');
        end
    end
end

