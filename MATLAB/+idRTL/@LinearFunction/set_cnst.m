function obj = set_cnst(obj,value)
%SET_CNST set constant value

    if ~isscalar(value) 
        error('Only scalar arguments.');
    end
    
    obj.cnst = value;
end

