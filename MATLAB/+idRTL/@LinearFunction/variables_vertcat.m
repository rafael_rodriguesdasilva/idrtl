function obj = variables_vertcat(obj,b,bvalues)
%VARIABLES_CAT concatenate two set of variables

    variables = [obj.variables(:);b(:)]';
    values = [obj.values(:);bvalues(:)]';
    todel = values==0;
    variables(todel) = [];
    values(todel) = [];
    if ~isempty(variables)
        len = length(variables);
        ind = [variables(:).idx;1:len]';
        ind = sortrows(ind,1);    
        [~,iind,~] = unique(ind(:,1));

        obj.variables = variables(ind(iind,2));
        obj.values = accumarray(ind(:,1),values);
        obj.values(obj.values==0) = [];
    else
        obj.variables = [];
        obj.values = [];
    end
end

