classdef LinearHybridSystem
    %LINEARHYBRIDSYSTEM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        Aq
        Bq
        cq
        Axq
        bxq
        Auq
        buq
        x
        u
        n
        m
        Q
    end
    
    methods
        function obj = LinearHybridSystem(n,m)
            %LINEARHYBRIDSYSTEM 
            obj.n = n;
            obj.x = idRTL.Variable(1:obj.n,obj.n,idRTL.VariableType.State);
            obj.m = m;
            obj.u = idRTL.Variable(1:obj.m,obj.m,idRTL.VariableType.Input);
        end
        
        function obj = add_mode(obj,A,B,c,Au,bu,Ax,bx)
            %METHOD1 Summary of this method goes here
            q = length(obj.Aq)+1;
            obj.Aq{q} = A;
            obj.Bq{q} = B;
            obj.cq{q} = c;
            if size(Ax,2)~=obj.n || size(Ax,1)~=size(bx,1) || size(bx,2)~=1
                error('Invalid state domain.');
            end
            obj.Axq{q} = Ax;
            obj.bxq{q} = bx;
            if isempty(obj.cq{q})
                obj.c{q} = zeros(obj.n,1);
            end
            if size(obj.Aq{q},1) ~= obj.n || size(obj.Aq{q},2) ~= obj.n || ...
                    size(obj.Bq{q},1) ~= obj.n || size(obj.Bq{q},2) ~= obj.m ...
                    any(size(obj.cq{q})~=[obj.n,1])
                error('Invalid matrices dimensions.');
            end
            if size(Au,2)~=obj.m || size(Au,1)~=size(bu,1) || size(bu,2)~=1
                error('Invalid input domain.');
            end
            obj.Auq{q} = Au;
            obj.buq{q} = bu;
            obj.Q{q} = q-1;
        end
        
        function modes = get_modes_of(obj,x)
            modes = [];
            for i=1:length(obj.Q)
                if obj.Axq{i}*x - obj.bxq{i} < eps
                    modes(end+1) = i;
                end
            end
        end
    end
end

