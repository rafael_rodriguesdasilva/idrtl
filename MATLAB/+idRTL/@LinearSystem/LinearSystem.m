classdef LinearSystem
    %LINEARSYSTEM x_{k+1} = A*x_k + B*u_k
    
    properties (SetAccess = private)
        A
        B
        c
        Au
        bu
        Ax
        bx
        x
        u
        n
        m
    end
    
    methods
        function obj = LinearSystem(A,B,c,Ax,bx,Au,bu)
            %LINEARSYSTEM Construct an instance of this class

            
            obj.A = A;
            obj.B = B;
            obj.c = c;
            [obj.n,obj.m] = size(obj.B);
            if isempty(obj.c)
                obj.c = zeros(obj.n,1);
            end
            if size(obj.A,1) ~= obj.n || size(obj.A,2) ~= obj.n || ...
                    any(size(obj.c)~=[obj.n,1])
                error('Invalid matrices dimensions.');
            end
            if size(Au,2)~=obj.m || size(Au,1)~=size(bu,1) || size(bu,2)~=1
                error('Invalid input domain.');
            end
            if size(Ax,2)~=obj.n || size(Ax,1)~=size(bx,1) || size(bx,2)~=1
                error('Invalid input domain.');
            end
            obj.Au = Au;
            obj.bu = bu;
            obj.Ax = Ax;
            obj.bx = bx;
            
            obj.x = idRTL.Variable(1:obj.n,obj.n,idRTL.VariableType.State);
            obj.u = idRTL.Variable(1:obj.m,obj.m,idRTL.VariableType.Input);
        end
    end
end

