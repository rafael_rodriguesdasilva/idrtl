classdef Variable < handle
    %VARIABLE A continuous variable
    
    properties (SetAccess = private)
        type
        idx
        dim
    end
    
    methods
        function obj = Variable(idx, dim, type)
            %VARIABLE Construct an instance of this class
            
            if nargin == 0
                return;
            end

            [m,n] = size(idx);
            assert(isscalar(dim),'dim must be scalar.');
            assert(isscalar(type),'type must be scalar.');
            
            obj(m,n) = idRTL.Variable;
            
            for i=1:m
                for j=1:n
                    obj(i,j).idx = idx(i,j);
                    obj(i,j).type = type;
                    obj(i,j).dim = dim;
                end
            end
            
        end
    end
end

