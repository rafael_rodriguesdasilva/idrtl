function c = ge(a,b)
%LE c = a - b >= 0

    if ~isscalar(a) || ~isscalar(b)
        error('Only scalar arguments.');
    end
    
    mu = a - b;
    c = idRTL.AtomicPredicate(mu);
end

