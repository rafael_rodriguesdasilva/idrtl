function c = le(a,b)
%LE c = b - a >= 0

    if ~isscalar(a) || ~isscalar(b)
        error('Only scalar arguments.');
    end
    
    mu = b - a;
    c = idRTL.AtomicPredicate(mu);
end

