function f = mtimes(a,b)
%MTIMES f = a * b

    assert(size(a,2)==size(b,1),'The inner dimensions of A and B do not agree.');
    
    if isscalar(a) && isscalar(b)
        f = idRTL.LinearFunction();
        if isnumeric(a) && isa(b,'idRTL.Variable')
            f = f.insert_var(a,b);
        elseif isa(a,'idRTL.Variable') && isnumeric(b)
            f = f.insert_var(b,a);
        else
            error('Invalid type');
        end
    else
        rows = size(a,1);
        cols = size(b,2);
        f(rows,cols) = idRTL.LinearFunction();
        for row=1:rows            
            for col=1:cols
                for i=1:size(a,2)
                    f(row,col) = f(row,col) + a(row,i)*b(i,col);
                end
            end
        end
    end

    
    
end

