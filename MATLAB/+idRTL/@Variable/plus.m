function f = plus(a,b)
%PLUS f = a + b

    if ~isscalar(a) || ~isscalar(b)
        error('Only scalar arguments.');
    end

    f = idRTL.LinearFunction();
    
    if isnumeric(a) && isnumeric(b)
        f = f.set_cnst(a + b);
    elseif isa(a,'idRTL.Variable') && isa(b,'idRTL.Variable')
        if a.idx == b.idx
            f = f.insert_var(2,a);
        else
            f = f.insert_var(1,a);
            f = f.insert_var(1,b);
        end
    elseif isa(a,'idRTL.Variable') && isnumeric(b)
        f = f.insert_var(1,a);
        f = f.set_cnst(b);
    elseif isa(b,'idRTL.Variable') && isnumeric(a)
        f = f.insert_var(1,b);
        f = f.set_cnst(a);
    else
        error('Invalid type');
    end
end

