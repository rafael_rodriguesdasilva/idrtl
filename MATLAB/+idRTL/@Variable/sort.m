function [b,ind] = sort(a)
%SORT Sort array elements of a Variable vector by index
%   SYNTAX
%   b = sort(a)
%   [b,ind] = sort(a)

    if ~isvector(a)
        error('Only vector arguments.');
    end
    
    len = length(a);
    ind = zeros(len,2);
    for i = 1:len
        ind(i,:) = [a(i).idx,i];        
    end
    ind = sortrows(ind,1);
    ind(:,1) = [];
    b = a(ind);
end

