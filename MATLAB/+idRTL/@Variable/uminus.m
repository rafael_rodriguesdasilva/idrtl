function f = uminus(a)
%UMINUS f = -a
    if ~isscalar(a)
        error('Only scalar arguments.');
    end

    f = idRTL.LinearFunction();
    f = f.insert_var(-1,a);
    
end

