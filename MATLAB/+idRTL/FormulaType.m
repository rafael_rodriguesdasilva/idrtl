classdef FormulaType
    %FORMULATYPE Type of continuous variables
    
   enumeration
      AtomicProposition, And, Or, Until, Release, Always, Eventually
   end
end

