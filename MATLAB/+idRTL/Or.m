function c = Or(a,b)
%OR 
    if isempty(a)
        c = b;
    elseif isempty(b)
        c = a;
    else
        c = idRTL.Formula([a;b],idRTL.FormulaType.Or);
    end
end

