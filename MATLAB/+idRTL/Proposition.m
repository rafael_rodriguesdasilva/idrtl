function f = Proposition(p)
%ATOM atomic proposition 

    f = idRTL.Formula(p,idRTL.FormulaType.AtomicProposition);
end

