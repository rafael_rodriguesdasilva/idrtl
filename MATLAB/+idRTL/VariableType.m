classdef VariableType
    %VARIABLETYPE Type of continuous variables
    
   enumeration
      State, Input
   end
end

