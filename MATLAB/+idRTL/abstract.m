function M = abstract(phi,sys,x0,q0)
%ABSTRACT Summary of this function goes here
%   Detailed explanation goes here
    N = length(phi.As);
    if isa(sys,'idRTL.LinearSystem') && nargin == 3
        M = idRTL.kripke.create(x0,0,...
                                phi.As,phi.bs,phi.label,...
                                {0},{sys.A},{sys.B},{sys.c},...
                                {sys.Ax},{sys.bx},...
                                {sys.Au},{sys.bu});
    elseif isa(sys,'idRTL.LinearHybridSystem') && nargin == 4
        M = idRTL.kripke.create(x0,q0,...
                                phi.As,phi.bs,phi.label,...
                                sys.Q',sys.Aq',sys.Bq',sys.cq',...
                                sys.Axq',sys.bxq',...
                                sys.Auq',sys.buq');
    end

end

