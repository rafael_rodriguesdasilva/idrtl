/*
 * mxinterface.c
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */

#include "mxinterface.h"

static int idkripke_get_idx(idmap_t *h_key_idx, int key) {
	int *h_idx = (int *)idlist_first((idlist_t *)h_key_idx->get_values2_at(h_key_idx,&key));

	if (h_idx==NULL) {
		return -1;
	}
	return *h_idx;
}
//static void * idmap_first_value1_at(idmap_t *h_map, void *h_value2) {
//	return idlist_first((idlist_t *)h_map->get_values1_at(h_map,h_value2));
//}
//static void * idmap_first_value2_at(idmap_t *h_map, void *h_value1) {
//	return idlist_first((idlist_t *)h_map->get_values2_at(h_map,h_value1));
//}
static int int_fromMXArray_with_name(const mxArray *pm, const char *name) {
	if ((pm!=NULL)&&((!mxIsScalar(pm))||(!mxIsDouble(pm)))) {
		char str[80];
		sprintf(str,"%s must be a scalar.", name);
		mexErrMsgIdAndTxt("mxinterface:invalidArgument",str);
	}

	if (pm==NULL) {
		return -1;
	}
	double *ptr = mxGetPr(pm);

	return ptr[0];
}
int int_fromMXArray(const mxArray *pm) {
	if ((pm!=NULL)&&((!mxIsScalar(pm))||(!mxIsDouble(pm)))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","int is not scalar");
	}

	if (pm==NULL) {
		return -1;
	}
	double *ptr = mxGetPr(pm);

	return ptr[0];
}
int idlabel_fromMXArray(mxArray *hmx_label) {
	return int_fromMXArray_with_name(hmx_label,"label");
}
int idmode_fromMXArray(mxArray *hmx_mode) {
	return int_fromMXArray_with_name(hmx_mode,"mode");
}
//idrobrun_t * idrun_to_robrun(idrun_t *h_run, double delta, idkripke_t *h_M) {
//	idlist_t *h_label_i;
//	idlinear_sys_t *h_sys_i;
//	idcsys_t *h_x_constr, *h_u_constr;
//	int si, nsamples_i;
//	idrb_node_t *it_s, *it_nsp, *it_l, *it_sys, *it_x, *it_u;
//	idrobrun_t *p_robrun = idrobrun(delta,h_M);
//
//	it_s 	= idrb_first(h_run->mp_states_intarray->mp_head);
//	it_nsp 	= idrb_first(h_run->mp_nsample_intarray->mp_head);
//	it_l 	= idrb_first(h_run->mp_h_label_array->mp_head);
//	it_sys 	= idrb_first(h_run->mp_h_sys_array->mp_head);
//	it_u 	= idrb_first(h_run->mp_h_u_constr_array->mp_head);
//	it_x 	= idrb_first(h_run->mp_h_x_constr_array->mp_head);
//
//	while((it_s!=idrb_nil(h_run->mp_states_intarray->mp_head))&&
//			(it_nsp != idrb_nil(h_run->mp_nsample_intarray->mp_head))&&
//			(it_l 	!= idrb_nil(h_run->mp_h_label_array->mp_head))&&
//			(it_sys != idrb_nil(h_run->mp_h_sys_array->mp_head))&&
//			(it_u 	!= idrb_nil(h_run->mp_h_u_constr_array->mp_head))&&
//			(it_x 	!= idrb_nil(h_run->mp_h_x_constr_array->mp_head))) {
//		si			= *(int *)it_s->value(it_s);
//		nsamples_i	= *(int *)it_nsp->value(it_nsp);
//		h_label_i	=  (idlist_t *)it_l->value(it_l);
//		h_sys_i		=  (idlinear_sys_t *)it_sys->value(it_sys);
//		h_u_constr	=  (idcsys_t *)it_u->value(it_u);
//		h_x_constr	=  (idcsys_t *)it_x->value(it_x);
//
//		p_robrun->append_step(p_robrun,nsamples_i,si,h_label_i,h_sys_i,h_u_constr,h_x_constr);
//
//		it_s 	= idrb_next(it_s);
//		it_nsp 	= idrb_next(it_nsp);
//		it_l 	= idrb_next(it_l);
//		it_sys 	= idrb_next(it_sys);
//		it_u 	= idrb_next(it_u);
//		it_x 	= idrb_next(it_x);
//	}
//
//	return p_robrun;
//}

idbool_t idRTL_MATLAB(double delta, double delta_progress, idformula_t *h_phi, idrobrun_t **hp_solution) {
	idbsc_t *p_bsc = idbsc(delta, delta_progress, h_phi);
	idfsearch_t *p_fsearch;
	idrobrun_t *p_solution = NULL;

	while(p_bsc->check(p_bsc) || !p_bsc->is_unsat(p_bsc)) {
		while(p_bsc->is_sat(p_bsc)) {
			p_fsearch = p_bsc->decode(p_bsc);
			mexPrintf("K=%d : sat\n", p_bsc->length(p_bsc));
			if (p_fsearch->search(p_fsearch)) {
				mexPrintf("FOUND dynamically feasible solution\n");
				mexPrintf("maximizing its robustness...\n");
				p_solution = idbsc_run_to_robrun(p_fsearch->mp_solution,delta,h_phi->mh_M);
				p_solution->compute(p_solution);
				break;
			}
			mexPrintf("add cex and check...\n");
			p_bsc->add_cex_and_check(p_bsc,p_fsearch->get_cex(p_fsearch),p_fsearch->isfair(p_fsearch));
			p_fsearch->destroy(p_fsearch);
			p_fsearch = NULL;
		}
		if (p_bsc->is_sat(p_bsc)) {
			break;
		}
		mexPrintf("K=%d : unsat\n", p_bsc->length(p_bsc));
	}

	*hp_solution = p_solution;
	p_bsc->destroy(p_bsc);

	return (p_solution!=NULL?TRUE:FALSE);
}

mxArray *MXArray_from_drun(idarray_t *h_states, idarray_t *h_nsamples, idkripke_t *h_M) {
	idrb_node_t *it_state, *it_nsample;
	int i, si, indi, nsample;
	idarray_t *p_s_array = idintarray();
	mxArray *hmx_out;

	it_state 	= idrb_first(h_states->mp_head);
	it_nsample 	= idrb_first(h_nsamples->mp_head);

	while((it_state!=idrb_nil(h_states->mp_head))&&(it_nsample!=idrb_nil(h_nsamples->mp_head))) {
		si 		= *(int *)it_state->value(it_state);
		nsample = *(int *)it_nsample->value(it_nsample);
		indi	=  h_M->get_bits_from_state(h_M,si);
		if (indi<0) {
			mexErrMsgTxt("state not found in indices list");
		}
		for(i=0;i<nsample;i++) {
			p_s_array->append(p_s_array,&indi);
		}
		it_state 	= idrb_next(it_state);
		it_nsample 	= idrb_next(it_nsample);
	}
	hmx_out = MXArray_from_idintarray(p_s_array,1);
	p_s_array->destroy(p_s_array);

	return hmx_out;
}

mxArray *MXArray_from_dblvector(double *h_vec, int rows, int cols) {
	int i, j;
	mxArray *hmx_pm = mxCreateDoubleMatrix(rows,cols,mxREAL);
	idrb_node_t *it;
	double *h_dbl = mxGetPr(hmx_pm);

	for(i=0;i<rows;i++) {
		for(j=0;j<cols;j++) {
			h_dbl[j*rows+i] = h_vec[i*cols+j];
		}
	}
	return hmx_pm;
}
mxArray *MXArray_from_idintarray(idarray_t *h_array, int cols) {
	int len = h_array->length(h_array);
	if (len%cols!=0) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Invalid column length (%dmod%d = %d != 0).", len, cols, len%cols);
	}
	int i, j, rows = (int)(len/cols);
	mxArray *hmx_pm = mxCreateDoubleMatrix(rows,cols,mxREAL);
	idrb_node_t *it;
	double *h_dbl = mxGetPr(hmx_pm);


	idrb_traverse(it,h_array->mp_head) {
		j = it->keyi(it)%cols;
		i = (int)floor(it->keyi(it)/cols);
		h_dbl[i*cols+j] = (double)(*(int *)it->value(it));
	}

	return hmx_pm;
}

static int idformula_type_fromMXArray_at(int i, mxArray *hmx_phi) {
	mxArray *hmx_type= mxGetFieldByNumber(hmx_phi,(mwIndex)i,mxGetFieldNumber(hmx_phi,"type"));

	if (!mxIsNumeric(hmx_type) || mxIsComplex(hmx_type) || !mxIsScalar(hmx_type)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).type must be a real numeric scalar.", i+1);
	}

	int type = (int)Dbl_fromMXArray(hmx_type);

	if ((type != RTL_ATOMICPROP)&&(type != RTL_AND)&&(type != RTL_OR)&&(type != RTL_UNTIL)&&
			(type != RTL_RELEASE)&&(type != RTL_ALWAYS)&&(type != RTL_EVENTUALLY)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).type must be one of the following values:\n\t * %d for atomic propositions\n\t * %d for and\n\t * %d for or\n\t * %d for until\n\t * %d for release\n\t * %d for always\n\t * %d for eventually.",
				i+1, RTL_ATOMICPROP, RTL_AND, RTL_OR, RTL_UNTIL, RTL_RELEASE, RTL_ALWAYS, RTL_EVENTUALLY);
	}

	return type;
}

static idarray_t *idformula_childs_fromMXArray_at(int i, mxArray *hmx_phi, int type, int nsubf) {
	mxArray *hmx_childs= mxGetFieldByNumber(hmx_phi,(mwIndex)i,mxGetFieldNumber(hmx_phi,"childs"));
	int nchilds, v, j;
	double *p_array;
	idarray_t *p_childs = idintarray();

	if ((hmx_childs!=NULL)&&(!mxIsEmpty(hmx_childs))&&(!mxIsDouble(hmx_childs) || (mxGetM(hmx_childs)!=1))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must be a double row vector.", i+1);
	}

	if ((hmx_childs!=NULL)&&(!mxIsEmpty(hmx_childs))) {
		nchilds = mxGetN(hmx_childs);
		if ((type == RTL_ATOMICPROP)&&(nchilds>0)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must be empty because type is ATOMIC PROPOSITION.", i+1);
		} else if ((type == RTL_AND)&&(nchilds!=2)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 2 because type is AND.", i+1);
		} else if ((type == RTL_OR)&&(nchilds!=2)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 2 because type is OR.", i+1);
		} else if ((type == RTL_UNTIL)&&(nchilds!=2)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 2 because type is UNTIL.", i+1);
		} else if ((type == RTL_RELEASE)&&(nchilds!=2)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 2 because type is RELEASE.", i+1);
		} else if ((type == RTL_ALWAYS)&&(nchilds!=1)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 1 because type is ALWAYS.", i+1);
		} else if ((type == RTL_EVENTUALLY)&&(nchilds!=1)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must has length 1 because type is EVENTUALLY.", i+1);
		}
		p_array = mxGetPr(hmx_childs);
		for(j=0;j<nchilds;j++) {
			v = (int)p_array[j];
			if ((v <= 0) || (v > nsubf) || (v == i+1)) {
				mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).childs must be between 1 and %d and different from %d.", i+1, nsubf, i+1);
			}
			p_childs->append(p_childs,&v);
		}
	}

	return p_childs;
}

static idarray_t *idformula_parents_fromMXArray_at(int i, mxArray *hmx_phi, int nsubf) {
	mxArray *hmx_parents= mxGetFieldByNumber(hmx_phi,(mwIndex)i,mxGetFieldNumber(hmx_phi,"parents"));
	int nparents, v, j;
	double *p_array;
	idarray_t *p_parents = idintarray();

	if ((hmx_parents!=NULL)&&(!mxIsEmpty(hmx_parents))&&(!mxIsDouble(hmx_parents) || (mxGetM(hmx_parents)!=1))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).parents must be a double row vector.", i+1);
	}

	if ((hmx_parents!=NULL)&&(!mxIsEmpty(hmx_parents))) {
		nparents = mxGetN(hmx_parents);
		p_array = mxGetPr(hmx_parents);
		for(j=0;j<nparents;j++) {
			v = (int)p_array[j];
			if ((v <= 0) || (v > nsubf) || (v == i+1)) {
				mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).parents must be between 0 and %d and different from %d.", i+1, nsubf, i+1);
			}
			p_parents->append(p_parents,&v);
		}
	}

	return p_parents;
}


static int idformula_atom_fromMXArray_at(int i, mxArray *hmx_phi, int type) {
	mxArray *hmx_atom= mxGetFieldByNumber(hmx_phi,(mwIndex)i,mxGetFieldNumber(hmx_phi,"atom"));

	int c = -1;

	if (type == RTL_ATOMICPROP) {
		if ((hmx_atom==NULL)|| mxIsEmpty(hmx_atom)|| !mxIsDouble(hmx_atom) || !mxIsScalar(hmx_atom)) {
					mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).atom must be a scalar.", i+1);
		}
		c = int_fromMXArray(hmx_atom);
	}

	return c;
}

static idbool_t idformula_isneg_fromMXArray_at(int i, mxArray *hmx_phi, int type) {
	mxArray *hmx_isneg= mxGetFieldByNumber(hmx_phi,(mwIndex)i,mxGetFieldNumber(hmx_phi,"isneg"));
	idbool_t isneg;
	double *p_array;

	if (type == RTL_ATOMICPROP) {
		if ((hmx_isneg!=NULL)&&(!mxIsEmpty(hmx_isneg))&&(!mxIsNumeric(hmx_isneg) || mxIsComplex(hmx_isneg) || !mxIsScalar(hmx_isneg))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","The field phi(%d).isneg must be a real numeric scalar.", i+1);
		}
		if ((hmx_isneg==NULL)|| mxIsEmpty(hmx_isneg)) {
			isneg = FALSE;
		} else {
			p_array = mxGetPr(hmx_isneg);
			if (p_array[0]==0) {
				isneg = FALSE;
			} else {
				isneg = TRUE;
			}
		}
	}

	return isneg;
}

static int idformula_fromMXArray_get_root(mxArray *hmx_phi, int *h_nsubf) {
	int i, parent, nsubf = mxGetM(hmx_phi);
	idarray_t *p_parents;
	*h_nsubf = nsubf;

	parent = -1;
	for(i=0;i<nsubf;i++) {
		p_parents = idformula_parents_fromMXArray_at(i,hmx_phi,nsubf);
		if (p_parents->length(p_parents)==0) {
			if (parent<0) {
				parent = i;
			} else {
				mexErrMsgIdAndTxt("mxinterface:invalidArgument","There is not an unique root formula (empty field phi(i).parents).");
			}
		}
		p_parents->destroy(p_parents);
	}
	if (parent<0) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Root formula not found (empty field phi(i).parents).");
	}
	return parent;
}

static idformula_t *idformula_fromMXArray_at(int i, mxArray *hmx_phi, idkripke_t *h_M, int nsubf) {
	int type;
	idarray_t *p_childs;
	idbool_t isneg;
	int label;
	idformula_t *p_parent = NULL, *p_child1 = NULL, *p_child2 = NULL;
	idrb_node_t *it;


	type = idformula_type_fromMXArray_at(i, hmx_phi);
	p_childs = idformula_childs_fromMXArray_at(i, hmx_phi, type, nsubf);
	label = idformula_atom_fromMXArray_at(i, hmx_phi, type);
	isneg = idformula_isneg_fromMXArray_at(i, hmx_phi, type);

	it = idrb_first(p_childs->mp_head);
	if (it != idrb_nil(p_childs->mp_head)) {
		p_child1 = idformula_fromMXArray_at(*(int *)it->value(it)-1,hmx_phi,h_M,nsubf);
		it = idrb_next(it);
		if (it != idrb_nil(p_childs->mp_head)) {
			p_child2 = idformula_fromMXArray_at(*(int *)it->value(it)-1,hmx_phi,h_M,nsubf);
		}
	}
	p_childs->destroy(p_childs);

	switch(type) {
	case RTL_ATOMICPROP:
		p_parent = id_mkAtomicProp(i,h_M,label,isneg);
		break;
	case RTL_AND:
		p_parent = id_mkAnd(i,p_child1,p_child2);
		break;
	case RTL_OR:
		assert(p_child1!=NULL);
		assert(p_child2!=NULL);
		p_parent = id_mkOr(i,p_child1,p_child2);
		break;
	case RTL_UNTIL:
		assert(p_child1!=NULL);
		assert(p_child2!=NULL);
		p_parent = id_mkUntil(i,p_child1,p_child2);
		break;
	case RTL_RELEASE:
		assert(p_child1!=NULL);
		assert(p_child2!=NULL);
		p_parent = id_mkRelease(i,p_child1,p_child2);
		break;
	case RTL_ALWAYS:
		assert(p_child1!=NULL);
		p_parent = id_mkAlways(i,p_child1);
		break;
	case RTL_EVENTUALLY:
		assert(p_child1!=NULL);
		p_parent = id_mkEventually(i,p_child1);
		break;
	}

	return p_parent;
}

idformula_t *idformula_fromMXArray(mxArray *hmx_phi, idkripke_t *h_M) {

	int nsubf, root;

	idformula_t *p_phi;

	root = idformula_fromMXArray_get_root(hmx_phi,&nsubf);

	p_phi = idformula_fromMXArray_at(root,hmx_phi,h_M,nsubf);




	return p_phi;
}


static iddblmatrix_t *idkripke_fromMXArray_get_x0(mxArray *h_pm) {
	mxArray *hmx_x0    = mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"x0"));

	if (!mxIsDouble(hmx_x0) || (mxGetN(hmx_x0)!=1)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Invalid field M.x0 (double column vector).");
	}

	return iddblmatrix_fromMXArray(hmx_x0);
}
static int idkripke_fromMXArray_get_q0(mxArray *h_pm) {
	mxArray *hmx_q0    = mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"q0"));

	int q0 = int_fromMXArray_with_name(hmx_q0,"M.q0");

	if (q0<0) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.q0 must be a non-negative scalar.");
	}

	return q0;
}
static int idkripke_fromMXArray_get_s0(mxArray *h_pm) {
	mxArray *hmx_s0    = mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"s0"));

	return int_fromMXArray_with_name(hmx_s0,"M.s0");
}

idbool_t idbool_fromMXArray(mxArray *hmx_pm) {
	if ((!mxIsDouble(hmx_pm)) ||  (!mxIsScalar(hmx_pm))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Invalid Boolean value.");
	}
	idbool_t value = FALSE;
	double *h_pdbl;

	h_pdbl = mxGetPr(hmx_pm);
	if ((int)(*h_pdbl) != 0) {
		value = TRUE;
	}

	return value;
}
static idbool_t idkripke_fromMXArray_is_fair(mxArray *h_pm) {
	mxArray *hmx_is_fair    = mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"is_fair"));
	idbool_t is_fair = FALSE;

	if ((!mxIsDouble(hmx_is_fair)) ||  (!mxIsScalar(hmx_is_fair))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Invalid field M.is_fair (double scalar, value ~= 0 == TRUE).");
	}

	is_fair = idbool_fromMXArray(hmx_is_fair);

	return is_fair;
}
iddblmatrix_t *iddblmatrix_fromMXArray(mxArray *hmx_A) {
	if (!mxIsDouble(hmx_A)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","It is not a double matrix.");
	}
	int 	i,j,
			m = mxGetM(hmx_A),
			n = mxGetN(hmx_A);
	iddblmatrix_t *p_A = iddblmatrix(m,n);
	double *h_dbl = mxGetPr(hmx_A);
	for(i=0;i<m;i++) {
		for(j=0;j<n;j++) {
			p_A->set_at(p_A,i,j,h_dbl[j*m+i]);
		}
	}
	return p_A;
}
mxArray *MXArray_from_iddblmatrix(iddblmatrix_t *h_A) {
	int 	i,j,
			m = h_A->rows(h_A),
			n = h_A->cols(h_A);
	mxArray *pmx_A = mxCreateNumericMatrix((mwSize)m,(mwSize)n,mxDOUBLE_CLASS,mxREAL);
	double *h_dbl = mxGetPr(pmx_A);
	for(i=0;i<m;i++) {
		for(j=0;j<n;j++) {
			h_dbl[i+j*m] = h_A->get(h_A,i,j);
		}
	}
	return pmx_A;
}
idlinear_sys_t *idlinear_sys_fromMXArray(mxArray *hmx_A, mxArray *hmx_B, mxArray *hmx_c) {
	iddblmatrix_t 	*p_A = iddblmatrix_fromMXArray(hmx_A),
					*p_B = iddblmatrix_fromMXArray(hmx_B),
					*p_c = iddblmatrix_fromMXArray(hmx_c);

	if (p_A->rows(p_A) != p_B->rows(p_B)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Linear affine system matrices A and B must have same number of rows.");
	}
	if (p_c->cols(p_c)!=1) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Linear affine system vector c must be a column vector.");
	}
	if (p_A->rows(p_A) != p_c->rows(p_c)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Linear affine system matrix A and vector c must have same number of rows.");
	}

	idlinear_sys_t 	*p_sys = idlinear_sys(p_A,p_B,p_c);

	return p_sys;
}
idpolytope_t *idpolytope_fromMXArray(mxArray *hmx_A, mxArray *hmx_b) {

	idmatrix_t 	*p_A = GMPmat_fromMXArray(hmx_A),
				*p_b = GMPmat_fromMXArray(hmx_b);
	if (p_b->cols(p_b)!=1) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Polytope vector b must be a column vector.");
	}
	if (p_A->rows(p_A) != p_b->rows(p_b)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Polytope matrix A and vector must have same number of rows.");
	}

	idpolytope_t *p_poly = H_idpolytope(p_A,p_b);

	return p_poly;
}
idlist_t *idintlist_fromMXArray(mxArray *hmx_list) {
	if ((hmx_list!=NULL)&&(!mxIsEmpty(hmx_list))&&((!mxIsDouble(hmx_list))||(mxGetM(hmx_list)!=1))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Integer list must be a row vector.");
	}
	int nsymbols, i, symbol;
	double *h_val;

	idlist_t *p_label = idintlist();
	if ((hmx_list!=NULL)&&(!mxIsEmpty(hmx_list))) {
		nsymbols = mxGetN(hmx_list);
		h_val = mxGetPr(hmx_list);
		for(i=0;i<nsymbols;i++) {
			symbol = (int)h_val[i];
			p_label->push(p_label,&symbol);
		}
	}

	return p_label;
}

idlist_t *idlabels_fromMXArray(mxArray *hmx_label) {
	if ((hmx_label!=NULL)&&(!mxIsEmpty(hmx_label))&&((!mxIsDouble(hmx_label))||(mxGetM(hmx_label)!=1))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Label must be a row vector.");
	}
	return idintlist_fromMXArray(hmx_label);
}
static void idkripke_fromMXArray_add_partition(idkripke_t *h_M, mxArray *hmx_Asi, mxArray *hmx_bsi,
		mxArray *hmx_labeli, mxArray *hmx_modei) {
	idpolytope_t *p_polyi = idpolytope_fromMXArray(hmx_Asi,hmx_bsi);
	idlist_t *p_labeli = idlabels_fromMXArray(hmx_labeli);
	int qi = (int)Dbl_fromMXArray(hmx_modei);
	idkripke_append_state(h_M,qi,p_polyi,p_labeli);
}
static void idkripke_fromMXArray_add_mode(idkripke_t *h_M, mxArray *hmx_Qi,
		mxArray *hmx_Ai, mxArray *hmx_Bi, mxArray *hmx_ci,
		mxArray *hmx_Awi, mxArray *hmx_bwi,mxArray *hmx_Aui, mxArray *hmx_bui) {

	idpolytope_t *p_statedom_i = idpolytope_fromMXArray(hmx_Awi,hmx_bwi),
				 *p_inputdom_i = idpolytope_fromMXArray(hmx_Aui,hmx_bui);;
	idlinear_sys_t *h_sys_i = idlinear_sys_fromMXArray(hmx_Ai,hmx_Bi,hmx_ci);
	int qi = (int)Dbl_fromMXArray(hmx_Qi);
	idkripke_append_mode(h_M,qi,h_sys_i,p_statedom_i,p_inputdom_i);
}
static void idkripke_fromMXArray_add_edges(idkripke_t *h_M, mxArray *hmx_delta) {
	if ((h_M->nstates(h_M)!=0)&&((hmx_delta==NULL)||(mxIsEmpty(hmx_delta)))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.delta cannot be empty.");
	}
	if ((h_M->nstates(h_M)!=0)&&((!mxIsDouble(hmx_delta))||(mxGetM(hmx_delta)!=h_M->m_nstates)||(mxGetN(hmx_delta)!=h_M->m_nstates))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.delta must be a double matrix %dx%d.",h_M->nstates,h_M->nstates);
	}

	int i, j, facet, si, sj;
	double *h_dbl = mxGetPr(hmx_delta);
	for(i=0;i<h_M->m_nstates;i++) {
		si = h_M->get_bits_from_state(h_M,i);
		for(j=0;j<h_M->m_nstates;j++) {
			sj = h_M->get_bits_from_state(h_M,j);
			facet = (int)h_dbl[j*h_M->m_nstates+i];
			if (facet>=0) {
				idkripke_insert_edge(h_M,si,sj,facet);
			}
		}
	}
}
static void idkripke_fromMXArray_add_unmarked(idkripke_t *h_M, mxArray *hmx_unmarked) {
	if ((hmx_unmarked!=NULL)&&(!mxIsEmpty(hmx_unmarked))&&((!mxIsDouble(hmx_unmarked))||
			(mxGetN(hmx_unmarked)!=1)||(mxGetM(hmx_unmarked)>=1))) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.unmarked must be a double column vector.");
	}

	if (hmx_unmarked==NULL) {
		return;
	}

	int i, si, nstates = mxGetM(hmx_unmarked);

	for(i=0;i<nstates;i++) {
		si = h_M->get_bits_from_state(h_M,i);
		h_M->mp_unmarked->push(h_M->mp_unmarked,&si);
	}
}
static void idkripke_fromMXArray_add_modes(idkripke_t *h_M,mxArray *h_pm) {
	mxArray *hmx_Q    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"Q")),
			*hmx_Au    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"Au")),
			*hmx_bu    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"bu")),
			*hmx_Aw    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"Aw")),
			*hmx_bw    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"bw")),
			*hmx_A    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"A")),
			*hmx_B    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"B")),
			*hmx_c    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"c"));
	int nsys, i;
	idpolytope_t *p_inputdom;
	idlinear_sys_t *p_sysi;


	if ((hmx_A==NULL)||(mxIsEmpty(hmx_A))) {
		if ((hmx_c!=NULL)&&(!mxIsEmpty(hmx_c))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, c also must be empty.");
		}
		if ((hmx_B!=NULL)&&(!mxIsEmpty(hmx_B))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, B also must be empty.");
		}
		if ((hmx_Au!=NULL)&&(!mxIsEmpty(hmx_Au))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Au also must be empty.");
		}
		if ((hmx_bu!=NULL)&&(!mxIsEmpty(hmx_bu))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, bu also must be empty.");
		}
		if ((hmx_Aw!=NULL)&&(!mxIsEmpty(hmx_Aw))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Aw also must be empty.");
		}
		if ((hmx_bw!=NULL)&&(!mxIsEmpty(hmx_bw))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, bw also must be empty.");
		}
		if ((hmx_Q!=NULL)&&(!mxIsEmpty(hmx_Q))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Q also must be empty.");
		}
		nsys = 0;
	} else {
		if ((hmx_A==NULL)||(!mxIsCell(hmx_A))||(mxGetN(hmx_A)!=1)||((mxGetM(hmx_A)==0))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","A must be a cell column vector with at least 1 row.");
		}
		nsys = mxGetM(hmx_A);
		if ((hmx_Q==NULL)||(!mxIsCell(hmx_Q))||(mxGetN(hmx_Q)!=1)||(mxGetM(hmx_Q)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Q must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_B==NULL)||(!mxIsCell(hmx_B))||(mxGetN(hmx_B)!=1)||(mxGetM(hmx_B)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","B must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_c==NULL)||(!mxIsCell(hmx_c))||(mxGetN(hmx_c)!=1)||(mxGetM(hmx_c)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","c must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_Au==NULL)||(!mxIsCell(hmx_Au))||(mxGetN(hmx_Au)!=1)||(mxGetM(hmx_Au)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Au must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_bu==NULL)||(!mxIsCell(hmx_bu))||(mxGetN(hmx_bu)!=1)||(mxGetM(hmx_bu)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","bu must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_Aw==NULL)||(!mxIsCell(hmx_Aw))||(mxGetN(hmx_Aw)!=1)||(mxGetM(hmx_Aw)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Au must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_bw==NULL)||(!mxIsCell(hmx_bw))||(mxGetN(hmx_bw)!=1)||(mxGetM(hmx_bw)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","bu must be a cell column vector with %d rows.", nsys);
		}
	}
	for(i=0;i<nsys;i++) {
		idkripke_fromMXArray_add_mode(h_M,mxGetCell(hmx_Q,i),mxGetCell(hmx_A,i),mxGetCell(hmx_B,i),mxGetCell(hmx_c,i),
				mxGetCell(hmx_Aw,i),mxGetCell(hmx_bw,i),mxGetCell(hmx_Au,i),mxGetCell(hmx_bu,i));
	}
}
static void idkripke_fromMXArray_add_states(idkripke_t *h_M,mxArray *h_pm) {
	mxArray *hmx_As    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"As")),
			*hmx_bs    		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"bs")),
			*hmx_label 		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"label")),
			*hmx_modes 		= mxGetFieldByNumber(h_pm,(mwIndex)0,mxGetFieldNumber(h_pm,"modes"));
	int nstates, i, nsys, nU;
	idpolytope_t *p_Ui;
	idlinear_sys_t *p_sysi;

	if ((hmx_As==NULL)||(mxIsEmpty(hmx_As))) {
		if ((hmx_bs!=NULL)&&(!mxIsEmpty(hmx_bs))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If M.As is empty; thus, M.bs also must be empty.");
		}
		if ((hmx_label!=NULL)&&(!mxIsEmpty(hmx_label))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If M.As is empty; thus, M.label also must be empty.");
		}
		if ((hmx_modes!=NULL)&&(!mxIsEmpty(hmx_modes))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If M.As is empty; thus, M.modes also must be empty.");
		}
		return;
	}
	if ((hmx_As==NULL)||(!mxIsCell(hmx_As))||(mxGetN(hmx_As)!=1)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.As must be a cell column vector.");
	}
	nstates = mxGetM(hmx_As);
	if ((hmx_bs==NULL)||(!mxIsCell(hmx_bs))||(mxGetN(hmx_bs)!=1)||(mxGetM(hmx_bs)!=nstates)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.bs must be a cell column vector with %d rows.", nstates);
	}
	if ((hmx_label==NULL)||(!mxIsCell(hmx_label))||(mxGetN(hmx_label)!=1)||(mxGetM(hmx_label)!=nstates)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.label must be a cell column vector with %d rows.", nstates);
	}
	if ((hmx_modes==NULL)||(!mxIsCell(hmx_modes))||(mxGetN(hmx_modes)!=1)||(mxGetM(hmx_modes)!=nstates)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.modes must be a cell column vector with %d rows.", nstates);
	}
	for(i=0;i<nstates;i++) {
		idkripke_fromMXArray_add_partition(h_M,mxGetCell(hmx_As,i),mxGetCell(hmx_bs,i),mxGetCell(hmx_label,i),
				mxGetCell(hmx_modes,i));
	}
}
int idkripke_q0_fromMXArray(idmatrix_t *hmx_q0) {
	if ((hmx_q0==NULL)||(!mxIsDouble(hmx_q0))||(mxGetM(hmx_q0)!=1)||(mxGetN(hmx_q0)!=1)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","M.q0 must be a scalar.");
	}
	double *h_val = mxGetPr(hmx_q0);
	return (int)*h_val;
}
idkripke_t *idkripke_fromMXArray(iddblmatrix_t *p_x0, int q0, idbool_t is_fair,
		mxArray *hmx_As, mxArray *hmx_bs, mxArray *hmx_label, mxArray *hmx_Q,
		mxArray *hmx_A, mxArray *hmx_B, mxArray *hmx_c, mxArray *hmx_Aw, mxArray *hmx_bw,
		mxArray *hmx_Au, mxArray *hmx_bu) {

	idkripke_t 	*p_M;

	int nstates = 0, i, nsys = 0, qi;
	idpolytope_t *p_Ui, *p_Wi;
	idlinear_sys_t *p_sysi;

	if ((hmx_As==NULL)||(mxIsEmpty(hmx_As))) {
		if ((hmx_bs!=NULL)&&(!mxIsEmpty(hmx_bs))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If As is empty; thus, bs also must be empty.");
		}
		if ((hmx_label!=NULL)&&(!mxIsEmpty(hmx_label))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If As is empty; thus, label also must be empty.");
		}
	} else {
		if ((hmx_As==NULL)||(!mxIsCell(hmx_As))||(mxGetN(hmx_As)!=1)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","As must be a cell column vector.");
		}
		nstates = mxGetM(hmx_As);
		if ((hmx_bs==NULL)||(!mxIsCell(hmx_bs))||(mxGetN(hmx_bs)!=1)||(mxGetM(hmx_bs)!=nstates)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","bs must be a cell column vector with %d rows.", nstates);
		}
		if ((hmx_label==NULL)||(!mxIsCell(hmx_label))||(mxGetN(hmx_label)!=1)||(mxGetM(hmx_label)!=nstates)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","label must be a cell column vector with %d rows.", nstates);
		}
	}

	if ((hmx_A==NULL)||(mxIsEmpty(hmx_A))) {
		if ((hmx_c!=NULL)&&(!mxIsEmpty(hmx_c))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, c also must be empty.");
		}
		if ((hmx_B!=NULL)&&(!mxIsEmpty(hmx_B))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, B also must be empty.");
		}
		if ((hmx_Aw!=NULL)&&(!mxIsEmpty(hmx_Aw))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Aw also must be empty.");
		}
		if ((hmx_bw!=NULL)&&(!mxIsEmpty(hmx_bw))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, bw also must be empty.");
		}
		if ((hmx_Au!=NULL)&&(!mxIsEmpty(hmx_Au))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Au also must be empty.");
		}
		if ((hmx_bu!=NULL)&&(!mxIsEmpty(hmx_bu))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, bu also must be empty.");
		}
		if ((hmx_Q!=NULL)&&(!mxIsEmpty(hmx_Q))) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","If A is empty; thus, Q also must be empty.");
		}
		nsys = 0;
	} else {
		if ((hmx_A==NULL)||(!mxIsCell(hmx_A))||(mxGetN(hmx_A)!=1)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","A must be a cell column vector.");
		}
		nsys = mxGetM(hmx_A);
		if ((hmx_B==NULL)||(!mxIsCell(hmx_B))||(mxGetN(hmx_B)!=1)||(mxGetM(hmx_B)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","B must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_c==NULL)||(!mxIsCell(hmx_c))||(mxGetN(hmx_c)!=1)||(mxGetM(hmx_c)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","c must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_Aw==NULL)||(!mxIsCell(hmx_Aw))||(mxGetN(hmx_Aw)!=1)||(mxGetM(hmx_Aw)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Aw must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_bu==NULL)||(!mxIsCell(hmx_bw))||(mxGetN(hmx_bw)!=1)||(mxGetM(hmx_bw)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","bw must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_Au==NULL)||(!mxIsCell(hmx_Au))||(mxGetN(hmx_Au)!=1)||(mxGetM(hmx_Au)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Au must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_bu==NULL)||(!mxIsCell(hmx_bu))||(mxGetN(hmx_bu)!=1)||(mxGetM(hmx_bu)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","bu must be a cell column vector with %d rows.", nsys);
		}
		if ((hmx_Q==NULL)||(!mxIsCell(hmx_Q))||(mxGetN(hmx_Q)!=1)||(mxGetM(hmx_Q)!=nsys)) {
			mexErrMsgIdAndTxt("mxinterface:invalidArgument","Q must be a cell column vector with %d rows.", nsys);
		}
	}

	if (nsys==1) {
		p_sysi = idlinear_sys_fromMXArray(mxGetCell(hmx_A,0),mxGetCell(hmx_B,0),mxGetCell(hmx_c,0));
		p_Ui = idpolytope_fromMXArray(mxGetCell(hmx_Au,0),mxGetCell(hmx_bu,0));
		p_Wi = idpolytope_fromMXArray(mxGetCell(hmx_Aw,0),mxGetCell(hmx_bw,0));
		p_M 		= idkripke_single_mode(p_x0,is_fair,p_Wi,p_sysi,p_Ui);
	} else {
		p_M 		= idkripke(p_x0,q0,is_fair);
		for(i=0;i<nsys;i++) {
			qi = int_fromMXArray_with_name(mxGetCell(hmx_Q,i),"Q");
			p_sysi = idlinear_sys_fromMXArray(mxGetCell(hmx_A,i),mxGetCell(hmx_B,i),mxGetCell(hmx_c,i));
			p_Wi = idpolytope_fromMXArray(mxGetCell(hmx_Aw,i),mxGetCell(hmx_bw,i));
			p_Ui = idpolytope_fromMXArray(mxGetCell(hmx_Au,i),mxGetCell(hmx_bu,i));
			p_M->add_mode(p_M,qi,p_sysi,p_Wi,p_Ui);
		}
	}
	for(i=0;i<nstates;i++) {
		p_M->add_polytope(p_M,
				idpolytope_fromMXArray(mxGetCell(hmx_As,i),mxGetCell(hmx_bs,i)),
				int_fromMXArray_with_name(mxGetCell(hmx_label,i),"label"));
	}

	return p_M;
}
idkripke_t *idkripke_fromMXArray_structure(mxArray *hmx_pm) {
	assert(hmx_pm!=NULL);

	iddblmatrix_t 	*p_x0;
	idkripke_t 	*p_M;
	idbool_t	is_fair;
	int dim, s0, q0, s0_idx;
	mxArray *hmx_s0;

	p_x0 		= idkripke_fromMXArray_get_x0(hmx_pm);
	dim 		= p_x0->rows(p_x0);
	q0 			= idkripke_fromMXArray_get_q0(hmx_pm);
	s0			= idkripke_fromMXArray_get_s0(hmx_pm);
	is_fair		= idkripke_fromMXArray_is_fair(hmx_pm);
	p_M 		= idkripke_empty(p_x0,q0,is_fair);

	idkripke_fromMXArray_add_modes(p_M,hmx_pm);
	idkripke_fromMXArray_add_states(p_M,hmx_pm);
	idkripke_fromMXArray_add_edges(p_M,mxGetFieldByNumber(hmx_pm,(mwIndex)0,mxGetFieldNumber(hmx_pm,"delta")));
	idkripke_fromMXArray_add_unmarked(p_M,mxGetFieldByNumber(hmx_pm,(mwIndex)0,mxGetFieldNumber(hmx_pm,"unmarked")));
	s0_idx = p_M->get_bits_from_state(p_M,s0);
	if ((p_M->m_initial>=0)&&(s0>=0)&&(s0_idx>=0)&&(s0_idx!=p_M->m_initial)) {
		mexErrMsgIdAndTxt("mxinterface:invalidArgument","Invalid initial state.");
	}

	return p_M;
}

/*  the gateway routine.  */
mxArray *MXArray_constantVector(size_t rows, double c)
{
  size_t i;
  mxArray *retVal;
  double *ptr;
  retVal = mxCreateNumericMatrix(rows, 1, mxDOUBLE_CLASS, mxREAL);
  ptr = mxGetPr(retVal);
  for (i = 0; i < rows; i++)
    ptr[i] = c;
  return retVal;
}

idmatrix_t *GMPmat_fromMXArray (const mxArray *pm) {
	idmatrix_t *A = id_zeros(mxGetM(pm),mxGetN(pm));
	size_t         i, j;
	double *ptr = mxGetPr(pm);

    for (i=0; i!=A->m; ++i){
    	for (j=0; j!=A->n; ++j){
    		A->setat(A,i,j,ptr[i + j*A->m]);
        }
    }

     return A;
}

void SparseVector_fromMXArray (const mxArray *pm, int *numnz, int **ind, double **val) {
	assert(mxGetM(pm)>0);
	assert(mxGetN(pm)==1);

	int i,dim = mxGetM(pm);
	double *ptr = mxGetPr(pm);

	(*numnz) = 0;
	(*ind) = malloc(dim*sizeof(int));
	(*val) = malloc(dim*sizeof(double));

	for(i=0;i<dim;i++) {
		if (abs(ptr[i]) > ID_ABSTOL) {
			(*ind)[(*numnz)] = i;
			(*val)[(*numnz)] = ptr[i];
			(*numnz)++;
		}
	}

	(*ind) = realloc((*ind),(*numnz)*sizeof(int));
	(*val) = realloc((*val),(*numnz)*sizeof(double));
}

idsparse_t *idsparse_fromMXArray (const mxArray *pm) {
	assert(mxGetM(pm)>0);
	assert(mxGetN(pm)==1);

	int i,nvars = mxGetM(pm), numnz = 0;
	double *ptr = mxGetPr(pm);

	idsparse_t *p_x = idsparse_byvalue(nvars,nvars,sizeof(double));

	for(i=0;i<nvars;i++) {
		if (abs(ptr[i]) > ID_ABSTOL) {
			p_x->set_at(p_x,numnz,i,&ptr[i]);
			numnz++;
		}
	}
	p_x->upd_numnz(p_x,numnz);

	return p_x;
}

double Dbl_fromMXArray (const mxArray *pm) {
	assert(mxIsScalar(pm));

	double *ptr = mxGetPr(pm);

	return ptr[0];
}



mxArray *MXArray_fromGMPmat(const idmatrix_t *A)
{
    mxArray *retVal;
    size_t i, j;
    double *ptr;
    retVal = mxCreateNumericMatrix(A->m, A->n, mxDOUBLE_CLASS, mxREAL);
    ptr = mxGetPr(retVal);
    for (i = 0; i != A->m; ++i)
    {
        for (j = 0; j != A->n; ++j)
        {
            ptr[i + j*A->m] = A->getat(A,i,j);//mpq_get_d(A->data[i*(A->n) + j]);
        }
    }
    return retVal;
}

mxArray *MXArray_from_int(int i) {
	mxArray *pmx = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
	double *ptr = mxGetPr(pmx);
	ptr[0] = (double)i;
	return pmx;
}

mxArray *MXArray_from_idintlist(idlist_t *h_list) {
	int nvalues = h_list->length(h_list);
	mxArray *pmx = mxCreateNumericMatrix(1, nvalues, mxDOUBLE_CLASS, mxREAL);
	if (nvalues>0) {
		double *ptr = mxGetPr(pmx);

		idrb_node_t  *it;
		int i, value;

		i = 0;
		idrb_traverse(it,h_list->mp_head) {
			value = *(int *)it->value(it);
			ptr[i] = (double)value;
			i++;
		}
		assert(i==nvalues);
	}

	return pmx;
}

mxArray *MXArray_from_idintlist_with_map(idlist_t *h_list, idmap_t *h_key_idx) {
	int nvalues = h_list->length(h_list);
	mxArray *pmx = mxCreateNumericMatrix(nvalues, 1, mxDOUBLE_CLASS, mxREAL);
	if (nvalues>0) {
		double *ptr = mxGetPr(pmx);

		idrb_node_t  *it;
		int i, idx, value;

		idrb_traverse(it,h_list->mp_head) {
			i = *(int *)it->value(it);
			idx = idkripke_get_idx(h_key_idx,i);
			if (idx>=0) {
				value = *(int *)it->value(it);
				ptr[idx] = (double)value;
			}
		}
	}

	return pmx;
}
mxArray *MXArray_from_idinttable_with_map(idtable_t *h_table, idmap_t *h_key_idx) {
	int nvalues = h_key_idx->m_nvalues1;
	mxArray *pmx = mxCreateNumericMatrix(nvalues, nvalues, mxDOUBLE_CLASS, mxREAL);
	double *ptr = mxGetPr(pmx);

	idrb_node_t  *iti, *itj;
	idarray_t *h_row;
	int i, j, idxi, idxj, value;

	for(i=0;i<nvalues;i++) {
		for(j=0;j<nvalues;j++) {
			ptr[i+j*nvalues] = -1.0;
		}
	}

	idrb_traverse(iti,h_table->mp_rows->mp_head) {
		i = iti->keyi(iti);
		idxi = idkripke_get_idx(h_key_idx,i);
		if (idxi>=0) {
			h_row = (idarray_t *)iti->value(iti);
			idrb_traverse(itj,h_row->mp_head) {
				j = itj->keyi(itj);
				idxj = idkripke_get_idx(h_key_idx,j);
				if (idxj>=0) {
					value = *(int *)itj->value(itj);
					ptr[idxi+idxj*nvalues] = (double)value;
				}
			}
		}
	}
	return pmx;
}


mxArray *MXArray_from_idkripke(idkripke_t *h_M) {
	assert(h_M!=NULL);

    mxArray *pmx;
    idrb_node_t *it;

    // kripke fields
    const int nfields = 18;
    const char *fnames[nfields];
    fnames[0] = "x0";
    fnames[1] = "q0";
    fnames[2] = "is_fair";
    fnames[3] = "s0";
    fnames[4] = "As";
    fnames[5] = "bs";
    fnames[6] = "label";
    fnames[7] = "modes";
    fnames[8] = "unmarked";
    fnames[9] = "delta";
    fnames[10] = "Q";
    fnames[11] = "Aw";
    fnames[12] = "bw";
    fnames[13] = "A";
    fnames[14] = "B";
    fnames[15] = "c";
    fnames[16] = "Au";
    fnames[17] = "bu";

    pmx = mxCreateStructMatrix(1, 1, nfields, fnames);

    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"x0"),
    		MXArray_from_iddblmatrix(h_M->mp_x0));
    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"q0"),
    		MXArray_from_int(h_M->m_q0));
    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"s0"),
    		MXArray_from_int(h_M->get_bits_from_state(h_M,h_M->m_initial)));
    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"is_fair"),
    		MXArray_from_int(h_M->m_isfair));
    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"unmarked"),
    		MXArray_from_idintlist_with_map(h_M->mp_unmarked,h_M->get_state_bit_map(h_M)));
    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"delta"),
    		MXArray_from_idinttable_with_map(h_M->mp_edges,h_M->get_state_bit_map(h_M)));

    if (h_M->m_nstates>0) {
    	mxArray *pmx_As, *pmx_bs, *pmx_label, *pmx_modes;
    	idpolytope_t *h_poly_i;
    	idlist_t *h_label_i, *h_modes_i;
    	int si, si_idx;
    	pmx_As 		= mxCreateCellMatrix((mwSize)h_M->m_nstates,(mwSize)1);
    	pmx_bs 		= mxCreateCellMatrix((mwSize)h_M->m_nstates,(mwSize)1);
    	pmx_label 	= mxCreateCellMatrix((mwSize)h_M->m_nstates,(mwSize)1);
    	pmx_modes 	= mxCreateCellMatrix((mwSize)h_M->m_nstates,(mwSize)1);

    	idrb_traverse(it,h_M->mp_states->mp_head) {
    		si = it->keyi(it);
    		si_idx = h_M->get_bits_from_state(h_M,si);
    		h_poly_i = (idpolytope_t *)it->value(it);
    		h_label_i = h_M->mp_state_label_map->get_values2_at(h_M->mp_state_label_map,&si);
    		h_modes_i = h_M->mp_state_mode_map->get_values2_at(h_M->mp_state_mode_map,&si);

    	    mxSetCell(pmx_As	,(mwIndex)si_idx,MXArray_fromGMPmat(h_poly_i->mp_A));
    	    mxSetCell(pmx_bs	,(mwIndex)si_idx,MXArray_fromGMPmat(h_poly_i->mp_b));
    	    mxSetCell(pmx_label	,(mwIndex)si_idx,MXArray_from_idintlist(h_label_i));
    	    mxSetCell(pmx_modes	,(mwIndex)si_idx,MXArray_from_idintlist(h_modes_i));

    	}


        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"As"),pmx_As);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"bs"),pmx_bs);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"label"),pmx_label);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"modes"),pmx_modes);
    }

    if (h_M->m_nmodes>0) {
    	mxArray *pmx_A, *pmx_B, *pmx_c, *pmx_Au, *pmx_bu, *pmx_Aw, *pmx_bw, *pmx_Q;
    	int i;
    	double qi;
    	idlinear_sys_t *h_sys_i;
    	idpolytope_t *h_Ui, *h_Wi;
    	pmx_Q = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_A = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_B = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_c = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_Au = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_bu = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_Aw = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);
    	pmx_bw = mxCreateCellMatrix((mwSize)h_M->m_nmodes,(mwSize)1);

    	i = 0;
    	idrb_traverse(it,h_M->mp_mode_sys->mp_head) {
    		qi = (double)it->keyi(it);
    		h_sys_i = (idlinear_sys_t *)it->value(it);
    		h_Ui = (idpolytope_t *)h_M->mp_mode_inputdom->get_at(h_M->mp_mode_inputdom,qi);
    		h_Wi = (idpolytope_t *)h_M->mp_mode_statedom->get_at(h_M->mp_mode_statedom,qi);
			mxSetCell(pmx_Q,(mwIndex)i,MXArray_fromDbl(&qi,1));
			mxSetCell(pmx_A,(mwIndex)i,MXArray_from_iddblmatrix(h_sys_i->A(h_sys_i)));
			mxSetCell(pmx_B,(mwIndex)i,MXArray_from_iddblmatrix(h_sys_i->B(h_sys_i)));
			mxSetCell(pmx_c,(mwIndex)i,MXArray_from_iddblmatrix(h_sys_i->c(h_sys_i)));
			mxSetCell(pmx_Au,(mwIndex)i,MXArray_fromGMPmat(h_Ui->mp_A));
			mxSetCell(pmx_bu,(mwIndex)i,MXArray_fromGMPmat(h_Ui->mp_b));
			mxSetCell(pmx_Aw,(mwIndex)i,MXArray_fromGMPmat(h_Wi->mp_A));
			mxSetCell(pmx_bw,(mwIndex)i,MXArray_fromGMPmat(h_Wi->mp_b));
			i++;
    	}
    	assert(i==h_M->m_nmodes);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"Q"),pmx_Q);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"A"),pmx_A);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"B"),pmx_B);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"c"),pmx_c);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"Au"),pmx_Au);
        mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"bu"),pmx_bu);
	    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"Aw"),pmx_Aw);
	    mxSetFieldByNumber(pmx,(mwIndex)0,mxGetFieldNumber(pmx,"bw"),pmx_bw);
    }

    return pmx;
}

mxArray *MXArray_empty() {
	return mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
}

mxArray *MXArray_fromDbl(double *value, int len) {
    mxArray *retVal;
    double *ptr;
    retVal = mxCreateNumericMatrix(len, 1, mxDOUBLE_CLASS, mxREAL);

    ptr = mxGetPr(retVal);
    for (int i=0; i<len;i++) {
		ptr[i] = value[i];
    }
    return retVal;
}

mxArray *MXArray_fromInt(int *value, int len) {
    mxArray *retVal;
    int *ptr;
    retVal = mxCreateNumericMatrix(len, 1, mxINT32_CLASS, mxREAL);

    ptr = (int *)mxGetPr(retVal);
    for (int i=0; i<len;i++) {
		ptr[i] = value[i];
    }
    return retVal;
}

size_t MXArray_to_integer(const mxArray *pm)
{
  if ( !mxIsUint32( pm ) || !mxIsScalar( pm ) )
    mexErrMsgIdAndTxt("MATLAB:myErrors:scalarInt","Input has to be scalar unsigned integer 32.");
  size_t retVal;
  unsigned int *temp;
  temp =  (unsigned int *)mxGetData( pm );
  retVal = (size_t) *temp;
  return retVal;
}

mxArray *VertConcat(const mxArray *A, const mxArray *b)
{
  size_t mA, mB, nA, nB;
  mA = mxGetM(A);
  mB = mxGetM(b);
  mxAssert(mA == mB, "A and b must have the same number of rows!");
  nA = mxGetN(A);
  nB = mxGetN(b);

  mxArray *retVal;
  retVal = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);

  double *data, *dA, *dB;
  dA = mxGetPr(A);
  dB = mxGetPr(b);
  data = mxMalloc( mA*(nA+nB)*sizeof(*data) );
  assert ( data != NULL );
  memcpy(data, dA, mA*nA*sizeof(double) );
  memcpy(data + mA*nA, dB, mB*nB*sizeof(double) );

  mxSetPr(retVal, data);
  mxSetM(retVal,mA);
  mxSetN(retVal,nA+nB);
  return retVal;
}

mxArray *VertBreakdown(const mxArray *res)
{
  size_t m, n;
  m = mxGetM(res);
  n = mxGetN(res) - 1;

  mxArray *A, *b;
  A = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
  b = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);

  double *A_data, *b_data, *res_data;
  res_data = mxGetPr(res);

  A_data = mxMalloc( m*n*sizeof(*A_data) );
  b_data = mxMalloc( m*sizeof(*b_data) );

  memcpy(b_data, res_data, m*sizeof(*res_data) );
  memcpy(A_data, res_data + m, m*n*sizeof(*res_data) );

  mxSetPr(A, A_data);
  mxSetM(A, m);
  mxSetN(A, n);

  mxSetPr(b, b_data);
  mxSetM(b, m);
  mxSetN(b, 1);


  mxArray *retVal;
  mwSize ndim=2, dims[]={1, 2}, nsubs=2, subs[2];
  mwIndex index;
  retVal = mxCreateCellArray( ndim, dims );

  subs[0] = 0;
  subs[1] = 0;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , A);

  subs[1] = 1;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , b);

  return retVal;

}

mxArray *VertBreakdownWithVolume(const mxArray *res, double Volume)
{
  size_t m, n;
  m = mxGetM(res);
  n = mxGetN(res) - 1;

  mxArray *A, *b, *vol;
  A = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
  b = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
  vol = mxCreateNumericMatrix(1,1, mxDOUBLE_CLASS, mxREAL);

  double *A_data, *b_data, *vol_data, *res_data;
  res_data = mxGetPr(res);

  A_data = mxMalloc( m*n*sizeof(*A_data) );
  b_data = mxMalloc( m*sizeof(*b_data) );
  vol_data = mxMalloc( sizeof(*vol_data) );

  memcpy(b_data, res_data, m*sizeof(*res_data) );
  memcpy(A_data, res_data + m, m*n*sizeof(*res_data) );
  *vol_data = Volume;

  mxSetPr(A, A_data);
  mxSetM(A, m);
  mxSetN(A, n);

  mxSetPr(b, b_data);
  mxSetM(b, m);
  mxSetN(b, 1);

  mxSetPr(vol, vol_data);

  mxArray *retVal;
  mwSize ndim=2, dims[]={1, 3}, nsubs=2, subs[2];
  mwIndex index;
  retVal = mxCreateCellArray( ndim, dims );

  subs[0] = 0;
  subs[1] = 0;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , A);

  subs[1] = 1;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , b);

  subs[1] = 2;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , vol);

  return retVal;

}



mxArray *createEmptyCell( )
{
  mxArray *A, *b;
  A = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);
  b = mxCreateNumericMatrix(0, 0, mxDOUBLE_CLASS, mxREAL);

  mxArray *retVal;
  mwSize ndim=2, dims[]={1, 2}, nsubs=2, subs[2];
  mwIndex index;
  retVal = mxCreateCellArray( ndim, dims );

  subs[0] = 0;
  subs[1] = 0;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , A);

  subs[1] = 1;
  index = mxCalcSingleSubscript(retVal, nsubs, subs);
  mxSetCell (retVal, index , b);

  return retVal;

}

mxArray *MXArray_stack(mxArray *A,mxArray *B)
{
  assert(A != NULL && B != NULL );
  assert( mxGetN(A) == mxGetN(B) );

  size_t i, mA, mB, n;
  n = mxGetN(A);
  mA = mxGetM(A);
  mB = mxGetM(B);

  mxArray *retVal;
  retVal = mxCreateNumericMatrix(mA + mB , n, mxDOUBLE_CLASS, mxREAL);
  double *ptr, *pA, *pB;
  ptr = mxGetPr(retVal);
  pA = mxGetPr(A);
  pB = mxGetPr(B);

  for (i = 0; i < n; i++)
  {
    memcpy(ptr +i*(mA+mB), pA + i*mA, mA*sizeof(*ptr) );
    memcpy(ptr + i*(mA+mB)+ mA, pB + i*mB, mB*sizeof(*ptr) );
  }
  mxDestroyArray(B);
  mxDestroyArray(A);
  return retVal;
}

mxArray *MXArray_fromGMPlist(struct GMPlist *A, size_t n)
{
    mxArray *retVal;
    double *ptr;
    retVal = mxCreateNumericMatrix(A->lte, n, mxDOUBLE_CLASS, mxREAL);
    ptr = GMPlist_toDouble(A, n);
    mxSetPr(retVal,ptr);

    return retVal;
}

