/*
 * mxinterface.h
 *
 *  Created on: Apr 11, 2019
 *      Author: rafael
 */

#ifndef MATLAB__IDSTL_MXINTERFACE_H_
#define MATLAB__IDSTL_MXINTERFACE_H_

#include "idrtl.h"
#include "mex.h"
#include "string.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#define MAXCHARS 80   /* max length of string contained in each field */

/* Interface between mxArray and GMPmat structures */
mxArray *MXArray_constantVector(size_t rows, double c);
idmatrix_t *GMPmat_fromMXArray (const mxArray *pm);
mxArray *MXArray_fromGMPmat(const idmatrix_t *A);
size_t MXArray_to_integer(const mxArray *pm);
mxArray *VertConcat(const mxArray *A, const mxArray *b);
mxArray *VertBreakdown(const mxArray *res);
mxArray *VertBreakdownWithVolume(const mxArray *res, double Volume);
mxArray *createEmptyCell( );
mxArray *MXArray_stack(mxArray *A, mxArray *B);
mxArray *MXArray_fromGMPlist(struct GMPlist *A, size_t n);
mxArray *MXArray_from_int(int i);
mxArray *MXArray_from_idintlist(idlist_t *h_list);
mxArray *MXArray_from_idintlist_with_map(idlist_t *h_list, idmap_t *h_key_idx);
mxArray *MXArray_from_idinttable_with_map(idtable_t *h_table, idmap_t *h_key_idx);
mxArray *MXArray_empty();

idformula_t *idformula_fromMXArray(mxArray *hmx_phi, idkripke_t *h_M);
idlinear_sys_t *idlinear_sys_fromMXArray(mxArray *hmx_A, mxArray *hmx_B, mxArray *hmx_c);
idpolytope_t *idpolytope_fromMXArray(mxArray *hmx_A, mxArray *hmx_b);
iddblmatrix_t *iddblmatrix_fromMXArray(mxArray *hmx_A);
idkripke_t *idkripke_fromMXArray_structure(mxArray *hmx_pm);
idkripke_t *idkripke_fromMXArray(iddblmatrix_t *p_x0, int q0, idbool_t is_fair,
		mxArray *hmx_As, mxArray *hmx_bs, mxArray *hmx_label, mxArray *hmx_modes,
		mxArray *hmx_A, mxArray *hmx_B, mxArray *hmx_c, mxArray *hmx_Aw, mxArray *hmx_bw,
		mxArray *hmx_Au, mxArray *hmx_bu);
mxArray *MXArray_fromDbl(double *value, int len);
mxArray *MXArray_fromInt(int *value, int len);
void SparseVector_fromMXArray (const mxArray *pm, int *numnz, int **ind, double **val);
double Dbl_fromMXArray (const mxArray *pm);
mxArray *MXArray_from_idkripke(idkripke_t *h_M);
mxArray *MXArray_from_iddblmatrix(iddblmatrix_t *h_A);
mxArray *MXArray_from_dblvector(double *h_vec, int rows, int cols);
mxArray *MXArray_from_idintarray(idarray_t *h_array, int cols);
mxArray *MXArray_from_drun(idarray_t *h_states, idarray_t *h_nsamples, idkripke_t *h_M);
int idkripke_q0_fromMXArray(idmatrix_t *hmx_q0);
idsparse_t *idsparse_fromMXArray (const mxArray *pm);
int int_fromMXArray(const mxArray *pm);
idlist_t *idlabels_fromMXArray(mxArray *hmx_label);
int idlabel_fromMXArray(mxArray *hmx_label);
int idmode_fromMXArray(mxArray *hmx_mode);
idlist_t *idintlist_fromMXArray(mxArray *hmx_list);

#endif /* MATLAB__IDSTL_MXINTERFACE_H_ */
