function pi = assert_box(x,x_lb,x_ub)
%ASSERT_BOX Summary of this function goes here
%   Detailed explanation goes here
    n = length(x_lb);
    pi = x_lb(1) <= x(1) & x(1) <= x_ub(1);
    for i = 2:n
        pi = pi & x_lb(i) <= x(i) & x(i) <= x_ub(i);
    end
end

