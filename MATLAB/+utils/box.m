function [A,b] = box(lb,ub)
%BOX Summary of this function goes here
%   Detailed explanation goes here

    lb=lb(:);
    ub=ub(:);
    dim = length(lb);
    A = [-eye(dim);eye(dim)];
    b = [-lb;ub];
end

