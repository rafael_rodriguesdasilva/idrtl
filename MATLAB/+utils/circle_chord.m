function c = circle_chord(r,alpi,alpf)
%CIRCLE_CHORD Summary of this function goes here
%   Detailed explanation goes here

    alpi = wrapTo2Pi(alpi);
    alpf = wrapTo2Pi(alpf);
    if (alpi<alpf)
        alp = alpf-alpi;
    else
        alp = alpi-alpf;
    end
    c = 2*r*sin(alp/2);
end

