function h = circle_chord_height(r,alpi,alpf)
%CLIRCLE_CHORD_HEIGHT Summary of this function goes here
%   Detailed explanation goes here
    c = utils.circle_chord(r,alpi,alpf);
    d = sqrt(r^2-(c/2)^2);
    h = r-d;
end

