function [A,B] = intdiffeq(dim,nint,ts)
%INTDIFFEQ Summary of this function goes here
%   Detailed explanation goes here

    if isscalar(nint)
        A = eye(nint*dim);
        B = zeros(nint*dim,dim);

        for i=1:dim 
            for j=1:nint
                B((j-1)*nint+i,i) = ts^(nint-j+1)/(nint-j+1);
                for k=1:nint-j
                    A((j-1)*nint+i,(k+j-1)*dim+i) = ts^k/k;
                end
            end
        end
    elseif isvector(nint)
        n = sum(nint);
        m = length(nint);
        A = eye(n);
        B = zeros(n,m);
        
        dim = zeros(m,1);
        
        for i=1:m
            dim(i) = sum(nint>=i);
        end

        for i=1:m
            for j=1:nint(i)
                B(sum(dim(1:j-1))+i,i) = ts^(nint(j)-j+1)/(nint(j)-j+1);
                for k=1:nint(i)-j
                    A(sum(dim(1:j-1))+i,sum(dim(1:j-1+k))+i) = ts^k/k;
                end
            end
        end       
    end
end

