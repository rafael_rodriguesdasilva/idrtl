function [a,b,c] = line_between_two_pts(p1,p2)
%LINE_BETWEEN_TWO_PTS Summary of this function goes here
%   Detailed explanation goes here
    if (abs(p1(1)-p2(1))>0.01)
        a = -(p1(2)-p2(2))/(p1(1)-p2(1));
        b = 1;
    elseif (abs(p1(2)-p2(2))>0.01)
        a = 1;
        b = -(p1(1)-p2(1))/(p1(2)-p2(2));
    else
        error('small radius');
    end
    c = a*p1(1)+b*p1(2);

end

