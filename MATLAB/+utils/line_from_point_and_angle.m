function [a,b,c] = line_from_point_and_angle(p,alp)
%LINE_FROM_POINT_AND_ANGLE Summary of this function goes here
%   Detailed explanation goes here
    alp = wrapToPi(alp);
    if (pi/4 <= alp && alp <= 3*pi/4) || (-3*pi/2 <= alp && alp <= -pi/4)
        a = 1;
        b = tan(alp+pi/2);
    else
        a = -tan(alp);
        b = 1;
   end
    c = a*p(1)+b*p(2);
end

