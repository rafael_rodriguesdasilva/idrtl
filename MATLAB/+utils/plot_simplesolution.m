function [outputArg1,outputArg2] = plot_simplesolution(M,Mplan,x)
%PLOT_SIMPLESOLUTION 

    idRTL.kripke.plot_partition(M,'nostates',true);
    hold on;
    idRTL.kripke.plot_path(Mplan,'nolabels',true,'nostates',true);
    hold on;
    plot(x(1,1),x(1,2),'p','MarkerFaceColor','b','markeredgecolor','b',...
                                                    'MarkerSize',12);
    if size(x,1)>1
        plot(x(:,1),x(:,2),'*-','linewidth',3,'color','b');
    end
    
end

