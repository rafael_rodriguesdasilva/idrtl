function [A,b] = poly_containing_arc(pc,r,alpi,alpf)
%POLY_CONTAINING_ARC Summary of this function goes here
%   Detailed explanation goes here
    alpm = (alpi+alpf)/2;
    p1 = pc(:) + [r*cos(alpi);
              r*sin(alpi)];
    p2 = pc(:) + [r*cos(alpf);
              r*sin(alpf)];
    p3 = pc(:) + [r*cos(alpm);
              r*sin(alpm)];
    [a1,b1,c1] = utils.tan_to_the_circle_at(pc,r,alpm); 
    [a2,b2,c2] = utils.line_between_two_pts(p1,p2);
    [a3,b3,c3] = utils.line_from_point_and_angle(p1,alpi);
    [a4,b4,c4] = utils.line_from_point_and_angle(p2,alpf);
    
    A = [a1,b1;
         a2,b2;
         a3,b3;
         a4,b4];
    b = [c1;c2;c3;c4];
    
    row2flip = any([A*p1>b+eps,A*p2>b+eps,A*p3>b+eps],2);
    A(row2flip,:) = -A(row2flip,:);
    b(row2flip) = -b(row2flip,:);
    row2flip = any([A*p1>b+eps,A*p2>b+eps,A*p3>b+eps],2);
    if any(row2flip)
        error('logic error');
    end    
end

