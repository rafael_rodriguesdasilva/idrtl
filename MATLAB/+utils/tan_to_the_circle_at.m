function [a,b,c] = tan_to_the_circle_at(pc,r,alp)
%TAN_TO_THE_CIRCLE_AT Summary of this function goes here
% equation from https://www.siyavula.com/read/maths/grade-12/analytical-geometry/07-analytical-geometry-03
    pt = pc + [r*cos(alp);
               r*sin(alp)];
    if (abs(pt(2)-pc(2))>0.01)
        a = (pt(1)-pc(1))/(pt(2)-pc(2));
        b = 1;
    elseif (abs(pt(1)-pc(1))>0.01)
        a = 1;
        b = (pt(2)-pc(2))/(pt(1)-pc(1));
    else
        error('small radius');
    end
    c = a*pt(1)+b*pt(2);
end

