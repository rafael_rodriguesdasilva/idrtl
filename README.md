# idRTL

## Requirements

* z3 (https://github.com/Z3Prover/z3)
* Gurobi (https://www.gurobi.com)
* GMP (https://gmplib.org/) (Ubuntu: sudo apt-get install libgmp3-dev)
* MATLAB

## Development Environment

* Ubuntu 16.04 LTS
* gcc-6
* MATLAB 2018a


## How to install the idRTL solver

```
.\configure
make
sudo make install
```

### Tips

* If z3 or Gurobi libraries are in a custom folder, you should define these folders in environment variables. A suggestion is to do as follows:
```
 env CFLAGS='-I/opt/gurobi800/linux64/include/' LDFLAGS='-L/opt/gurobi800/linux64/lib/' ./configure
```

## How to install the idRTL MATLAB toolbox
> Note that this toolbox requires the solver.

* Copy the folder MATLAB in your prefered location.
* If necessary, change/add include folders in the script install_idRTL. 
* In MATLAB, 
```
install_idRTL
```

=======


