/*
 * ex_polytope.c
 *
 *  Created on: Jul 28, 2019
 *      Author: rafael
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

idmatrix_t *mk_Abox(int dim) {
	idmatrix_t *p_Abox = id_zeros(2*dim,dim);
	int i;

	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i,i,-1.0);
	}
	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i+dim,i,1.0);
	}

	return p_Abox;
}
idmatrix_t *mk_bbox(double *h_lb, double *h_ub, int dim) {
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	for(i=0;i<dim;i++) {
		p_bbox->setat(p_bbox,i,0,-h_lb[i]);
	}
	for(i=0;i<dim;i++) {
		p_bbox->setat(p_bbox,i+dim,0,h_ub[i]);
	}

	return p_bbox;
}
iddblmatrix_t *mk_colvector(double *h_val, int dim) {
	iddblmatrix_t *p_vec = iddblmatrix(dim,1);
	int i;

	for(i=0;i<dim;i++) {
		p_vec->set_at(p_vec,i,0,h_val[i]);
	}

	return p_vec;
}
idlinear_sys_t *mk_integrator(int dim, double ts) {
	iddblmatrix_t *p_A = iddblmatrix(dim,dim);
	iddblmatrix_t *p_B = iddblmatrix(dim,1);
	iddblmatrix_t *p_c = iddblmatrix(dim,1);
	idlinear_sys_t *p_sys;

	// double integrator
	for(int i=0;i<dim;i++) {
		double a = 1.0;
		p_A->set_at(p_A,i,i,a);
		for(int j=i+1;j<dim;j++) {
			a = a*ts;
			p_A->set_at(p_A,i,j,a/(j-i));
		}
		a = a*ts;
		p_B->set_at(p_B,i,0,a/(dim-i));
	}
	p_sys = idlinear_sys(p_A,p_B,p_c);

	return p_sys;
}

idlinear_sys_t *mk_double_integrator(double ts) {
	idlinear_sys_t *p_sys;

	// double integrator
	p_sys = mk_integrator(2,ts);

	return p_sys;
}

int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	int n = 2, m = 1;
	double 	vec[] = {1.0,1.0},
			X_lb[] = {0.0,0.0},
			X_ub[] = {2.0,2.0},
			U_lb[] = {-0.5},
			U_ub[] = { 0.5},
			ts = 1.0;

	idbool_t is_fair = FALSE;
	idkripke_t *p_M = idkripke_single_mode(mk_colvector(vec,n),is_fair,H_idpolytope(mk_Abox(n),mk_bbox(X_lb,X_ub,n)),
			mk_double_integrator(ts),H_idpolytope(mk_Abox(m),mk_bbox(U_lb,U_ub,m)));

	X_ub[0] = 1.0;
	X_ub[1] = 1.0;
	p_M->add_polytope(p_M,H_idpolytope(mk_Abox(n),mk_bbox(X_lb,X_ub,n)),0);

	p_M->print(p_M,stdout);


	id_destroy_global_constants();

	return 0;
}
