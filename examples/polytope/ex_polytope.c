/*
 * ex_polytope.c
 *
 *  Created on: Jul 28, 2019
 *      Author: rafael
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

idmatrix_t *mk_Abox(int dim) {
	idmatrix_t *p_Abox = id_zeros(2*dim,dim);
	int i;

	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i,i,-1.0);
	}
	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i+dim,i,1.0);
	}

	return p_Abox;
}
idmatrix_t *mk_bbox(double *h_lb, double *h_ub, int dim) {
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	for(i=0;i<dim;i++) {
		p_bbox->setat(p_bbox,i,0,-h_lb[i]);
	}
	for(i=0;i<dim;i++) {
		p_bbox->setat(p_bbox,i+dim,0,h_ub[i]);
	}

	return p_bbox;
}


int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	idpolytope_t *p_P1, *p_P2, *p_P3, *p_P, *p_Paux;
	idpredicate_t *p_pi, *h_pi;
	idsparse_t *p_lhs;
	idarray_t *p_region;
	idrb_node_t *it;
	int i, dim = 2,	 facet1, facet2;
	double 	P1_lb[] = {0.0,0.0},
			P1_ub[] = {1.0,1.0},
			P2_lb[] = {1.0,0.0},
			P2_ub[] = {2.0,1.0},
			P3_lb[] = {0.5,0.5},
			P3_ub[] = {1.5,1.5},
			one = 1.0;
	char sense;
	iddblmatrix_t *p_x;

	p_P1 = H_idpolytope(mk_Abox(dim), mk_bbox(P1_lb,P1_ub,dim));
	p_P2 = H_idpolytope(mk_Abox(dim), mk_bbox(P2_lb,P2_ub,dim));
	p_P3 = H_idpolytope(mk_Abox(dim), mk_bbox(P3_lb,P3_ub,dim));

	printf("P1=\n");
	p_P1->print(p_P1,stdout);

	printf("P2=\n");
	p_P2->print(p_P2,stdout);

	printf("P3=\n");
	p_P3->print(p_P3,stdout);

	printf("pi=\n");
	p_lhs = idsparse_byvalue(dim,1,sizeof(double));
	p_lhs->set_at(p_lhs,0,0,&one);
	p_pi = id_mkLe(p_lhs,0.5);
	p_pi->print(p_pi,stdout);

//	printf("P1 and P3=\n");
//	p_P = p_P1->and_with_polytope(p_P1,p_P3);
//	p_P->print(p_P,stdout);
//	p_P->destroy(p_P);
//
//	printf("P1 and pi=\n");
//	p_P = p_P1->and_with_predicate(p_P1,p_pi);
//	p_P->print(p_P,stdout);
//	p_P->destroy(p_P);
//
//	printf("P1 copy=\n");
//	p_P = p_P1->cpy(p_P1);
//	p_P->print(p_P,stdout);
//	printf("P1 copy %s P1\n",(p_P->is_equal_to(p_P,p_P1)?"==":"!="));
//	p_P->destroy(p_P);
//
//	printf("P1/P3=\n");
//	p_region = p_P1->difference(p_P1,p_P3);
//	idrb_traverse(it,p_region->mp_head) {
//		printf("region[%d]=\n", it->keyi(it));
//		p_P = (idpolytope_t *)it->value(it);
//		p_P->print(p_P,stdout);
//		p_P->destroy(p_P);
//	}
//	p_region->destroy(p_region);

//	printf("P1 and P2=\n");
//	p_P = p_P1->and_with_polytope(p_P1,p_P2);
//	p_P->print(p_P,stdout);
//	if (p_P1->is_adjacent_to(p_P1,p_P2,p_P,&facet1,&facet2,&p_Paux)) {
//		printf("P1 and P2 are adjacentes\n");
//		printf("P1 adjacent facet %d is\n", facet1);
//		h_pi = (idpredicate_t *)idlist_first(p_P1->mp_x_constr->mp_row_pi->get_values2_at(
//				p_P1->mp_x_constr->mp_row_pi,&facet1));
//		assert(h_pi!=NULL);
//		h_pi->print(h_pi,stdout);
//		printf("P2 adjacent facet %d is\n", facet2);
//		h_pi = (idpredicate_t *)idlist_first(p_P2->mp_x_constr->mp_row_pi->get_values2_at(
//				p_P2->mp_x_constr->mp_row_pi,&facet2));
//		assert(h_pi!=NULL);
//		h_pi->print(h_pi,stdout);
//		if (p_Paux!=NULL) {
//			printf("P1 and P2 can be merged\n");
//			p_Paux->print(p_Paux,stdout);
//			p_Paux->destroy(p_Paux);
//		}
//	}
//	p_P->destroy(p_P);

	p_x = iddblmatrix(dim,1);
	p_x->set_at(p_x,0,0,0.25);
	p_x->set_at(p_x,1,0,0.25);
	printf("x = \n");
	p_x->print(p_x,stdout);
	printf("x is%sinside P1\n",(p_P1->isinside_dblcolvector(p_P1,p_x)?" ":" NOT "));
	printf("x is%sinside P2\n",(p_P2->isinside_dblcolvector(p_P2,p_x)?" ":" NOT "));
	printf("x is%sinside P3\n",(p_P3->isinside_dblcolvector(p_P3,p_x)?" ":" NOT "));
	p_x->destroy(p_x);

	p_P1->destroy(p_P1);
	p_P2->destroy(p_P2);
	p_P3->destroy(p_P3);
	p_pi->destroy(p_pi);

	id_destroy_global_constants();

	return 0;
}
