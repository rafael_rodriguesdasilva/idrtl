/*
 * main.c
 *
 *  Created on: Jul 27, 2019
 *      Author: rafael
 */

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	int i, nvars = 2, numnz = 2, *h_ind;
	idsparse_t *p_lhs = iddblsparse(nvars,numnz);
	double val, *h_val, rhs;
	char sense;
	idpredicate_t *p_pi1, *p_pi2;

	val = 1.0;
	p_lhs->set_at(p_lhs,0,0,&val);
	val = -1.0;
	p_lhs->set_at(p_lhs,1,1,&val);

	p_pi1 = id_mkLe(p_lhs,0.5);
	printf("pi1 =");
	p_pi1->print(p_pi1,stdout);

	printf("nvars = %d\n", p_pi1->nvars(p_pi1));
	printf("isLessEqual = %s\n", (p_pi1->isLessEqual(p_pi1)?"TRUE":"FALSE"));
	printf("isEqual = %s\n", (p_pi1->isEqual(p_pi1)?"TRUE":"FALSE"));
	p_pi1->get(p_pi1,&numnz,&h_ind,&h_val,&sense,&rhs);

	for(i=0;i<numnz;i++) {
		printf("val[%d] = %.3f\n", h_ind[i], h_val[i]);
	}
	printf("sense = %s\n",(sense==GRB_LESS_EQUAL?"<=":"=="));
	printf("rhs = %.3f\n", rhs);

	p_pi2 = p_pi1->cpy(p_pi1);
	printf("pi1 copy =");
	p_pi2->print(p_pi2,stdout);

	printf("pi1 %s pi1 copy\n", (p_pi1->cmp(p_pi1,p_pi2)==0?"==":"!="));

	p_pi2->neg(p_pi2);
	printf("pi2 =");
	p_pi2->print(p_pi2,stdout);

	printf("pi1 %s pi2\n", (p_pi1->cmp(p_pi1,p_pi2)==0?"==":"!="));


	p_pi1->destroy(p_pi1);
	p_pi2->destroy(p_pi2);

	id_destroy_global_constants();

	return 0;
}
