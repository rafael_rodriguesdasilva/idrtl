/*
 * ex_polytope.c
 *
 *  Created on: Jul 28, 2019
 *      Author: rafael
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

idmatrix_t *mk_Abox(int dim) {
	idmatrix_t *p_Abox = id_zeros(2*dim,dim);
	int i;

	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i,i,-1.0);
	}
	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i+dim,i,1.0);
	}

	return p_Abox;
}
idmatrix_t *mk_bbox_2d(double x_lb, double x_ub, double y_lb, double y_ub) {
	const int dim = 2;
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	p_bbox->setat(p_bbox,0,0,-x_lb);
	p_bbox->setat(p_bbox,1,0,-y_lb);
	p_bbox->setat(p_bbox,2,0, x_ub);
	p_bbox->setat(p_bbox,3,0, y_ub);

	return p_bbox;
}
idmatrix_t *mk_bbox_1d(double x_lb, double x_ub) {
	const int dim = 1;
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	p_bbox->setat(p_bbox,0,0,-x_lb);
	p_bbox->setat(p_bbox,1,0, x_ub);

	return p_bbox;
}

iddblmatrix_t *mk_colvector(double x, double y) {
	const int dim = 2;
	iddblmatrix_t *p_vec = iddblmatrix(dim,1);
	int i;

	p_vec->set_at(p_vec,0,0,x);
	p_vec->set_at(p_vec,1,0,y);

	return p_vec;
}
idlinear_sys_t *mk_integrator(int nchains, int dim, double ts) {
	iddblmatrix_t *p_A = iddblmatrix(nchains*dim,nchains*dim);
	iddblmatrix_t *p_B = iddblmatrix(nchains*dim,dim);
	iddblmatrix_t *p_c = iddblmatrix(nchains*dim,1);
	idlinear_sys_t *p_sys;

	// double integrator
	for(int k=0;k<dim;k++) {
		for(int i=0;i<nchains;i++) {
			double a = 1.0;
			p_A->set_at(p_A,i+k*nchains,i+k*nchains,a);
			for(int j=i+1;j<nchains;j++) {
				a = a*ts;
				p_A->set_at(p_A,i+k*nchains,j+k*nchains,a/(j-i));
			}
			a = a*ts;
			p_B->set_at(p_B,i+k*nchains,k,a/(nchains-i));
		}
	}
	p_sys = idlinear_sys(p_A,p_B,p_c);

	return p_sys;
}

idlinear_sys_t *mk_2dsingle_integrator(double ts) {
	idlinear_sys_t *p_sys;

	// double integrator
	p_sys = mk_integrator(1,2,ts);

	return p_sys;
}
idformula_t *mk_formula(idkripke_t *h_M) {

	idformula_t *p_phi, *p_a, *p_b, *p_c, *p_notc;

	p_a = id_mkAtomicProp(0,h_M,0,FALSE);
	p_b = id_mkAtomicProp(1,h_M,1,FALSE);

	p_phi = id_mkUntil(2,p_a,p_b);

	return p_phi;
}


int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	int n = 2, m = 2, safe = 0, target = 1;
	double 	ts = 1.0;

	idbool_t is_fair = FALSE;
	idkripke_t *p_M = idkripke_single_mode(mk_colvector(1.0,-5.5),is_fair,
			H_idpolytope(mk_Abox(n),mk_bbox_2d(-10.0,1.85,-10.0,2.0)),
			mk_2dsingle_integrator(ts),H_idpolytope(mk_Abox(m),mk_bbox_2d(-2.0,2.0,-2.0,2.0)));
	idrobrun_t *p_solution;

	p_M->add_polytope(p_M,H_idpolytope(mk_Abox(n),mk_bbox_2d(-10.0,1.85,-10.0,2.0)),safe);
	p_M->add_polytope(p_M,H_idpolytope(mk_Abox(n),mk_bbox_2d(- 0.5, 0.5 ,- 0.5, 0.5)),target);

	printf("Kripke = \n");
	p_M->print(p_M,stdout);

	idformula_t *p_phi = mk_formula(p_M);

	if (idRTL(0.1,0.1,p_phi,&p_solution)) {
		printf("Solution found = \n");
		p_solution->print(p_solution,stdout);
		p_solution->destroy(p_solution);
	}

	p_phi->destroy(p_phi);
	p_M->destroy(p_M);

	id_destroy_global_constants();

	return 0;
}
