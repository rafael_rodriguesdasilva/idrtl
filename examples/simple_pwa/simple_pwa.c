/*
 * ex_polytope.c
 *
 *  Created on: Jul 28, 2019
 *      Author: rafael
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

#define MIN(a,b) (a<=b?a:b)

idmatrix_t *mk_Abox(int dim) {
	idmatrix_t *p_Abox = id_zeros(2*dim,dim);
	int i;

	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i,i,-1.0);
	}
	for(i=0;i<dim;i++) {
		p_Abox->setat(p_Abox,i+dim,i,1.0);
	}

	return p_Abox;
}
idmatrix_t *mk_bbox_2d(double x_lb, double x_ub, double y_lb, double y_ub) {
	const int dim = 2;
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	p_bbox->setat(p_bbox,0,0,-x_lb);
	p_bbox->setat(p_bbox,1,0,-y_lb);
	p_bbox->setat(p_bbox,2,0, x_ub);
	p_bbox->setat(p_bbox,3,0, y_ub);

	return p_bbox;
}
idmatrix_t *mk_bbox_1d(double x_lb, double x_ub) {
	const int dim = 1;
	idmatrix_t *p_bbox = id_zeros(2*dim,1);
	int i;

	p_bbox->setat(p_bbox,0,0,-x_lb);
	p_bbox->setat(p_bbox,1,0, x_ub);

	return p_bbox;
}

iddblmatrix_t *mk_colvector(double x, double y) {
	const int dim = 2;
	iddblmatrix_t *p_vec = iddblmatrix(dim,1);
	int i;

	p_vec->set_at(p_vec,0,0,x);
	p_vec->set_at(p_vec,1,0,y);

	return p_vec;
}
idlinear_sys_t *mk_integrator(int nchains, int dim, double ts) {
	iddblmatrix_t *p_A = iddblmatrix(nchains*dim,nchains*dim);
	iddblmatrix_t *p_B = iddblmatrix(nchains*dim,dim);
	iddblmatrix_t *p_c = iddblmatrix(nchains*dim,1);
	idlinear_sys_t *p_sys;

	// double integrator
	for(int k=0;k<dim;k++) {
		for(int i=0;i<nchains;i++) {
			double a = 1.0;
			p_A->set_at(p_A,i+k*nchains,i+k*nchains,a);
			for(int j=i+1;j<nchains;j++) {
				a = a*ts;
				p_A->set_at(p_A,i+k*nchains,j+k*nchains,a/(j-i));
			}
			a = a*ts;
			p_B->set_at(p_B,i+k*nchains,k,a/(nchains-i));
		}
	}
	p_sys = idlinear_sys(p_A,p_B,p_c);

	return p_sys;
}

idlinear_sys_t *mk_2dsingle_integrator(double ts) {
	idlinear_sys_t *p_sys;

	// double integrator
	p_sys = mk_integrator(1,2,ts);

	return p_sys;
}
idformula_t *mk_until_formula(idkripke_t *h_M, int safe, int target) {

	idformula_t *p_phi, *p_safe, *p_target;

	p_safe = id_mkAtomicProp(0,h_M,safe,FALSE);
	p_target = id_mkAtomicProp(1,h_M,target,FALSE);

	p_phi = id_mkUntil(2,p_safe,p_target);

	return p_phi;
}

idpolytope_t *mk_workspace() {
	idmatrix_t *p_A = id_zeros(3,2);
	idmatrix_t *p_b = id_zeros(3,1);

	// x1+x2<=4
	p_A->setat(p_A,0,0, 1.0);
	p_A->setat(p_A,0,1, 1.0);
	p_b->setat(p_b,0,0, 4.0);
	//-x1+x2<=0
	p_A->setat(p_A,1,0,-1.0);
	p_A->setat(p_A,1,1, 1.0);
	p_b->setat(p_b,1,0, 0.0);
	//-x2<=-0.1
	p_A->setat(p_A,2,1,-1.0);
	p_b->setat(p_b,2,0,-0.1);

	return H_idpolytope(p_A,p_b);
}
iddblmatrix_t *mk_eye(int rows, int cols) {
	iddblmatrix_t *p_A = iddblmatrix(rows,cols);
	int i;
	for(i=0;i<MIN(rows,cols);i++) {
		p_A->set_at(p_A,i,i,1.0);
	}
	return p_A;
}
idlinear_sys_t *mk_mode1_sys() {
	return idlinear_sys(mk_eye(2,2),mk_eye(2,2),mk_colvector(0.1,0.2));
}
idlinear_sys_t *mk_mode2_sys() {
	return idlinear_sys(mk_eye(2,2),mk_eye(2,2),mk_colvector(0.0,0.0));
}
idpolytope_t *mk_mode1_statedom() {
	idmatrix_t *p_A = id_zeros(3,2);
	idmatrix_t *p_b = id_zeros(3,1);

	// x1+x2<=4
	p_A->setat(p_A,0,0, 1.0);
	p_A->setat(p_A,0,1, 1.0);
	p_b->setat(p_b,0,0, 4.0);
	//-x2<=-0.1
	p_A->setat(p_A,1,1,-1.0);
	p_b->setat(p_b,1,0,-0.1);
	//-x1<=-2
	p_A->setat(p_A,2,0,-1.0);
	p_b->setat(p_b,2,0,-2.0);

	return H_idpolytope(p_A,p_b);

}
idpolytope_t *mk_mode2_statedom() {
	idmatrix_t *p_A = id_zeros(3,2);
	idmatrix_t *p_b = id_zeros(3,1);

	// x1+x2<=4
	p_A->setat(p_A,0,0,-1.0);
	p_A->setat(p_A,0,1, 1.0);
	p_b->setat(p_b,0,0, 0.0);
	//-x2<=-0.1
	p_A->setat(p_A,1,1,-1.0);
	p_b->setat(p_b,1,0,-0.1);
	// x1<=2
	p_A->setat(p_A,2,0, 1.0);
	p_b->setat(p_b,2,0, 2.0);

	return H_idpolytope(p_A,p_b);
}
idpolytope_t *mk_2dbox(double x_lb, double x_ub, double y_lb, double y_ub) {
	return H_idpolytope(mk_Abox(2),mk_bbox_2d(x_lb,x_ub,y_lb,y_ub));
}
idpolytope_t *mk_target() {
	idmatrix_t *p_A = id_zeros(4,2);
	idmatrix_t *p_b = id_zeros(4,1);

	// x1+x2<=4
	p_A->setat(p_A,0,0, 1.0);
	p_A->setat(p_A,0,1, 1.0);
	p_b->setat(p_b,0,0, 4.0);
	//-x2<=-0.1
	p_A->setat(p_A,1,1,-1.0);
	p_b->setat(p_b,1,0,-0.1);
	//-x1<=-3
	p_A->setat(p_A,2,0,-1.0);
	p_b->setat(p_b,2,0,-3.0);
	// x1<=2
	p_A->setat(p_A,3,0, 1.0);
	p_b->setat(p_b,3,0, 5.0);

	return H_idpolytope(p_A,p_b);
}

int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	int n = 2, m = 2, safe = 0, target = 1;
	double 	ts = 1.0;

	idbool_t is_fair = FALSE;
	iddblmatrix_t *p_x0 = mk_colvector(1.0,0.2);
	idpolytope_t *p_statedom_q0 = mk_mode1_statedom(),
				 *p_statedom_q1 = mk_mode2_statedom();
	int q0 = (p_statedom_q0->isinside_dblcolvector(p_statedom_q0,p_x0)?0:
				(p_statedom_q1->isinside_dblcolvector(p_statedom_q1,p_x0)?1:-1));
	idkripke_t *p_M = idkripke(p_x0,q0,is_fair);
	idrobrun_t *p_solution;

	p_M->add_mode(p_M,0,mk_mode1_sys(),p_statedom_q0,mk_2dbox(-0.1,0.2,-0.1,0.2));
	p_M->add_mode(p_M,1,mk_mode2_sys(),p_statedom_q1,mk_2dbox(-0.1,0.2,-0.1,0.2));
	p_M->add_polytope(p_M,mk_workspace(),safe);
	p_M->add_polytope(p_M,mk_target(),target);

	printf("Kripke = \n");
	p_M->print(p_M,stdout);

	idformula_t *p_phi = mk_until_formula(p_M,safe,target);

	if (idRTL(0.001,0.0001,p_phi,&p_solution)) {
		printf("Solution found = \n");
		p_solution->print(p_solution,stdout);
		p_solution->destroy(p_solution);
	}

	p_phi->destroy(p_phi);
	p_M->destroy(p_M);

	id_destroy_global_constants();

	return 0;
}
