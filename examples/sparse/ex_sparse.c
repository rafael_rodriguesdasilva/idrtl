/*
 * ex_polytope.c
 *
 *  Created on: Jul 28, 2019
 *      Author: rafael
 */


#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "idrtl.h"

int main(int argc, char *argv[]) {

	srand(time(NULL));   // Initialization, should only be called once.

	id_set_global_constants();

	idsparse_t 	*p_s1 = idsparse_byvalue(3,2,sizeof(double)),
				*p_s2;
	double val, lhs = 1.0;
	int numnz, n=2;

	val = 2.0;
	p_s1->set_at(p_s1,0,0,&val);
	val = 3.0;
	p_s1->set_at(p_s1,0,2,&val);

	p_s2 = p_s1->cpy(p_s1);
	p_s2->upd_nvars(p_s2,p_s2->nvars(p_s2)+1);
	p_s2->upd_numnz(p_s2,p_s2->numnz(p_s2)+1);
	numnz = p_s2->numnz(p_s2);
	p_s2->set_at(p_s2,numnz-1,n,&lhs);
	p_s2->destroy(p_s2);

	p_s1->destroy(p_s1);

	id_destroy_global_constants();

	return 0;
}
