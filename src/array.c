/*
 * array.c
 *
 *  Created on: Jul 3, 2019
 *      Author: rafael
 */


#include "array.h"

static idarray_t *idarray_default_constructor(void) {
	idarray_t *obj = malloc(sizeof(idarray_t));
	assert(obj!=NULL);

	obj->destroy = idarray_destroy;
	obj->cpy = idarray_cpy;
	obj->set = idarray_set;
	obj->set_at = NULL;
	obj->append = NULL;
	obj->remove_at = idarray_remove_at;
	obj->length = idarray_length;
	obj->get_at = idarray_get_at;
	obj->to_sparse = idarray_to_sparse;
	obj->numnz = idarray_numnz;

#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	static int idarray_last_id = 0;
	obj->m_id = idarray_last_id;
	printf("debug memleak construct array %d\n", obj->m_id);
	idarray_last_id++;
	idarray_destroy_cnt++;
#endif
	obj->m_length = 0;
	obj->m_sizeof_value = NULL;
	obj->mh_cpy_value = NULL;
	obj->mp_head = NULL;

	return obj;
}


/* public functions */
idarray_t *     	idarray_byvalue(size_t sizeof_value) {
	idarray_t *obj = idarray_default_constructor();

	obj->m_sizeof_value = sizeof_value;
	obj->mh_cpy_value = NULL;
	obj->set_at = idarray_set_at_byvalue;
	obj->append = idarray_append_byvalue;
	obj->mp_head = idrb_with_integer_key(free);

	return obj;
}
idarray_t *     	idarray_byptr(void (*destroy_value)(void *value), void* (*cpy_value)(void *value)) {
	idarray_t *obj = idarray_default_constructor();

	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy_value = cpy_value;
	obj->set_at = idarray_set_at_byref;
	obj->append = idarray_append_byref;
	if (destroy_value==NULL) {
		obj->mp_head = idrb_with_integer_key(free);
	} else {
		obj->mp_head = idrb_with_integer_key(destroy_value);
	}


	return obj;
}
idarray_t *     	idarray_byref(void) {
	idarray_t *obj = idarray_default_constructor();

	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy_value = idref_cpy;
	obj->set_at = idarray_set_at_byref;
	obj->append = idarray_append_byref;
	obj->mp_head = idrb_with_integer_key(NULL);

	return obj;
}
void             idarray_destroy(idarray_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
		idarray_destroy_cnt--;
		printf("debug memleak destroy array %d still remains %d to destroy\n", obj->m_id, idarray_destroy_cnt);
		if (idarray_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif

		idrb_destroy(obj->mp_head);
		free(obj);
	}
}
idarray_t *     	idarray_cpy(idarray_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it;
	idarray_t *lst = idarray_default_constructor();
	void *p_value, *h_value;
	int i;

	lst->m_sizeof_value = obj->m_sizeof_value;
	lst->mh_cpy_value = obj->mh_cpy_value;
	lst->set_at = obj->set_at;
	lst->append = obj->append;
	lst->mp_head = idrb_with_integer_key(obj->mp_head->v.destroy);

	idrb_traverse(it, obj->mp_head) {
		i = it->keyi(it);
		h_value = it->value(it);
		if (lst->mh_cpy_value!=NULL) {
			p_value = lst->mh_cpy_value(h_value);
		} else {
			p_value = malloc(lst->m_sizeof_value);
			assert(p_value!=NULL);
			memcpy(p_value,h_value,lst->m_sizeof_value);
		}
		lst->mp_head->insert(lst->mp_head,&i,p_value);
		lst->m_length = (lst->m_length>i?lst->m_length:i+1);
	}
	assert(lst->m_length==obj->m_length);

	return lst;
}

/* set functions */
void            	idarray_set(idarray_t *obj, int numnz, int *p_ind, void *p_val) {
	assert(obj!=NULL);
	assert(p_ind!=NULL);
	assert(p_val!=NULL);
	int i;

	for(i=0;i<numnz;i++) {
		obj->set_at(obj,p_ind[i],p_val + i*obj->m_sizeof_value);
	}
}

void            idarray_set_at_byref(idarray_t *obj, int i, void *p_value) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,&i,&fnd);

	if (fnd) {
		h_node->delete_node(h_node);
	}
	if (p_value!=NULL) {
		obj->mp_head->insert(obj->mp_head,&i,p_value);
		obj->m_length = (obj->m_length>i?obj->m_length:i+1);
	}
}
void            idarray_set_at_byvalue(idarray_t *obj, int i, void *h_value) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,&i,&fnd);
	void *p_value;

	if ((fnd)&&(h_value==NULL)) {
		h_node->delete_node(h_node);
	} else if (fnd) {
		if (obj->mh_cpy_value==NULL) {
			p_value = h_node->value(h_node);
			memcpy(p_value,h_value,obj->m_sizeof_value);
		} else {
			h_node->delete_node(h_node);
			p_value = obj->mh_cpy_value(h_value);
			obj->mp_head->insert(obj->mp_head,&i,p_value);
		}
	} else if (h_value!=NULL) {
		if (obj->mh_cpy_value==NULL) {
			p_value = malloc(obj->m_sizeof_value);
			assert(p_value!=NULL);
			memcpy(p_value,h_value,obj->m_sizeof_value);
		} else {
			p_value = obj->mh_cpy_value(h_value);
		}
		obj->mp_head->insert(obj->mp_head,&i,p_value);
		obj->m_length = (obj->m_length>i?obj->m_length:i+1);
	}
}
int            idarray_append_byref(idarray_t *obj, void *p_value) {
	assert(obj!=NULL);

	int i;
	if (p_value!=NULL) {
		i = obj->length(obj);
		obj->mp_head->insert(obj->mp_head,&i,p_value);
		obj->m_length++;
		return i;
	}
	return -1;
}
int            idarray_append_byvalue(idarray_t *obj, void *h_value) {
	assert(obj!=NULL);

	int i;
	void *p_value;

	if (h_value!=NULL) {
		i = obj->length(obj);
		if (obj->mh_cpy_value==NULL) {
			p_value = malloc(obj->m_sizeof_value);
			assert(p_value!=NULL);
			memcpy(p_value,h_value,obj->m_sizeof_value);

		} else {
			p_value = obj->mh_cpy_value(h_value);
			obj->mp_head->insert(obj->mp_head,&i,p_value);
		}
		obj->mp_head->insert(obj->mp_head,&i,p_value);
		obj->m_length++;

		return i;
	}

	return -1;
}
void idarray_remove_at(idarray_t *obj, int i) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,&i,&fnd);

	if (fnd) {
		h_node->delete_node(h_node);
	}
}

/* get functions */
int           idarray_length(idarray_t *obj) {
	assert(obj!=NULL);
	return obj->m_length;
}
void *        idarray_get_at(idarray_t *obj, int i) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t *h_node;

	if ((0<=i)&&(i<obj->length(obj))) {

		h_node = obj->mp_head->find_key_n(obj->mp_head,&i,&fnd);

		if (fnd) {
			return h_node->value(h_node);
		}
	}

	return NULL;
}
void	        	idarray_to_sparse(idarray_t *obj, int *h_numnz, int **hp_ind, void **hp_val) {
	assert(obj!=NULL);
	assert(h_numnz!=NULL);
	assert(hp_ind!=NULL);
	assert(hp_val!=NULL);
	assert(obj->mh_cpy_value==NULL);

	idrb_node_t *it;
	int cnt, numnz;
	void *p_values;
	int *p_keys;

	numnz = obj->numnz(obj);
	p_keys = malloc(numnz*sizeof(int));
	assert(p_keys!=NULL);
	p_values = malloc(numnz*obj->m_sizeof_value);
	assert(p_values!=NULL);

	cnt = 0;
	idrb_traverse(it, obj->mp_head) {
		p_keys[cnt] = it->keyi(it);
		memcpy(p_values+cnt*obj->m_sizeof_value,it->value(it),obj->m_sizeof_value);
		cnt++;
	}
	*hp_ind =  p_keys;
	*hp_val =  p_values;
	*h_numnz = numnz;
}

int           	idarray_numnz(idarray_t *obj) {
	assert(obj!=NULL);

	return obj->mp_head->length(obj->mp_head);
}
void *        	idarray_byref_pop(idarray_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it = idrb_first(obj->mp_head);
	void *h_value = it->value(it);
	it->delete_node(it);
	return h_value;
}
