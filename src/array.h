/*
 * array.h
 *
 * Array data type is a dynamic indexed table of values.
 *
 *  Created on: Jul 3, 2019
 *      Author: rafael
 */

#ifndef SOLVER_ARRAY_H_
#define SOLVER_ARRAY_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
// create an array which values are passed by reference
extern idarray_t *     	idarray_byvalue(size_t sizeof_value);
// create an array which values are pointers
extern idarray_t *     	idarray_byptr(void (*destroy_value)(void *value), void* (*cpy_value)(void *value));
// create an array which values are reference
extern idarray_t *     	idarray_byref();
extern void             idarray_destroy(idarray_t *obj);
extern idarray_t *     	idarray_cpy(idarray_t *obj);

/* set functions */
extern void            	idarray_set(idarray_t *obj, int numnz, int *p_ind, void *p_val);
extern void            	idarray_set_at_byref(idarray_t *obj, int i, void *p_value);
extern void            	idarray_set_at_byvalue(idarray_t *obj, int i, void *h_value);
extern int            	idarray_append_byref(idarray_t *obj, void *p_value);
extern int            	idarray_append_byvalue(idarray_t *obj, void *h_value);
extern void         	idarray_remove_at(idarray_t *obj, int i);

/* get functions */
extern int           	idarray_length(idarray_t *obj);
extern int           	idarray_numnz(idarray_t *obj);
extern void *        	idarray_get_at(idarray_t *obj, int i);
extern void	        	idarray_to_sparse(idarray_t *obj, int *h_numnz, int **hp_ind, void **hp_val);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	static int		    idarray_destroy_cnt = 0;
#endif
#define idintarray(void) idarray_byvalue(sizeof(int));
extern void *        	idarray_byref_pop(idarray_t *obj);

struct idarray_s{
	/* capsulated member functions */

	// destroy an array
	void       	(*destroy)(idarray_t *obj);
	// copy an array
	idarray_t *	(*cpy)(idarray_t *obj);

	/* set functions */
	void       	(*set)(idarray_t *obj, int numnz, int *h_ind, void *h_val);
	void       	(*set_at)(idarray_t *obj, int i, void *value);
	int       	(*append)(idarray_t *obj, void *value);
	void  		(*remove_at)(idarray_t *obj, int i);

	/* get functions */
	int      	(*length)(idarray_t *obj);
	int      	(*numnz)(idarray_t *obj);
	void * 		(*get_at)(idarray_t *obj, int i);
	void	   	(*to_sparse)(idarray_t *obj, int *h_numnz, int **hp_ind, void **hp_val);
	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	int		      m_id;
#endif
	idrb_node_t 	*mp_head;
	int    			m_length;
	size_t			m_sizeof_value;
	void*          (*mh_cpy_value)(void *value);
};


#ifdef __cplusplus
}
#endif


#endif /* SOLVER_ARRAY_H_ */
