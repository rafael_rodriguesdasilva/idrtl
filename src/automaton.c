/*
 * automaton.c
 *
 *  Created on: Jun 4, 2019
 *      Author: rafael
 */

#include "automaton.h"

static idautomaton_t *	idautomaton_default_constructor(int nstates, int nsymbols, idbool_t isfair) {
	assert(nstates>0);
	assert(nsymbols>0);

	idautomaton_t *obj = malloc(sizeof(idautomaton_t));
	assert(obj!=NULL);

#ifdef IDRTL_DEBUG_MEMLEAK_AUTOMATON
	static int idautomaton_last_id = 0;
	obj->m_id = idautomaton_last_id;
	printf("debug memleak construct automaton %d\n", obj->m_id);
	idautomaton_last_id++;
	idautomaton_destroy_cnt++;
#endif
	obj->m_nsymbols = nsymbols;
	obj->m_nstates = nstates;
	obj->m_nbits = ceil(log(obj->m_nstates)/log(2));;
	obj->m_initial = 0;
	obj->m_isfair = isfair;
	obj->mp_edges 			= NULL;
	obj->mp_edges_map		= NULL;
	obj->mp_marked			= NULL;
	obj->mp_p_bitvec_array	= NULL;
	obj->m_z3_id = rand();

	obj->add_transition = idautomaton_add_transition;
	obj->cpy = idautomaton_cpy;
	obj->destroy = idautomaton_destroy;
	obj->is_fair = idautomaton_is_fair;
	obj->z3_bitvec_at = idautomaton_z3_bitvec_at;
	obj->z3_state = idautomaton_z3_state;
	obj->z3_transitions = idautomaton_z3_transitions;
	obj->z3_marked = idautomaton_z3_marked;

	return obj;
}

/* public functions */
idautomaton_t *	idautomaton(int nstates, int nsymbols, idbool_t isfair) {
	assert(nstates>0);
	assert(nsymbols>0);

	idautomaton_t *obj = idautomaton_default_constructor(nstates,nsymbols,isfair);
	obj->mp_edges 			= idintlisttable();
	obj->mp_edges_map		= idintmap();
	obj->mp_marked			= idintlist();
	obj->mp_p_bitvec_array	= idarray_byptr(free,NULL);

	return obj;
}
void          	idautomaton_destroy(idautomaton_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_AUTOMATON
		idautomaton_destroy_cnt--;
		printf("debug memleak destroy automaton %d still remains %d to destroy\n", obj->m_id, idautomaton_destroy_cnt);
		if (idautomaton_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idlist_destroy(obj->mp_marked);
		idtable_destroy(obj->mp_edges);
		idmap_destroy(obj->mp_edges_map);
		idarray_destroy(obj->mp_p_bitvec_array);
		free(obj);
	}
}
idautomaton_t * 	idautomaton_cpy(idautomaton_t *obj) {
	assert(obj!=NULL);

	idautomaton_t *p_A = idautomaton_default_constructor(obj->m_nstates, obj->m_nsymbols, obj->m_isfair);
	p_A->mp_edges 			= obj->mp_edges->cpy(obj->mp_edges);
	p_A->mp_edges_map 		= obj->mp_edges_map->cpy(obj->mp_edges_map);
	p_A->mp_marked			= obj->mp_marked->cpy(obj->mp_marked);
	p_A->mp_p_bitvec_array	= obj->mp_p_bitvec_array->cpy(obj->mp_p_bitvec_array);


	return p_A;
}

/* set functions */
void             idautomaton_add_transition(idautomaton_t *obj, int from, int to, int symbol) {
	assert(obj!=NULL);
	assert((0 <= from)&&(from <= obj->m_nstates));
	assert((0 <= to)&&(to <= obj->m_nstates));
	assert(obj->mp_edges!=NULL);
	assert(obj->mp_edges_map!=NULL);

	idlist_t *p_symbols, *h_symbols = (idlist_t *)obj->mp_edges->get_at(obj->mp_edges,from,to);
	if (h_symbols==NULL) {
		p_symbols = idintlist();
		p_symbols->push(p_symbols,&symbol);
		obj->mp_edges->set_at(obj->mp_edges,from,to,p_symbols);
	} else {
		h_symbols->push(h_symbols,&symbol);
	}
	obj->mp_edges_map->set(obj->mp_edges_map,&from,&to);
}

/* get functions */
idbool_t        idautomaton_is_fair(idautomaton_t *obj) {
	assert(obj!=NULL);
	return obj->m_isfair;
}

/* SMT operations */
Z3_ast         *idautomaton_z3_bitvec_at(idautomaton_t *obj, Z3_context ctx, int K) {
	assert(obj!=NULL);
	Z3_ast *p_bitvec;
	int i;
	char str[80];



	if (K==obj->mp_p_bitvec_array->length(obj->mp_p_bitvec_array)) {
		p_bitvec = malloc(obj->m_nbits*sizeof(Z3_ast));
		assert(p_bitvec!=NULL);

		for(i=0;i<obj->m_nbits;i++) {
			sprintf(str,"q%d_b%d_%d",obj->m_z3_id,i,K);
			p_bitvec[i] = mk_bool_var(ctx,str);
		}

		obj->mp_p_bitvec_array->append(obj->mp_p_bitvec_array,p_bitvec);
	} else {
		p_bitvec = (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K);
		if (p_bitvec==NULL) {
			p_bitvec = malloc(obj->m_nbits*sizeof(Z3_ast));
			assert(p_bitvec!=NULL);

			for(i=0;i<obj->m_nbits;i++) {
				sprintf(str,"q%d_b%d_%d",obj->m_z3_id,i,K);
				p_bitvec[i] = mk_bool_var(ctx,str);
			}

			obj->mp_p_bitvec_array->set_at(obj->mp_p_bitvec_array,K,p_bitvec);
		}
	}


	return p_bitvec;
}
Z3_ast          idautomaton_z3_state(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, int state) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);
	assert((0<=state)&&(state<obj->m_nstates));

	Z3_ast args[obj->m_nbits];
	int i, b;

	for(i=0;i<obj->m_nbits;i++) {
		b = (state >> i) & 1;

		if (b) {
			args[i] = bitvec[i];
		} else {
			args[i] = Z3_mk_not(ctx,bitvec[i]);
		}
	}

	return Z3_mk_and(ctx,obj->m_nbits,args);
}

Z3_ast          idautomaton_z3_transitions(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvecK,
		Z3_ast *bitvecKplus, Z3_ast *state_bitvecK, idkripke_t *M) {
	assert(obj!=NULL);
	assert(bitvecK!=NULL);
	assert(bitvecKplus!=NULL);

	idrb_node_t *it1, *it2, *it;
	idarray_t *h_to_array;
	int to, from, symbol, *p_ind, numnz;
	idarray_t *p_ast_array = idarray_byvalue(sizeof(Z3_ast));
	Z3_ast args3[3], arg, *p_args;
	idlist_t *h_symbols;

	idrb_traverse(it1,obj->mp_edges->mp_rows->mp_head) {
		from = it1->keyi(it1);
		h_to_array = (idarray_t *)it1->value(it1);
		idrb_traverse(it2,h_to_array->mp_head) {
			to		= it2->keyi(it2);
			h_symbols 	= (idlist_t *)it2->value(it2);
			idrb_traverse(it,h_symbols->mp_head) {
				symbol = *(int *)it->value(it);
				args3[0] = obj->z3_state(obj,ctx,bitvecK,from);
				args3[1] = obj->z3_state(obj,ctx,bitvecKplus,to);
				args3[2] = M->z3_state(M,ctx,state_bitvecK,symbol);
				arg = Z3_mk_and(ctx,3,args3);
				p_ast_array->append(p_ast_array,&arg);
			}
		}
	}
	p_ast_array->to_sparse(p_ast_array,&numnz,&p_ind,&p_args);
	arg = Z3_mk_or(ctx,numnz,p_args);
	free(p_ind);
	free(p_args);
	p_ast_array->destroy(p_ast_array);

	return arg;
}

Z3_ast          idautomaton_z3_marked(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, Z3_ast loop_exists) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);

	if (idrb_empty(obj->mp_marked->mp_head)) {
		return Z3_mk_true(ctx);
	}

	int si, cnt;
	idrb_node_t *it;
	Z3_ast args[obj->mp_marked->mp_head->length(obj->mp_marked->mp_head)+1];

	cnt = 0;
	idrb_traverse(it,obj->mp_marked->mp_head) {
		si = *(int *)it->value(it);
		args[cnt] = obj->z3_state(obj,ctx,bitvec,si);
		cnt++;
	}

	if (obj->is_fair(obj)) {
		if (cnt==1) {
			return mk_or(ctx,Z3_mk_not(ctx,loop_exists),args[0]);
		}
		return mk_or(ctx,Z3_mk_not(ctx,loop_exists),Z3_mk_and(ctx,cnt,args));
	}
	if (cnt==1) {
		return args[0];
	}
	return Z3_mk_and(ctx,cnt,args);
}
