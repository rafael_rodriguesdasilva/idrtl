/*
 * automaton.h
 *
 *  Created on: Jun 4, 2019
 *      Author: rafael
 */

#ifndef AUTOMATON_H_
#define AUTOMATON_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


/* public functions */
extern idautomaton_t *	idautomaton(int nstates, int nsymbols, idbool_t isfair);
extern void          	idautomaton_destroy(idautomaton_t *obj);
extern idautomaton_t * 	idautomaton_cpy(idautomaton_t *obj);

/* set functions */
extern void             idautomaton_add_transition(idautomaton_t *obj, int from, int to, int symbol);

/* get functions */
extern idbool_t        idautomaton_is_fair(idautomaton_t *obj);

/* SMT operations */
extern Z3_ast         *idautomaton_z3_bitvec_at(idautomaton_t *obj, Z3_context ctx, int K);
extern Z3_ast          idautomaton_z3_state(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, int state);
extern Z3_ast          idautomaton_z3_transitions(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvecK,
												Z3_ast *bitvecKplus, Z3_ast *state_bitvecK, idkripke_t *M);
extern Z3_ast          idautomaton_z3_marked(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, Z3_ast loop_exists);

#ifdef IDRTL_DEBUG_MEMLEAK_AUTOMATON
	static int		    idautomaton_destroy_cnt = 0;
#endif

struct idautomaton_s{
	/* capsulated member functions */
	void             (*destroy)(idautomaton_t *obj);
	idautomaton_t * 	 (*cpy)(idautomaton_t *obj);

	/* set functions */
	void 			 (*add_transition)(idautomaton_t *obj, int from, int to, int symbol);

	/* get functions */
	idbool_t		 (*is_fair)(idautomaton_t *obj);

	/* SMT operations */
	Z3_ast         *(*z3_bitvec_at)(idautomaton_t *obj, Z3_context ctx, int K);
	Z3_ast          (*z3_state)(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, int state);
	Z3_ast          (*z3_transitions)(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus, Z3_ast *state_bitvecK, idkripke_t *M);
	Z3_ast          (*z3_marked)(idautomaton_t *obj, Z3_context ctx, Z3_ast *bitvec, Z3_ast loop_exists);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_AUTOMATON
	int		      m_id;
#endif
	int			m_nstates,        // number of discrete states in the Kripke structure
				m_nsymbols,
				m_nbits,
				m_initial;
	idtable_t	*mp_edges;
	idmap_t		*mp_edges_map;
	idlist_t	*mp_marked;			// set of unmarked states
	idbool_t    m_isfair;
	idarray_t	*mp_p_bitvec_array;
	int			m_z3_id;
};


#ifdef __cplusplus
}
#endif



#endif /* AUTOMATON_H_ */
