/*
 * bsc.c
 *
 *  Created on: May 23, 2019
 *      Author: rafael
 */

#include"bsc.h"

//#define IDBSC_DEBUG_CREATE
//#define IDBSC_DEBUG_L1
//#define IDBSC_DEBUG_L2

idrobrun_t * idbsc_run_to_robrun(idrun_t *h_run, double delta, idkripke_t *h_M) {
	assert(h_run!=NULL);
	assert(h_M!=NULL);
	idlist_t *h_label_i;
	idlinear_sys_t *h_sys_i;
	idcsys_t *h_x_constr, *h_u_constr;
	int si, nsamples_i;
	idrb_node_t *it_s, *it_nsp, *it_l, *it_sys, *it_x, *it_u;
	idrobrun_t *p_robrun = idrobrun(delta,h_M);

	it_s 	= idrb_first(h_run->mp_states_intarray->mp_head);
	it_nsp 	= idrb_first(h_run->mp_nsample_intarray->mp_head);
	it_l 	= idrb_first(h_run->mp_h_label_array->mp_head);
	it_sys 	= idrb_first(h_run->mp_h_sys_array->mp_head);
	it_u 	= idrb_first(h_run->mp_h_u_constr_array->mp_head);
	it_x 	= idrb_first(h_run->mp_h_x_constr_array->mp_head);

	while((it_s!=idrb_nil(h_run->mp_states_intarray->mp_head))&&
			(it_nsp != idrb_nil(h_run->mp_nsample_intarray->mp_head))&&
			(it_l 	!= idrb_nil(h_run->mp_h_label_array->mp_head))&&
			(it_sys != idrb_nil(h_run->mp_h_sys_array->mp_head))&&
			(it_u 	!= idrb_nil(h_run->mp_h_u_constr_array->mp_head))&&
			(it_x 	!= idrb_nil(h_run->mp_h_x_constr_array->mp_head))) {
		si			= *(int *)it_s->value(it_s);
		nsamples_i	= *(int *)it_nsp->value(it_nsp);
		h_label_i	=  (idlist_t *)it_l->value(it_l);
		h_sys_i		=  (idlinear_sys_t *)it_sys->value(it_sys);
		h_u_constr	=  (idcsys_t *)it_u->value(it_u);
		h_x_constr	=  (idcsys_t *)it_x->value(it_x);

		p_robrun->append_step(p_robrun,nsamples_i,si,h_label_i,h_sys_i,h_u_constr,h_x_constr);

		it_s 	= idrb_next(it_s);
		it_nsp 	= idrb_next(it_nsp);
		it_l 	= idrb_next(it_l);
		it_sys 	= idrb_next(it_sys);
		it_u 	= idrb_next(it_u);
		it_x 	= idrb_next(it_x);
	}

	return p_robrun;
}
idbool_t idRTL(double delta, double delta_progress, idformula_t *h_phi, idrobrun_t **hp_solution) {
	idbsc_t *p_bsc = idbsc(delta, delta_progress, h_phi);
	idfsearch_t *p_fsearch;
	idrobrun_t *p_solution = NULL;

	while(p_bsc->check(p_bsc) || !p_bsc->is_unsat(p_bsc)) {
		while(p_bsc->is_sat(p_bsc)) {
			p_fsearch = p_bsc->decode(p_bsc);
			printf("K=%d : sat\n", p_bsc->length(p_bsc));
			if (p_fsearch->search(p_fsearch)) {
				printf("FOUND dynamically feasible solution\n");
				printf("maximizing its robustness...\n");
				p_solution = idbsc_run_to_robrun(p_fsearch->mp_solution,delta,h_phi->mh_M);
				p_solution->compute(p_solution);
				break;
			}
			printf("add cex and check...\n");
			p_bsc->add_cex_and_check(p_bsc,p_fsearch->get_cex(p_fsearch),p_fsearch->isfair(p_fsearch));
			p_fsearch->destroy(p_fsearch);
			p_fsearch = NULL;
		}
		if (p_bsc->is_sat(p_bsc)) {
			break;
		}
		printf("K=%d : unsat\n", p_bsc->length(p_bsc));
	}

	*hp_solution = p_solution;
	p_bsc->destroy(p_bsc);
	idfsearch_destroy(p_fsearch);

	return (p_solution!=NULL?TRUE:FALSE);
}


static void idbsc_mk_bitvec_phi(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);

	idformula_t *h_phi = obj->mh_phi;
	Z3_ast *p_bitvec;

	int K = obj->length(obj)-1;
	int len;
	p_bitvec = h_phi->bitvecK_get(h_phi,obj->m_ctx,&len);
	obj->mp_p_bitvec_phi_array->append(obj->mp_p_bitvec_phi_array,p_bitvec);

	if (K>0) {
		assert(len==obj->m_nphi);
	} else {
		obj->m_nphi = len;
	}
}

static void idbsc_init_loopvars(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	idkripke_t *h_M = obj->mh_phi->mh_M;

	obj->m_loop_exists = mk_bool_var(obj->m_ctx,"exists");
	obj->mp_bitvecE = h_M->z3_bitvecE(h_M,obj->m_ctx);
}

static Z3_ast idbsc_loop_base(idbsc_t *obj) {
	assert(obj!=NULL);

	Z3_ast inL_0 = mk_bool_var(obj->m_ctx,"inL_0");
	obj->mp_inLoop_array->set_at(obj->mp_inLoop_array,0,&inL_0);
	return Z3_mk_not(obj->m_ctx,inL_0);
}
static Z3_ast idbsc_loop_atK(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	if (obj->length(obj)<=0) {
		return Z3_mk_true(obj->m_ctx);
	}


	idkripke_t *h_M = obj->mh_phi->mh_M;
	char str[80];
	int i, K = obj->length(obj)-1;
	Z3_ast *h_sK, *h_sKminus, *h_sE, sKminus_iff_sE[h_M->nbits(h_M)], sKminus_eq_sE, args3[3], inL_K, *h_inL_Kminus;

	h_sK 		= (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K);
	h_sKminus 	= (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K-1);
	h_sE 		= (Z3_ast *)obj->mp_bitvecE;

	sprintf(str,"inL_%d",K);
	inL_K = mk_bool_var(obj->m_ctx,str);
	obj->mp_inLoop_array->set_at(obj->mp_inLoop_array,K,&inL_K);
	h_inL_Kminus = (Z3_ast *)obj->mp_inLoop_array->get_at(obj->mp_inLoop_array,K-1);

	sprintf(str,"l_%d",K);
	obj->m_loopK = mk_bool_var(obj->m_ctx,str);

	for(i=0;i<h_M->nbits(h_M);i++) {
		sKminus_iff_sE[i] = Z3_mk_iff(obj->m_ctx,h_sKminus[i],h_sE[i]);
	}
	sKminus_eq_sE = Z3_mk_and(obj->m_ctx,h_M->nbits(h_M),sKminus_iff_sE);
	args3[0] = Z3_mk_implies(obj->m_ctx,obj->m_loopK,sKminus_eq_sE);

	args3[1] = Z3_mk_iff(obj->m_ctx,inL_K,
								mk_or(obj->m_ctx,*h_inL_Kminus,obj->m_loopK));

	args3[2] = Z3_mk_implies(obj->m_ctx,*h_inL_Kminus,Z3_mk_not(obj->m_ctx,obj->m_loopK));


	return Z3_mk_and(obj->m_ctx,3,args3);
}
static Z3_ast idbsc_loop_kdependent(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	idkripke_t *h_M = obj->mh_phi->mh_M;
	Z3_ast *h_sK, *h_sE, sK_iff_sE[h_M->nbits(h_M)], sK_eq_sE, *h_inLoopK;
	int i, K = obj->length(obj)-1;

	h_sK 			= (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K);
	h_sE 			= (Z3_ast *)obj->mp_bitvecE;
	h_inLoopK		= (Z3_ast *)obj->mp_inLoop_array->get_at(obj->mp_inLoop_array,K);
	for(i=0;i<h_M->nbits(h_M);i++) {
		sK_iff_sE[i] = Z3_mk_iff(obj->m_ctx,h_sK[i],h_sE[i]);
	}
	sK_eq_sE = Z3_mk_and(obj->m_ctx,h_M->nbits(h_M),sK_iff_sE);


	return mk_and(obj->m_ctx, 	Z3_mk_iff(obj->m_ctx,obj->m_loop_exists,*h_inLoopK),sK_eq_sE);
}

static void             idbsc_print_model(idbsc_t *obj, Z3_model m, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

    unsigned num_constants;
    unsigned i, j, l, si;
    idlist_t *h_label;
    idformula_t *h_phi = obj->mh_phi;
    idkripke_t *h_M = h_phi->mh_M;
    int K = obj->length(obj), nbits = obj->nbits(obj), nphi = h_phi->length(h_phi);
    int bit, k, L;
    int path[K], h_sE;
    idbool_t value, exists, inL[K], phiK[nphi][K+1], phiE[nphi], phiL[nphi], aux_phiK[nphi][K], aux_phiE[nphi];
    char str[80], *pstr;

    if (!m) return;

    for(i=0;i<(unsigned)K;i++) {
    	path[i] = 0;
    	inL[i] = FALSE;
    }
    for(j=0;j<(unsigned)nphi;j++) {
    	phiE[j] = FALSE;
    	phiL[j] = FALSE;
    	aux_phiE[j] = FALSE;
        for(i=0;i<(unsigned)K;i++) {
        	phiK[j][i] = FALSE;
        	aux_phiK[j][i] = FALSE;
        }
        phiK[j][K] = FALSE;
    }
    h_sE = 0;
    exists = FALSE;

    num_constants = Z3_model_get_num_consts(obj->m_ctx, m);
    for (i = 0; i < num_constants; i++) {
        Z3_symbol name;
        Z3_func_decl cnst = Z3_model_get_const_decl(obj->m_ctx, m, i);
        Z3_ast a, v;
        Z3_bool ok;
        Z3_lbool val;
        name = Z3_get_decl_name(obj->m_ctx, cnst);
        sprintf(str,"%s",Z3_get_symbol_string (obj->m_ctx, name));

        a = Z3_mk_app(obj->m_ctx, cnst, 0, 0);
        v = a;
        ok = Z3_model_eval(obj->m_ctx, m, a, 1, &v);
        val = Z3_get_bool_value(obj->m_ctx, v);
        if (val == Z3_L_TRUE) {
        	value = TRUE;
        } else {
        	value = FALSE;
        }

        // parse generated path
        if (str[0]=='b') {
        	pstr = strchr(str,'_'); // returns a pointer to char '_'
        	assert(pstr!=NULL);

        	*pstr = '\0';
        	bit = atoi(str+1); // takes the left integer
        	if (*(pstr+1)=='E') {
            	if (value)
            		h_sE += (int)pow(2,bit);
        	} else {
             	k = atoi(pstr+1);// takes the right integer
            	if (value) {
            		path[k] += (int)pow(2,bit);
            	}
        	}
        } else if (str[0]=='l') {
        	if (value) {
            	pstr = strchr(str,'_'); // returns a pointer to char '_'
            	assert(pstr!=NULL);
            	L = atoi(pstr+1);// takes the right integer
        	}
        } else if (strcmp(str,"exists")==0) {
           	if (value) {
           		exists = TRUE;
           	}
        } else if (strncmp(str,"inL",3)==0) {
        	if (value) {
            	pstr = strchr(str,'_'); // returns a pointer to char '_'
            	assert(pstr!=NULL);
            	k = atoi(pstr+1);// takes the right integer
            	inL[k] = TRUE;
        	}
        } else if (strncmp(str,"phiE",4)==0) {
        	if (value) {
        		bit = atoi(str+4);
        		phiE[bit] = TRUE;
        	}
        } else if (strncmp(str,"phiL",4)==0) {
        	if (value) {
        		bit = atoi(str+4);
        		phiL[bit] = TRUE;
        	}
        } else if (strncmp(str,"phi",3)==0) {
        	if (value) {
            	pstr = strchr(str,'_'); // returns a pointer to char '_'
            	assert(pstr!=NULL);
            	*pstr = '\0';
        		bit = atoi(str+3); // takes the left integer
        		k = atoi(pstr+1);// takes the right integer
        		phiK[bit][k] = TRUE;

        	}
        } else if (strncmp(str,"aux_phiE",8)==0) {
        	if (value) {
        		bit = atoi(str+8);
        		aux_phiE[bit] = TRUE;
        	}
        } else if (strncmp(str,"aux_phi",7)==0) {
        	if (value) {
            	pstr = strchr(str+7,'_'); // returns a pointer to char '_'
            	assert(pstr!=NULL);
            	*pstr = '\0';
        		bit = atoi(str+7); // takes the left integer
        		k = atoi(pstr+1);// takes the right integer
        		aux_phiK[bit][k] = TRUE;

        	}
       }
    }

    // print the path
    if (exists)
    	fprintf(out,"Loop exists.\n");
    else
    	fprintf(out,"no loop.\n");
    fprintf(out,"run = ");
    for(i=0;i<(unsigned)K;i++) {
    	if ((exists)&&(L==i))
    		fprintf(out,"(");
    	si = obj->mh_phi->mh_M->get_state_from_bits(obj->mh_phi->mh_M,path[i]);
    	fprintf(out,"s[%d]", si);
    }
	if (exists)
		fprintf(out,")^w\n");
	else
		fprintf(out,"\n");
    fprintf(out,"path = ");
    for(i=0;i<(unsigned)K;i++) {
    	if ((exists)&&(L==i))
    		fprintf(out,"(");
    	si = obj->mh_phi->mh_M->get_state_from_bits(obj->mh_phi->mh_M,path[i]);
    	h_label = h_M->mp_state_label_map->get_values2_at(h_M->mp_state_label_map,&si);
    	fprintf(out,"{%s}", idintlist_to_str(h_label,str));
    }
	if (exists)
		fprintf(out,")^w\n");
	else
		fprintf(out,"\n");
    fprintf(out,"h_sE = %d\n", h_sE);
    fprintf(out,"inL = ");
    for(i=0;i<(unsigned)K;i++) {
    	if (inL[i]) {
    		fprintf(out,"T");
    	} else {
    		fprintf(out,"F");
    	}
    }
	fprintf(out,"\n");
    for(j=0;j<(unsigned)nphi;j++) {
    	fprintf(out,"phi%d = ", j);
        for(i=0;i<(unsigned)K+1;i++) {
        	if (phiK[j][i]) {
        		fprintf(out,"T");
        	} else {
        		fprintf(out,"F");
        	}
        }
    	fprintf(out,"\n");
    }
    for(j=0;j<(unsigned)nphi;j++) {
    	fprintf(out,"aux_phi%d = ", j);
        for(i=0;i<(unsigned)K;i++) {
        	if (aux_phiK[j][i]) {
        		fprintf(out,"T");
        	} else {
        		fprintf(out,"F");
        	}
        }
    	fprintf(out,"\n");
    }
    for(j=0;j<(unsigned)nphi;j++) {
    	fprintf(out,"phiE%d = ", j);
		if (phiE[j]) {
			fprintf(out,"true\n");
		} else {
			fprintf(out,"false\n");
		}
    	fprintf(out,"phiL%d = ", j);
		if (phiL[j]) {
			fprintf(out,"true\n");
		} else {
			fprintf(out,"false\n");
		}
    	fprintf(out,"aux_phiE%d = ", j);
		if (aux_phiE[j]) {
			fprintf(out,"true\n");
		} else {
			fprintf(out,"false\n");
		}
    }
}
//static void             idbsc_print_run(idbsc_t *obj, Z3_model m, FILE *out) {
//	assert(obj!=NULL);
//	assert(out!=NULL);
//	assert(obj->mh_phi!=NULL);
//	assert(obj->mh_phi->mh_M!=NULL);
//
//    unsigned num_constants;
//    unsigned i, j, l, si;
//    idlist_t *h_label;
//    idformula_t *h_phi = obj->mh_phi;
//    idkripke_t *h_M = h_phi->mh_M;
//    int K = obj->length(obj), nbits = obj->nbits(obj), nphi = h_phi->length(h_phi);
//    int bit, k, L;
//    int path[K], h_sE;
//    idbool_t value, exists, inL[K], phiK[nphi][K+1], phiE[nphi], phiL[nphi], aux_phiK[nphi][K], aux_phiE[nphi];
//    char str[80], *pstr;
//
//    if (!m) return;
//
//    for(i=0;i<(unsigned)K;i++) {
//    	path[i] = 0;
//    	inL[i] = FALSE;
//    }
//    for(j=0;j<(unsigned)nphi;j++) {
//    	phiE[j] = FALSE;
//    	phiL[j] = FALSE;
//    	aux_phiE[j] = FALSE;
//        for(i=0;i<(unsigned)K;i++) {
//        	phiK[j][i] = FALSE;
//        	aux_phiK[j][i] = FALSE;
//        }
//        phiK[j][K] = FALSE;
//    }
//    h_sE = 0;
//    exists = FALSE;
//
//    num_constants = Z3_model_get_num_consts(obj->m_ctx, m);
//    for (i = 0; i < num_constants; i++) {
//        Z3_symbol name;
//        Z3_func_decl cnst = Z3_model_get_const_decl(obj->m_ctx, m, i);
//        Z3_ast a, v;
//        Z3_bool ok;
//        Z3_lbool val;
//        name = Z3_get_decl_name(obj->m_ctx, cnst);
//        sprintf(str,"%s",Z3_get_symbol_string (obj->m_ctx, name));
//
//        a = Z3_mk_app(obj->m_ctx, cnst, 0, 0);
//        v = a;
//        ok = Z3_model_eval(obj->m_ctx, m, a, 1, &v);
//        val = Z3_get_bool_value(obj->m_ctx, v);
//        if (val == Z3_L_TRUE) {
//        	value = TRUE;
//        } else {
//        	value = FALSE;
//        }
//
//        // parse generated path
//        if (str[0]=='b') {
//        	pstr = strchr(str,'_'); // returns a pointer to char '_'
//        	assert(pstr!=NULL);
//
//        	*pstr = '\0';
//        	bit = atoi(str+1); // takes the left integer
//        	if (*(pstr+1)=='E') {
//            	if (value)
//            		h_sE += (int)pow(2,bit);
//        	} else {
//             	k = atoi(pstr+1);// takes the right integer
//            	if (value) {
//            		path[k] += (int)pow(2,bit);
//            	}
//        	}
//        } else if (str[0]=='l') {
//        	if (value) {
//            	pstr = strchr(str,'_'); // returns a pointer to char '_'
//            	assert(pstr!=NULL);
//            	L = atoi(pstr+1);// takes the right integer
//        	}
//        } else if (strcmp(str,"exists")==0) {
//           	if (value) {
//           		exists = TRUE;
//           	}
//        } else if (strncmp(str,"inL",3)==0) {
//        	if (value) {
//            	pstr = strchr(str,'_'); // returns a pointer to char '_'
//            	assert(pstr!=NULL);
//            	k = atoi(pstr+1);// takes the right integer
//            	inL[k] = TRUE;
//        	}
//        } else if (strncmp(str,"phiE",4)==0) {
//        	if (value) {
//        		bit = atoi(str+4);
//        		phiE[bit] = TRUE;
//        	}
//        } else if (strncmp(str,"phiL",4)==0) {
//        	if (value) {
//        		bit = atoi(str+4);
//        		phiL[bit] = TRUE;
//        	}
//        } else if (strncmp(str,"h_phi",3)==0) {
//        	if (value) {
//            	pstr = strchr(str,'_'); // returns a pointer to char '_'
//            	assert(pstr!=NULL);
//            	*pstr = '\0';
//        		bit = atoi(str+3); // takes the left integer
//        		k = atoi(pstr+1);// takes the right integer
//        		phiK[bit][k] = TRUE;
//
//        	}
//        } else if (strncmp(str,"aux_phiE",8)==0) {
//        	if (value) {
//        		bit = atoi(str+8);
//        		aux_phiE[bit] = TRUE;
//        	}
//        } else if (strncmp(str,"aux_phi",7)==0) {
//        	if (value) {
//            	pstr = strchr(str+7,'_'); // returns a pointer to char '_'
//            	assert(pstr!=NULL);
//            	*pstr = '\0';
//        		bit = atoi(str+7); // takes the left integer
//        		k = atoi(pstr+1);// takes the right integer
//        		aux_phiK[bit][k] = TRUE;
//
//        	}
//       }
//    }
//
//    // print the path
//    fprintf(out,"run = ");
//    for(i=0;i<(unsigned)K;i++) {
//    	if ((exists)&&(L==i))
//    		fprintf(out,"(");
//    	si = obj->mh_phi->mh_M->get_state_from_bits(obj->mh_phi->mh_M,path[i]);
//    	fprintf(out,"s[%d]", si);
//    }
//	if (exists)
//		fprintf(out,")^w\n");
//	else
//		fprintf(out,"\n");
//    fprintf(out,"path = ");
//    for(i=0;i<(unsigned)K;i++) {
//    	if ((exists)&&(L==i))
//    		fprintf(out,"(");
//    	si = obj->mh_phi->mh_M->get_state_from_bits(obj->mh_phi->mh_M,path[i]);
//    	h_label = h_M->mp_state_label_map->get_values2_at(h_M->mp_state_label_map->get_values2_at,&si);
//    	fprintf(out,"{%s}", idintlist_to_str(h_label,str));
//    }
//	if (exists)
//		fprintf(out,")^w\n");
//	else
//		fprintf(out,"\n");
//
//}

static Z3_ast *idbsc_mk_bitvec(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	idkripke_t *h_M = obj->mh_phi->mh_M;
	Z3_ast *p_bitvec;

	int K = obj->m_length;
	p_bitvec = h_M->z3_bitvec_at(h_M,obj->m_ctx,K);
	obj->mp_p_bitvec_array->append(obj->mp_p_bitvec_array,p_bitvec);
	obj->m_length++;

	return p_bitvec;
}


int           idbsc_nbits(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	return obj->mh_phi->mh_M->nbits(obj->mh_phi->mh_M);
}


idbsc_t *     	idbsc(double delta, double delta_progress, idformula_t *h_phi) {
#ifdef IDBSC_DEBUG_CREATE
	printf("idbsc:idbsc: starting...\n");
#endif

	idbsc_t *obj = malloc(sizeof(idbsc_t));
	assert(obj!=NULL);

	obj->destroy = idbsc_destroy;
	obj->check = idbsc_check;
	obj->length = idbsc_length;
	obj->nstates = idbsc_nstates;
	obj->nbits = idbsc_nbits;
	obj->is_sat = idbsc_is_sat;
	obj->decode = idbsc_decode;
	obj->add_cex_and_check = idbsc_add_cex_and_check;
	obj->is_unsat = idbsc_is_unsat;


	int i;
	char str[80];

#ifdef IDRTL_DEBUG_MEMLEAK_BSC
	static int idbsc_last_id = 0;
	obj->m_id = idbsc_last_id;
	printf("debug memleak construct bsc %d\n", obj->m_id);
	idbsc_last_id++;
	idbsc_destroy_cnt++;
#endif
	obj->mh_phi   = h_phi;
	obj->m_length = 0;
	obj->m_ctx    = mk_context();
	obj->m_s      = mk_solver(obj->m_ctx);
	obj->m_issat  = FALSE;
	obj->mp_cex   = idcex(h_phi->mh_M);
	obj->mp_p_bitvec_array = idarray_byptr(free,NULL);
	obj->mp_p_bitvec_phi_array = idarray_byptr(free,NULL);
	obj->mp_inLoop_array = idarray_byvalue(sizeof(Z3_ast));
	obj->m_delta = delta;
	obj->m_delta_progress = delta_progress;
	idbsc_init_loopvars(obj);

#ifdef IDBSC_DEBUG_CREATE
	printf("idbsc:idbsc: END.\n");
#endif

	return obj;
}
void             idbsc_destroy(idbsc_t *obj) {
	int i;
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_BSC
		idbsc_destroy_cnt--;
		printf("debug memleak destroy bsc %d still remains %d to destroy\n", obj->m_id, idbsc_destroy_cnt);
		if (idbsc_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_p_bitvec_array);
		idarray_destroy(obj->mp_p_bitvec_phi_array);
		idarray_destroy(obj->mp_inLoop_array);
		Z3_del_context(obj->m_ctx);
		idcex_destroy(obj->mp_cex);
		free(obj->mp_bitvecE);
		free(obj);
	}
}

int           idbsc_nstates(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);
	return obj->mh_phi->mh_M->m_nstates;
}

int           idbsc_length(idbsc_t *obj) {
	assert(obj!=NULL);
	return obj->m_length;
}

idbool_t           idbsc_check(idbsc_t *obj) {
#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: starting...\n");
#endif

	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);
	int i, j, K = obj->length(obj);
	char str[80];
	idformula_t *h_phi = obj->mh_phi;
	idkripke_t *h_M = h_phi->mh_M;
	idcex_t *h_cex = obj->mp_cex;
	Z3_ast *h_bitvecK, *h_bitvecKminus, a, *h_inLoopK;
	obj->m_issat = FALSE;


#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: create state bit vector at K=%d...\n", K);
#endif
	h_bitvecK = idbsc_mk_bitvec(obj);

#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: declare formula at K=%d...\n", K);
#endif
	h_phi->declare_at(h_phi,obj->m_ctx,K);

	idbsc_mk_bitvec_phi(obj);

	if (K==0) {
#ifdef IDBSC_DEBUG_L2
		printf("debug idbsc_check: initialize...\n", K);
#endif
		// initial conditions
		a = h_M->z3_state(h_M,obj->m_ctx,h_bitvecK,h_M->m_initial);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);

		// base
		a = idbsc_loop_base(obj);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);

		h_phi->assert_base(h_phi,obj->m_ctx,obj->m_s);
		h_phi->assert_eventrtl_base(h_phi,obj->m_ctx,obj->m_s,obj->m_loop_exists);
		h_phi->assert_laststate_base(h_phi,obj->m_ctx,obj->m_s,obj->m_loop_exists);

	} else {
#ifdef IDBSC_DEBUG_L2
		printf("debug idbsc_check: declare transition\n", K);
#endif
		h_bitvecKminus = (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K-1);
		a = h_M->z3_edges(h_M,obj->m_ctx,h_bitvecKminus,h_bitvecK);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);
		a = h_M->z3_transitions(h_M,obj->m_ctx,h_bitvecKminus,h_bitvecK);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);

		a = idbsc_loop_atK(obj);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);
	}

#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: assert formula\n", K);
#endif
	h_inLoopK = (Z3_ast *)obj->mp_inLoop_array->get_at(obj->mp_inLoop_array,K);
	assert(h_inLoopK!=NULL);
	h_phi->assert_at(h_phi,obj->m_ctx,obj->m_s,K,h_bitvecK);
	h_phi->assert_eventrtl_at(h_phi,obj->m_ctx,obj->m_s,K,*h_inLoopK);
	h_phi->assert_laststate_at(h_phi,obj->m_ctx,obj->m_s,K,obj->m_loopK);
	h_cex->z3_transitions(h_cex,obj->m_ctx,obj->m_s,h_bitvecK,K);



	Z3_solver_push(obj->m_ctx,obj->m_s);
	// Now start k-dependent constraints

#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: assert k-independent constraints\n", K);
#endif
	a = h_M->z3_unmarked(h_M,obj->m_ctx,h_bitvecK);
	Z3_solver_assert(obj->m_ctx,obj->m_s,a);


	a = idbsc_loop_kdependent(obj);
	Z3_solver_assert(obj->m_ctx,obj->m_s,a);

	h_phi->assert_eventrtl_kdependent(h_phi,obj->m_ctx,obj->m_s);
	h_phi->assert_laststate_kdependent(h_phi,obj->m_ctx,obj->m_s);
	h_cex->z3_marked(h_cex,obj->m_ctx,obj->m_s,obj->m_loop_exists,K);

#ifdef IDBSC_DEBUG_L2
    	printf("debug idbsc_check: s =\n%s",Z3_solver_to_string(obj->m_ctx, obj->m_s));
#endif
    Z3_lbool result = Z3_solver_check(obj->m_ctx, obj->m_s);
    switch (result) {
    case Z3_L_FALSE:
#ifdef IDBSC_DEBUG_L1
    	printf("check: unsat\n");
#endif
    	obj->m_issat = FALSE;
        break;
    case Z3_L_UNDEF:
        printf("unknown\n");
        Z3_model m      = Z3_solver_get_model(obj->m_ctx, obj->m_s);
        if (m) Z3_model_inc_ref(obj->m_ctx, m);
        	printf("potential model:\n%s\n", Z3_model_to_string(obj->m_ctx, m));
        break;
    case Z3_L_TRUE:
#ifdef IDBSC_DEBUG_L1
    	printf("check: sat\n");
#endif
    	obj->m_issat = TRUE;
       	break;
   	}

    assert((result==Z3_L_FALSE)||(result==Z3_L_TRUE));
    Z3_solver_pop(obj->m_ctx,obj->m_s,1);

#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: END.\n");
#endif
	return obj->m_issat;

}

idfsearch_t *         idbsc_decode(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	if (!obj->is_sat(obj)) {
		return NULL;
	}

	Z3_model m = Z3_solver_get_model(obj->m_ctx, obj->m_s);
#ifdef IDBSC_DEBUG_L2
	printf("debug idbsc_check: K = %d\n", obj->length(obj));
   	printf("debug idbsc_check: model =\n");
    idbsc_print_model(obj,m,stdout);
//   	printf("debug idbsc_check: run =\n");
//	idbsc_print_run(obj,m,stdout);
	if (obj->mp_cex->mp_p_cexs_array->length(obj->mp_cex->mp_p_cexs_array)>0) {
		obj->mp_cex->z3_print(obj->mp_cex,obj->m_ctx,m,obj->length(obj),stdout);
	}
#endif

	return idfsearch_decode(obj->m_delta, obj->m_delta_progress, obj->mh_phi->mh_M,obj->m_ctx, m, obj->length(obj));
}

idbool_t      idbsc_is_sat(idbsc_t *obj) {
	assert(obj!=NULL);

	return obj->m_issat;
}

idbool_t             idbsc_add_cex_and_check(idbsc_t *obj, idarray_t *h_states_intarray, idbool_t isfair) {
	assert(obj!=NULL);
	assert(h_states_intarray!=NULL);

	int k, K = obj->length(obj)-1;
	Z3_ast *bitvecK, a;
	idformula_t *h_phi = obj->mh_phi;
	idkripke_t *h_M = h_phi->mh_M;
	idcex_t *h_cex = obj->mp_cex;

	h_cex->add_cex(h_cex,h_states_intarray,isfair,K);

	for(k=1;k<obj->length(obj);k++) {
		bitvecK = obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,k);

		h_cex->z3_init_last_cex_at(h_cex,obj->m_ctx,obj->m_s,k,bitvecK);
	}

	Z3_solver_push(obj->m_ctx,obj->m_s);
	// Now start k-dependent constraints



	a = idbsc_loop_kdependent(obj);
	Z3_solver_assert(obj->m_ctx,obj->m_s,a);

	h_phi->assert_eventrtl_kdependent(h_phi,obj->m_ctx,obj->m_s);
	h_phi->assert_laststate_kdependent(h_phi,obj->m_ctx,obj->m_s);
	a = h_M->z3_unmarked(h_M,obj->m_ctx,bitvecK);
	Z3_solver_assert(obj->m_ctx,obj->m_s,a);
	h_cex->z3_marked(h_cex,obj->m_ctx,obj->m_s,obj->m_loop_exists,K);
#ifdef IDBSC_DEBUG_L2
//    	printf("debug add_cex_and_check: s =\n%s",Z3_solver_to_string(obj->m_ctx, obj->m_s));
#endif

    Z3_lbool result = Z3_solver_check(obj->m_ctx, obj->m_s);
    switch (result) {
    case Z3_L_FALSE:
#ifdef IDBSC_DEBUG_L1
    	printf("add_cex_and_check: unsat\n");
#endif
    	obj->m_issat = FALSE;
        break;
    case Z3_L_UNDEF:
        printf("unknown\n");
        Z3_model m      = m = Z3_solver_get_model(obj->m_ctx, obj->m_s);
        if (m) Z3_model_inc_ref(obj->m_ctx, m);
        	printf("potential model:\n%s\n", Z3_model_to_string(obj->m_ctx, m));
        break;
    case Z3_L_TRUE:
#ifdef IDBSC_DEBUG_L1
    	printf("add_cex_and_check: sat\n");
#endif
    	obj->m_issat = TRUE;
       	break;
   	}

    assert((result==Z3_L_FALSE)||(result==Z3_L_TRUE));
    Z3_solver_pop(obj->m_ctx,obj->m_s,1);

	return obj->m_issat;
}


idbool_t             idbsc_is_unsat(idbsc_t *obj) {
	assert(obj!=NULL);
	assert(obj->mh_phi!=NULL);
	assert(obj->mh_phi->mh_M!=NULL);

	if (obj->is_sat(obj)) {
		return FALSE;
	}

	int K = obj->length(obj);
	int i ,j;
	Z3_ast args4[4], a, *h_bitvecK, *h_bitveci, *h_bitvecj, *h_bitvec_phii, *h_bitvec_phij, *h_inLoopi, *h_inLoopj;
	idbool_t isunsat;
	idkripke_t * h_M = obj->mh_phi->mh_M;
	idcex_t *h_cex = obj->mp_cex;

	Z3_solver_push(obj->m_ctx,obj->m_s);

	h_bitvecK = obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,K-1);
	a = h_M->z3_unmarked(h_M,obj->m_ctx,h_bitvecK);
	Z3_solver_assert(obj->m_ctx,obj->m_s,a);
	h_cex->z3_marked(h_cex,obj->m_ctx,obj->m_s,obj->m_loop_exists,K-1);

	for(i=0;i<K-1;i++) {
		h_bitveci = (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,i);
		h_bitvec_phii = (Z3_ast *)obj->mp_p_bitvec_phi_array->get_at(obj->mp_p_bitvec_phi_array,i);
		h_inLoopi = (Z3_ast *)obj->mp_inLoop_array->get_at(obj->mp_inLoop_array,i);
		for(j=i+1;j<K;j++) {
			h_bitvecj = (Z3_ast *)obj->mp_p_bitvec_array->get_at(obj->mp_p_bitvec_array,j);
			h_bitvec_phij = (Z3_ast *)obj->mp_p_bitvec_phi_array->get_at(obj->mp_p_bitvec_phi_array,j);
			h_inLoopj = (Z3_ast *)obj->mp_inLoop_array->get_at(obj->mp_inLoop_array,j);
			args4[0] = mk_bitvec_neq(obj->m_ctx,h_bitveci,h_bitvecj,obj->nbits(obj));
			args4[1] = Z3_mk_not(obj->m_ctx,*h_inLoopi);
			args4[2] = Z3_mk_not(obj->m_ctx,*h_inLoopj);
			args4[3] = mk_bitvec_neq(obj->m_ctx,h_bitvec_phii,h_bitvec_phij,obj->m_nphi);
		}
		a = Z3_mk_or(obj->m_ctx,4,args4);
		Z3_solver_assert(obj->m_ctx,obj->m_s,a);
	}

//	printf("idbsc:debug_formula: solver =\n%s\n",Z3_solver_to_string (obj->m_ctx, obj->m_s));

    Z3_lbool result = Z3_solver_check(obj->m_ctx, obj->m_s);
    Z3_model m;
    switch (result) {
    case Z3_L_FALSE:
#ifdef IDBSC_DEBUG_L1
    	printf("is_unsat: TRUE\n");
#endif
    	isunsat = TRUE;
        break;
    case Z3_L_UNDEF:
        printf("is_unsat: unknown\n");
       m = Z3_solver_get_model(obj->m_ctx, obj->m_s);
        if (m) Z3_model_inc_ref(obj->m_ctx, m);
        	printf("potential model:\n%s\n", Z3_model_to_string(obj->m_ctx, m));
        break;
    case Z3_L_TRUE:
#ifdef IDBSC_DEBUG_L1
    	printf("is_unsat: FALSE\n");
#endif
    	isunsat = FALSE;
//    	m = Z3_solver_get_model(obj->m_ctx, obj->m_s);
//    	obj->debug_print_model(obj,m,stdout);
       	break;
   	}

    assert((result==Z3_L_FALSE)||(result==Z3_L_TRUE));
    Z3_solver_pop(obj->m_ctx,obj->m_s,1);

    return isunsat;
}
