/*
 * bsc.h
 *
 *  Created on: May 23, 2019
 *      Author: rafael
 */

#ifndef BSC_H_
#define BSC_H_


#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

extern idrobrun_t * idbsc_run_to_robrun(idrun_t *h_run, double delta, idkripke_t *h_M);
extern idbool_t idRTL(double delta, double delta_progress, idformula_t *h_phi, idrobrun_t **hp_solution);

/* public functions */
extern idbsc_t *     	idbsc(double delta, double delta_progress, idformula_t *h_phi);
extern void             idbsc_destroy(idbsc_t *obj);

/* get functions */
extern int           idbsc_length(idbsc_t *obj);
extern int           idbsc_nstates(idbsc_t *obj);
extern int           idbsc_nbits(idbsc_t *obj);
extern idbool_t      idbsc_is_sat(idbsc_t *obj);

/* operation */
extern idbool_t             idbsc_check(idbsc_t *obj);
extern idbool_t             idbsc_is_unsat(idbsc_t *obj);
extern idbool_t             idbsc_add_cex_and_check(idbsc_t *obj, idarray_t *h_states_intarray, idbool_t isfair);
extern idfsearch_t *         idbsc_decode(idbsc_t *obj);

#ifdef IDRTL_DEBUG_MEMLEAK_BSC
	static int		    idbsc_destroy_cnt = 0;
#endif

struct idbsc_s{
	/* capsulated member functions */
	void             (*destroy)(idbsc_t *obj);

	/* get functions */
	int          (*length)(idbsc_t *obj);
	int          (*nstates)(idbsc_t *obj);
	int          (*nbits)(idbsc_t *obj);
	idbool_t     (*is_sat)(idbsc_t *obj);

	/* operation */
	idbool_t		(*check)(idbsc_t *obj);
	idbool_t     	(*is_unsat)(idbsc_t *obj);
	idbool_t		(*add_cex_and_check)(idbsc_t *obj, idarray_t *h_states_intarray, idbool_t isfair);
	idkripke_t *    (*decode)(idbsc_t *obj);

	/* aux functions */
	Z3_ast           (*state_at)(idbsc_t *obj, int state, int k);
	Z3_ast           (*transitions_at)(idbsc_t *obj, int k);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_BSC
	int		      m_id;
#endif
	Z3_context 	 m_ctx;
	Z3_solver 	 m_s;
	idarray_t	*mp_p_bitvec_array,     // Bit vector representing the states
				*mp_p_bitvec_phi_array, // Bit vector representing the formulas
				*mp_inLoop_array;
	Z3_ast       m_loopK,
				 m_loop_exists,
				*mp_bitvecE;
	idformula_t *mh_phi;
	int			 m_length,
				 m_nphi; // number of formulas
	idbool_t     m_issat;
	idcex_t     *mp_cex;
	double 		m_delta,
				m_delta_progress;

};



#ifdef __cplusplus
}
#endif


#endif /* BSC_H_ */
