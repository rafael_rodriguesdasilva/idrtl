/*
 * cex.c
 *
 *  Created on: Jun 2, 2019
 *      Author: rafael
 */

#include"cex.h"

//#define IDCEX_DEBUG_L1
//#define IDCEX_DEBUG_L2


/* public functions */
idcex_t *     idcex(idkripke_t *h_M) {
	assert(h_M!=NULL);

	idcex_t *obj = malloc(sizeof(idcex_t));
	assert(obj!=NULL);

#ifdef IDRTL_DEBUG_MEMLEAK_CEX
	static int idcex_last_id = 0;
	obj->m_id = idcex_last_id;
	printf("debug memleak construct cex %d\n", obj->m_id);
	idcex_last_id++;
	idcex_destroy_cnt++;
#endif
	obj->mh_M = h_M;
	obj->mp_p_cexs_array = idarray_byptr(idautomaton_destroy,idautomaton_cpy);

	obj->add_cex = idcex_add_cex;
	obj->destroy = idcex_destroy;
	obj->z3_transitions = idcex_z3_transitions;
	obj->z3_marked = idcex_z3_marked;
	obj->z3_init_last_cex_at = idcex_z3_init_last_cex_at;
	obj->z3_print = idcex_z3_print;

	return obj;
}
void          idcex_destroy(idcex_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_CEX
		idcex_destroy_cnt--;
		printf("debug memleak destroy cex %d still remains %d to destroy\n", obj->m_id, idcex_destroy_cnt);
		if (idcex_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_p_cexs_array);
		free(obj);
	}
}

/* set functions */
void          idcex_add_cex(idcex_t *obj, idarray_t *h_states_intarray, idbool_t isfair) {
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:add_cex: starting...\n");
#endif
	assert(obj!=NULL);
	assert(obj->mh_M!=NULL);
	assert(h_states_intarray!=NULL);

	int 			i, s0, s1, s2, q, s, q_dump, q_end,
					nstates = h_states_intarray->length(h_states_intarray); // minus initial and plus dump states
	idrb_node_t 	*it, *it1,
					*h_s_list = h_states_intarray->mp_head;

	q_dump = nstates-1; // dump state
	q_end = nstates-2; // final state


	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	// First, we compute the complement of prefix of Mplan with length len as an automaton
	// We create a dump state, which means that is not a prefix of Mplan with length len. This is the only unmarked state.
	// if isfair is true, it is a Buchi automaton
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:add_cex: prefix = {");
	idrb_traverse(it,h_states_intarray->mp_head) {
		if (it!=idrb_first(h_states_intarray->mp_head)) {
			printf(",");
		}
		printf("%d",*(int *)it->value(it));
	}
	printf("}\n");
	printf("debug idcex:add_cex: creating deterministic %s automatong with %d states and %d symbols\n",
			(isfair?"Buechi":"finite"), nstates, obj->mh_M->nstates(obj->mh_M));
#endif
	idautomaton_t *p_A = idautomaton(nstates,obj->mh_M->nstates(obj->mh_M),isfair);
	// Note that we do not add an initial state during the conversion to automaton, because the system M always starts in a
	// single initial state.

	q = 0;
	it = idrb_first(h_s_list);
	s1 = *(int *)it->value(it);
	it = idrb_next(it);
	s2 = *(int *)it->value(it);
	it = idrb_next(it);
	while(it != idrb_nil(h_s_list)) {
		s0 = s1;
		s1 = s2;
		s2 = *(int *)it->value(it);
		it = idrb_next(it);
#ifdef IDCEX_DEBUG_L1
		if ((q==0)||(s0==s2)) {
			printf("debug idcex:add_cex: Post(%d) = {%d, %d}\n",s1, s1, s2);
		} else {
			printf("debug idcex:add_cex: Post(%d) = {%d, %d, %d}\n",s1, s0, s1, s2);
		}
#endif
		if ((q>0)&&(s0!=s2)) {
#ifdef IDCEX_DEBUG_L1
			printf("debug idcex:add_cex: adding transition (q_%d,s_%d,q_%d)\n",q,s0,q-1);
#endif
			p_A->add_transition(p_A,q,q-1,s0);
		}
#ifdef IDCEX_DEBUG_L1
		printf("debug idcex:add_cex: adding transition (q_%d,s_%d,q_%d)\n",q,s1,q);
#endif
		p_A->add_transition(p_A,q,q,s1);
#ifdef IDCEX_DEBUG_L1
		printf("debug idcex:add_cex: adding transition (q_%d,s_%d,q_%d)\n",q,s2,q+1);
#endif
		p_A->add_transition(p_A,q,q+1,s2);

#ifdef IDCEX_DEBUG_L2
		printf("debug idcex:add_cex: adding transition to dump state q_%d\n",q_dump);
#endif
		// All symbols not in transitions of Mplan go to dump state.
		idrb_traverse(it1,obj->mh_M->mp_state_bit_map->mp_values2_at) {
			s = *(int *)it1->keyg(it1);
			if (((q==0)||(s!=s0))&&(s!=s1)&&(s!=s2)) {
#ifdef IDCEX_DEBUG_L1
				printf("debug idcex:add_cex: adding transition (q_%d,s_%d,q_%d)\n",q,s,q_dump);
#endif
				p_A->add_transition(p_A,q,q_dump,s);
			}
		}
		q++;
	}

	// Once in the last state, always in last state.
	// Once in the dump state, always in dump state.
#ifdef IDCEX_DEBUG_L2
	printf("debug idcex:add_cex: adding transition to last q_%d and dump q_%d states\n",q_end, q_dump);
#endif
	idrb_traverse(it1,obj->mh_M->mp_state_bit_map->mp_values2_at) {
		s = *(int *)it1->keyg(it1);
#ifdef IDCEX_DEBUG_L1
		printf("debug idcex:add_cex: adding to end transition (q_%d,s_%d,q_%d)\n",q_end,s,q_end);
#endif
		p_A->add_transition(p_A,q_end,q_end,s);
#ifdef IDCEX_DEBUG_L1
		printf("debug idcex:add_cex: adding to dump transition (q_%d,s_%d,q_%d)\n",q_dump,s,q_dump);
#endif
		p_A->add_transition(p_A,q_dump,q_dump,s);
	}

	p_A->mp_marked->push(p_A->mp_marked,&q_dump); // only dump state is marked
	/////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef IDCEX_DEBUG_L2
	printf("debug idcex:add_cex: final transition relation:\n");
	idintlisttable_print(p_A->mp_edges,stdout);
	printf("debug idcex:add_cex: it is a deterministic %s automaton with marked states:\n",(p_A->is_fair(p_A)?"Buechi":"finite"));
	idintlist_print(p_A->mp_marked,stdout);
#endif

	p_A->m_z3_id = obj->mp_p_cexs_array->length(obj->mp_p_cexs_array);
	obj->mp_p_cexs_array->append(obj->mp_p_cexs_array,p_A);

#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:add_cex: END.\n");
#endif
}

/* SMT operations */
void          idcex_z3_init_last_cex_at(idcex_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast *h_state_bitvecK) {
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:z3_init_last_cex_at %d: starting ...\n",K);
#endif
	assert(obj!=NULL);
	assert(h_state_bitvecK!=NULL);

	if (obj->mp_p_cexs_array->length(obj->mp_p_cexs_array)==0)
		return; // do nothing

	idrb_node_t *h_last = idrb_last(obj->mp_p_cexs_array->mp_head);
	idautomaton_t *h_A_last = (idautomaton_t *)h_last->value(h_last);

	Z3_ast *h_bitvecKminus,*h_bitvecK, a;



	if (K==1) {
		h_bitvecK = h_A_last->z3_bitvec_at(h_A_last,ctx,0);
#ifdef IDCEX_DEBUG_L2
		printf("debug idcex:z3_init_last_cex_at %d: set initial state\n",K);
#endif
		a = h_A_last->z3_state(h_A_last,ctx,h_bitvecK,h_A_last->m_initial);
		Z3_solver_assert(ctx,s,a);

#ifdef IDCEX_DEBUG_L2
		printf("%s\n",Z3_ast_to_string(ctx,a));
#endif
	}
	h_bitvecK = h_A_last->z3_bitvec_at(h_A_last,ctx,K);
	h_bitvecKminus = h_A_last->z3_bitvec_at(h_A_last,ctx,K-1);

#ifdef IDCEX_DEBUG_L2
	printf("debug idcex:z3_init_last_cex_at %d:\n",K);
#endif
	a = h_A_last->z3_transitions(h_A_last,ctx,h_bitvecKminus,h_bitvecK,h_state_bitvecK,obj->mh_M);
	Z3_solver_assert(ctx,s,a);

#ifdef IDCEX_DEBUG_L2
	printf("%s\n",Z3_ast_to_string(ctx,a));
#endif

#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:add_cex: z3_init_last_cex_at %d: END.\n",K);
#endif

}
void          idcex_z3_transitions(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast *h_state_bitvecK, int K) {
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:z3_transitions: starting ...\n");
#endif
	assert(obj!=NULL);
	assert(h_state_bitvecK!=NULL);


	Z3_ast *h_bitvecKminus,*h_bitvecK, a;
	idautomaton_t *h_A;
	idrb_node_t *it;

	if (obj->mp_p_cexs_array->length(obj->mp_p_cexs_array)==0)
		return; // do nothing


	idrb_traverse(it,obj->mp_p_cexs_array->mp_head) {
		h_A = (idautomaton_t *)it->value(it);

		if (K==1) {
			h_A->z3_bitvec_at(h_A,ctx,0);
		}
		h_bitvecK = h_A->z3_bitvec_at(h_A,ctx,K);
		h_bitvecKminus = h_A->z3_bitvec_at(h_A,ctx,K-1);

#ifdef IDCEX_DEBUG_L2
		printf("debug idcex:z3_transitions id = %d and K=%d:\n",h_A->m_z3_id,K);
#endif
		a = h_A->z3_transitions(h_A,ctx,h_bitvecKminus,h_bitvecK,h_state_bitvecK,obj->mh_M);
		Z3_solver_assert(ctx,s,a);

#ifdef IDCEX_DEBUG_L2
		printf("%s\n",Z3_ast_to_string(ctx,a));
#endif
	}

#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:z3_transitions: END.\n");
#endif
}
void          idcex_z3_marked(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists, int K) {
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:z3_marked at %d: starting ...\n", K);
#endif
	assert(obj!=NULL);

	if (obj->mp_p_cexs_array->length(obj->mp_p_cexs_array)==0)
		return; // do nothing

	Z3_ast *h_bitvecK, a;
	idautomaton_t *h_A;
	idrb_node_t *it;


	idrb_traverse(it,obj->mp_p_cexs_array->mp_head) {
		h_A = (idautomaton_t *)it->value(it);
		h_bitvecK = h_A->z3_bitvec_at(h_A,ctx,K);

#ifdef IDCEX_DEBUG_L2
		printf("debug idcex:z3_marked id = %d and K=%d:\n",h_A->m_z3_id,K);
#endif
		a = h_A->z3_marked(h_A,ctx,h_bitvecK,loop_exists);
		Z3_solver_assert(ctx,s,a);

#ifdef IDCEX_DEBUG_L2
		printf("%s\n",Z3_ast_to_string(ctx,a));
#endif
	}
#ifdef IDCEX_DEBUG_L1
	printf("debug idcex:z3_marked: END.\n");
#endif
}

void 		 idcex_z3_print(idcex_t *obj, Z3_context ctx, Z3_model m, int K, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);

    unsigned num_constants;
    unsigned i;
    idkripke_t *h_M = obj->mh_M;
    int ncexs = obj->mp_p_cexs_array->length(obj->mp_p_cexs_array);
    int id, k, bit, L;
    int path[ncexs][K];
    char str[80], *h_str1, *h_str2;
    idbool_t loop_exists = FALSE;


    if (!m) return;

    for(id=0;id<ncexs;id++) {
        for(k=0;k<K;k++) {
        	path[id][k] = 0;
        }
    }
    num_constants = Z3_model_get_num_consts(ctx, m);
    for (i = 0; i < num_constants; i++) {
        Z3_symbol name;
        Z3_func_decl cnst = Z3_model_get_const_decl(ctx, m, i);
        Z3_ast a, v;
        Z3_bool ok;
        Z3_lbool val;
        name = Z3_get_decl_name(ctx, cnst);
        sprintf(str,"%s",Z3_get_symbol_string (ctx, name));

        if (str[0]=='q') {
        	h_str1 = strchr(str,'_'); // returns a pointer to char '_'
        	assert(h_str1!=NULL);
        	h_str2 = strchr(h_str1+1,'_'); // returns a pointer to char 'b'
        	assert(h_str2!=NULL);

        	*h_str1 = '\0';
        	*h_str2 = '\0';
        	id  = atoi(str+1); // q
        	bit = atoi(h_str1+2); //_b
        	k   = atoi(h_str2+1); // _

            a = Z3_mk_app(ctx, cnst, 0, 0);
            v = a;
            ok = Z3_model_eval(ctx, m, a, 1, &v);
            val = Z3_get_bool_value(ctx, v);
            if (val == Z3_L_TRUE) {
            	path[id][k] += (int)pow(2,bit);
            }
        } else if (str[0]=='l') {
        	a = Z3_mk_app(ctx, cnst, 0, 0);
            v = a;
            ok = Z3_model_eval(ctx, m, a, 1, &v);
            val = Z3_get_bool_value(ctx, v);
            if (val == Z3_L_TRUE) {
            	h_str1 = strchr(str,'_'); // returns a pointer to char '_'
            	assert(h_str1!=NULL);
            	L = atoi(h_str1+1);// takes the right integer
        	}
        } else if (strcmp(str,"exists")==0) {
        	a = Z3_mk_app(ctx, cnst, 0, 0);
            v = a;
            ok = Z3_model_eval(ctx, m, a, 1, &v);
            val = Z3_get_bool_value(ctx, v);
            if (val == Z3_L_TRUE) {
            	loop_exists = TRUE;
            }
        }
    }

    // print the path
    for(id=0;id<ncexs;id++) {
    	fprintf(out,"cex[%d] = ",id);
        for(k=0;k<K;k++) {
           	if ((loop_exists)&&(L==i))
            		fprintf(out,"(");
            	fprintf(out,"q[%d]", path[id][k]);
        }
    	if (loop_exists)
    		fprintf(out,")^w\n");
    	else
    		fprintf(out,"\n");
    }
}
