/*
 * cex.h
 *
 *  Created on: Jun 2, 2019
 *      Author: rafael
 */

#ifndef CEX_H_
#define CEX_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


/* public functions */
extern idcex_t *     idcex(idkripke_t *M);
extern void          idcex_destroy(idcex_t *obj);

/* set functions */
extern void          idcex_add_cex(idcex_t *obj, idarray_t *h_states_intarray, idbool_t isfair);

/* SMT operations */
extern void          idcex_z3_init_last_cex_at(idcex_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast *h_state_bitvecK);
extern void          idcex_z3_transitions(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast *h_state_bitvecK, int K);
extern void          idcex_z3_marked(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists, int K);
extern void 		 idcex_z3_print(idcex_t *obj, Z3_context ctx, Z3_model m, int K, FILE *out);

#ifdef IDRTL_DEBUG_MEMLEAK_CEX
	static int		    idcex_destroy_cnt = 0;
#endif


struct idcex_s{
	/* capsulated member functions */
	void             (*destroy)(idcex_t *obj);

	/* set functions */
	void 			 (*add_cex)(idcex_t *obj, idkripke_t *Mplan, int len, idbool_t isfair);

	/* get functions */

	/* SMT operations */
	void        	(*z3_init_last_cex_at)(idcex_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast *h_state_bitvecK);
	void          	(*z3_transitions)(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast *h_state_bitvecK, int K);
	void          	(*z3_marked)(idcex_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists, int K);
	void          	(*z3_print)(idcex_t *obj, Z3_context ctx, Z3_model m, int K, FILE *out);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_CEX
	int		      m_id;
#endif
	idkripke_t 		*mh_M;
	idarray_t 		*mp_p_cexs_array;
};


#ifdef __cplusplus
}
#endif


#endif /* CEX_H_ */
