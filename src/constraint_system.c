/*
 * constraint_system.c
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#include "constraint_system.h"

idcsys_t * 	id_mk_csys_empty(int nconstrs) {
	idcsys_t *obj = malloc(sizeof(idcsys_t));
	assert(obj!=NULL);

	obj->destroy 	= idcsys_destroy;
	obj->cpy 		= idcsys_cpy;
	obj->get_at 	= idcsys_get_at;
	obj->nconstrs 	= idcsys_nconstrs;
	obj->print 		= idcsys_print;
	obj->find		= idcsys_find;
	obj->get_halfspace = idcsys_get_halfspace;
	obj->get_A 		= idcsys_get_A;
	obj->get_b 		= idcsys_get_b;

#ifdef IDRTL_DEBUG_MEMLEAK_CONSTRAINT_SYSTEM
	static int idcsys_last_id = 0;
	obj->m_id = idcsys_last_id;
	printf("debug memleak construct idcsys %d\n", obj->m_id);
	idcsys_last_id++;
	idcsys_destroy_cnt++;
#endif

	obj->m_nconstrs	= nconstrs;
	obj->mp_row_pi	= NULL;
	obj->m_nvars 	= -1;
	obj->mp_A		= NULL;
	obj->mp_b		= NULL;

	return obj;
}


/* public functions */
idcsys_t * 	id_mk_csys_from_inequalities(idmatrix_t *h_A, idmatrix_t *h_b) {
	assert(h_A!=NULL);
	assert(h_b!=NULL);
	assert(h_b->rows(h_b)==h_A->rows(h_A));
	assert(h_b->cols(h_b)==1);

	if (h_A->rows(h_A)==0) {
		idcsys_t *obj = id_mk_csys_empty(0);
		obj->m_nvars = h_A->cols(h_A);
		obj->mp_row_pi = idmap_byvalue_and_ptr(sizeof(int),idintcmpfunc_,idpredicate_destroy,idpredicate_cmp,idpredicate_cpy);
		obj->mp_A = h_A->cpy(h_A);
		obj->mp_b = h_b->cpy(h_b);
		return obj;
	}


	int 	row, col, numnz, fnd, nconstrs,
			nvars 	= h_A->cols(h_A);
	double val;
	idsparse_t *p_lhs, *h_lhs;
	idpredicate_t *p_pi;
	idmap_t *p_row_pi	= idmap_byvalue_and_ptr(sizeof(int),idintcmpfunc_,idpredicate_destroy,idpredicate_cmp,idpredicate_cpy);
	struct GMPlist *p_A_lst = GMPlist_create(),
			*p_b_lst = GMPlist_create();

	nconstrs = 0;
	for(row=0;row<h_A->rows(h_A);row++) {
		p_lhs = idsparse_byvalue(nvars,nvars,sizeof(double));
		numnz = 0;
		for(col=0;col<nvars;col++) {
			val = ID_ABSTOL*round(h_A->getat(h_A,row,col)/ID_ABSTOL);
			if (fabs(val) >= ID_ABSTOL) {
				p_lhs->set_at(p_lhs,numnz,col,&val);
				numnz++;
			} else {
				h_A->setat(h_A,row,col,0.0);
			}
		}
		p_lhs->upd_numnz(p_lhs,numnz);
		p_pi = id_mkLe(p_lhs,h_b->getat(h_b,row,0));
		h_lhs = p_pi->lhs_get(p_pi);
		for(col=0;col<h_lhs->numnz(h_lhs);col++) {
			h_A->setat(h_A,row,h_lhs->ind(h_lhs)[col],((double *)h_lhs->values(h_lhs))[col]);
		}
		h_b->setat(h_b,row,0,p_pi->rhs_get(p_pi));
		p_row_pi->mp_values1_at->find_key_n(p_row_pi->mp_values1_at,p_pi,&fnd);
		if (!fnd) {
			p_row_pi->set(p_row_pi,&nconstrs,p_pi);
			p_A_lst = p_A_lst->push(p_A_lst,h_A->mpq_row_extract(h_A,row));
			p_b_lst = p_b_lst->push(p_b_lst,h_b->mpq_row_extract(h_b,row));
			nconstrs++;
		} else {
			p_pi->destroy(p_pi);
		}
	}
	assert(p_row_pi->m_nvalues1==p_row_pi->m_nvalues2);

	idcsys_t *obj = id_mk_csys_empty(nconstrs);
	obj->m_nvars = nvars;
	obj->mp_row_pi = p_row_pi;

	obj->mp_A = p_A_lst->to_matrix(p_A_lst,nvars);
	obj->mp_b = p_b_lst->to_matrix(p_b_lst,1);

	return obj;
}


void      				idcsys_destroy(idcsys_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_REACH_LP
		idcsys_destroy_cnt--;
		printf("debug memleak destroy idcsys %d still remains %d to destroy\n", obj->m_id, idcsys_destroy_cnt);
		if (idcsys_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idmap_destroy(obj->mp_row_pi);
		GMPmat_destroy(obj->mp_A);
		GMPmat_destroy(obj->mp_b);
		free(obj);
	}
}
idcsys_t * 	idcsys_cpy(idcsys_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_row_pi!=NULL);

	idcsys_t *p_cs = id_mk_csys_empty(obj->m_nconstrs);
	p_cs->m_nvars = obj->m_nvars;
	p_cs->mp_row_pi = obj->mp_row_pi->cpy(obj->mp_row_pi);
	if (obj->mp_A!=NULL) {
		assert(obj->mp_b!=NULL);
		p_cs->mp_A = obj->mp_A->cpy(obj->mp_A);
		p_cs->mp_b = obj->mp_b->cpy(obj->mp_b);
	}

	return p_cs;
}

/* get functions */
void	idcsys_get_at(idcsys_t *obj, int row, int *h_numnz, int **hh_ind, double **hh_val,
		char *h_sense, double *h_rhs) {
	assert(obj!=NULL);
	assert(h_numnz!=NULL);
	assert(hh_ind!=NULL);
	assert(hh_val);
	assert(h_sense!=NULL);
	assert(h_rhs!=NULL);
	assert((0<=row)&&(row<obj->nconstrs(obj)));

	idlist_t *h_pi_lst = obj->mp_row_pi->get_values2_at(obj->mp_row_pi,&row);
	idrb_node_t *h_node = idrb_first(h_pi_lst->mp_head);
	idpredicate_t *h_pi = (idpredicate_t *)h_node->value(h_node);

	h_pi->get(h_pi,h_numnz,hh_ind,hh_val,h_sense,h_rhs);
}
int idcsys_nconstrs(idcsys_t *obj) {
	assert(obj!=NULL);
	return obj->m_nconstrs;
}

void idcsys_print(idcsys_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);

	int i;
	idrb_node_t *it, *h_node;
	idpredicate_t *h_pi;
	idlist_t *h_pi_lst;

	idrb_traverse(it,obj->mp_row_pi->mp_values2_at) {
		h_pi_lst = (idlist_t *)it->value(it);
		i = *(int *)it->keyg(it);
		h_node	= idrb_first(h_pi_lst->mp_head);
		h_pi = (idpredicate_t *)h_node->value(h_node);
		fprintf(out,"[%d]\t",i);
		h_pi->print(h_pi,out);
	}
}
static void idcsys_to_H(idcsys_t *obj) {
	assert(obj!=NULL);

	int 	i,j,numeq,
			rows = obj->m_nconstrs,
			cols = obj->m_nvars;
	idmatrix_t  *p_Aeq, *p_beq,
				*p_A = id_zeros(rows,cols),
				*p_b = id_zeros(rows,1);

	idrb_node_t *it, *h_node;
	idpredicate_t *h_pi;
	idlist_t *h_pi_lst;
	int 	numnz, *h_ind;
	double *h_val, rhs;
	char sense;

	idrb_traverse(it,obj->mp_row_pi->mp_values2_at) {
		h_pi_lst = (idlist_t *)it->value(it);
		i = *(int *)it->keyg(it);
		h_node	= idrb_first(h_pi_lst->mp_head);
		h_pi = (idpredicate_t *)h_node->value(h_node);
		h_pi->get(h_pi,&numnz,&h_ind,&h_val,&sense,&rhs);
		if (sense == GRB_EQUAL) {
			numeq++;
		}
		for(j=0;j<numnz;j++) {
			p_A->setat(p_A,i,h_ind[j],h_val[j]);
		}
		p_b->setat(p_b,i,0,rhs);
	}
	if (numeq>0) {
		p_Aeq = id_zeros(numeq,cols);
		p_beq = id_zeros(numeq,1);
		i = 0;
		idrb_traverse(it,obj->mp_row_pi->mp_values2_at) {
			h_pi_lst = (idlist_t *)it->value(it);
			i = *(int *)it->keyg(it);
			h_node	= idrb_first(h_pi_lst->mp_head);
			h_pi = (idpredicate_t *)h_node->value(h_node);
			if (h_pi->isEqual(h_pi)) {
				h_pi->get(h_pi,&numnz,&h_ind,&h_val,&sense,&rhs);
				for(j=0;j<numnz;j++) {
					p_A->setat(p_A,i,h_ind[j],h_val[j]);
				}
				p_b->setat(p_b,i,0,rhs);
				i++;
			}
		}

		p_A = p_A->stackVert(p_A,p_Aeq);
		p_b = p_b->stackVert(p_b,p_beq);
	}

	obj->mp_A = p_A;
	obj->mp_b = p_b;
}

idpredicate_t * idcsys_find(idcsys_t *obj, idpredicate_t *h_pi, int *h_idx_out) {
	assert(obj!=NULL);


	int fnd;
	idrb_node_t *h_rows = obj->mp_row_pi->mp_values1_at->find_key_n(obj->mp_row_pi->mp_values1_at,h_pi,&fnd);
	int *h_row;

	if (fnd) {
		if (h_idx_out!=NULL) {
			h_row = (int *)idlist_first((idlist_t *)h_rows->value(h_rows));
			assert(h_row!=NULL);
			*h_idx_out = *h_row;
		}
		return (idpredicate_t *)h_rows->keyg(h_rows);
	} else {
		if (h_idx_out!=NULL) {
			*h_idx_out = -1;
		}
		return NULL;
	}
}
idmatrix_t * idcsys_get_A(idcsys_t *obj) {
	if (obj->mp_A==NULL) {
		assert(obj->mp_b==NULL);
		idcsys_to_H(obj);
	}
	return obj->mp_A;
}
idmatrix_t * idcsys_get_b(idcsys_t *obj) {
	if (obj->mp_b==NULL) {
		assert(obj->mp_A==NULL);
		idcsys_to_H(obj);
	}
	return obj->mp_b;
}
void idcsys_get_halfspace(idcsys_t *obj, idmatrix_t **hp_A_out, idmatrix_t **hp_b_out) {
	assert(obj!=NULL);
	assert(hp_A_out!=NULL);
	assert(hp_b_out!=NULL);

	if (obj->mp_A==NULL) {
		assert(obj->mp_b==NULL);
		idcsys_to_H(obj);
	}
	*hp_A_out = obj->mp_A->cpy(obj->mp_A);
	*hp_b_out = obj->mp_b->cpy(obj->mp_b);
}


idcsys_t *	id_mk_csys_equalities_from_iddblmatrix_vector(iddblmatrix_t *p_A) {
	assert(p_A!=NULL);
	assert((p_A->cols(p_A)==1)||(p_A->rows(p_A)==1));

	idcsys_t *p_csys;
	int i, nconstrs;
	idsparse_t *p_lhs;
	idmap_t *h_map;
	double lhs = 1.0;
	idpredicate_t *p_pi;
	if (p_A->cols(p_A)==1) {
		nconstrs = p_A->rows(p_A);
		p_csys = id_mk_csys_empty(nconstrs);
		p_csys->m_nvars = nconstrs;
		p_csys->mp_row_pi	= idmap_byvalue_and_ptr(sizeof(int),idintcmpfunc_,idpredicate_destroy,idpredicate_cmp,idpredicate_cpy);
		h_map = p_csys->mp_row_pi;
		for(i=0;i<nconstrs;i++) {
			p_lhs = idsparse_byvalue(nconstrs,1,sizeof(double));
			p_lhs->set_at(p_lhs,0,i,&lhs);
			p_pi = id_mkEq(p_lhs,p_A->get(p_A,i,0));
			h_map->set(h_map,&i,p_pi);
		}
	} else {
		nconstrs = p_A->cols(p_A);
		p_csys = id_mk_csys_empty(nconstrs);
		p_csys->m_nvars = nconstrs;
		p_csys->mp_row_pi	= idmap_byvalue_and_ptr(sizeof(int),idintcmpfunc_,idpredicate_destroy,idpredicate_cmp,idpredicate_cpy);
		h_map = p_csys->mp_row_pi;
		for(i=0;i<nconstrs;i++) {
			p_lhs = idsparse_byvalue(nconstrs,1,sizeof(double));
			p_lhs->set_at(p_lhs,0,i,&lhs);
			p_pi = id_mkEq(p_lhs,p_A->get(p_A,0,i));
			h_map->set(h_map,&i,p_pi);
		}
	}

	return p_csys;
}
