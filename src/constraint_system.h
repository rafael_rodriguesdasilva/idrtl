/*
 * constraint_system.h
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#ifndef SOLVER_CONSTRAINT_SYSTEM_H_
#define SOLVER_CONSTRAINT_SYSTEM_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idcsys_t * 	id_mk_csys_from_inequalities(idmatrix_t *h_A, idmatrix_t *h_b);
extern idcsys_t *	id_mk_csys_equalities_from_iddblmatrix_vector(iddblmatrix_t *p_A);
extern idcsys_t * 	id_mk_csys_empty(int nconstrs);

extern void      	idcsys_destroy(idcsys_t *obj);
extern idcsys_t * 	idcsys_cpy(idcsys_t *obj);

/* get functions */
extern void	idcsys_get_at(idcsys_t *obj, int row, int *h_numnz, int **hh_ind, double **hh_val,
		char *h_sense, double *h_rhs);
extern int idcsys_nconstrs(idcsys_t *obj);
extern void idcsys_print(idcsys_t *obj, FILE *out);
extern idpredicate_t * idcsys_find(idcsys_t *obj, idpredicate_t *h_pi, int *h_idx_out);

/* operation */
extern void idcsys_get_halfspace(idcsys_t *obj, idmatrix_t **hp_A_out, idmatrix_t **hp_b_out);
extern idmatrix_t * idcsys_get_A(idcsys_t *obj);
extern idmatrix_t * idcsys_get_b(idcsys_t *obj);

#ifdef IDRTL_DEBUG_MEMLEAK_CONSTRAINT_SYSTEM
	static int		    idcsys_destroy_cnt = 0;
#endif


struct idcsys_s{
	/* capsulated member functions */
	void         	(*destroy)(idcsys_t *obj);
	idcsys_t *		(*cpy)(idcsys_t *obj);

	/* get functions */
	void          	(*get_at)(idcsys_t *obj, int row, int *h_numnz, int **hh_ind, double **hh_val,
			char *h_sense, double *h_rhs);
	int 			(*nconstrs)(idcsys_t *obj);
	void (*print)(idcsys_t *obj, FILE *out);
	idpredicate_t * (*find)(idcsys_t *obj, idpredicate_t *h_pi, int *h_idx_out);
	void (*get_halfspace)(idcsys_t *obj, idmatrix_t **hp_A_out, idmatrix_t **hp_b_out);
	idmatrix_t * (*get_A)(idcsys_t *obj);
	idmatrix_t * (*get_b)(idcsys_t *obj);

	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_CONSTRAINT_SYSTEM
	int		      m_id;
#endif
	int 			m_nvars,
					m_nconstrs;
	idmap_t			*mp_row_pi;
	idmatrix_t		*mp_A,
					*mp_b;
};

#ifdef __cplusplus
}
#endif


#endif /* SOLVER_CONSTRAINT_SYSTEM_H_ */
