/*
 * dblmatrix.c
 *
 *  Created on: Jul 11, 2019
 *      Author: rafael
 */

#include "dblmatrix.h"

iddblmatrix_t * 	iddblmatrix(int rows, int cols) {
	assert(rows>0);
	assert(cols>0);

	iddblmatrix_t *obj = malloc(sizeof(iddblmatrix_t));
	assert(obj!=NULL);

	obj->destroy 			= iddblmatrix_destroy;
	obj->cpy  				= iddblmatrix_cpy;
	obj->set_at				= iddblmatrix_set_at;
	obj->set_from_gmpmatrix	= iddblmatrix_set_from_gmpmatrix;
	obj->get				= iddblmatrix_get;
	obj->get_row			= iddblmatrix_get_row;
	obj->get_col			= iddblmatrix_get_col;
	obj->rows				= iddblmatrix_rows;
	obj->cols				= iddblmatrix_cols;
	obj->print				= iddblmatrix_print;

	obj->m_cols = cols;
	obj->m_rows = rows;
	obj->mp_values = malloc(rows*cols*sizeof(double));
	assert(obj->mp_values!=NULL);
	int i,j;
	for(i=0;i<rows;i++) {
		for(j=0;j<cols;j++) {
			obj->set_at(obj,i,j,0.0);
		}
	}

#ifdef IDRTL_DEBUG_MEMLEAK_DBLMATRIX
	static int iddblmatrix_last_id = 0;
	obj->m_id = iddblmatrix_last_id;
	printf("debug memleak construct dblmatrix %d\n", obj->m_id);
	iddblmatrix_last_id++;
	iddblmatrix_destroy_cnt++;
#endif

	return obj;
}

iddblmatrix_t * 	iddblmatrix_multiply(iddblmatrix_t *h_A, iddblmatrix_t *h_B) {
	assert(h_A!=NULL);
	assert(h_B!=NULL);
	assert(h_A->cols(h_A)==h_B->rows(h_B));

	iddblmatrix_t *obj = iddblmatrix(h_A->rows(h_A), h_B->cols(h_B));

	int i, j, k;
	double v;
	for(i=0;i<h_A->rows(h_A);i++) {
		for(j=0;j<h_B->cols(h_B);j++) {
			v = 0.0;
			for(k=0;k<h_B->rows(h_B);k++) {
				v += h_A->get(h_A,i,k)*h_B->get(h_B,k,j);
			}
			obj->set_at(obj,i,j,v);
		}
	}
	return obj;
}

iddblmatrix_t * 	iddblmatrix_multiply_and_return_A(iddblmatrix_t *h_A, iddblmatrix_t *h_B) {
	assert(h_A!=NULL);
	assert(h_B!=NULL);
	assert(h_A->rows(h_A)==h_B->cols(h_B));
	assert(h_A->cols(h_A)==h_B->rows(h_B));

	int i, j, k, rows = h_A->rows(h_A), cols = h_B->cols(h_B);
	double v[rows*cols];
	for(i=0;i<rows;i++) {
		for(j=0;j<cols;j++) {
			v[i*cols+j] = 0.0;
			for(k=0;k<h_B->rows(h_B);k++) {
				v[i*cols+j] += h_A->get(h_A,i,k)*h_B->get(h_B,k,j);
			}
		}
	}
	for(i=0;i<rows;i++) {
		for(j=0;j<cols;j++) {
			h_A->set_at(h_A,i,j,v[i*cols+j]);
		}
	}
	return h_A;
}

void      			iddblmatrix_destroy(iddblmatrix_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_DBLMATRIX
		iddblmatrix_destroy_cnt--;
		printf("debug memleak destroy dblmatrix %d still remains %d to destroy\n", obj->m_id, iddblmatrix_destroy_cnt);
		if (iddblmatrix_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		free(obj->mp_values);
		free(obj);
	}
}


iddblmatrix_t * 	iddblmatrix_cpy(iddblmatrix_t *obj) {
	iddblmatrix_t *p_A = iddblmatrix(obj->rows(obj),obj->cols(obj));

	int i, j;
	for(i=0;i<obj->rows(obj);i++) {
		for(j=0;j<obj->cols(obj);j++) {
			p_A->set_at(p_A,i,j,obj->get(obj,i,j));
		}
	}

	return p_A;
}

/* set functions */
void      			iddblmatrix_set_at(iddblmatrix_t *obj, int row, int col, double value) {
	assert(obj!=NULL);
	assert((0<=row)&&(row<obj->rows(obj)));
	assert((0<=col)&&(col<obj->cols(obj)));

	obj->mp_values[row*obj->cols(obj)+col] = value;
}
void        		iddblmatrix_set_from_gmpmatrix(iddblmatrix_t *obj, idmatrix_t *h_A) {
	assert(obj!=NULL);
	assert(h_A!=NULL);
	assert(h_A->rows(h_A)==obj->rows(obj));
	assert(h_A->cols(h_A)==obj->cols(obj));

	int i, j;
	for(i=0;i<obj->rows(obj);i++) {
		for(j=0;j<obj->cols(obj);j++) {
			obj->set_at(obj,i,j,h_A->getat(h_A,i,j));
		}
	}

}

/* get functions */
extern double		iddblmatrix_get(iddblmatrix_t *obj, int row, int col) {
	assert(obj!=NULL);
	assert((0<=row)&&(row<obj->rows(obj)));
	assert((0<=col)&&(col<obj->cols(obj)));

	return obj->mp_values[row*obj->cols(obj)+col];
}
void			iddblmatrix_get_row(iddblmatrix_t *obj, int row, int *h_numnz, int **hp_ind, double **hp_val) {
	assert(obj!=NULL);
	assert((0<=row)&&(row<obj->rows(obj)));
	assert(h_numnz!=NULL);
	assert(hp_ind!=NULL);
	assert(hp_val!=NULL);

	int numnz, *p_ind, j;
	double *p_val;

	p_ind = malloc(obj->cols(obj)*sizeof(int));
	assert(p_ind!=NULL);
	p_val = malloc(obj->cols(obj)*sizeof(double));
	assert(p_val!=NULL);

	numnz = 0;
	for(j=0;j<obj->cols(obj);j++) {
		p_val[numnz] = obj->get(obj,row,j);
		if (fabs(p_val[numnz])>=ID_ABSTOL) {
			p_ind[numnz] = j;
			numnz++;
		}
	}
	*h_numnz = numnz;
	*hp_ind = p_ind;
	*hp_val = p_val;
}
void			iddblmatrix_get_col(iddblmatrix_t *obj, int col, int *h_numnz, int **hp_ind, double **hp_val) {
	assert(obj!=NULL);
	assert((0<=col)&&(col<obj->cols(obj)));
	assert(h_numnz!=NULL);
	assert(hp_ind!=NULL);
	assert(hp_val!=NULL);

	int numnz, *p_ind, i;
	double *p_val;

	p_ind = malloc(obj->cols(obj)*sizeof(int));
	assert(p_ind!=NULL);
	p_val = malloc(obj->cols(obj)*sizeof(double));
	assert(p_val!=NULL);

	numnz = 0;
	for(i=0;i<obj->cols(obj);i++) {
		p_val[numnz] = obj->get(obj,i,col);
		if (fabs(p_val[numnz])>=ID_ABSTOL) {
			p_ind[numnz] = i;
			numnz++;
		}
	}
	*h_numnz = numnz;
	*hp_ind = p_ind;
	*hp_val = p_val;
}
int 				iddblmatrix_rows(iddblmatrix_t *obj) {
	assert(obj!=NULL);
	return obj->m_rows;
}
int 				iddblmatrix_cols(iddblmatrix_t *obj) {
	assert(obj!=NULL);
	return obj->m_cols;
}
iddblmatrix_t * iddblmatrix_right_concatenate(iddblmatrix_t *p_A, iddblmatrix_t *p_B) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_B->rows(p_B)==p_A->rows(p_A));

	int rows = p_A->rows(p_A), cols = p_A->cols(p_A)+p_B->cols(p_B);
	double *p_val = malloc(rows*cols*sizeof(double));
	assert(p_val);

	int i, j;
	for(i=0;i<rows;i++) {
		for(j=0;j<p_A->cols(p_A);j++) {
			p_val[i*cols+j] = p_A->get(p_A,i,j);
		}
		for(j=0;j<p_B->cols(p_B);j++) {
			p_val[i*cols+j+p_A->cols(p_A)] = p_B->get(p_B,i,j);
		}
	}

	free(p_A->mp_values);
	p_A->mp_values = p_val;
	p_A->m_cols = cols;
	p_B->destroy(p_B);

	return p_A;
}
void			iddblmatrix_print(iddblmatrix_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);

	int i, j;
	for(i=0;i<obj->rows(obj);i++) {
		for(j=0;j<obj->cols(obj);j++) {
			if (j>0) {
				fprintf(out,"\t");
			}
			fprintf(out,"%.3f",obj->get(obj,i,j));
		}
		fprintf(out,";\n");
	}

}
