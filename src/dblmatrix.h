/*
 * dblmatrix.h
 *
 *  Created on: Jul 11, 2019
 *      Author: rafael
 */

#ifndef SOLVER_DBLMATRIX_H_
#define SOLVER_DBLMATRIX_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern iddblmatrix_t * 	iddblmatrix(int rows, int cols);
extern iddblmatrix_t * 	iddblmatrix_multiply(iddblmatrix_t *h_A, iddblmatrix_t *h_B);
extern iddblmatrix_t * 	iddblmatrix_multiply_and_return_A(iddblmatrix_t *h_A, iddblmatrix_t *h_B);
extern iddblmatrix_t *	iddblmatrix_right_concatenate(iddblmatrix_t *obj, iddblmatrix_t *p_A);

extern void      		iddblmatrix_destroy(iddblmatrix_t *obj);
extern iddblmatrix_t * 	iddblmatrix_cpy(iddblmatrix_t *obj);

/* set functions */
extern void      	iddblmatrix_set_at(iddblmatrix_t *obj, int row, int col, double value);
extern void        	iddblmatrix_set_from_gmpmatrix(iddblmatrix_t *obj, idmatrix_t *h_A);

/* get functions */
extern double		iddblmatrix_get(iddblmatrix_t *obj, int row, int col);
extern void			iddblmatrix_get_row(iddblmatrix_t *obj, int row, int *h_numnz, int **hp_ind, double **hp_val);
extern void			iddblmatrix_get_col(iddblmatrix_t *obj, int col, int *h_numnz, int **hp_ind, double **hp_val);
extern int 			iddblmatrix_rows(iddblmatrix_t *obj);
extern int 			iddblmatrix_cols(iddblmatrix_t *obj);
extern void			iddblmatrix_print(iddblmatrix_t *obj, FILE *out);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_DBLMATRIX
	static int		    iddblmatrix_destroy_cnt = 0;
#endif


struct iddblmatrix_s{
	/* capsulated member functions */
	void           	(*destroy)(iddblmatrix_t *obj);
	iddblmatrix_t *		(*cpy)(iddblmatrix_t *obj);

	/* get functions */
	void          	(*set_at)(iddblmatrix_t *obj, int row, int col, double value);
	void         	(*set_from_gmpmatrix)(iddblmatrix_t *obj, idmatrix_t *h_A);
	void         	(*set_from_dblmatrix)(iddblmatrix_t *obj, iddblmatrix_t *h_A);

	/* get functions */
	double         	(*get)(iddblmatrix_t *obj, int row, int col);
	void          	(*get_row)(iddblmatrix_t *obj, int row, int *h_numnz, int **hp_ind, double **hp_val);
	void          	(*get_col)(iddblmatrix_t *obj, int col, int *h_numnz, int **hp_ind, double **hp_val);
	int 			(*rows)(iddblmatrix_t *obj);
	int 			(*cols)(iddblmatrix_t *obj);
	void			(*print)(iddblmatrix_t *obj, FILE *out);


	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_DBLMATRIX
	int		      m_id;
#endif
	double 		*mp_values;
	int 		m_rows,
				m_cols;
};

#ifdef __cplusplus
}
#endif

#endif /* SOLVER_DBLMATRIX_H_ */
