/*
 * feasibility_search.c
 *
 *  Created on: Jul 14, 2019
 *      Author: rafael
 */

#include "feasibility_search.h"

//#define FSEARCH_DEBUG_L1
//#define FSEARCH_DEBUG_L2

static idfsearch_t * 	idfsearch_default_constructor(double delta, double delta_progress, idbool_t isfair, int L) {

	idfsearch_t *obj = malloc(sizeof(idfsearch_t));
	assert(obj!=NULL);

	obj->cpy = idfsearch_cpy;
	obj->destroy = idfsearch_destroy;
	obj->push = idfsearch_push;
	obj->get_solution = idfsearch_get_solution;
	obj->get_cex = idfsearch_get_cex;
	obj->search = idfsearch_search;
	obj->isfair = idfsearch_isfair;
	obj->L = idfsearch_L;

#ifdef IDRTL_DEBUG_MEMLEAK_FEAS
	static int idfsearch_last_id = 0;
	obj->m_id = idfsearch_last_id;
	printf("debug memleak construct fsearch %d\n", obj->m_id);
	idfsearch_last_id++;
	idfsearch_destroy_cnt++;
#endif

	obj->mp_states_intarray = NULL;
	obj->mp_h_label_array = NULL;
	obj->mp_h_sys_array = NULL;
	obj->mp_h_u_constr_array = NULL;
	obj->mp_h_x_constr_array = NULL;
	obj->mp_cex = NULL;
	obj->mp_solution = NULL;
	obj->m_delta = delta;
	obj->m_delta_progress = delta_progress;
	obj->m_L = L;
	obj->m_isfair = isfair;
	obj->m_length = 0;

	return obj;
}

static void idfsearch_z3_parse_variables(Z3_context ctx, Z3_model m, Z3_func_decl cnst, const char *h_name,
															int *h_path, int K, int *h_L, idbool_t *h_loopexists) {
	char *h_ptr;
	int bit, k;

	if (h_name[0]=='b') {
		// b[bit]_[k]
		h_ptr = strchr(h_name,'_'); // returns a pointer to char '_'
		assert(h_ptr!=NULL);

		*h_ptr = '\0';
		bit = atoi(h_name+1); // takes the left integer

		if (*(h_ptr+1)!='E') {
			k = atoi(h_ptr+1);// takes the right integer

			if (z3_get_bool(ctx,m,cnst)) {
				h_path[k] += (int)pow(2,bit);
			}
		}
	 } else if (h_name[0]=='l') {
		 if (z3_get_bool(ctx,m,cnst)) {
			h_ptr = strchr(h_name,'_'); // returns a pointer to char '_'
			assert(h_ptr!=NULL);
			*h_L = atoi(h_ptr+1);// takes the right integer
		}
	 } else if (strcmp(h_name,"exists")==0) {
		 if (z3_get_bool(ctx,m,cnst)) {
			*h_loopexists = TRUE;
		 }
	}

}
/* public functions */
idfsearch_t * 	idfsearch_decode(double delta, double delta_progress, idkripke_t *h_M, Z3_context ctx, Z3_model m, int K) {
	assert(h_M!=NULL);

	if (!m) return NULL;

    unsigned num_constants;
    unsigned ui;


    int i, si, qi;

    int path[K], L;
    idbool_t loop_exists = FALSE;
    char str[80];
    idfsearch_t *p_fsearch;
    idpolytope_t *h_poly;
    idlist_t *h_label;
    idlinear_sys_t *h_sys;
    idpolytope_t *h_inputdom;
    idcsys_t *h_x_constr, *h_u_constr;

    for(i=0;i<K;i++) {
    	path[i] = 0;
    }

    num_constants = Z3_model_get_num_consts(ctx, m);
    for (i = 0; i < num_constants; i++) {
        Z3_symbol name;
        Z3_func_decl cnst = Z3_model_get_const_decl(ctx, m, i);
        name = Z3_get_decl_name(ctx, cnst);
        sprintf(str,"%s",Z3_get_symbol_string (ctx, name));

        idfsearch_z3_parse_variables(ctx,m,cnst,str,path,K,&L,&loop_exists);
    }
    assert(h_M->m_initial==h_M->get_state_from_bits(h_M,path[0]));
    i=0;
    si = h_M->get_state_from_bits(h_M,path[i]);
    qi = h_M->get_mode_from_bits(h_M,path[i]);
    h_poly = (idpolytope_t *)h_M->mp_states->get_at(h_M->mp_states,si);
    h_label = (idlist_t *)h_M->mp_state_label_map->get_values2_at(h_M->mp_state_label_map,&si);
    h_sys = (idlinear_sys_t *)h_M->mp_mode_sys->get_at(h_M->mp_mode_sys,qi);
    h_inputdom = (idpolytope_t *)h_M->mp_mode_inputdom->get_at(h_M->mp_mode_inputdom,qi);
    h_u_constr = h_inputdom->get_x_constr(h_inputdom);
    h_x_constr = h_M->mp_x0_constr;
    p_fsearch = idfsearch(delta,delta_progress,loop_exists,L,si,h_label,h_sys,h_u_constr,h_x_constr);
    h_x_constr = h_poly->get_x_constr(h_poly);
    p_fsearch->push(p_fsearch,si,h_label,h_sys,h_u_constr,h_x_constr);
    for(i=1;i<K;i++) {
    	si = h_M->get_state_from_bits(h_M,path[i]);
        h_poly = (idpolytope_t *)h_M->mp_states->get_at(h_M->mp_states,si);
        h_label = (idlist_t *)h_M->mp_state_label_map->get_values2_at(h_M->mp_state_label_map,&si);
        h_sys = (idlinear_sys_t *)h_M->mp_mode_sys->get_at(h_M->mp_mode_sys,qi);
        h_inputdom = (idpolytope_t *)h_M->mp_mode_inputdom->get_at(h_M->mp_mode_inputdom,qi);
        h_u_constr = h_inputdom->get_x_constr(h_inputdom);
        h_x_constr = h_poly->get_x_constr(h_poly);
        p_fsearch->push(p_fsearch,si,h_label,h_sys,h_u_constr,h_x_constr);
    }

	return p_fsearch;
}


idfsearch_t * 	idfsearch(double delta, double delta_progress, idbool_t isfair, int L, int si, idlist_t *h_label,
		idlinear_sys_t *h_x0_sys, idcsys_t *h_u0_constr, idcsys_t *h_x0_constr) {
	assert(h_x0_sys!=NULL);
	assert(h_x0_constr!=NULL);
	assert(h_u0_constr!=NULL);

	idfsearch_t *obj = idfsearch_default_constructor(delta, delta_progress, isfair, L);

	obj->mp_states_intarray = idintarray();
	obj->mp_h_label_array = idarray_byref();
	obj->mp_h_sys_array = idarray_byref();
	obj->mp_h_u_constr_array = idarray_byref();
	obj->mp_h_x_constr_array = idarray_byref();

	obj->push(obj,si,h_label,h_x0_sys,h_u0_constr,h_x0_constr);

	return obj;
}
void      		idfsearch_destroy(idfsearch_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_FEAS
		idfsearch_destroy_cnt--;
		printf("debug memleak destroy fsearch %d still remains %d to destroy\n", obj->m_id, idfsearch_destroy_cnt);
		if (idfsearch_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_states_intarray);
		idarray_destroy(obj->mp_h_label_array);
		idarray_destroy(obj->mp_h_sys_array);
		idarray_destroy(obj->mp_h_u_constr_array);
		idarray_destroy(obj->mp_h_x_constr_array);
		idrun_destroy(obj->mp_solution);
		idarray_destroy(obj->mp_cex);
		free(obj);
	}
}
idfsearch_t * 	idfsearch_cpy(idfsearch_t *obj) {
	assert(obj!=NULL);

	idfsearch_t *fsearch = idfsearch_default_constructor(obj->m_delta, obj->m_delta_progress, obj->m_isfair, obj->m_L);

	fsearch->mp_states_intarray = idarray_cpy(obj->mp_states_intarray);
	fsearch->mp_h_label_array = idarray_cpy(obj->mp_h_label_array);
	fsearch->mp_h_sys_array = idarray_cpy(obj->mp_h_sys_array);
	fsearch->mp_h_u_constr_array = idarray_cpy(obj->mp_h_u_constr_array);
	fsearch->mp_h_x_constr_array = idarray_cpy(obj->mp_h_x_constr_array);
	fsearch->m_length = obj->m_length;

	return fsearch;
}

/* set functions */
void      		idfsearch_push(idfsearch_t *obj, int si, idlist_t *h_label, idlinear_sys_t *h_x_sys,
		idcsys_t *h_u_constr, idcsys_t *h_x_constr) {
	assert(obj!=NULL);
	assert(h_x_sys!=NULL);
	assert(h_x_constr!=NULL);
	assert(h_u_constr!=NULL);

	obj->mp_states_intarray->append(obj->mp_states_intarray,&si);
	obj->mp_h_label_array->append(obj->mp_h_label_array,h_label);
	obj->mp_h_sys_array->append(obj->mp_h_sys_array,h_x_sys);
	obj->mp_h_u_constr_array->append(obj->mp_h_u_constr_array,h_u_constr);
	obj->mp_h_x_constr_array->append(obj->mp_h_x_constr_array,h_x_constr);
	obj->m_length++;
}

/* get functions */
idrun_t *     	idfsearch_get_solution(idfsearch_t *obj) {
	assert(obj!=NULL);

	return obj->mp_solution;
}
idautomaton_t *	idfsearch_get_cex(idfsearch_t *obj) {
	assert(obj!=NULL);

	return obj->mp_cex;
}
idbool_t 	idfsearch_isfair(idfsearch_t *obj) {
	assert(obj!=NULL);

	return obj->m_isfair;
}
int	idfsearch_L(idfsearch_t *obj) {
	assert(obj!=NULL);

	return obj->m_L;
}

/* operation */
static void idfsearch_destroy_open_set(idrb_node_t *p_open_set) {
	idrun_t *p_run;
	idrb_node_t *it;

	if (p_open_set == NULL) {
		return;
	}

	idrb_traverse(it,p_open_set) {
		p_run = (idrun_t *)it->value(it);
		p_run->destroy(p_run);
	}
	p_open_set->destroy(p_open_set);
}
static idrun_t *idfsearch_pop(idrb_node_t *h_open_set, double *h_slack) {
	assert(h_open_set!=NULL);
	assert(h_slack!=NULL);
	assert(idrb_first(h_open_set)!=idrb_nil(h_open_set));

	idrun_t *p_parent;
	idrb_node_t *h_parent_node;

	h_parent_node = idrb_first(h_open_set);
	p_parent = (idrun_t *)h_parent_node->value(h_parent_node);
	*h_slack = h_parent_node->keyd(h_parent_node);
	h_open_set->delete_node(h_parent_node);

	return p_parent;
}

static idrun_t *idfsearch_push_levelup_childs(idrb_node_t *h_open_set, idrun_t *h_parent,
		int level, double slack_old, double delta, double delta_progress) {
#ifdef FSEARCH_DEBUG_L1
	printf("*************************************************************************\n");
	printf("debug idfsearch_push_levelup_childs: starting...\n");
#endif
	assert(h_open_set!=NULL);
	assert(h_parent!=NULL);

	idrun_t *p_child;
	idrb_node_t *it_state, *it_nsamples, *it_sys, *it_label, *it_x_constr, *it_u_constr;
	int i, j, s0, nsamples0, s1, nsamples1;
	idlinear_sys_t *h_sys0, *h_sys1;
	idlist_t *h_label0, *h_label1;
	idcsys_t *h_x0_constr, *h_x1_constr, *h_u0_constr, *h_u1_constr;
	double slack;

	for(i=2;i<level-1;i++) {
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_push_levelup_childs: child %d of %d\n", i-1,level-3);
#endif
		p_child = idrun();

		it_state	= idrb_first(h_parent->mp_states_intarray->mp_head);
		it_nsamples	= idrb_first(h_parent->mp_nsample_intarray->mp_head);
		it_sys 		= idrb_first(h_parent->mp_h_sys_array->mp_head);
		it_label 	= idrb_first(h_parent->mp_h_label_array->mp_head);
		it_x_constr	= idrb_first(h_parent->mp_h_x_constr_array->mp_head);
		it_u_constr	= idrb_first(h_parent->mp_h_u_constr_array->mp_head);


		j = 0;
		while(it_state!=idrb_nil(h_parent->mp_states_intarray->mp_head)) {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_levelup_childs: adding step %d of child %d\n", j, i-1);
#endif
			s1 			= *(int *)it_state->value(it_state);
			nsamples1	= *(int *)it_nsamples->value(it_nsamples);
			h_label1 	= (idlist_t *)it_label->value(it_label);
			h_sys1 		= (idlinear_sys_t *)it_sys->value(it_sys);
			h_u1_constr	= (idcsys_t *)it_u_constr->value(it_u_constr);
			h_x1_constr	= (idcsys_t *)it_x_constr->value(it_x_constr);

			it_state	= idrb_next(it_state);
			it_nsamples	= idrb_next(it_nsamples);
			it_sys 		= idrb_next(it_sys);
			it_label 	= idrb_next(it_label);
			it_x_constr	= idrb_next(it_x_constr);
			it_u_constr	= idrb_next(it_u_constr);

			if (j==i) {
				p_child->append_step(p_child, 1,s1,h_label1,h_sys1,h_u1_constr,h_x1_constr);
				p_child->append_step(p_child, 1,s0,h_label0,h_sys0,h_u0_constr,h_x0_constr);
			}
			p_child->append_step(p_child, nsamples1,s1,h_label1,h_sys1,h_u1_constr,h_x1_constr);

			s0 			= s1;
			h_label0 	= h_label1;
			h_sys0 		= h_sys1;
			h_u0_constr	= h_u1_constr;
			h_x0_constr	= h_x1_constr;
			j++;
		}

		p_child->compute(p_child);
		slack = p_child->slack(p_child);

		if (slack <= delta) {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_levelup_childs: SOLUTION FOUND!\n");
#endif
			break;
		}
		if (slack_old - slack > delta_progress) {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_levelup_childs: child %d added\n", i-1);
#endif

			h_open_set->insert(h_open_set,&slack,p_child);
		} else {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_levelup_childs: child %d discarded\n", i-1);
#endif
			p_child->destroy(p_child);
		}
		p_child = NULL;
	}
#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_push_levelup_childs: END.\n");
	printf("*************************************************************************\n");
#endif
	return p_child;
}

static idrun_t *idfsearch_push_samelevel_childs(idrb_node_t *h_open_set, idrun_t *h_parent,
		int level, double slack_old, double delta, double delta_progress) {
#ifdef FSEARCH_DEBUG_L1
	printf("*************************************************************************\n");
	printf("debug idfsearch_push_samelevel_childs: starting...\n");
#endif
	assert(h_open_set!=NULL);
	assert(h_parent!=NULL);

	idrun_t *p_child;
	int i;
	double slack;

	for(i=1;i<level-1;i++) {
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_push_samelevel_childs: adding child %d of %d\n", i, level-1);
#endif
		p_child = h_parent->cpy(h_parent);
		p_child->increment_at(p_child,i,1);

		p_child->compute(p_child);
		slack = p_child->slack(p_child);

		if (slack <= delta) {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_samelevel_childs: SOLUTION FOUND!\n");
#endif
			break;
		}
		if (slack_old - slack > delta_progress) {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_samelevel_childs: child %d added\n", i);
#endif
			h_open_set->insert(h_open_set,&slack,p_child);
		} else {
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_push_samelevel_childs: child %d discarded\n", i);
#endif
			p_child->destroy(p_child);
		}
		p_child = NULL;
	}

#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_push_samelevel_childs: END.\n");
	printf("*************************************************************************\n");
#endif
	return p_child;
}

static idrun_t * idfsearch_greedy_best_first_search(idfsearch_t *obj, idrun_t *p_initial) {
#ifdef FSEARCH_DEBUG_L1
	printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
	printf("debug idfsearch_greedy_best_first_search: starting...\n");
#endif
	assert(obj!=NULL);
	assert(p_initial!=NULL);

	idrun_t *p_child, *p_parent;
	idrb_node_t *p_open_set;
	double slack_old, slack;
	int level;

	p_initial->compute(p_initial);
	slack = p_initial->slack(p_initial);
	if (slack <= obj->m_delta) {
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_greedy_best_first_search: SOLUTION FOUND!\n");
		printf("debug idfsearch_greedy_best_first_search: END.\n");
		printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
#endif
		return p_child;
	}
	level = p_initial->nsteps(p_initial)-1;

	p_open_set = idrb_with_double_key(NULL);
	p_open_set->insert(p_open_set,&slack,p_initial);

	while(!idrb_empty(p_open_set)) {
		p_parent = idfsearch_pop(p_open_set,&slack_old);
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_greedy_best_first_search: parent run:\n");
		p_parent->print(p_parent,stdout);

#endif

		if (level<p_parent->nsteps(p_parent)) {
			level = p_parent->nsteps(p_parent);

			p_child = idfsearch_push_levelup_childs(p_open_set,p_parent,level,slack_old,obj->m_delta,obj->m_delta_progress);
			if (p_child!=NULL) {
				p_parent->destroy(p_parent);
				p_open_set->destroy(p_open_set);
#ifdef FSEARCH_DEBUG_L1
				printf("debug idfsearch_greedy_best_first_search: SOLUTION FOUND!\n");
				printf("debug idfsearch_greedy_best_first_search: END.\n");
				printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
#endif
				return p_child;
			}
		}
		p_child = idfsearch_push_samelevel_childs(p_open_set,p_parent,level,slack_old,obj->m_delta,obj->m_delta_progress);
		if (p_child!=NULL) {
			p_parent->destroy(p_parent);
			p_open_set->destroy(p_open_set);
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_greedy_best_first_search: SOLUTION FOUND!\n");
			printf("debug idfsearch_greedy_best_first_search: END.\n");
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
#endif
			return p_child;
		}
		p_parent->destroy(p_parent);
	}
	idfsearch_destroy_open_set(p_open_set);
#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_greedy_best_first_search: NO solution found!\n");
	printf("debug idfsearch_greedy_best_first_search: END.\n");
	printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
#endif
	return NULL;
}

idbool_t   	idfsearch_search(idfsearch_t *obj) {
	assert(obj!=NULL);

#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: starting...\n");
#endif

	if (obj->m_length < 3) {
		// trivially holds
		// TODO: create a trajectory with unique state
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: trivially holds because length is: %d < 3.\n", obj->m_length);
		printf("debug idfsearch_search: END.\n");
#endif
		return TRUE;
	}

	idreach_lp_t *p_reach;
	idslfeas_lp_t *p_feas;
	idrun_t *p_run;
	idrb_node_t *it_state, *it_sys, *it_label, *it_x_constr, *it_u_constr;
	idlinear_sys_t *h_sys0, *h_sys1, *h_sys2;
	idlist_t *h_label0, *h_label1, *h_label2;
	idcsys_t *h_x0_constr, *h_u0_constr, *h_x1_constr, *h_u1_constr, *h_x2_constr, *h_u2_constr;
	int s0, s1, s2, nsample, len;
	idarray_t *p_cex;

	// Initialize search
#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: initialize...\n");
#endif
	idrun_destroy(obj->mp_solution);
	obj->mp_solution = NULL;
	idautomaton_destroy(obj->mp_cex);
	obj->mp_cex = NULL;
	p_cex = idintarray();

	it_state	= idrb_first(obj->mp_states_intarray->mp_head);
	it_sys 		= idrb_first(obj->mp_h_sys_array->mp_head);
	it_label 	= idrb_first(obj->mp_h_label_array->mp_head);
	it_x_constr	= idrb_first(obj->mp_h_x_constr_array->mp_head);
	it_u_constr	= idrb_first(obj->mp_h_u_constr_array->mp_head);

	s0 			= *(int *)it_state->value(it_state);
	h_label0 	= (idlist_t *)it_label->value(it_label);
	h_sys0 		= (idlinear_sys_t *)it_sys->value(it_sys);
	h_u0_constr	= (idcsys_t *)it_u_constr->value(it_u_constr);
	h_x0_constr	= (idcsys_t *)it_x_constr->value(it_x_constr);
	p_cex->append(p_cex,&s0);

	it_state	= idrb_next(it_state);
	it_sys 		= idrb_next(it_sys);
	it_label 	= idrb_next(it_label);
	it_x_constr	= idrb_next(it_x_constr);
	it_u_constr	= idrb_next(it_u_constr);

	s1 			= *(int *)it_state->value(it_state);
	h_label1 	= (idlist_t *)it_label->value(it_label);
	h_sys1 		= (idlinear_sys_t *)it_sys->value(it_sys);
	h_u1_constr	= (idcsys_t *)it_u_constr->value(it_u_constr);
	h_x1_constr	= (idcsys_t *)it_x_constr->value(it_x_constr);
	p_cex->append(p_cex,&s1);

	it_state	= idrb_next(it_state);
	it_sys 		= idrb_next(it_sys);
	it_label 	= idrb_next(it_label);
	it_x_constr	= idrb_next(it_x_constr);
	it_u_constr	= idrb_next(it_u_constr);

	s2 			= *(int *)it_state->value(it_state);
	h_label2 	= (idlist_t *)it_label->value(it_label);
	h_sys2 		= (idlinear_sys_t *)it_sys->value(it_sys);
	h_u2_constr	= (idcsys_t *)it_u_constr->value(it_u_constr);
	h_x2_constr	= (idcsys_t *)it_x_constr->value(it_x_constr);
	p_cex->append(p_cex,&s2);

#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: check reachability from step 0 to 2\n");
#endif
	p_reach = idreach_lp(obj->m_delta,obj->m_delta_progress,h_sys1,h_u1_constr,h_x0_constr,h_x2_constr);

	nsample = -1;
	while(p_reach->go_on(p_reach,&nsample,25));

	if (!p_reach->is_reachable(p_reach)) {
		p_reach->destroy(p_reach);
		obj->mp_cex = p_cex;
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: there is no feasible solution because it is not reachable from step 0 to 2\n");
		printf("debug idfsearch_search: END.\n");
#endif
		return FALSE;
	}
#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: check feasibility from step 0 to 2\n");
#endif
	p_feas = idslfeas_lp(obj->m_delta,obj->m_delta_progress,h_sys1,h_u1_constr,h_x0_constr,h_x1_constr,h_x2_constr);

	while(p_feas->go_on(p_feas,&nsample,25));

	if (!p_feas->is_feasible(p_feas)) {
		p_reach->destroy(p_reach);
		p_feas->destroy(p_feas);
		obj->mp_cex = p_cex;
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: there is no feasible solution because it is not feasible from step 0 to 2\n");
		printf("debug idfsearch_search: END.\n");
#endif
		return FALSE;
	}
	p_reach->destroy(p_reach);
	p_feas->destroy(p_feas);

#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: initialize root run\n");
#endif
	p_run = idrun();
	p_run->append_step(p_run,1,s1,h_label0,h_sys0,h_u0_constr,h_x0_constr);
	p_run->append_step(p_run,(nsample==1?1:nsample-1),s1,h_label1,h_sys1,h_u1_constr,h_x1_constr);
	p_run->append_step(p_run,1,s2,h_label2,h_sys2,h_u2_constr,h_x2_constr);
#ifdef FSEARCH_DEBUG_L1
	p_run->print(p_run,stdout);
#endif

	if (obj->m_length==3) {
		p_run->compute(p_run);
		obj->mp_solution = p_run;
		p_cex->destroy(p_cex);
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: trivially holds because length is: %d == 3.\n", obj->m_length);
		p_run->print(p_run,stdout);
		printf("debug idfsearch_search: END.\n");
#endif
		return TRUE;
	}

	// start seach
#ifdef FSEARCH_DEBUG_L1
	printf("debug idfsearch_search: starting the main loop\n");
#endif
	for(len=3;len<obj->m_length;len++) {
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: length = %d < %d\n", len, obj->m_length);
#endif
		s0 			= s1;
		h_label0 	= h_label1;
		h_sys0 		= h_sys1;
		h_u0_constr	= h_u1_constr;
		h_x0_constr	= h_x1_constr;

		s1 			= s2;
		h_label1 	= h_label2;
		h_sys1 		= h_sys2;
		h_u1_constr	= h_u2_constr;
		h_x1_constr	= h_x2_constr;

		it_state	= idrb_next(it_state);
		it_sys 		= idrb_next(it_sys);
		it_label 	= idrb_next(it_label);
		it_x_constr	= idrb_next(it_x_constr);
		it_u_constr	= idrb_next(it_u_constr);

		s2 			= *(int *)it_state->value(it_state);
		h_label2 	= (idlist_t *)it_label->value(it_label);
		h_sys2 		= (idlinear_sys_t *)it_sys->value(it_sys);
		h_u2_constr	= (idcsys_t *)it_u_constr->value(it_u_constr);
		h_x2_constr	= (idcsys_t *)it_x_constr->value(it_x_constr);
		p_cex->append(p_cex,&s2);

#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: check reachability from step %d (s_%d) to %d (s_%d)\n", len-2,s0,len,s2);
#endif
		p_reach = idreach_lp(obj->m_delta,obj->m_delta_progress,h_sys1,h_u1_constr,h_x0_constr,h_x2_constr);

		nsample = -1;
		while(p_reach->go_on(p_reach,&nsample,25));

		if (!p_reach->is_reachable(p_reach)) {
			p_reach->destroy(p_reach);
			obj->mp_cex = p_cex;
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_search: there is no feasible solution because it is not reachable from step %d to %d\n", len-2,len);
			printf("debug idfsearch_search: END.\n");
#endif
			return FALSE;
		}
#ifdef FSEARCH_DEBUG_L1
		printf("debug idfsearch_search: check feasibility from step %d (s_%d) to %d (s_%d)\n", len-2,s0,len,s2);
#endif
		p_feas = idslfeas_lp(obj->m_delta,obj->m_delta_progress,h_sys1,h_u1_constr,h_x0_constr,h_x1_constr,h_x2_constr);

		while(p_feas->go_on(p_feas,&nsample,25));

		if (!p_feas->is_feasible(p_feas)) {
			p_reach->destroy(p_reach);
			p_feas->destroy(p_feas);
			obj->mp_cex = p_cex;
#ifdef FSEARCH_DEBUG_L1
			printf("debug idfsearch_search: there is no feasible solution because it is not feasible from step %d to %d\n", len-2,len);
			printf("debug idfsearch_search: END.\n");
#endif
			return FALSE;
		}
		p_reach->destroy(p_reach);
		p_feas->destroy(p_feas);

		p_run->increment_at(p_run,p_run->nsteps(p_run)-1,nsample-1);
		p_run->append_step(p_run,1,s2,h_label2,h_sys2,h_u2_constr,h_x2_constr);

		p_run = idfsearch_greedy_best_first_search(obj,p_run);

		if (p_run==NULL) {
			obj->mp_cex = p_cex;
			return FALSE;
		}
	}

	obj->mp_solution = p_run;
	p_cex->destroy(p_cex);
	return TRUE;
}
