/*
 * feasibility_search.h
 *
 *  Created on: Jul 14, 2019
 *      Author: rafael
 */

#ifndef SOLVER_FEASIBILITY_SEARCH_H_
#define SOLVER_FEASIBILITY_SEARCH_H_


#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idfsearch_t * 	idfsearch_decode(double delta, double delta_progress, idkripke_t *h_M, Z3_context ctx, Z3_model m, int K);
extern idfsearch_t * 	idfsearch(double delta, double delta_progress, idbool_t isfair, int L, int si, idlist_t *h_label,
		idlinear_sys_t *h_x0_sys, idcsys_t *h_u0_constr, idcsys_t *h_x0_constr);
extern void      		idfsearch_destroy(idfsearch_t *obj);
extern idfsearch_t * 	idfsearch_cpy(idfsearch_t *obj);

/* set functions */
extern void      		idfsearch_push(idfsearch_t *obj, int si, idlist_t *h_label,
		idlinear_sys_t *h_x_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);

/* get functions */
extern idrun_t *     	idfsearch_get_solution(idfsearch_t *obj);
extern idautomaton_t *	idfsearch_get_cex(idfsearch_t *obj);
extern idbool_t 	idfsearch_isfair(idfsearch_t *obj);
extern int	idfsearch_L(idfsearch_t *obj);

/* operation */
extern idbool_t   	idfsearch_search(idfsearch_t *obj);

#ifdef IDRTL_DEBUG_MEMLEAK_FEAS
	static int		    idfsearch_destroy_cnt = 0;
#endif


struct idfsearch_s{
	/* capsulated member functions */
	void           	(*destroy)(idfsearch_t *obj);
	idfsearch_t *		(*cpy)(idfsearch_t *obj);

	/* get functions */
	void          	(*push)(idfsearch_t *obj, int si, idlist_t *h_label,
			idlinear_sys_t *h_x_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);

	/* get functions */
	idrun_t *     	(*get_solution)(idfsearch_t *obj);
	idautomaton_t *	(*get_cex)(idfsearch_t *obj);
	idbool_t 	(*isfair)(idfsearch_t *obj);
	int	(*L)(idfsearch_t *obj);


	/* operation */
	idbool_t   	(*search)(idfsearch_t *obj);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_FEAS
	int		      m_id;
#endif
	idarray_t 	*mp_h_sys_array,
				*mp_h_x_constr_array,
				*mp_h_u_constr_array,
				*mp_states_intarray,
				*mp_h_label_array;
	int			m_length,
				m_L;
	idarray_t 	*mp_cex;
	idrun_t			*mp_solution;
	double 			m_delta,
					m_delta_progress;
	idbool_t	m_isfair;
};

#ifdef __cplusplus
}
#endif


#endif /* SOLVER_FEASIBILITY_SEARCH_H_ */
