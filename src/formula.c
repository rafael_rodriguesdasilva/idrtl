/*
 * formula.c
 *
 *  Created on: May 23, 2019
 *      Author: rafael
 */

#include"formula.h"

//#define IDFORMULA_DEBUG

void             idformula_destroy(idformula_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_FORMULA
		idformula_destroy_cnt--;
		printf("debug memleak destroy formula %d still remains %d to destroy\n", obj->m_id, idbsc_destroy_cnt);
		if (idformula_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idformula_destroy(obj->mp_child1);
		idformula_destroy(obj->mp_child2);
		free(obj);
	}
}

static idformula_t *idformula_default_constructor(void) {
	idformula_t *obj = malloc(sizeof(idformula_t));
	assert(obj!=NULL);

#ifdef IDRTL_DEBUG_MEMLEAK_FORMULA
	static int idformula_last_id = 0;
	obj->m_id = idformula_last_id;
	printf("debug memleak construct formula %d\n", obj->m_id);
	idformula_last_id++;
	idformula_destroy_cnt++;
#endif
	obj->mh_M = NULL;
	obj->mp_child1 = NULL;
	obj->mp_child2 = NULL;
	obj->m_type = RTL_ATOMICPROP;
	obj->m_isneg = FALSE;
	obj->m_ind  = -1;

	obj->destroy = idformula_destroy;
	obj->length = idformula_length;
	obj->type_str = idformula_type_str;
	obj->declare_at = idformula_declare_at;
	obj->assert_at = idformula_assert_at;
	obj->assert_laststate_at = idformula_assert_laststate_at;
	obj->assert_eventrtl_at = idformula_assert_eventrtl_at;
	obj->assert_laststate_kdependent = idformula_assert_laststate_kdependent;
	obj->assert_eventrtl_kdependent = idformula_assert_eventrtl_kdependent;
	obj->assert_laststate_base = idformula_assert_laststate_base;
	obj->assert_eventrtl_base = idformula_assert_eventrtl_base;
	obj->assert_base = idformula_assert_base;
	obj->bitvecK_get = idformula_bitvecK_get;

	return obj;
}


idformula_t *    id_mkAtomicProp(int ind, idkripke_t *h_M, int label, idbool_t isneg) {
	assert(h_M!=NULL);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = h_M;
	phi->m_label = label;
	phi->m_type = RTL_ATOMICPROP;
	phi->m_isneg = isneg;
	phi->m_ind  = ind;

	return phi;
}
idformula_t *    id_mkAnd(int ind, idformula_t *p_A,idformula_t *p_B) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_A->mh_M!=NULL);
	assert(p_A->mh_M == p_B->mh_M);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child1 = p_A;
	phi->mp_child2 = p_B;
	phi->m_type = RTL_AND;
	phi->m_ind  = ind;

	return phi;
}
idformula_t *    id_mkOr(int ind, idformula_t *p_A,idformula_t *p_B) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_A->mh_M!=NULL);
	assert(p_A->mh_M == p_B->mh_M);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child1 = p_A;
	phi->mp_child2 = p_B;
	phi->m_type = RTL_OR;
	phi->m_ind  = ind;

	return phi;
}
idformula_t *    id_mkUntil(int ind, idformula_t *p_A,idformula_t *p_B) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_A->mh_M!=NULL);
	assert(p_A->mh_M == p_B->mh_M);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child1 = p_A;
	phi->mp_child2 = p_B;
	phi->m_type = RTL_UNTIL;
	phi->m_ind  = ind;

	return phi;
}
idformula_t *    id_mkRelease(int ind, idformula_t *p_A,idformula_t *p_B) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_A->mh_M!=NULL);
	assert(p_A->mh_M == p_B->mh_M);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child1 = p_A;
	phi->mp_child2 = p_B;
	phi->m_type = RTL_RELEASE;
	phi->m_ind  = ind;

	return phi;
}
idformula_t *    id_mkEventually(int ind, idformula_t *p_A) {
	assert(p_A!=NULL);
	assert(p_A->mh_M!=NULL);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child2 = p_A;
	phi->m_type = RTL_EVENTUALLY;
	phi->m_isneg = FALSE;
	phi->m_ind  = ind;

	return phi;
}

idformula_t *    id_mkAlways(int ind, idformula_t *p_A) {
	assert(p_A!=NULL);
	assert(p_A->mh_M!=NULL);

	idformula_t *phi = idformula_default_constructor();

	phi->mh_M = p_A->mh_M;
	phi->mp_child2 = p_A;
	phi->m_type = RTL_ALWAYS;
	phi->m_ind  = ind;

	return phi;
}

int              idformula_length(idformula_t *obj) {
	assert(obj!=NULL);

	if (obj->m_type == RTL_ATOMICPROP) {
		return 1;
	} else if ((obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_EVENTUALLY)) {
		return obj->mp_child2->length(obj->mp_child2) + 1;
	} else {
		return obj->mp_child1->length(obj->mp_child1) + obj->mp_child2->length(obj->mp_child2) + 1;
	}
}

const char *     idformula_type_str(idformula_t *obj) {
	assert(obj!=NULL);
	switch(obj->m_type) {
	case RTL_ATOMICPROP:
		return "atomic proposition";
		break;
	case RTL_AND:
		return "and";
		break;
	case RTL_OR:
		return "or";
		break;
	case RTL_UNTIL:
		return "until";
		break;
	case RTL_RELEASE:
		return "release";
		break;
	case RTL_ALWAYS:
		return "always";
		break;
	case RTL_EVENTUALLY:
		return "eventually";
		break;
	}

	return "unknown";
}

void  idformula_assert_laststate_base(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists) {
	assert(obj!=NULL);

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_laststate_base(obj->mp_child1,ctx,s,loop_exists);
		obj->mp_child2->assert_laststate_base(obj->mp_child2,ctx,s,loop_exists);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_laststate_base(obj->mp_child2,ctx,s,loop_exists);
	}

#ifdef IDFORMULA_DEBUG
	printf("idformula:assert_laststate_base for %s of type = %s:\n", Z3_ast_to_string(ctx,obj->m_phiK), obj->type_str(obj));
#endif

	// not Exists -> (|[phi]|_L = false)
	Z3_ast LastStateBase = Z3_mk_implies(ctx,Z3_mk_not(ctx,loop_exists),Z3_mk_not(ctx,obj->m_phiL));
	Z3_solver_assert(ctx,s,LastStateBase);

#ifdef IDFORMULA_DEBUG
	printf("%s\n", Z3_ast_to_string(ctx,LastStateBase));
#endif

}


void  idformula_assert_eventrtl_base(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists) {
	assert(obj!=NULL);

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_eventrtl_base(obj->mp_child1,ctx,s,loop_exists);
		obj->mp_child2->assert_eventrtl_base(obj->mp_child2,ctx,s,loop_exists);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_eventrtl_base(obj->mp_child2,ctx,s,loop_exists);
	}


	if ((obj->m_type == RTL_EVENTUALLY)||(obj->m_type == RTL_UNTIL)) {
#ifdef IDFORMULA_DEBUG
		printf("idformula:assert_eventrtl_base for %s of type = %s:\n", Z3_ast_to_string(ctx,obj->m_phiK), obj->type_str(obj));
#endif
		// (Exists->(|[phi1 U phi2]|_E -> <<E phi2>>_E)) and (<<E phi2>>_0 = false)
		Z3_ast EventRTL = mk_and(ctx,Z3_mk_implies(ctx,loop_exists,Z3_mk_implies(ctx,obj->m_phiE,obj->m_aux_phiE)),
								Z3_mk_not(ctx,obj->m_aux_phiK));
		Z3_solver_assert(ctx,s,EventRTL);
#ifdef IDFORMULA_DEBUG
		printf("%s\n", Z3_ast_to_string(ctx,EventRTL));
#endif

	} else if ((obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_RELEASE)) {
#ifdef IDFORMULA_DEBUG
		printf("idformula:assert_eventrtl_base for %s of type = %s:\n", Z3_ast_to_string(ctx,obj->m_phiK), obj->type_str(obj));
#endif
		// (Exists->(|[phi1 R phi2]|_E <- <<A phi2>>_E)) and (<<A phi2>>_0 = true)
		Z3_ast EventRTL = mk_and(ctx,Z3_mk_implies(ctx,loop_exists,Z3_mk_implies(ctx,obj->m_aux_phiE,obj->m_phiE)),
								obj->m_aux_phiK);
		Z3_solver_assert(ctx,s,EventRTL);
#ifdef IDFORMULA_DEBUG
		printf("%s\n", Z3_ast_to_string(ctx,EventRTL));
#endif
	}
}

void             idformula_assert_base(idformula_t *obj, Z3_context ctx, Z3_solver s) {
	assert(obj!=NULL);

#ifdef IDFORMULA_DEBUG
	printf("idformula:assert_base for %s of type = \n", Z3_ast_to_string(ctx,obj->m_phiK), obj->type_str(obj));
#endif

	Z3_solver_assert(ctx,s,obj->m_phiK);
}

void           idformula_declare_at(idformula_t *obj, Z3_context ctx, int K) {
	assert(obj!=NULL);

	char str[80];

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->declare_at(obj->mp_child1,ctx,K);
		obj->mp_child2->declare_at(obj->mp_child2,ctx,K);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->declare_at(obj->mp_child2,ctx,K);
	}

	if (K==0) {
		// define auxiliaries variables
		sprintf(str, "phiE%d", obj->m_ind);
		obj->m_phiE = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
		printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_phiE));
#endif

		sprintf(str, "phiL%d", obj->m_ind);
		obj->m_phiL = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
		printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_phiL));
#endif

		sprintf(str, "phi%d_0", obj->m_ind);
		obj->m_phiKplus = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
		printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_phiKplus));
#endif

		if ((obj->m_type == RTL_UNTIL)||(obj->m_type == RTL_EVENTUALLY)||
				(obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_RELEASE)) {
			// auxiliaries variables for the eventually encoding
			sprintf(str, "aux_phiE%d", obj->m_ind);
			obj->m_aux_phiE = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
			printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_aux_phiE));
#endif

			sprintf(str, "aux_phi%d_0", obj->m_ind);
			obj->m_aux_phiK = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
			printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_aux_phiK));
#endif
		}
	} else if ((obj->m_type == RTL_UNTIL)||(obj->m_type == RTL_EVENTUALLY)||
			(obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_RELEASE)) {
		obj->m_aux_phiKminus = obj->m_aux_phiK;
		sprintf(str, "aux_phi%d_%d", obj->m_ind,K);
		obj->m_aux_phiK = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
		printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_aux_phiK));
#endif
	}

	obj->m_phiK = obj->m_phiKplus;
	sprintf(str, "phi%d_%d", obj->m_ind, K+1);
	obj->m_phiKplus = mk_bool_var(ctx,str);
#ifdef IDFORMULA_DEBUG
	printf("idformula:declare_at: %s = %s\n", str, Z3_ast_to_string(ctx,obj->m_phiKplus));
#endif
}
void           idformula_assert_laststate_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast loopK) {
	assert(obj!=NULL);

	if (K<=0) {
		// do nothing
		return;
	}

	Z3_ast LastState;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_laststate_at(obj->mp_child1,ctx,s,K,loopK);
		obj->mp_child2->assert_laststate_at(obj->mp_child2,ctx,s,K,loopK);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_laststate_at(obj->mp_child2,ctx,s,K,loopK);
	}

#ifdef IDFORMULA_DEBUG
	printf("idformula:assert laststate for %s at %d of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),K,obj->type_str(obj));
#endif

	LastState = Z3_mk_implies(ctx,loopK,Z3_mk_iff(ctx,obj->m_phiL,obj->m_phiK));
	Z3_solver_assert(ctx,s,LastState);

#ifdef IDFORMULA_DEBUG
	printf("%s\n", Z3_ast_to_string(ctx,LastState));
#endif
}

void           idformula_assert_eventrtl_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast inLoopK) {
	assert(obj!=NULL);

	if (K<=0) {
		// do nothing
		return;
	}

	Z3_ast EventRTL;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_eventrtl_at(obj->mp_child1,ctx,s,K,inLoopK);
		obj->mp_child2->assert_eventrtl_at(obj->mp_child2,ctx,s,K,inLoopK);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_eventrtl_at(obj->mp_child2,ctx,s,K,inLoopK);
	}


	if ((obj->m_type == RTL_EVENTUALLY)||(obj->m_type == RTL_UNTIL)) {
#ifdef IDFORMULA_DEBUG
		printf("idformula:assert eventrtl for %s at %d of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),K,obj->type_str(obj));
#endif

		EventRTL = Z3_mk_iff(ctx,obj->m_aux_phiK,
							mk_or(ctx,obj->m_aux_phiKminus,
									     mk_and(ctx,inLoopK,obj->mp_child2->m_phiK)));
		Z3_solver_assert(ctx,s,EventRTL);

#ifdef IDFORMULA_DEBUG
		printf("%s\n", Z3_ast_to_string(ctx,EventRTL));
#endif

	} else if ((obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_RELEASE)) {
#ifdef IDFORMULA_DEBUG
		printf("idformula:assert eventrtl for %s at %d of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),K,obj->type_str(obj));
#endif

		EventRTL = Z3_mk_iff(ctx,obj->m_aux_phiK,
							mk_and(ctx,obj->m_aux_phiKminus,
									mk_or(ctx,
											Z3_mk_not(ctx,inLoopK),
											obj->m_phiK)));
		Z3_solver_assert(ctx,s,EventRTL);

#ifdef IDFORMULA_DEBUG
		printf("%s\n", Z3_ast_to_string(ctx,EventRTL));
#endif
	}
}

void           idformula_assert_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast* h_bitvecK) {
	assert(obj!=NULL);
	assert(obj->mh_M!=NULL);

	Z3_ast phiK;
	idkripke_t *h_M = obj->mh_M;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_at(obj->mp_child1,ctx,s,K,h_bitvecK);
		obj->mp_child2->assert_at(obj->mp_child2,ctx,s,K,h_bitvecK);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_at(obj->mp_child2,ctx,s,K,h_bitvecK);
	}


#ifdef IDFORMULA_DEBUG
	printf("idformula:assert %s at %d of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),K,obj->type_str(obj));
#endif
	switch(obj->m_type) {
	case RTL_ATOMICPROP:
		if (obj->m_isneg) {
			phiK = Z3_mk_iff(ctx,obj->m_phiK,Z3_mk_not(ctx,h_M->z3_atomicprop(h_M,ctx,h_bitvecK,obj->m_label)));
		} else {
			phiK = Z3_mk_iff(ctx,obj->m_phiK,h_M->z3_atomicprop(h_M,ctx,h_bitvecK,obj->m_label));
		}
		break;
	case RTL_AND:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,mk_and(ctx,obj->mp_child1->m_phiK,obj->mp_child2->m_phiK));
		break;
	case RTL_OR:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,mk_or(ctx,obj->mp_child1->m_phiK,obj->mp_child2->m_phiK));
		break;
	case RTL_UNTIL:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,	mk_or(ctx,obj->mp_child2->m_phiK,
											mk_and(ctx,obj->mp_child1->m_phiK,obj->m_phiKplus)));
		break;
	case RTL_RELEASE:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,	mk_and(ctx,obj->mp_child2->m_phiK,
											mk_or(ctx,obj->mp_child1->m_phiK,obj->m_phiKplus)));
		break;
	case RTL_ALWAYS:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,mk_and(ctx,obj->mp_child2->m_phiK,obj->m_phiKplus));
		break;
	case RTL_EVENTUALLY:
		phiK = Z3_mk_iff(ctx,obj->m_phiK,mk_or(ctx,obj->mp_child2->m_phiK, obj->m_phiKplus));
		break;
	}

	Z3_solver_assert(ctx,s,phiK);

#ifdef IDFORMULA_DEBUG
	printf("%s\n", Z3_ast_to_string(ctx,phiK));
#endif
}

void           idformula_assert_laststate_kdependent(idformula_t *obj, Z3_context ctx, Z3_solver s) {
	assert(obj!=NULL);

	Z3_ast LastState;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_laststate_kdependent(obj->mp_child1,ctx,s);
		obj->mp_child2->assert_laststate_kdependent(obj->mp_child2,ctx,s);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_laststate_kdependent(obj->mp_child2,ctx,s);
	}

#ifdef IDFORMULA_DEBUG
	printf("idformula:assert k-dependent last state of %s of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),obj->type_str(obj));
#endif
	LastState = mk_and(ctx,Z3_mk_iff(ctx,obj->m_phiE,obj->m_phiK),
							Z3_mk_iff(ctx,obj->m_phiL,obj->m_phiKplus));
	Z3_solver_assert(ctx,s,LastState);
#ifdef IDFORMULA_DEBUG
	printf("%s\n", Z3_ast_to_string(ctx,LastState));
#endif
}

void           idformula_assert_eventrtl_kdependent(idformula_t *obj, Z3_context ctx, Z3_solver s) {
	assert(obj!=NULL);

	Z3_ast EventRTL;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		obj->mp_child1->assert_eventrtl_kdependent(obj->mp_child1,ctx,s);
		obj->mp_child2->assert_eventrtl_kdependent(obj->mp_child2,ctx,s);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		obj->mp_child2->assert_eventrtl_kdependent(obj->mp_child2,ctx,s);
	}

	if ((obj->m_type == RTL_UNTIL)||(obj->m_type == RTL_EVENTUALLY)||
			(obj->m_type == RTL_ALWAYS)||(obj->m_type == RTL_RELEASE)) {
#ifdef IDFORMULA_DEBUG
		printf("idformula:assert k-dependent eventrtl of %s of type %s:\n", Z3_ast_to_string(ctx,obj->m_phiK),obj->type_str(obj));
#endif

		EventRTL = Z3_mk_iff(ctx,obj->m_aux_phiE,obj->m_aux_phiK);
		Z3_solver_assert(ctx,s,EventRTL);

#ifdef IDFORMULA_DEBUG
		printf("%s\n", Z3_ast_to_string(ctx,EventRTL));
#endif
	}
}

Z3_ast *         idformula_bitvecK_get(idformula_t *obj, Z3_context ctx, int *h_len_out) {
	assert(obj!=NULL);
	assert(h_len_out!=NULL);

	Z3_ast *child1 = NULL, *child2 = NULL, *retval = NULL;
	int child1_len = 0, child2_len = 0;
	int i, j;

	if ((obj->m_type == RTL_AND) || (obj->m_type == RTL_OR) ||
			(obj->m_type == RTL_UNTIL) || (obj->m_type == RTL_RELEASE)) {
		child1 = obj->mp_child1->bitvecK_get(obj->mp_child1,ctx,&child1_len);
		child2 = obj->mp_child2->bitvecK_get(obj->mp_child2,ctx,&child2_len);
	} else if ((obj->m_type == RTL_ALWAYS) || (obj->m_type == RTL_EVENTUALLY)) {
		child2 = obj->mp_child2->bitvecK_get(obj->mp_child2,ctx,&child2_len);
	}

	retval = malloc((child1_len+child2_len+1)*sizeof(Z3_ast));
	i=0;
	for(j=0;j<child1_len;j++) {
		retval[i] = child1[j];
		i++;
	}
	for(j=0;j<child2_len;j++) {
		retval[i] = child2[j];
		i++;
	}
	retval[i] = obj->m_phiK;
	i++;

	if (child1!=NULL)
		free(child1);
	if (child2!=NULL)
		free(child2);

	(*h_len_out) = i;
	return retval;
}
