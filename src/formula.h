/*
 * formula.h
 *
 *  Created on: May 23, 2019
 *      Author: rafael
 */

#ifndef FORMULA_H_
#define FORMULA_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idformula_t *    id_mkAtomicProp(int ind, idkripke_t *h_M, int label, idbool_t isneg);
extern idformula_t *    id_mkAnd(int ind, idformula_t *A,idformula_t *B);
extern idformula_t *    id_mkOr(int ind, idformula_t *A,idformula_t *B);
extern idformula_t *    id_mkUntil(int ind, idformula_t *A,idformula_t *B);
extern idformula_t *    id_mkRelease(int ind, idformula_t *A,idformula_t *B);
extern idformula_t *    id_mkEventually(int ind, idformula_t *A);
extern idformula_t *    id_mkAlways(int ind, idformula_t *A);

extern void             idformula_destroy(idformula_t *obj);
extern int              idformula_length(idformula_t *obj);
extern const char *     idformula_type_str(idformula_t *obj);

/* BSC operations */
extern void             idformula_assert_laststate_base(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists);
extern void             idformula_assert_eventrtl_base(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists);
extern void             idformula_assert_base(idformula_t *obj, Z3_context ctx, Z3_solver s);
extern void             idformula_declare_at(idformula_t *obj, Z3_context ctx, int K);
extern void             idformula_assert_laststate_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast loopK);
extern void             idformula_assert_eventrtl_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast inLoopK);
extern void             idformula_assert_at(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast* bitvecK);
extern void             idformula_assert_laststate_kdependent(idformula_t *obj, Z3_context ctx, Z3_solver s);
extern void             idformula_assert_eventrtl_kdependent(idformula_t *obj, Z3_context ctx, Z3_solver s);
extern Z3_ast *         idformula_bitvecK_get(idformula_t *obj, Z3_context ctx, int *len_out);

#ifdef IDRTL_DEBUG_MEMLEAK_FORMULA
	static int		    idformula_destroy_cnt = 0;
#endif

struct idformula_s{
	/* capsulated member functions */
	void             (*destroy)(idformula_t *obj);
	int              (*length)(idformula_t *obj);
	const char *     (*type_str)(idformula_t *obj);

	/* BSC operations */
	void             (*assert_laststate_base)(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists);
	void             (*assert_eventrtl_base)(idformula_t *obj, Z3_context ctx, Z3_solver s, Z3_ast loop_exists);
	void             (*assert_base)(idformula_t *obj, Z3_context ctx, Z3_solver s);
	void             (*declare_at)(idformula_t *obj, Z3_context ctx, int K);
	void             (*assert_at)(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast* bitvecK);
	void             (*assert_laststate_at)(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast loopK);
	void             (*assert_eventrtl_at)(idformula_t *obj, Z3_context ctx, Z3_solver s, int K, Z3_ast inLoopK);
	void             (*assert_laststate_kdependent)(idformula_t *obj, Z3_solver s, Z3_context ctx);
	void             (*assert_eventrtl_kdependent)(idformula_t *obj, Z3_solver s, Z3_context ctx);
	Z3_ast *         (*bitvecK_get)(idformula_t *obj, Z3_context ctx, int *len_out);


	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_FORMULA
	int		      m_id;
#endif
	idformula_t 		*mp_child1;
	idformula_t 		*mp_child2;
	int					 m_type;
	int					 m_ind;
	int				 	m_label;
	idbool_t			 m_isneg;
	idkripke_t          *mh_M;
	Z3_ast				 m_phiK;
	Z3_ast				 m_phiKplus;
	Z3_ast				 m_phiE;
	Z3_ast				 m_phiL;
	Z3_ast				 m_aux_phiK;
	Z3_ast				 m_aux_phiKminus;
	Z3_ast				 m_aux_phiE;
};

#endif /* FORMULA_H_ */
