/*
 * gmpdata.c
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *
 *      Functions to work with GMP data types from Geometric calculation library,
 *      found in http://worc4021.github.io/
 *
 *
 */

#include "gmpdata.h"

void mpz_to_mpq(mpq_t *rop, mpz_t *op, size_t m)
{
    size_t i;
    assert( rop != NULL && op != NULL);
    if (!mpz_sgn(op[0]))
    {
        mpz_t norm;
        mpz_init(norm);
        mpz_norm(norm, op, m);
        for (i = 0; i < m; ++i)
        {
            mpq_set_num(rop[i], op[i]);
            mpq_set_den(rop[i], norm);
            mpq_canonicalize(rop[i]);
        }
        mpz_clear(norm);
    }else if (mpz_sgn(op[0]) == -1)
    {
      mpz_t helper;
      mpz_init(helper);
      mpz_neg(helper, op[0]);
      for (i = 0; i < m; ++i)
        {
            mpq_set_num(rop[i], op[i]);
            mpq_set_den(rop[i], helper);
            mpq_canonicalize(rop[i]);
        }
      mpz_clear(helper);

    }else{
        for (i = 0; i < m; ++i)
        {
            mpq_set_num(rop[i], op[i]);
            mpq_set_den(rop[i], op[0]);
            mpq_canonicalize(rop[i]);
        }
    }
}


mpq_t *mpz_to_mpq_vec(mpz_t *op, size_t m)
{
    size_t i;
    assert( op != NULL);
    mpq_t *retVal;
    retVal = malloc(m*sizeof(*retVal));
    if (zero(op[0]))
    {
        mpz_t norm;
        mpz_init(norm);
        mpz_norm(norm, op, m);
        for (i = 0; i < m; ++i)
        {
            mpq_init(retVal[i]);
            mpq_set_num(retVal[i], op[i]);
            mpq_set_den(retVal[i], norm);
            mpq_canonicalize(retVal[i]);
        }
        mpz_clear(norm);
    }else if ( !(mpz_cmp_si(op[0],0L)+1) ){

        mpq_t valueHolder;
        mpq_init(valueHolder);

        for (i = 0; i < m; ++i)
        {
            mpq_init(retVal[i]);
            mpq_set_num(valueHolder, op[i]);
            mpq_set_den(valueHolder, op[0]);
            mpq_canonicalize(valueHolder);
            mpq_neg(retVal[i],valueHolder);
        }
        mpq_clear(valueHolder);

    }else{
        for (i = 0; i < m; ++i)
        {
            mpq_init(retVal[i]);
            mpq_set_num(retVal[i], op[i]);
            mpq_set_den(retVal[i], op[0]);
            mpq_canonicalize(retVal[i]);
        }
    }
    return retVal;
}

void mpz_norm(mpz_t norm, mpz_t *row, size_t m)
{
    assert( row != NULL );
    mpz_t help;
    mpz_init(help);
    size_t i;
    for (i = 0; i < m; ++i)
    {
        mpz_addmul(help, row[i], row[i]);
    }
    mpz_sqrt(norm, help);
    mpz_clear(help);
}


mpz_t *mpz_row_init_one(size_t m)
{
    assert(m > 0);
    mpz_t *retVal;
    retVal = malloc(m*sizeof(mpz_t));
    size_t i;
    assert(retVal != NULL);
    for (i = 0; i < m; ++i)
    {
        mpz_init_set_ui(retVal[i],1L);
    }
    return retVal;
}

void mpq_row_init(mpq_t *row, size_t m)
{
    assert(row != NULL);
    size_t i;
    for (i = 0; i < m; ++i)
    {
        mpq_init(row[i]);
    }
}

void mpz_row_clean(mpz_t *row, size_t m)
{
    assert(row != NULL);
    size_t i;
    for (i = 0; i < m; ++i)
    {
        mpz_clear(row[i]);
    }
    free(row);
}

void mpq_row_clean(mpq_t *row, size_t m)
{
    assert(row != NULL);
    size_t i;
    for (i = 0; i < m; ++i)
    {
        mpq_clear(row[i]);
    }
    free(row);
}


#ifdef DEBUG
void mpz_row_print(mpz_t *row, size_t n)
{
    assert ( row != NULL );
    size_t i;
    for (i = 0; i < n; ++i)
    {
        mpz_out_str(stdout, 10, row[i]);
        fprintf(stdout, " " );
    }
    fprintf(stdout, "\n" );
}

void mpq_row_print(mpq_t *row, size_t n)
{
    assert ( row != NULL );
    size_t i;
    for (i = 0; i < n; ++i)
    {
        mpq_out_str(stdout, 10, row[i]);
        fprintf(stdout, " " );
    }
    fprintf(stdout, "\n" );
}



void mpz_print(mpz_t op)
{
    assert( op != NULL );
    fprintf(stdout, "\n");
    mpz_out_str(stdout, 10, op);
    fprintf(stdout, "\n");
}

void mpq_print(mpq_t op)
{
    assert( op != NULL );
    fprintf(stdout, "\n");
    mpq_out_str(stdout, 10, op);
    fprintf(stdout, "\n");
}

void mpz_print_product(mpz_t numA, mpz_t denA, mpz_t numB, mpz_t denB)
{
    assert( numA != NULL && denA != NULL && numB != NULL && denB != NULL );
    mpq_t a,b, res;
    mpq_init(a);
    mpq_init(b);
    mpq_init(res);

    mpq_set_num(a, numA);
    mpq_set_den(a, denA);
    mpq_canonicalize(a);

    mpq_set_num(b, numB);
    mpq_set_den(b, denB);
    mpq_canonicalize(b);

    mpq_mul(res, a, b);

    mpq_print(res);

    mpq_clear(res);
    mpq_clear(b);
    mpq_clear(a);
}
#endif /* DEBUG */

mpq_t *mpq_normalised_row(mpq_t *row, size_t m)
{
  assert ( row != NULL );
  mpq_t zero;
  mpq_init(zero);
  assert( mpq_equal(row[0],zero) );
  size_t i;
  mpq_t *retVal, norm, curVal, Mi;
  retVal = malloc( sizeof(*retVal)*(m-1) );
  assert(retVal != NULL );
  mpq_init(norm);
  mpq_init(curVal);
  mpq_init(Mi);
  mpq_set_ui(Mi, 1L , m-1);

  for (i = 1; i < m; i++)
  {
    mpq_abs(curVal,row[i]);
    mpq_add(norm,norm,curVal);
  }
  mpq_mul(norm, norm, Mi);
  mpq_canonicalize(norm);

  for (i = 0; i < m-1; i++)
  {
    mpq_init(retVal[i]);
    mpq_div(retVal[i],row[i+1],Mi);
    mpq_canonicalize(retVal[i]);
  }
  return retVal;
  mpq_row_clean(row, m);
  mpq_clear(norm);
  mpq_clear(curVal);
  mpq_clear(Mi);

}

void lprint(long *array, size_t l)
{
    fprintf(stdout, "\n");
    size_t i;
    for (i = 0; i < l; i++)
        fprintf(stdout, "%ld ", array[i]);
    fprintf(stdout, "\n");
}
