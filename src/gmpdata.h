/*
 * gmpdata.h
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      Functions to work with GMP data types from Geometric calculation library,
 *      found in http://worc4021.github.io/
 *
 */

#ifndef GMPDATA_H_
#define GMPDATA_H_

#undef __cplusplus

#ifdef __cplusplus
extern "C" {
#endif

#include <gmp.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include "lrslib.h"

void mpz_to_mpq(mpq_t *rop, mpz_t *op, size_t m);
mpq_t *mpz_to_mpq_vec(mpz_t *op, size_t m); // used in lrsinterface.c (H2V,V2H)
void mpz_norm(mpz_t norm, mpz_t *row, size_t m);
mpz_t *mpz_row_init_one(size_t m); // used in lrsinterface.c (V2H)
void mpq_row_init(mpq_t *row, size_t m);
void mpz_row_init(mpz_t *row, size_t m);
void mpq_row_clean(mpq_t *row, size_t m);
void mpz_row_clean(mpz_t *row, size_t m); // used in lrsinterface.c (V2H)

mpq_t *mpq_normalised_row(mpq_t *row, size_t m);
void lprint(long *array, size_t l);

// nothing

#ifdef __cplusplus
}
#endif

#endif /* GMPDATA_H_ */
