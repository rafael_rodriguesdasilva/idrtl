/*
 * gmplist.c
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      GMPlist structure from from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#ifndef GMPLIST_C_
#define GMPLIST_C_

#include "gmplist.h"

struct GMPlist *GMPlist_create() {
	struct GMPlist *retVal = malloc(sizeof(struct GMPlist));
	assert(retVal!=NULL);
	retVal->next = NULL;
	retVal->lte = 0;

	retVal->push = GMPlist_push;
	retVal->pull = GMPlist_pull;
	retVal->to_matrix = GMPlist_toGMPmat;
	retVal->to_double = GMPlist_toDouble;

	return retVal;
}


struct GMPlist *GMPlist_push(struct GMPlist * elem, mpq_t* data){
  struct GMPlist * nextElem = GMPlist_create();

  nextElem->next = elem;
  nextElem->lte = elem->lte + 1;

  nextElem->data = data;
  return nextElem;
}

mpq_t *GMPlist_pull(struct GMPlist ** elem, size_t n){
  assert(*elem !=  NULL);
  mpq_t* retVal = malloc( n*sizeof(mpq_t) );
  size_t i;
  struct GMPlist * curElem = (*elem);
  for (i = 0; i < n; i++)
  {
    mpq_init(retVal[i]);
    mpq_set(retVal[i],curElem->data[i]);
    mpq_clear(curElem->data[i]);
  }

  (*elem) = curElem->next;
  free(curElem);
  return retVal;
}

idmatrix_t *GMPlist_toGMPmat(struct GMPlist * elem, size_t n){
  assert( elem->next != NULL );
  idmatrix_t *retVal;
  struct GMPlist * curElem;
  struct GMPlist * nextElem;
  curElem = elem;
  size_t i, lte;
  retVal = malloc(sizeof(*retVal));
  retVal->empty = 0;
  retVal->m = elem->lte;
  retVal->n = n;
  retVal->data = malloc( retVal->m*n*sizeof(*retVal->data) );

  while (curElem->next != NULL){
    nextElem = curElem->next;
    lte = curElem->lte;
    for (i=0; i < n; i++){
      mpq_init(retVal->data[n*(lte-1)+i]);
      mpq_set(retVal->data[n*(lte-1)+i], curElem->data[i]);
      mpq_clear(curElem->data[i]);
    }
    free(curElem->data);
    free(curElem);
    curElem = nextElem;

  }
  free(curElem);
  GMPmat_initfuncs(retVal);
  return retVal;
}

double *GMPlist_toDouble(struct GMPlist * elem, size_t n){
  assert( elem->next != NULL );
  double* retVal;
  struct GMPlist * curElem;
  struct GMPlist * nextElem;
  curElem = elem;
  size_t i, lte;

  retVal = malloc((elem->lte)*n*sizeof(double));

  while (curElem->next != NULL){
    nextElem = curElem->next;
    lte = curElem->lte;
    for (i=0; i < n; i++){
      retVal[n*(lte-1)+i] = mpq_get_d(curElem->data[i]);
      mpq_clear(curElem->data[i]);
    }
    free(curElem);
    curElem = nextElem;

  }
  return retVal;
}


#endif /* GMPLIST_C_ */
