/*
 * gmplist.h
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      GMPlist structure from from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#ifndef GMPLIST_H_
#define GMPLIST_H_

#include "gmpmat.h"

#ifdef __cplusplus
extern "C" {
#endif

struct GMPlist *GMPlist_create();
/* Addition of GMPlist */
struct GMPlist * GMPlist_push(struct GMPlist* elem, mpq_t* data);
mpq_t *GMPlist_pull(struct GMPlist** elem, size_t n);

struct GMPmat *GMPlist_toGMPmat(struct GMPlist* elem, size_t n);
double *GMPlist_toDouble(struct GMPlist* elem, size_t n);

struct GMPlist {
	/* capsulated member functions */
	struct GMPlist * (*push)(struct GMPlist* elem, mpq_t* data);
	mpq_t *(*pull)(struct GMPlist** elem, size_t n);

	struct GMPmat *(*to_matrix)(struct GMPlist* elem, size_t n);
	double *(*to_double)(struct GMPlist* elem, size_t n);

	/* private variables - do not access directly */
    mpq_t* data;
    size_t lte;
    struct GMPlist* next;
};





#ifdef __cplusplus
}
#endif

#endif /* GMPLIST_H_ */
