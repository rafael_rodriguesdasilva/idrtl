/*
 * gmpmat.c
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      GMPmat structure from from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#include "gmpmat.h"

/////////////////////////////////////////////////////////
// private functions
idmatrix_t *GMPmat_initfuncs(idmatrix_t *A) {
    A->destroy   = GMPmat_destroy;
    A->cpy      = GMPmat_create_copy;
    A->print     = GMPmat_dbl_print;
    A->multiply  = GMPmat_multiply;
    A->add  		= GMPmat_add;
    A->neg  		= GMPmat_neg;
    A->iszero    = GMPmat_iszero;

    A->stackVert = GMPmat_stackVert;
    A->stackHoriz = GMPmat_stackHoriz;
    A->VertBreakdown = GMPmat_VertBreakdown;
    A->liftHoriz = GMPmat_liftHoriz;
    A->unliftHoriz = GMPmat_unliftHoriz;
    A->liftVert = GMPmat_liftVert;
    A->unliftVert = GMPmat_unliftVert;

    A->setat     = GMPmat_setValue;
    A->getat     = GMPmat_getValue_d;
    A->getRow    = GMPmat_getRow;
    A->count_zeros = GMPmat_count_zeros;
    A->mpq_row_extract = GMPmat_mpq_row_extract;
    A->to_sparse_rowvector = GMPmat_to_sparse_rowvector;
    A->to_sparse_colvector = GMPmat_to_sparse_colvector;

    A->rows      = GMPmat_Rows;
    A->cols      = GMPmat_Cols;
    return A;
}
idmatrix_t *GMPmat_create (size_t m, size_t n, int init)
     {
       idmatrix_t *A;
       size_t         i, j;

       A = malloc (sizeof (*A));
       assert (A != NULL);
       GMPmat_initfuncs(A);

       A->empty = 0;
       A->m    = m;
       A->n    = n;
       A->data = malloc (m*n*sizeof(*A->data));
       assert (A->data != NULL );

       if (init != 0)
       {
         for (i=0; i!=m; ++i){
           for (j=0; j!=n; ++j){
            mpq_init (A->data[i*(A->n) + j]);

          }
        }
       }

       return (A);
}
void GMPmat_getValue (mpq_t rop, const idmatrix_t *A, size_t r, size_t c)
{
       GMPmat_validate_indices (A, r, c);
       mpq_set (rop, A->data[r*(A->n) + c]);
}
void GMPmat_validate_indices (const idmatrix_t *A, size_t r, size_t c)
     {
       assert (A != NULL);
       assert (r < A->m);
       assert (c < A->n);
     }
void GMPmat_setRow(const idmatrix_t *A, const mpq_t *row, size_t r)
{
  assert ( A != NULL && row != NULL );
  size_t i;
  for (i = 0; i < GMPmat_Cols(A); i++)
  {
    mpq_set(A->data[r*A->n+i],row[i]);
  }
}
/////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////
// constructors
idmatrix_t *id_eye(size_t m, size_t n) {
	idmatrix_t *matrix = id_zeros(m,n);
	for(size_t i=0;((i<matrix->rows(matrix))&&(i<matrix->cols(matrix)));i++) {
		matrix->setat(matrix,i,i,1.0);
	}
	return matrix;
}
idmatrix_t *id_zeros(size_t m, size_t n) {
	idmatrix_t *matrix = GMPmat_create (m, n, 1);
	return matrix;
}
idmatrix_t *id_ones(size_t m, size_t n) {
	idmatrix_t *matrix = id_zeros(m,n);
	for(size_t i=0;i<matrix->rows(matrix);i++) {
		for(size_t j=0;j<matrix->cols(matrix);j++) {
			matrix->setat(matrix,i,j,1.0);
		}
	}
	return matrix;
}
/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////
// public matrix operations
idmatrix_t *GMPmat_liftHoriz(idmatrix_t *A)
{
  assert ( A != NULL );
  size_t m,n, i, j;
  m = GMPmat_Rows(A);
  n = GMPmat_Cols(A) + 1;
  idmatrix_t *retVal;
  retVal = GMPmat_create(m, n, 0);
  mpq_t curVal;
  mpq_init(curVal);

  for (i = 0; i < m; i++)
  {
    mpq_init(retVal->data[i*n]);
    mpq_canonicalize(retVal->data[i*n]);
    for (j = 1; j < n; j++)
    {
      GMPmat_getValue(curVal, A, i, j-1);
      mpq_canonicalize(curVal);
      mpq_init(retVal->data[i*n + j]);
      mpq_set(retVal->data[i*n + j],curVal);
    }
  }
  GMPmat_destroy(A);
  return retVal;
}

idmatrix_t *GMPmat_unliftHoriz(idmatrix_t *A)
{
  assert ( A != NULL );
  size_t m, n, i, curRow;
  m = GMPmat_Rows(A);
  n = GMPmat_Cols(A) - 1;
  idmatrix_t *retVal;
  retVal = GMPmat_create(GMPmat_count_zeros(A), n, 1);
  curRow = 0;
  mpq_t curVal, zero;
  mpq_init(curVal);
  mpq_init(zero);
  mpq_canonicalize(zero);
  mpq_t *currentRow;
  currentRow = malloc(A->m*sizeof(*currentRow));

  for (i = 0; i < m; i++)
  {
    if (mpq_equal(A->data[i*A->n],zero))
      {
        currentRow = A->mpq_row_extract(A, i);
        currentRow = mpq_normalised_row(currentRow, GMPmat_Cols(A));
        GMPmat_setRow(retVal, currentRow, curRow);
        curRow += 1;
      }
  }
  mpq_clear(curVal);
  mpq_clear(zero);
  GMPmat_destroy(A);
  return retVal;

}

idmatrix_t *GMPmat_liftVert(idmatrix_t *A) {
	assert ( A != NULL );
	size_t m, n, i, j;
	m = GMPmat_Rows(A)+1;
	n = GMPmat_Cols(A);
//	idmatrix_t *retVal = GMPmat_create(m,n,0);
//	for(i=0;i<m;i++) {
//		for(j=0;j<n;j++) {
//			if (i<m-1)
//				mpq_set(retVal->data[i*n+j],A->data[i*n+j]);
//			else
//				mpq_init(retVal->data[i*n+j]);
//		}
//	}
	idmatrix_t *retVal = id_zeros(m,n);
	for(i=0;i<m-1;i++) {
		for(j=0;j<n;j++) {
			retVal->setat(retVal,i,j,A->getat(A,i,j));
		}
	}
	A->destroy(A);
	return retVal;
}
idmatrix_t *GMPmat_unliftVert(idmatrix_t *A) {
	assert ( A != NULL );
	size_t m, n, i, j;
	m = GMPmat_Rows(A)-1;
	n = GMPmat_Cols(A);
	idmatrix_t *retVal = GMPmat_create(m,n,0);
	for(i=0;i<m;i++) {
		for(j=0;j<n;j++) {
			mpq_set(retVal->data[i*n+j],A->data[i*n+j]);
		}
	}
	A->destroy(A);
	return A;
}

idmatrix_t *GMPmat_copy (idmatrix_t *A) {
	idmatrix_t *B = id_zeros(A->n,A->m);
	for(size_t i=0;i<A->rows(A);i++) {
		for(size_t j=0;j<A->cols(A);j++) {
			B->setat(B,i,j,A->getat(A,i,j));
		}
	}

	return B;
}

size_t GMPmat_count_zeros(const idmatrix_t *A)
{
  assert( A != NULL );
  size_t i, retVal = 0;
  mpq_t curVal;
  mpq_init(curVal);
  for (i = 0; i < GMPmat_Rows(A); i++)
    {
      GMPmat_getValue(curVal, A, i, 0);
      if (mpq_sgn(curVal) == 0)
        retVal += 1;
    }
  return retVal;
  mpq_clear(curVal);
}

void GMPmat_dbl_print(const idmatrix_t *A, FILE *out) {
    assert( A!= NULL );
    size_t i, j;
	for(i=0;i<A->rows(A);i++) {
		for(j=0;j<A->cols(A);j++) {
			if (j>0) fprintf(out,",");
			fprintf(out,"%.3f",A->getat(A,i,j));
		}
		fprintf(out,"\n");
	}
}

void GMPmat_getRow(idmatrix_t *A, mpz_t *ropN, mpz_t *ropD, size_t r)
{
    assert( r < A->m );
    assert( ropN != NULL && ropD != NULL );
    size_t i;
    for (i = 0; i < GMPmat_Cols(A); ++i)
    {
        mpz_set(ropN[i], mpq_numref(A->data[r*(A->n) + i]));
        mpz_set(ropD[i], mpq_denref(A->data[r*(A->n) + i]));
    }
}

void GMPmat_destroy (idmatrix_t *A)
     {

       if (A != NULL) {
		   size_t i, j;
		   for (i = 0; i < GMPmat_Rows(A); ++i)
		   {
			for (j = 0; j < GMPmat_Cols(A); ++j)
			{
				mpq_clear(A->data[i*(A->n) + j]);
			}
		   }
		   if (A->data != NULL) free (A->data);
		   A->data = NULL;
		   A->n   = 0;
		   A->m   = 0;
		   free (A);
       }

     }



idmatrix_t *GMPmat_create_eye(size_t m, size_t n) {
	idmatrix_t *retVal;
	size_t         i;

	retVal = GMPmat_create (m, n, 1);

	if (m<=n) {
		for(i=0;i<m;i++) {
			GMPmat_setValue(retVal,i,i,1.0);
		}
	} else {
		for(i=0;i<n;i++) {
			GMPmat_setValue(retVal,i,i,1.0);
		}
	}

	return retVal;
}
idmatrix_t *GMPmat_create_copy(idmatrix_t *A) {
	idmatrix_t *retVal;
	size_t         i, j;

	retVal = GMPmat_create (GMPmat_Rows(A), GMPmat_Cols(A), 1);

	for(i=0;i<GMPmat_Rows(A);i++) {
		for(j=0;j<GMPmat_Cols(A);j++) {
			GMPmat_setValue(retVal,i,j,GMPmat_getValue_d(A,i,j));
		}
	}

	return retVal;
}


size_t GMPmat_Rows (const idmatrix_t *A)
     {
       assert (A != NULL);

       return (A->m);
     }

size_t GMPmat_Cols (const idmatrix_t *A)
     {
       assert (A != NULL);

       return (A->n);
     }

void GMPmat_setValue (const idmatrix_t *A, size_t r, size_t c, double val)
{
       GMPmat_validate_indices (A, r, c);
       mpq_set_d(A->data[r*(A->n) + c], val);
}


double GMPmat_getValue_d (const idmatrix_t *A, size_t r, size_t c) {
	mpq_t rop;
	double retVal;
	mpq_init(rop);
	GMPmat_getValue (rop, A, r, c);
	retVal = mpq_get_d(rop);
	mpq_clear(rop);
	return retVal;
}

void GMPmat_invertSignForFacetEnumeration(idmatrix_t *A)
{
  assert ( A != NULL );
  size_t i, j, m, n;
  m = GMPmat_Rows(A);
  n = GMPmat_Cols(A);
  mpq_t holder;
  mpq_init(holder);
  for (i = 0; i < m; i++)
  {
    for (j = 1; j < n; j++)
    {
      GMPmat_getValue(holder, A, i, j);
      mpq_neg(A->data[i*(A->n) + j], holder);
    }
  }
  mpq_clear(holder);
}



idmatrix_t *GMPmat_stackVert(idmatrix_t *A, idmatrix_t *B)
{
  assert ( A != NULL && B != NULL );
  assert ( A->n == B->n );
  idmatrix_t *retVal;
  retVal = GMPmat_create(A->m + B->m, A->n, 0);
  size_t i,j, n = A->n;

  for (i = 0; i<A->m ; i++)
  {
    for (j = 0; j < A->n ; j++)
    {
      mpq_init(retVal->data[i*n + j]);
      mpq_set(retVal->data[i*n + j],A->data[i*n + j]);
    }
  }

  for (i = 0; i< B->m ; i++)
  {
    for (j = 0; j < n; j++)
    {
      mpq_init(retVal->data[A->m*n+i*n +j]);
      mpq_set(retVal->data[A->m*n+i*n +j],B->data[i*n + j]);
    }
  }
  GMPmat_destroy(B);
  GMPmat_destroy(A);
  return retVal;
}

idmatrix_t *GMPmat_stackHoriz(idmatrix_t *A, idmatrix_t *B)
{
  assert ( A != NULL && B != NULL );
  assert ( A->m == B->m );
  idmatrix_t *retVal;
  retVal = GMPmat_create(A->m, A->n + B->n, 0);
  size_t i,j, n = A->n + B->n;

  for (i = 0; i<A->m ; i++)
  {
    for (j = 0; j < A->n ; j++)
    {
      mpq_init(retVal->data[i*n + j]);
      mpq_set(retVal->data[i*n + j],A->data[i*A->n + j]);
    }
  }
  for (i = 0; i< B->m ; i++)
  {
    for (j = 0; j < B->n; j++)
    {
      mpq_init(retVal->data[i*n +j+A->n]);
      mpq_set(retVal->data[i*n +j+A->n],B->data[i*B->n + j]);
    }
  }
  GMPmat_destroy(B);
  GMPmat_destroy(A);
  return retVal;
}

idmatrix_t *GMPmat_VertBreakdown(idmatrix_t *A) {
	assert(A!=NULL);

	size_t n,m,i,j;
	idmatrix_t *retVal, *Acpy;

	Acpy = A->cpy(A);
	n = A->cols(A);
	m = A->rows(A);
	retVal = GMPmat_create(m,1,0);
    for (i = 0; i < m; ++i)
    	for (j = 0; j < n; ++j)
    		mpq_clear(A->data[i*(A->n) + j]);
    if (A->data != NULL) free(A->data);
    A->data = NULL;
    A->n--;
    A->data = malloc(A->n*A->m*sizeof(mpq_t));
    assert(A->data!=NULL);

    for(i=0;i<m;i++) {
		mpq_init(retVal->data[i]);
		mpq_set(retVal->data[i],Acpy->data[i*n]);
		for(j=1;j<n;j++) {
			mpq_init(A->data[i*(n-1)+j-1]);
			mpq_set(A->data[i*(n-1)+j-1],Acpy->data[i*n+j]);
		}
	}

    Acpy->destroy(Acpy);
	return retVal;
}

idmatrix_t *GMPmat_multiply(idmatrix_t *A, idmatrix_t *B) {
	assert ( A != NULL && B != NULL );
	assert ( A->cols(A) == B->rows(B) );

	size_t m, n, N, i, j, k;
	m = A->rows(A);
	n = B->cols(B);
	N = A->cols(A);
	idmatrix_t *retVal;
	retVal = id_zeros(m,n);
	double sum;

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			sum = 0.0;
			for(k=0;k<N;k++) {
				sum = sum + A->getat(A,i,k)*B->getat(B,k,j);
			}
			retVal->setat(retVal,i,j,sum);
	    }
	}
	A->destroy(A);
	B->destroy(B);
	return retVal;
}

idmatrix_t *GMPmat_add(idmatrix_t *A, idmatrix_t *B) {
	assert ( A != NULL && B != NULL );
	assert ( GMPmat_Cols(A) == GMPmat_Cols(B) );
	assert ( GMPmat_Rows(A) == GMPmat_Rows(B) );
	size_t m, n, i, j;
	m = GMPmat_Rows(A);
	n = GMPmat_Cols(A);
	idmatrix_t *retVal;
	retVal = GMPmat_create(m, n, 0);
	mpq_t curVal, aVal, bVal;
	mpq_init(curVal);
	mpq_init(aVal);
	mpq_init(bVal);

	for (i = 0; i < m; i++)
	{
		mpq_init(retVal->data[i*n]);
		mpq_canonicalize(retVal->data[i*n]);
		for (j = 0; j < n; j++)
		{
			GMPmat_getValue(aVal, A, i, j);
			GMPmat_getValue(bVal, B, i, j);
			mpq_add(curVal, aVal, bVal);
			mpq_canonicalize(curVal);
			mpq_init(retVal->data[i*n + j]);
			mpq_set(retVal->data[i*n + j],curVal);
	    }
	}

	A->destroy(A);
	B->destroy(B);
	return retVal;
}
idmatrix_t *GMPmat_neg(idmatrix_t *A) {
	assert ( A != NULL);
	size_t m, n, i, j;
	m = GMPmat_Rows(A);
	n = GMPmat_Cols(A);
	idmatrix_t *retVal;
	retVal = GMPmat_create(m, n, 1);
	mpq_t curVal, aVal;
	mpq_init(curVal);
	mpq_init(aVal);

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			GMPmat_getValue(aVal, A, i, j);
			mpq_neg(curVal, aVal);
			mpq_canonicalize(curVal);
			mpq_init(retVal->data[i*n + j]);
			mpq_set(retVal->data[i*n + j],curVal);
	    }
	}

	A->destroy(A);
	return retVal;
}
int GMPmat_iszero(idmatrix_t *A, int r, int c) {
    GMPmat_validate_indices (A, r, c);
    mpq_t curVal;
    mpq_init(curVal);
    GMPmat_getValue(curVal, A, r, c);
    return (mpq_sgn(curVal) == 0?1:0);
}

void GMPmat_to_sparse_rowvector(idmatrix_t *A, int row, int *h_numnz, int **hp_ind, double **hp_val) {
	assert ( A != NULL);
	GMPmat_validate_indices(A,row,0);

	int n, j;
    double v;
    int *p_ind, numnz;
    double *p_val;

    n = A->cols(A);
    p_ind = malloc(n*sizeof(int));
	assert(p_ind!=NULL);
	p_val = malloc(n*sizeof(double));
	assert(p_val!=NULL);
	numnz = 0;


	for(j=0;j<n;j++) {
		v = A->getat(A,row,j);
		if (fabs(v) >= 0.0000000001) {
			p_ind[numnz] = j;
			p_val[numnz] = v;
			numnz++;
		}
	}
	*hp_ind = p_ind;
	*hp_val = p_val;
	*h_numnz = numnz;
}
void GMPmat_to_sparse_colvector(idmatrix_t *A, int col, int *h_numnz, int **hp_ind, double **hp_val) {
	assert ( A != NULL);
	GMPmat_validate_indices(A,0,col);

	int n, m, i;
    double v;
    int *p_ind, numnz;
    double *p_val;

    n = A->cols(A);
    m = A->rows(A);
    p_ind = malloc(m*sizeof(int));
	assert(p_ind!=NULL);
	p_val = malloc(m*sizeof(double));
	assert(p_val!=NULL);
	numnz = 0;

	for(i=0;i<m;i++) {
		v = A->getat(A,i,col);
		if (fabs(v) >= 0.0000000001) {
			p_ind[numnz] = i;
			p_val[numnz] = v;
			numnz++;
		}
	}
	*hp_ind = p_ind;
	*hp_val = p_val;
	*h_numnz = numnz;
}

/* Functions to work with GMP data types */


mpq_t *GMPmat_mpq_row_extract(const idmatrix_t *A, size_t r)
{
    assert ( A != NULL && r < GMPmat_Rows(A) );
    mpq_t *retVal;
    size_t i, n;
    n = GMPmat_Cols(A);
    retVal = malloc( n*sizeof(*retVal) );
    for ( i = 0; i < n; ++i )
    {
        mpq_init( retVal[i] );
        GMPmat_getValue( retVal[i], A, r, i);
    }
    return retVal;

}
