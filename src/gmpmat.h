/*
 * gmpmat.h
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      GMPmat structure from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#ifndef GMPMAT_H_
#define GMPMAT_H_

#include "gmpdata.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct GMPmat idmatrix_t;

/* constructors */
idmatrix_t *id_eye(size_t m, size_t n);
idmatrix_t *id_zeros(size_t m, size_t n);
idmatrix_t *id_ones(size_t m, size_t n);

struct GMPmat {
	/* capsulated member functions */
	void         (*destroy)(const idmatrix_t *A);
	idmatrix_t * (*cpy)(const idmatrix_t *A);

	/* matrix operations */
	idmatrix_t * (*multiply)(idmatrix_t *A, idmatrix_t *B);
	idmatrix_t * (*add)(idmatrix_t *A, idmatrix_t *B);
	idmatrix_t * (*neg)(idmatrix_t *A);
	idmatrix_t * (*stackVert)(idmatrix_t *A, idmatrix_t *B);
	idmatrix_t * (*stackHoriz)(idmatrix_t *A, idmatrix_t *B);
	idmatrix_t * (*VertBreakdown)(idmatrix_t *A);
	idmatrix_t * (*liftHoriz)(idmatrix_t *A);
	idmatrix_t * (*unliftHoriz)(idmatrix_t *A);
	idmatrix_t * (*liftVert)(idmatrix_t *A);
	idmatrix_t * (*unliftVert)(idmatrix_t *A);

	/* set functions */
	void    (*setat)(const idmatrix_t *A, size_t r, size_t c, double val);

	/* get functions */
	double  (*getat)(const idmatrix_t *A, size_t r, size_t c);
	void 	(*getRow)(idmatrix_t *A, mpz_t *ropN, mpz_t *ropD, size_t r);
	void    (*print)(const idmatrix_t *A, FILE *out);
	size_t  (*count_zeros)(const idmatrix_t *A);
	size_t  (*rows)(const idmatrix_t *A);
	size_t  (*cols)(const idmatrix_t *A);
	int     (*iszero)(idmatrix_t *A, int r, int c);
	mpq_t * (*mpq_row_extract)(const idmatrix_t *A, size_t r);
	void 	(*to_sparse_rowvector)(idmatrix_t *A, int row, int *h_numnz, int **hp_ind, double **hp_val);
	void 	(*to_sparse_colvector)(idmatrix_t *A, int col, int *h_numnz, int **hp_ind, double **hp_val);

	/* private variables - do not access directly */
    mpq_t *data;
    size_t m, n;
    char empty;
};

/* public functions created for idSTL solver */
void GMPmat_destroy (idmatrix_t *A);
idmatrix_t *GMPmat_create_copy(idmatrix_t *A);

/* matrix operations */
idmatrix_t *GMPmat_liftHoriz(idmatrix_t *A);
idmatrix_t *GMPmat_unliftHoriz(idmatrix_t *A);
idmatrix_t *GMPmat_liftVert(idmatrix_t *A);
idmatrix_t *GMPmat_unliftVert(idmatrix_t *A);
void GMPmat_invertSignForFacetEnumeration(idmatrix_t *A);
idmatrix_t *GMPmat_stackVert(idmatrix_t *A, idmatrix_t *B);
idmatrix_t *GMPmat_stackHoriz(idmatrix_t *A, idmatrix_t *B);
idmatrix_t *GMPmat_VertBreakdown(idmatrix_t *A);
idmatrix_t *GMPmat_multiply(idmatrix_t *A, idmatrix_t *B);
idmatrix_t *GMPmat_add(idmatrix_t *A, idmatrix_t *B);
idmatrix_t *GMPmat_neg(idmatrix_t *A);


/* set functions */
void GMPmat_setValue (const idmatrix_t *A, size_t r, size_t c, double val);

/* get functions */
size_t GMPmat_count_zeros(const idmatrix_t *A);
void GMPmat_dbl_print(const idmatrix_t *A, FILE *out);
void GMPmat_getRow(idmatrix_t *A, mpz_t *ropN, mpz_t *ropD, size_t r);
size_t GMPmat_Rows (const idmatrix_t *A);
size_t GMPmat_Cols (const idmatrix_t *A);
double GMPmat_getValue_d (const idmatrix_t *A, size_t r, size_t c);
int GMPmat_iszero(idmatrix_t *A, int r, int c);
mpq_t *GMPmat_mpq_row_extract(const idmatrix_t *A, size_t r);
void GMPmat_to_sparse_rowvector(idmatrix_t *A, int row, int *h_numnz, int **hp_ind, double **hp_val);
void GMPmat_to_sparse_colvector(idmatrix_t *A, int col, int *h_numnz, int **hp_ind, double **hp_val);



#ifdef __cplusplus
}
#endif

#endif /* GMPMAT_H_ */
