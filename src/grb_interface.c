/*
 * grb_interface.c
 *
 *  Created on: Jun 15, 2019
 *      Author: rafael
 */


#include "grb_interface.h"

//#define GRB_INTERFACE_DEBUG_ADDVAR
//#define GRB_INTERFACE_DEBUG_ADDCONSTR


GRBmodel * grb_mk_model() {
	GRBmodel 	*model;
	assert(idGRB_NoError(GRBnewmodel(grbenv_, &model, "lp", 0, NULL, NULL, NULL, NULL, NULL))==0);

	return model;
}


int grb_optimize(GRBmodel *model, double *objval) {
	assert(model!=NULL);
	assert(objval!=NULL);

	int optimstatus;

	assert(idGRB_NoError(GRBoptimize(model))==0);
	assert(idGRB_NoError(GRBgetintattr(model, GRB_INT_ATTR_STATUS, &optimstatus))==0);
	if (optimstatus == GRB_OPTIMAL) {
		assert(idGRB_NoError(GRBgetdblattr(model, GRB_DBL_ATTR_OBJVAL, objval))==0);
	}
	return optimstatus;
}

void grb_log_lp_problem(GRBmodel *model, const char *prefix, int id) {
	assert(model!=NULL);
	assert(prefix!=NULL);
	char name[80];

	sprintf(name,"%s_%d.lp", prefix, id);
	assert(idGRB_NoError(GRBwrite(model,name))==0);

}
void grb_log_solution(GRBmodel *model, const char *prefix, int id) {
	assert(model!=NULL);
	assert(prefix!=NULL);
	char name[80];

	sprintf(name,"%s_%d.sol", prefix, id);
	assert(idGRB_NoError(GRBwrite(model,name))==0);

}

GRBmodel * grb_copy_model(GRBmodel *orig) {
	assert(orig!=NULL);

	GRBupdatemodel(orig);
	GRBmodel *copy = GRBcopymodel(orig);

	return copy;
}

// Variables

void grb_mk_positive_slack_variable(GRBmodel *model, int loop_id, double obj) {
	assert(model!=NULL);
	char name[80];

	sprintf(name,"s_%d", loop_id);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
	printf("debug grb_mk_positive_slack_variable: adding %s\n", name);
#endif
	assert(idGRB_NoError(GRBaddvar(model, 0, NULL, NULL, obj, 0.0, GRB_INFINITY, GRB_CONTINUOUS, name))==0);

}
void grb_del_positive_slack_variable(GRBmodel *model, int step, int loop_id, int i) {

	int varnum = grb_get_varnum_byname(model,"s",step,loop_id,i);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
	printf("debug grb_mk_positive_slack_variable: deleting s_%d[%s][%d] = %d\n", step,loop_id,i,varnum);
#endif
	assert(idGRB_NoError(GRBdelvars(model, 1, &varnum))==0);
}
void grb_mk_positive_slack_variable_with_step(GRBmodel *model, int step, int loop_id, double obj) {
	assert(model!=NULL);
	char name[80];

	sprintf(name,"s_%d[%d]", step, loop_id);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
	printf("debug grb_mk_positive_slack_variable_with_step: adding %s\n", name);
#endif
	assert(idGRB_NoError(GRBaddvar(model, 0, NULL, NULL, obj, 0.0, GRB_INFINITY, GRB_CONTINUOUS, name))==0);
}
void grb_mk_positive_slack_variable_array(GRBmodel *model, int step, int loop_id, int i, double obj) {
	assert(model!=NULL);
	char name[80];

	sprintf(name,"s_%d[%d][%d]", step, loop_id, i);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
	printf("debug grb_mk_positive_slack_variable_array: adding %s\n", name);
#endif
	assert(idGRB_NoError(GRBaddvar(model, 0, NULL, NULL, obj, 0.0, GRB_INFINITY, GRB_CONTINUOUS, name))==0);
}
void grb_mk_state_variable_array(GRBmodel *model, int step, int loop_id, int n) {
	assert(model!=NULL);
	int i;
	char name[80];

	for(i=0;i<n;i++) {
		sprintf(name,"x_%d[%d][%d]", step, loop_id, i);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
		printf("debug grb_mk_state_variable_array: adding %s\n", name);
#endif
		assert(idGRB_NoError(GRBaddvar(model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, name))==0);
	}

}
void grb_mk_input_variable_array(GRBmodel *model, int step, int loop_id, int m) {
	assert(model!=NULL);
	int i;
	char name[80];

	for(i=0;i<m;i++) {
		sprintf(name,"u_%d[%d][%d]", step, loop_id, i);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
		printf("debug grb_mk_input_variable_array: adding %s\n", name);
#endif
		assert(idGRB_NoError(GRBaddvar(model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, name))==0);
	}
}
void grb_get_state_variable_array(GRBmodel *model, int step, int loop_id, int n, double *h_values) {
	assert(model!=NULL);
	assert(h_values!=NULL);

	int start = grb_get_varnum_byname(model, "x", step, loop_id, 0);

	assert(idGRB_NoError(GRBgetdblattrarray(model, GRB_DBL_ATTR_X, start, n, h_values))==0);
}
void grb_get_input_variable_array(GRBmodel *model, int step, int loop_id, int m, double *h_values) {
	assert(model!=NULL);
	assert(h_values!=NULL);

	int start = grb_get_varnum_byname(model, "u", step, loop_id, 0);

	assert(idGRB_NoError(GRBgetdblattrarray(model, GRB_DBL_ATTR_X, start, m, h_values))==0);
}
double grb_get_slack_variable(GRBmodel *model, int step, int loop_id) {
	assert(model!=NULL);

	double value;
	int start = grb_get_scalar_varnum_byname(model, "s", step, loop_id);

	assert(idGRB_NoError(GRBgetdblattrarray(model, GRB_DBL_ATTR_X, start, 1, &value))==0);

	return value;
}
int grb_get_varnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	char name[80];
	int varnum;

	GRBupdatemodel(model);

	if (step<0) {
		sprintf(name,"%s_%d", prefix, loop_id);

	} else if (i<0) {
		sprintf(name,"%s_%d[%d]", prefix, step, loop_id);

	} else {
		sprintf(name,"%s_%d[%d][%d]", prefix, step, loop_id, i);

	}
	assert(idGRB_NoError(GRBgetvarbyname(model,name,&varnum))==0);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
		printf("debug grb_get_varnum_byname: name=%s, varnum=%d\n",name,varnum);
#endif

	return varnum;
}

int grb_get_scalar_varnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	char name[80];
	int varnum;

	GRBupdatemodel(model);
	sprintf(name,"%s_%d[%d]", prefix, step, loop_id);
	assert(idGRB_NoError(GRBgetvarbyname(model,name,&varnum))==0);
#ifdef GRB_INTERFACE_DEBUG_ADDVAR
		printf("debug grb_get_varnum_byname: name=%s, varnum=%d\n",name,varnum);
#endif

	return varnum;
}

// Constraints
void grb_add_constr_with_ind_starting_at(GRBmodel *model, const char *prefix, int step, int loop_id, int i,
		int ind_offset, int numnz, int *h_ind, double *h_val, char sense, double rhs) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	int j, ind[numnz];

	for(j=0;j<numnz;j++) {
		ind[j] = h_ind[j]+ind_offset;
	}
	grb_add_constr(model,prefix,step,loop_id,i, numnz, ind, h_val, sense, rhs);
}
void grb_add_constr(GRBmodel *model, const char *prefix, int step, int loop_id, int i,
		int numnz, int *h_ind, double *h_val, char sense, double rhs) {
	assert(model!=NULL);
	assert(prefix!=NULL);
	char name[80];

	sprintf(name,"%s_%d[%d][%d]", prefix, step, loop_id, i);
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf("debug grb_add_constr add %s :", name);
	idconstraint_print(stdout,numnz,h_ind,h_val,sense,rhs);
#endif

	assert(idGRB_NoError(GRBaddconstr(model, numnz, h_ind, h_val, sense, rhs, name))==0);
}
void grb_add_ubconstr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	int j, ind[numnz+1];
	double val[numnz+1];

	ind[0] = ind_slack;
	val[0] = -1.0;
	for(j=0;j<numnz;j++) {
		ind[j+1] = h_ind[j]+ind_offset;
		val[j+1] = h_val[j];
	}
	numnz++;

	grb_add_constr(model,prefix,step,loop_id,i, numnz, ind, val, GRB_LESS_EQUAL, rhs);
}
void grb_add_lbconstr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	int j, ind[numnz+1];
	double val[numnz+1];

	ind[0] = ind_slack;
	val[0] = 1.0;
	for(j=0;j<numnz;j++) {
		ind[j+1] = h_ind[j]+ind_offset;
		val[j+1] = h_val[j];
	}
	numnz++;

	grb_add_constr(model,prefix,step,loop_id,i, numnz, ind, val, GRB_LESS_EQUAL, rhs);
}
void grb_add_inftynorm_constr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs) {
	assert(model!=NULL);
	assert(prefix!=NULL);
	int j, ind[numnz+1];
	double val[numnz+1];
	char name[80];

	ind[0] = ind_slack;
	val[0] = -1.0;
	for(j=0;j<numnz;j++) {
		ind[j+1] = h_ind[j]+ind_offset;
		val[j+1] = h_val[j];
	}
	grb_add_constr(model,prefix,step,loop_id,i, numnz+1, ind, val, GRB_LESS_EQUAL, rhs);

	sprintf(name,"%sL", prefix);
	ind[0] = ind_slack;
	val[0] = -1.0;
	for(j=0;j<numnz;j++) {
		ind[j+1] = h_ind[j]+ind_offset;
		val[j+1] = -h_val[j];
	}
	grb_add_constr(model,name,step,loop_id,i, numnz+1, ind, val, GRB_LESS_EQUAL, -rhs);
}

void grb_add_inftynorm_constr_with_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int numnz, int *h_ind, double *h_val, double rhs, double delta) {
	assert(model!=NULL);
	assert(prefix!=NULL);
	int j;
	char name[80];

	for(j=0;j<numnz;j++) {
		h_ind[j] = h_ind[j]+ind_offset;
	}
	grb_add_constr(model,prefix,step,loop_id,i, numnz, h_ind, h_val, GRB_LESS_EQUAL, delta+rhs);

	sprintf(name,"%sL", prefix);
	for(j=0;j<numnz;j++) {
		h_val[j] = -h_val[j];
	}
	grb_add_constr(model,name,step,loop_id,i, numnz, h_ind, h_val, GRB_LESS_EQUAL, delta-rhs);
}

void grb_del_constr_array(GRBmodel *model, int offset, int nconstr) {
	int i, ind[nconstr];
	assert(model!=NULL);

	for(i=0;i<nconstr;i++) {
		ind[i] = offset+i;
	}

	assert(idGRB_NoError(GRBdelconstrs(model,nconstr,ind))==0);
}
void grb_del_constrs_byprefix(GRBmodel *model, const char *prefix, int step, int loop_id, int numdel) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	int i, ind[numdel];

#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf("debug grb_del_constrs_byprefix: delete %s_%d[%d] :", prefix, step, loop_id);
#endif
	ind[0] = grb_get_constrnum_byname(model, prefix, step, loop_id, 0);
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf(" %s_%d[%d][0] = %d", prefix, step, loop_id, ind[0]);
#endif
	for(i=1;i<numdel;i++) {
		ind[i] = ind[i-1]+1;
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
		printf(", %s_%d[%d][%d] = %d", prefix, step, loop_id, i, ind[i]);
#endif
	}
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf("\n");
#endif
	assert(idGRB_NoError(GRBdelconstrs(model,numdel,ind))==0);
}
void grb_del_constr_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	int index;

	index = grb_get_constrnum_byname(model, prefix, step, loop_id, i);
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf("debug grb_del_constrs_byprefix: delete %s_%d[%d][%d] = %d\n", prefix, step, loop_id, i, index);
#endif
	assert(idGRB_NoError(GRBdelconstrs(model,1,&index))==0);
}
int grb_get_constrnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i) {
	assert(model!=NULL);
	assert(prefix!=NULL);

	char name[80];
	int index;
	GRBupdatemodel(model);

	sprintf(name,"%s_%d[%d][%d]", prefix, step, loop_id, i);
	assert(idGRB_NoError(GRBgetconstrbyname(model,name,&index))==0);
#ifdef GRB_INTERFACE_DEBUG_ADDCONSTR
	printf("debug grb_get_constrnum_byname: name=%s, varnum=%d\n",name,index);
#endif

	return index;
}
