/*
 * grb_interface.h
 *
 *  Created on: Jun 15, 2019
 *      Author: rafael
 */

#ifndef GRB_INTERFACE_H_
#define GRB_INTERFACE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


extern GRBmodel * grb_mk_model();

// Variables
extern void grb_mk_positive_slack_variable(GRBmodel *model, int loop_id, double obj);
extern void grb_mk_positive_slack_variable_array(GRBmodel *model, int step, int loop_id, int i, double obj);
extern void grb_mk_state_variable_array(GRBmodel *model, int step, int loop_id, int n);
extern void grb_mk_input_variable_array(GRBmodel *model, int step, int loop_id, int m);
extern void grb_get_state_variable_array(GRBmodel *model, int step, int loop_id, int n, double *h_values);
extern void grb_get_input_variable_array(GRBmodel *model, int step, int loop_id, int m, double *h_values);
extern double grb_get_slack_variable(GRBmodel *model, int step, int loop_id);
extern int grb_get_varnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i);
extern int grb_get_scalar_varnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id);
extern void grb_del_positive_slack_variable(GRBmodel *model, int step, int loop_id, int i);
extern void grb_mk_positive_slack_variable_with_step(GRBmodel *model, int step, int loop_id, double obj);

// Constraints
extern void grb_add_constr_with_ind_starting_at(GRBmodel *model, const char *prefix, int step, int loop_id, int i,
		int ind_offset, int numnz, int *h_ind, double *h_val, char sense, double rhs);
extern void grb_add_constr(GRBmodel *model, const char *prefix, int step, int loop_id, int i,
		int numnz, int *h_ind, double *h_val, char sense, double rhs);
extern void grb_add_ubconstr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs);
extern void grb_add_lbconstr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs);
extern void grb_add_inftynorm_constr_with_slack_and_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int ind_slack, int numnz, int *h_ind, double *h_val, double rhs);
extern void grb_add_inftynorm_constr_with_ind_starting_at(GRBmodel *model,
		const char *prefix, int step, int loop_id, int i,
		int ind_offset, int numnz, int *h_ind, double *h_val, double rhs, double delta);
extern void grb_del_constr_array(GRBmodel *model, int offset, int nconstr);
extern void grb_del_constrs_byprefix(GRBmodel *model, const char *prefix, int step, int loop_id, int numdel);
extern int grb_get_constrnum_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i);
extern void grb_del_constr_byname(GRBmodel *model, const char *prefix, int step, int loop_id, int i);

// Operations
extern int grb_optimize(GRBmodel *model, double *objval);
extern void grb_log_lp_problem(GRBmodel *model, const char *prefix, int k);
extern void grb_log_solution(GRBmodel *model, const char *prefix, int k);
extern GRBmodel * grb_copy_model(GRBmodel *orig);


//extern double grb_x_get_byname(GRBmodel *model, const char *name);
//extern void grb_constr_del_byname(GRBmodel *model, const char *name);
//
//extern void grb_vararray_mk(GRBmodel *model, const char *prefix, int id, int nvars);
//extern void grb_vararray_del(GRBmodel *model, const char *prefix, int id, int nvars);
//extern void grb_var_del_byname(GRBmodel *model, const char *name);
//extern void grb_vararray_del(GRBmodel *model, const char *prefix, int id, int nvars);
//extern void grb_constrarray_del(GRBmodel *model, const char *prefix, int id, int nconstrs);


#ifdef __cplusplus
}
#endif

#endif /* GRB_INTERFACE_H_ */
