/*
 * idrtl.h
 *
 *  Created on: Jan 27, 2019
 *      Author: rafael
 *
 *      Some standards:
 *      size   : it refers to size of a data, normally in bytes
 *      length : or simply len, is the number of elements in an array
 *      m      : number of rows
 *      n      : number o columns
 *      ind    : indices
 *      val    : double values
 */

#ifndef IDSTL_H_
#define IDSTL_H_

#undef __cplusplus

#include "utils.h"

//#define IDRTL_DEBUG_MEMLEAK_KRIPKE
//#define IDRTL_DEBUG_MEMLEAK_POLYTOPE
//#define IDRTL_DEBUG_MEMLEAK_PREDICATE
//#define IDRTL_DEBUG_MEMLEAK_ARRAY
//#define IDRTL_DEBUG_MEMLEAK_LIST
//#define IDRTL_DEBUG_MEMLEAK_MAP
//#define IDRTL_DEBUG_MEMLEAK_RBNODE
//#define IDRTL_DEBUG_MEMLEAK_REACH_LP
//#define IDRTL_DEBUG_MEMLEAK_SLFEAS_LP
//#define IDRTL_DEBUG_MEMLEAK_DBLMATRIX
//#define IDRTL_DEBUG_MEMLEAK_CONSTRAINT_SYSTEM
//#define IDRTL_DEBUG_MEMLEAK_LINEAR_SYS
//#define IDRTL_DEBUG_MEMLEAK_RUN
//#define IDRTL_DEBUG_MEMLEAK_ROBRUN
//#define IDRTL_DEBUG_MEMLEAK_FEAS
//#define IDRTL_DEBUG_MEMLEAK_BSC
//#define IDRTL_DEBUG_MEMLEAK_FORMULA
//#define IDRTL_DEBUG_MEMLEAK_CEX
//#define IDRTL_DEBUG_MEMLEAK_AUTOMATON
//#define IDRTL_DEBUG_MEMLEAK_TABLE
//#define IDRTL_DEBUG_MEMLEAK_SPARSE


enum idkeytype_e {IDKEY_INT = 0, IDKEY_DOUBLE = 1, IDKEY_CUSTOM = 2};

/* types */
typedef struct idrb_node_s idrb_node_t;
typedef enum idkeytype_e idkeytype_t;
typedef struct idsparse_s idsparse_t;
typedef struct idmap_s idmap_t;
typedef struct idlist_s idlist_t;
typedef struct idarray_s idarray_t;
typedef struct idtable_s idtable_t;
typedef struct idlinear_sys_s idlinear_sys_t;
typedef struct idpredicate_s idpredicate_t;
typedef struct idcsys_s idcsys_t;
typedef struct idpolytope_s idpolytope_t;
typedef struct idkripke_s idkripke_t;
typedef struct iddblmatrix_s iddblmatrix_t;
typedef struct idreach_lp_s idreach_lp_t;
typedef struct idslfeas_lp_s idslfeas_lp_t;
typedef struct idfsearch_s idfsearch_t;
typedef struct idbsc_s idbsc_t;
typedef struct idformula_s idformula_t;
typedef struct idcex_s idcex_t;
typedef struct idautomaton_s idautomaton_t;
typedef struct idrun_s idrun_t;
typedef struct idrobrun_s idrobrun_t;


#include "red_black_tree.h"
#include "lrsinterface.h"
#include "z3_interface.h"
#include "grb_interface.h"
#include "sparse.h"
#include "list.h"
#include "array.h"
#include "map.h"
#include "table.h"
#include "dblmatrix.h"
#include "linear_dynamical_system.h"
#include "constraint_system.h"
#include "predicate.h"
#include "polytope.h"
#include "kripke.h"
#include "reachability_search_lp.h"
#include "selfloop_feasibility_search.h"
#include "run.h"
#include "robust_run.h"
#include "feasibility_search.h"
#include "automaton.h"
#include "cex.h"
#include "formula.h"
#include "bsc.h"



#endif /* IDSTL_H_ */
