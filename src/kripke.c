/*
 * kripke.c
 *
 *  Created on: Mar 13, 2019
 *      Author: rafael
 */

#include "kripke.h"

#define IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
#define IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
#define IDKRIPKE_DEBUG_ADJACENCY
//#define IDKRIPKE_DEBUG_DESTROY

static void idkripke_adjacency_find_between_list(idkripke_t *obj, int *h_si, idlist_t *h_lst);

/* private functions */

static void idkripke_init_state_mode_bit_map(idkripke_t *obj){
	int i, s, q;

	idrb_node_t *it;
	if (obj->mp_state_bit_map == NULL) {
		obj->m_nbits_state = ceil(log(obj->m_nstates)/log(2)); // initialize number of bits
		obj->m_nbits_mode = ceil(log(obj->m_nmodes)/log(2)); // initialize number of bits
		obj->mp_state_bit_map = idintmap();
		i = 0;
		idrb_traverse(it,obj->mp_states->mp_head) {
			s = it->keyi(it);
			obj->mp_state_bit_map->set(obj->mp_state_bit_map,&s,&i);
			i++;
		}
		assert(i==obj->nstates(obj));
		obj->mp_mode_bit_map = idintmap();
		i = 0;
		idrb_traverse(it,obj->mp_mode_sys->mp_head) {
			q = it->keyi(it);
			obj->mp_mode_bit_map->set(obj->mp_mode_bit_map,&q,&i);
			i++;
		}
		assert(i==obj->m_nmodes);
	}

}
void idkripke_insert_edge(idkripke_t *obj, int from, int to, int facet) {
	assert(obj!=NULL);
	assert(obj->mp_edges!=NULL);
	assert(obj->mp_edges_map!=NULL);

	int *h_facet;

	h_facet = (int *)obj->mp_edges->get_at(obj->mp_edges,from,to);
	if (h_facet == NULL) {
		obj->mp_edges->set_at(obj->mp_edges,from,to,&facet);
		obj->mp_edges_map->set(obj->mp_edges_map,&from,&to);
	} else {
		*h_facet = facet;
	}
}

static void idkripke_remove_edge_of(idkripke_t *obj, int si) {
	assert(obj!=NULL);
	assert(obj->mp_edges!=NULL);
	assert(obj->mp_edges_map!=NULL);

	obj->mp_edges->remove_row(obj->mp_edges,si);
	obj->mp_edges->remove_col(obj->mp_edges,si);
	obj->mp_edges_map->remove_value1(obj->mp_edges_map,&si);
	obj->mp_edges_map->remove_value2(obj->mp_edges_map,&si);
}

static void idkripke_is_initial_at(idkripke_t *obj, int si) {
	assert(obj!=NULL);

	idpolytope_t *state = (idpolytope_t *)obj->mp_states->get_at(obj->mp_states,si);

	if (state->isinside_dblcolvector(state,obj->mp_x0)) {
		obj->m_initial = si;
	}
}

static void idkripke_remove_state_at(idkripke_t *obj, int si) {
	assert(obj!=NULL);
	assert(obj->mp_states!=NULL);
	assert(obj->mp_states->get_at(obj->mp_states,si)!=NULL);

	obj->mp_states->remove_at(obj->mp_states,si);
	obj->mp_state_label_map->remove_value1(obj->mp_state_label_map,&si);
	obj->mp_state_mode_map->remove_value1(obj->mp_state_mode_map,&si);
	idkripke_remove_edge_of(obj,si);
	if (obj->m_initial==si) {
		obj->m_initial=-1;
	}

	obj->m_nstates--;
}

void idkripke_append_mode(idkripke_t *obj, int q, idlinear_sys_t *p_sys, idpolytope_t *p_statedom, idpolytope_t *p_inputdom) {
	assert(obj!=NULL);
	assert(p_sys!=NULL);
	assert(p_statedom!=NULL);
	assert(p_inputdom!=NULL);
	assert(p_statedom->isfulldim(p_statedom));
	assert(p_inputdom->isfulldim(p_inputdom));
	assert(q>=0);
	assert(obj->mp_mode_sys->get_at(obj->mp_mode_sys,q)==NULL);

	obj->m_nmodes++;
	obj->mp_mode_sys->set_at(obj->mp_mode_sys,q,p_sys);
	obj->mp_mode_statedom->set_at(obj->mp_mode_statedom,q,p_statedom);
	obj->mp_mode_inputdom->set_at(obj->mp_mode_inputdom,q,p_inputdom);
}

int idkripke_append_state(idkripke_t *obj, int q, idpolytope_t *p_state, idlist_t *p_labels) {
	assert(obj!=NULL);
	assert(p_state!=NULL);
	assert(p_labels!=NULL);
	assert(q>=0);
	assert(obj->mp_mode_sys->get_at(obj->mp_mode_sys,q)!=NULL);

	assert(p_state->isfulldim(p_state));

	if (obj->mp_state_bit_map!=NULL) {
		assert(obj->mp_mode_bit_map!=NULL);
		obj->mp_state_bit_map->destroy(obj->mp_state_bit_map);
		obj->mp_mode_bit_map->destroy(obj->mp_mode_bit_map);
		obj->mp_state_bit_map = NULL;
		obj->mp_mode_bit_map = NULL;
	}
	int si = obj->mp_states->append(obj->mp_states,p_state);
	obj->mp_state_label_map->set_values2(obj->mp_state_label_map,&si,p_labels);
	obj->mp_state_mode_map->set(obj->mp_state_mode_map,&si,&q);
	idkripke_insert_edge(obj,si,si,0);
	idkripke_is_initial_at(obj,si);
	obj->m_nstates++;
	return si;
}

// We assume that adjacency is non-directional
static idbool_t idkripke_is_union_convex_and_check_adjacency_between(idkripke_t *obj, int *h_si, int sj) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
	printf("debug adjacency between s_%d and s_%d: starting...\n", *h_si, sj);
	char str[80];
#endif
	assert(obj!=NULL);

	int si = *h_si, qi, qj;
	idpolytope_t *h_poly_i, *h_poly_j, *p_poly_inter, *p_poly_merge = NULL;
	int adj_facet_i, adj_facet_j, snew, fnd, sk;
	idlist_t *h_list, *p_list, *h_label_i, *h_label_j, *p_label;

	if (obj->mp_edges_map->exists(obj->mp_edges_map,&si,&sj)) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
		printf("debug adjacency between s_%d and s_%d: already exists\n", si, sj);
		idinttable_print(obj->mp_edges,stdout);
#endif

		return FALSE;
	}

	h_poly_i = (idpolytope_t *)obj->mp_states->get_at(obj->mp_states,si);
	assert(h_poly_i!=NULL);
	h_label_i = (idlist_t *)obj->mp_state_label_map->get_values2_at(obj->mp_state_label_map,&si);
	assert(h_label_i!=NULL);
	qi = *(int *)idlist_first((idlist_t *)obj->mp_state_mode_map->get_values2_at(obj->mp_state_mode_map,&si));

#ifdef IDKRIPKE_DEBUG_ADJACENCY
	printf("debug adjacency between s_%d and s_%d: s_%d label = %s, mode = q_%d, and poly = \n", si, sj, si,
			idintlist_to_str(h_label_i,str), qi);
	h_poly_i->print(h_poly_i,stdout);
#endif

	h_poly_j = obj->mp_states->get_at(obj->mp_states,sj);
	if (h_poly_j==NULL) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
		printf("debug adjacency between s_%d and s_%d: s_% does not exist anymore\n", si, sj, sj);
#endif
		return FALSE;
	}
	h_label_j = (idlist_t *)obj->mp_state_label_map->get_values2_at(obj->mp_state_label_map,&sj);
	assert(h_label_j!=NULL);
	qj = *(int *)idlist_first((idlist_t *)obj->mp_state_mode_map->get_values2_at(obj->mp_state_mode_map,&sj));
#ifdef IDKRIPKE_DEBUG_ADJACENCY
	printf("debug adjacency between s_%d and s_%d: s_%d label = %s, mode = q_%d, and poly = \n", si, sj, sj,
			idintlist_to_str(h_label_j,str), qj);
	h_poly_j->print(h_poly_j,stdout);
#endif


	p_poly_inter = h_poly_i->and_with_polytope(h_poly_i,h_poly_j);
#ifdef IDKRIPKE_DEBUG_ADJACENCY
	printf("debug adjacency between s_%d and s_%d: instersection = \n", si, sj);
	p_poly_inter->print(p_poly_inter,stdout);
#endif

	if ((h_label_i->is_equal_to(h_label_i,h_label_j))&&(qi==qj)) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
		printf("debug adjacency between s_%d and s_%d: they have same labels and modes\n", si, sj);
#endif
		if (h_poly_i->is_adjacent_to(h_poly_i,h_poly_j,p_poly_inter,&adj_facet_i,&adj_facet_j,&p_poly_merge)) {
			if (p_poly_merge!=NULL) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
				printf("debug adjacency between s_%d and s_%d: they are adjacent and can be merged into\n", si, sj);
				p_poly_merge->print(p_poly_merge,stdout);
#endif
				p_label = h_label_i->cpy(h_label_i);

				snew = idkripke_append_state(obj,qi,p_poly_merge,p_label);
				// assuming non-directional adjacency here!!!!
				p_list = idintlist();
				p_list->push_from_list(p_list,obj->mp_edges_map->get_values2_at(obj->mp_edges_map,&si));
				p_list->push_from_list(p_list,obj->mp_edges_map->get_values2_at(obj->mp_edges_map,&sj));
				idkripke_remove_state_at(obj,si);
				idkripke_remove_state_at(obj,sj);
				p_list->remove(p_list,&si);
				p_list->remove(p_list,&sj);
				p_list->remove(p_list,&snew);

#ifdef IDKRIPKE_DEBUG_ADJACENCY
				printf("debug adjacency between s_%d and s_%d: union IS CONVEX.\n", si, sj);
				printf("***********************************************************\n", si, sj);
				printf("debug adjacency between s_%d and s_%d: check edges below of the convex union s_%d.\n", si, sj, snew);
#endif


				idkripke_adjacency_find_between_list(obj,&snew,p_list);

#ifdef IDKRIPKE_DEBUG_ADJACENCY
				printf("***********************************************************\n", si, sj);
				printf("debug adjacency between s_%d and s_%d: END.\n", si, sj);
#endif
				*h_si = snew;
				p_list->destroy(p_list);
				p_poly_inter->destroy(p_poly_inter);
				return TRUE;
			} else {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
				printf("debug adjacency between s_%d and s_%d: they are adjacent\n", si, sj);
#endif
				// polytope adjacency implies bi-direcional edges
				idkripke_insert_edge(obj,si,sj,adj_facet_i);
				idkripke_insert_edge(obj,sj,si,adj_facet_j);
			}
		}
	} else {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
		printf("debug adjacency between s_%d and s_%d: they have different labels\n", si, sj);
#endif
		if (h_poly_i->is_adjacent_to(h_poly_i,h_poly_j,p_poly_inter,&adj_facet_i,&adj_facet_j,NULL)) {
#ifdef IDKRIPKE_DEBUG_ADJACENCY
			printf("debug adjacency between s_%d and s_%d: they are adjacent\n", si, sj);
#endif
			// polytope adjacency implies bi-direcional edges
			idkripke_insert_edge(obj,si,sj,adj_facet_i);
			idkripke_insert_edge(obj,sj,si,adj_facet_j);

		}
	}
	p_poly_inter->destroy(p_poly_inter);
#ifdef IDKRIPKE_DEBUG_ADJACENCY
	printf("debug adjacency between s_%d and s_%d: union IS NOT CONVEX END.\n", si, sj);
#endif
	return FALSE;
}


// We assume that adjacency is non-directional
static void idkripke_adjacency_find_between_list(idkripke_t *obj, int *h_si, idlist_t *h_lst) {
	assert(obj!=NULL);
	assert(obj->mp_states!=NULL);
	int si = *h_si;
	assert(obj->mp_states->get_at(obj->mp_states,si)!=NULL);

	int sj;
	idbool_t ismerged;
	idrb_node_t *it, *h_rblst;

	if (h_lst==NULL) {
		// assuming non-directional adjacency here!!!!
		h_rblst = obj->mp_states->mp_head;
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
		printf("debug adjacency_find_between_list: s_%d and list \n{", si);
		int cnt_dbg=0;
		idrb_traverse(it, h_rblst) {
			if (it != idrb_first(h_rblst)) {
				printf(",");
			}
			if (cnt_dbg>10) {
				printf("\n");
				cnt_dbg = 0;
			}
			printf("%d",it->keyi(it));
		}
		printf("}\n");
#endif
		idrb_traverse(it, h_rblst) {
			sj = it->keyi(it);
			if (idkripke_is_union_convex_and_check_adjacency_between(obj,&si,sj)!=FALSE) {
				h_lst->remove(h_lst,&sj);
				*h_si = si;
				idkripke_adjacency_find_between_list(obj,h_si,h_lst);
				break;
			}
		}
	} else {
		h_rblst = h_lst->mp_head;
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
		printf("debug adjacency_find_between_list: s_%d and list \n{", si);
		int cnt_dbg=0;
		idrb_traverse(it, h_rblst) {
			if (it != idrb_first(h_rblst)) {
				printf(",");
			}
			if (cnt_dbg>10) {
				printf("\n");
				cnt_dbg = 0;
			}
			printf("%d",*(int *)it->value(it));
		}
		printf("\n");
#endif
		idrb_traverse(it, h_rblst) {
			sj = *(int *)it->value(it);
			if (idkripke_is_union_convex_and_check_adjacency_between(obj,&si,sj)!=FALSE) {
				h_lst->remove(h_lst,&sj);
				*h_si = si;
				idkripke_adjacency_find_between_list(obj,h_si,h_lst);
				break;
			}
		}
	}
}

// split a inner polytope from a state at index ind if we have a new label.
// We assume that adjacency is non-directional
static int idkripke_split_inner_polytope(idkripke_t *obj,
		int si, int q, idpolytope_t *h_inner_poly, idlist_t *h_label_toappend,
		idpolytope_t *h_poly_i, idlist_t *h_label_i) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	char str[80];
	printf("debug split_inner_polytope: starting... label to append = %s\n",
			idintlist_to_str(h_label_toappend,str));
#endif

	assert(obj!=NULL);
	assert(h_inner_poly!=NULL);
	assert(h_label_toappend!=NULL); // it needs a label to append or we should not split
	assert(h_label_toappend->length(h_label_toappend)>0); // it needs a label to append or we should not split
	assert(obj->mp_states!=NULL);
	assert(obj->mp_states->get_at(obj->mp_states,si)!=NULL);
	assert(obj->mp_mode_sys->get_at(obj->mp_mode_sys,q)!=NULL);

	if (h_poly_i->is_equal_to(h_poly_i,h_inner_poly)) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
		printf("debug split_inner_polytope: we only need to update label of polytope s_%d.\n", si);
#endif
		idrb_node_t *it;
		void *h_value;
		idrb_traverse(it,h_label_toappend->mp_head) {
			h_value = it->value(it);
			obj->mp_state_label_map->set(obj->mp_state_label_map,&si,h_value);
		}

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
		printf("debug split_inner_polytope: the inner polytope is the state s_%d END.\n", si);
#endif

		return si;
	}

	idlist_t *p_edges_to_check;
	idpolytope_t *p_outter_poly, *p_inner_poly;
	idlist_t 	*p_label,
				*p_label_i = h_label_i->cpy(h_label_i),
				*p_label_new = h_label_toappend->cpy(h_label_toappend);
	p_label_new->push_from_list(p_label_new,p_label_i);
	idarray_t *p_poly_array;
	int soutter, sinner;
	idrb_node_t *it;



#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: s_%d, mode = q_%d, label = %s\n", si, q, idintlist_to_str(p_label_i,str));
	printf("debug split_inner_polytope: new label = %s\n", idintlist_to_str(p_label_new,str));
	printf("debug split_inner_polytope: s_%d poly = \n",si);
	h_poly_i->print(h_poly_i,stdout);
	printf("debug split_inner_polytope: nstates = %d, and inner_poly = \n",obj->m_nstates);
	h_inner_poly->print(h_inner_poly,stdout);
#endif

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: we need to split the inner polytope.\n");
#endif

	// assuming non-directional adjacency here!!!!
	p_edges_to_check = idintlist();
	p_edges_to_check->push_from_list(p_edges_to_check,obj->mp_edges_map->get_values2_at(obj->mp_edges_map,&si));

	p_poly_array = h_poly_i->difference(h_poly_i,h_inner_poly);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: s_%d polytope / inner polytope = %d polytopes\n", si, p_poly_array->length(p_poly_array));
#endif

	idkripke_remove_state_at(obj,si);
	p_edges_to_check->remove(p_edges_to_check,&si);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: s_%d removed; thus, nstates = %d\n", si, obj->m_nstates);
	printf("debug split_inner_polytope: edges to check = \n");
	idintlist_print(p_edges_to_check,stdout);
#endif


	// claim: Given a polytope P in a partitioned workspace and its set of adjacent polytopes Adj, any inner polytope Pi of P (Pi subseteq P)
	// will be adjacent to a subset of Adj (Adji subseteq Adj)
	// It is a trivial conclusion; thus, I will not prove.

	p_inner_poly = h_inner_poly->cpy(h_inner_poly);
	sinner = idkripke_append_state(obj, q, p_inner_poly, p_label_new);
	idkripke_adjacency_find_between_list(obj, &sinner, p_edges_to_check);
	p_edges_to_check->push(p_edges_to_check,&sinner);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: inner s_%d added; thus, nstates = %d\n", sinner, obj->m_nstates);
	printf("debug split_inner_polytope: s_%d is adjacent to states with following indices:\n", sinner);
	idintlist_print(obj->mp_edges_map->get_values2_at(obj->mp_edges_map,&sinner),stdout);
	printf("debug split_inner_polytope: edges to check = \n");
	idintlist_print(p_edges_to_check,stdout);
#endif

	idrb_traverse(it,p_poly_array->mp_head) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
		printf("debug split_inner_polytope: get outter polytope %d\n",it->keyi(it));
#endif
		p_outter_poly = (idpolytope_t *)it->value(it);
		if (p_outter_poly->isfulldim(p_outter_poly)) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
			printf("debug split_inner_polytope: adds outter polytope %d =\n",it->keyi(it));
			p_outter_poly->print(p_outter_poly,stdout);
#endif
			p_label = p_label_i->cpy(p_label_i);
			soutter = idkripke_append_state(obj, q, p_outter_poly, p_label);
			idkripke_adjacency_find_between_list(obj, &soutter, p_edges_to_check);
			p_edges_to_check->push(p_edges_to_check,&soutter);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
			printf("debug split_inner_polytope: outter s_%d added; thus, nstates = %d\n", soutter, obj->m_nstates);
			printf("debug split_inner_polytope: s_%d is adjacent to states with following indices:\n", soutter);
			idintlist_print((idlist_t *)obj->mp_edges_map->get_values2_at(obj->mp_edges_map,&soutter),stdout);
			printf("debug split_inner_polytope: edges to check = \n");
			idintlist_print(p_edges_to_check,stdout);
#endif
		} else {
			p_outter_poly->destroy(p_outter_poly);
		}
	}
	p_poly_array->destroy(p_poly_array);
	p_edges_to_check->destroy(p_edges_to_check);
	p_label_i->destroy(p_label_i);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	printf("debug split_inner_polytope: the inner polytope is the state s_%d END.\n", sinner);
#endif

	return sinner;
}

int             idkripke_nbits(idkripke_t *obj) {
	assert(obj!=NULL);

	if ((obj->m_nbits_state <= 0)||(obj->m_nbits_mode <= 0)) {
		return ceil(log(obj->m_nstates)/log(2)) + ceil(log(obj->m_nmodes)/log(2));
	} else {
		return obj->m_nbits_state+obj->m_nbits_mode;
	}
}

int             idkripke_nstates(idkripke_t *obj) {
	assert(obj!=NULL);

	return obj->m_nstates;
}

static idkripke_t *     idkripke_default_constructor(iddblmatrix_t *p_x0, int q0, idbool_t isfair) {
	assert(p_x0!=NULL);
	assert(p_x0->cols(p_x0)==1); // column vector

	idkripke_t *obj = malloc(sizeof(idkripke_t));
	assert(obj!=NULL);

#ifdef IDRTL_DEBUG_MEMLEAK_KRIPKE
	static int idkripke_last_id = 0;
	obj->m_id = idkripke_last_id;
	printf("debug memleak construct kripke %d\n", obj->m_id);
	idkripke_last_id++;
	idkripke_destroy_cnt++;
#endif
	obj->m_nstates   			= 0;
	obj->m_nmodes				= 0;
	obj->m_nbits_state 			= 0;
	obj->m_nbits_mode 			= 0;
	obj->m_initial   			= -1;
	obj->m_isfair  				= isfair;
	obj->m_q0					= q0;
	obj->mp_edges				= NULL;
	obj->mp_edges_map			= NULL;
	obj->mp_mode_sys			= NULL;
	obj->mp_mode_statedom		= NULL;
	obj->mp_mode_inputdom		= NULL;
	obj->mp_state_bit_map		= NULL;
	obj->mp_mode_bit_map		= NULL;
	obj->mp_state_label_map 	= NULL;
	obj->mp_state_mode_map		= NULL;
	obj->mp_states 				= NULL;
	obj->mp_transitions_map		= NULL;
	obj->mp_unmarked			= NULL;
	obj->mp_x0					= p_x0;
	obj->mp_x0_constr			= id_mk_csys_equalities_from_iddblmatrix_vector(obj->mp_x0);

	obj->cpy              		= idkripke_cpy;
	obj->destroy          		= idkripke_destroy;
	obj->add_polytope 			= idkripke_add_polytope;
	obj->add_mode				= idkripke_add_mode;
	obj->rem_transition_between_modes = idkripke_rem_transition_between_modes;
	obj->dim					= idkripke_dim;
	obj->nbits					= idkripke_nbits;
	obj->nstates				= idkripke_nstates;
	obj->is_fair          		= idkripke_is_fair;
	obj->z3_atomicprop    		= idkripke_z3_atomicprop;
	obj->z3_bitvec_at			= idkripke_z3_bitvec_at;
	obj->z3_state				= idkripke_z3_state;
	obj->z3_transitions			= idkripke_z3_transitions;
	obj->z3_unmarked			= idkripke_z3_unmarked;
	obj->z3_bitvecE				= idkripke_z3_bitvecE;
	obj->z3_edges				= idkripke_z3_edges;
	obj->get_state_bit_map		= idkripke_get_state_bit_map;
	obj->get_bits_from_state	= idkripke_get_bits_from_state;
	obj->get_state_from_bits	= idkripke_get_state_from_bits;
	obj->get_mode_from_bits		= idkripke_get_mode_from_bits;
	obj->z3_mode				= idkripke_z3_mode;
	obj->print					= idkripke_print;
	obj->get_bits_from_mode		= idkripke_get_bits_from_mode;

	return obj;


}
/* public functions */
idkripke_t *     idkripke_single_mode(iddblmatrix_t *p_x0, idbool_t isfair, idpolytope_t *p_workspace,
		idlinear_sys_t *p_sys, idpolytope_t *p_inputdom) {
	assert(p_sys!=NULL);
	assert(p_inputdom!=NULL);
	assert(p_sys->n(p_sys)==p_workspace->m_dim);
	assert(p_sys->m(p_sys)==p_inputdom->m_dim);

	int q0 = 0;
	idkripke_t *obj = idkripke_default_constructor(p_x0,q0,isfair);
	obj->mp_edges				= idtable_byvalue(sizeof(int));
	obj->mp_edges_map			= idmap_byvalues(sizeof(int),idintcmpfunc_,sizeof(int),idintcmpfunc_);
	obj->mp_mode_sys			= idarray_byptr(idlinear_sys_destroy,idlinear_sys_cpy);
	obj->mp_mode_statedom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_mode_inputdom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_state_label_map 	= idintmap();
	obj->mp_state_mode_map		= idintmap();
	obj->mp_states 				= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_transitions_map		= idintmap();
	obj->mp_unmarked			= idintlist();

	obj->add_mode(obj,q0,p_sys,p_workspace,p_inputdom);

	return obj;
}

idkripke_t *     idkripke(iddblmatrix_t *p_x0, int q0, idbool_t isfair) {
	assert(q0>=0);

	idkripke_t *obj = idkripke_default_constructor(p_x0,q0,isfair);
	obj->mp_edges				= idtable_byvalue(sizeof(int));
	obj->mp_edges_map			= idmap_byvalues(sizeof(int),idintcmpfunc_,sizeof(int),idintcmpfunc_);
	obj->mp_mode_sys			= idarray_byptr(idlinear_sys_destroy,idlinear_sys_cpy);
	obj->mp_mode_statedom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_mode_inputdom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_state_label_map 	= idintmap();
	obj->mp_state_mode_map		= idintmap();
	obj->mp_states 				= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_transitions_map		= idintmap();
	obj->mp_unmarked			= idintlist();

	return obj;
}

void             idkripke_destroy(idkripke_t *obj) {
#ifdef IDKRIPKE_DEBUG_DESTROY
	printf("debug destroy kripke\n");
#endif
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_KRIPKE
		idkripke_destroy_cnt--;
		printf("debug memleak destroy kripke %d still remains %d to destroy\n", obj->m_id, idkripke_destroy_cnt);
		if (idkripke_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
#ifdef IDKRIPKE_DEBUG_DESTROY
		obj->print(obj,stdout);
		printf("debug destroy mp_edges\n");
#endif
		idtable_destroy(obj->mp_edges);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_edges_map\n");
#endif
		idmap_destroy(obj->mp_edges_map);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_mode_sys\n");
#endif
		idarray_destroy(obj->mp_mode_sys);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_mode_statedom\n");
#endif
		idarray_destroy(obj->mp_mode_statedom);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_mode_inputdom\n");
#endif
		idarray_destroy(obj->mp_mode_inputdom);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_state_bit_map\n");
#endif
		idmap_destroy(obj->mp_state_bit_map);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_state_label_map\n");
#endif
		idmap_destroy(obj->mp_state_label_map);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_state_mode_map\n");
#endif
		idmap_destroy(obj->mp_state_mode_map);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_states\n");
#endif
		idarray_destroy(obj->mp_states);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_unmarked\n");
#endif
		idlist_destroy(obj->mp_unmarked);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_x0\n");
#endif
		iddblmatrix_destroy(obj->mp_x0);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy mp_x0_constr\n");
#endif
		idcsys_destroy(obj->mp_x0_constr);
#ifdef IDKRIPKE_DEBUG_DESTROY
		printf("debug destroy obj\n");
#endif
		free(obj);
	}
#ifdef IDKRIPKE_DEBUG_DESTROY
	printf("debug destroy END\n");
#endif

}
idkripke_t * 	idkripke_cpy(idkripke_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_nstates>0);

	idkripke_t *p_M = idkripke_default_constructor(obj->mp_x0->cpy(obj->mp_x0), obj->m_q0, obj->m_isfair);
	p_M->mp_edges				= obj->mp_edges->cpy(obj->mp_edges);
	p_M->mp_edges_map			= obj->mp_edges_map->cpy(obj->mp_edges_map);
	p_M->mp_mode_sys			= obj->mp_mode_sys->cpy(obj->mp_mode_sys);
	p_M->mp_mode_statedom		= obj->mp_mode_statedom->cpy(obj->mp_mode_statedom);
	p_M->mp_mode_inputdom		= obj->mp_mode_inputdom->cpy(obj->mp_mode_inputdom);
	if (p_M->mp_state_bit_map!=NULL) {
		assert(obj->mp_mode_bit_map!=NULL);
		p_M->mp_state_bit_map		= obj->mp_state_bit_map->cpy(obj->mp_state_bit_map);
		p_M->mp_mode_bit_map		= obj->mp_mode_bit_map->cpy(obj->mp_mode_bit_map);
	}
	p_M->mp_state_label_map 	= obj->mp_state_label_map->cpy(obj->mp_state_label_map);
	p_M->mp_state_mode_map		= obj->mp_state_mode_map->cpy(obj->mp_state_mode_map);
	p_M->mp_states 				= obj->mp_states->cpy(obj->mp_states);
	p_M->mp_transitions_map		= obj->mp_transitions_map->cpy(obj->mp_transitions_map);
	p_M->mp_unmarked			= obj->mp_unmarked->cpy(obj->mp_unmarked);
	p_M->m_nstates = obj->m_nstates;
	p_M->m_nbits_mode = obj->m_nbits_mode;
	p_M->m_nbits_state = obj->m_nbits_state;
	p_M->m_initial = obj->m_initial;

	return p_M;
}


// set functions
static void idkripke_add_polytope_to_mode(idkripke_t *obj, int q, idpolytope_t *h_poly, int symbol) {
	assert(obj!=NULL);
	assert(h_poly!=NULL);
	assert(obj->mp_mode_sys->get_at(obj->mp_mode_sys,q)!=NULL);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("=======================================================================================\n");
	printf("debug add_polytope to mode q_%d: starting...\n", q);
	printf("debug add_polytope to mode q_%d: nstates = %d, s0 = %d \n", q, obj->m_nstates, obj->m_initial);
	if (symbol>=0) {
		printf("debug add_polytope to mode q_%d: adding label = {%d}, poly = \n", q, symbol);
	} else {
		printf("debug add_polytope to mode q_%d: adding label = {}, poly = \n", q);
	}
	h_poly->print(h_poly,stdout);
	char str[80];
#endif


	idarray_t *p_array1, *p_array2, *p_outter_poly_array;
	idpolytope_t *p_poly_to_add, *p_poly_inter, *p_outter_poly, *p_poly;
	idlist_t *p_inner_states, *p_labels;
	int flag, si, sinner;
	idbool_t exists_state_in_kripke_that_intersects_poly_to_add;
	idarray_t *h_array_now, *h_array_next;
	idlist_t *h_states;
	idrb_node_t *it, *it_outter_poly, *it_remainers;
	idpolytope_t *h_poly_i;
	idlist_t *h_label_i;

	p_labels = idintlist();
	if (symbol>=0) {
		p_labels->push(p_labels,&symbol);
	}
	p_poly = h_poly->cpy(h_poly);

	h_states = obj->mp_state_mode_map->get_values1_at(obj->mp_state_mode_map,&q);
	assert(h_states!=NULL);
	assert(h_states->length(h_states)>0);
	p_inner_states = idintlist();

	// During merging with existing states we execute poly-(poly & state) which may generate multiple polytopes.
	p_array1 = idpolyarray_byref();
	p_array2 = idpolyarray_byref();
	p_array1->append(p_array1,p_poly);
	h_array_now = NULL;
	h_array_next = NULL;

	// Here, we merge all subsets of poly which intersects with an existing state
	flag=0;

	do {
		h_array_now 	= (flag==0?p_array1:p_array2);
		h_array_next 	= (flag==0?p_array2:p_array1);
		flag = (flag+1)%2;


		while(!idrb_empty(h_array_now->mp_head)) {
			p_poly_to_add = (idpolytope_t *)idarray_byref_pop(h_array_now);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
			printf("debug add_polytope to mode q_%d: poly to add = \n", q);
			p_poly_to_add->print(p_poly_to_add,stdout);
#endif

			exists_state_in_kripke_that_intersects_poly_to_add = FALSE;

			idrb_traverse(it,h_states->mp_head) {
				si = *(int *)it->value(it);
				if (!p_inner_states->exists(p_inner_states,&si)) {
					h_poly_i = (idpolytope_t *)obj->mp_states->get_at(obj->mp_states,si);
					h_label_i = obj->mp_state_label_map->get_values2_at(obj->mp_state_label_map,&si);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
					printf("debug add_polytope to mode q_%d: nstates=%d, labels[%d] = %s, s_%d = \n", q,
							obj->m_nstates,si,idintlist_to_str(h_label_i,str),si);
					h_poly_i->print(h_poly_i,stdout);
#endif

					p_poly_inter = h_poly_i->and_with_polytope(h_poly_i,p_poly_to_add);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
					printf("debug add_polytope to mode q_%d: poly of s_%d intersection with poly to add = \n", q, si);
					p_poly_inter->print(p_poly_inter,stdout);
#endif

					if (p_poly_inter->isfulldim(p_poly_inter)) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
						printf("debug add_polytope to mode q_%d: split\n", q);
#endif

						p_outter_poly_array = p_poly_to_add->difference(p_poly_to_add,p_poly_inter);
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
						printf("debug add_polytope to mode q_%d: the difference poly-sinter results into %d poly\n",
								q,p_outter_poly_array->length(p_outter_poly_array));
#endif

						idrb_traverse(it_outter_poly,p_outter_poly_array->mp_head) {
							p_outter_poly = (idpolytope_t *)it_outter_poly->value(it_outter_poly);
		#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
							printf("debug add_polytope to mode q_%d: outter poly %d = \n",q,
									it_outter_poly->keyi(it_outter_poly));
							p_outter_poly->print(p_outter_poly,stdout);
		#endif
							if (!p_outter_poly->isempty(p_outter_poly)) {
								h_array_next->append(h_array_next,p_outter_poly);
							} else {
								p_outter_poly->destroy(p_outter_poly);
							}
						}
						p_outter_poly_array->destroy(p_outter_poly_array);

						if (!h_label_i->is_equal_to(h_label_i,p_labels)) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
							printf("---------------------------------------------------------------------------------------\n");
							printf("debug add_polytope to mode q_%d: split inner poly from s_%d\n",q,si);
#endif

							sinner = idkripke_split_inner_polytope(obj,si,q,p_poly_inter,p_labels,
									h_poly_i,h_label_i);
							p_inner_states->push(p_inner_states,&sinner);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
							printf("debug add_polytope to mode q_%d: edges = \n",q);
							idinttable_print(obj->mp_edges,stdout);
							printf("---------------------------------------------------------------------------------------\n");
#endif
						}

						p_poly_to_add->destroy(p_poly_to_add);
						p_poly_to_add = NULL;

						while(!idrb_empty(h_array_now->mp_head)) {
							p_poly_to_add = (idpolytope_t *)idarray_byref_pop(h_array_now);
							h_array_next->append(h_array_next,p_poly_to_add);
							p_poly_to_add = NULL;
						}

						exists_state_in_kripke_that_intersects_poly_to_add = TRUE;
						p_poly_inter->destroy(p_poly_inter);
						break;
					} // if p_poly_inter is full dimensional
					p_poly_inter->destroy(p_poly_inter);
				} // if si is not in inner polytope list
			} // for all states in mode
			idpolytope_destroy(p_poly_to_add);
		} // for all poly to add in the array_now
	} while ((!idrb_empty(h_array_next->mp_head))&&(exists_state_in_kripke_that_intersects_poly_to_add));

	p_labels->destroy(p_labels);
	p_array1->destroy(p_array1);
	p_array2->destroy(p_array2);
	p_inner_states->destroy(p_inner_states);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	obj->print(obj,stdout);
#endif
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("debug add_polytope to mode q_%d: END\n, mode");
	printf("=======================================================================================\n");
#endif

}


void idkripke_add_polytope(idkripke_t *obj, idpolytope_t *p_poly, int symbol) {
	assert(obj!=NULL);
	assert(p_poly!=NULL);
	assert(obj->m_nstates>0);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("=======================================================================================\n");
	printf("debug add_polytope: starting...\n");
	printf("debug add_polytope: nstates = %d, s0 = %d \n", obj->m_nstates, obj->m_initial);
	if (symbol>=0) {
		printf("debug add_polytope: adding label = {%d}, poly = \n", symbol);
	} else {
		printf("debug add_polytope: adding label = {}, poly = \n");
	}
	p_poly->print(p_poly,stdout);
	char str[80];
#endif

	int q;
	idrb_node_t *it;
	idrb_traverse(it,obj->mp_mode_sys->mp_head) {
		q = it->keyi(it);
		idkripke_add_polytope_to_mode(obj,q,p_poly,symbol);
	}
	p_poly->destroy(p_poly);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	obj->print(obj,stdout);
#endif
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("debug add_polytope: END\n");
	printf("=======================================================================================\n");
#endif
}
void             idkripke_add_mode(idkripke_t *obj,
		int q, idlinear_sys_t *p_sys, idpolytope_t *p_statedom, idpolytope_t *p_inputdom) {
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("=======================================================================================\n");
	printf("debug add_mode: starting...\n");
	printf("debug add_mode: nmodes = %d\n", obj->m_nmodes);
	printf("debug add_mode: adding mode = q_%d, linear dynamics = \n", q);
	p_sys->print(p_sys,stdout);
	printf("debug add_mode: state domain = \n");
	p_statedom->print(p_statedom,stdout);
	printf("debug add_mode: input domain = \n");
	p_inputdom->print(p_inputdom,stdout);
	char str[80];
#endif

	int si;
	idlist_t *p_label;
	p_label = idintlist();
	idkripke_append_mode(obj,q,p_sys,p_statedom,p_inputdom);
	si = idkripke_append_state(obj, q, p_statedom->cpy(p_statedom), p_label);
	idkripke_adjacency_find_between_list(obj, &si, NULL);

#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L2
	obj->print(obj,stdout);
#endif
#ifdef IDKRIPKE_DEBUG_ADD_POLYTOPE_L1
	printf("debug add_mode: END\n");
	printf("=======================================================================================\n");
#endif
}

void             idkripke_rem_transition_between_modes(idkripke_t *obj, int q0, int q1) {
	assert(obj!=NULL);
	assert(obj->mp_transitions_map!=NULL);

	obj->mp_transitions_map->set(obj->mp_transitions_map,&q0,&q1);
}
int             idkripke_dim(idkripke_t *obj) {
	assert(obj!=NULL);
	return obj->mp_x0->rows(obj->mp_x0);
}

idbool_t        idkripke_is_fair(idkripke_t *obj) {
	assert(obj!=NULL);

	return obj->m_isfair;
}

idkripke_t * idkripke_empty(iddblmatrix_t *p_x0, int q0, idbool_t isfair) {
	assert(q0>=0);

	idkripke_t *obj = idkripke_default_constructor(p_x0,q0,isfair);
	obj->mp_edges				= idtable_byvalue(sizeof(int));
	obj->mp_edges_map			= idmap_byvalues(sizeof(int),idintcmpfunc_,sizeof(int),idintcmpfunc_);
	obj->mp_mode_sys			= idarray_byptr(idlinear_sys_destroy,idlinear_sys_cpy);
	obj->mp_mode_statedom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_mode_inputdom		= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_state_label_map 	= idintmap();
	obj->mp_state_mode_map		= idintmap();
	obj->mp_states 				= idarray_byptr(idpolytope_destroy,idpolytope_cpy);
	obj->mp_transitions_map		= idintmap();
	obj->mp_unmarked			= idintlist();

	return obj;
}
Z3_ast         *idkripke_z3_bitvec_at(idkripke_t *obj, Z3_context ctx, int K) {
	assert(obj!=NULL);

	Z3_ast *bitvec;
	int i, s;
	char str[80];
	idrb_node_t *it;


	if (obj->mp_state_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}

	bitvec = malloc(obj->nbits(obj)*sizeof(Z3_ast));
	assert(bitvec!=NULL);

	for(i=0;i<obj->nbits(obj);i++) {
		sprintf(str,"b%d_%d",i,K);
		bitvec[i] = mk_bool_var(ctx,str);
	}

	return bitvec;
}
Z3_ast          idkripke_z3_state(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int state) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);

	Z3_ast args[obj->nbits(obj)];
	int i, b, bb, *h_bb;

	//bb = idintmap_get_first_value2_from_value1(obj->mp_state_bit_map,state);
	bb = obj->get_bits_from_state(obj,state);
	assert(bb>=0);

	for(i=0;i<obj->m_nbits_state;i++) {
		b = (bb >> i) & 1;

		if (b) {
			args[i] = bitvec[i];
		} else {
			args[i] = Z3_mk_not(ctx,bitvec[i]);
		}
	}

	return Z3_mk_and(ctx,obj->nbits(obj),args);
}
Z3_ast          idkripke_z3_mode(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int mode) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);

	Z3_ast args[obj->nbits(obj)];
	int i, b, bb, *h_bb;

//	bb = idintmap_get_first_value2_from_value1(obj->mp_mode_bit_map,mode)+obj->m_nbits_state;
	bb = obj->get_bits_from_mode(obj,mode);
	assert(bb>=0);

	for(i=obj->m_nbits_state;i<obj->nbits(obj);i++) {
		b = (bb >> i) & 1;

		if (b) {
			args[i] = bitvec[i+obj->m_nbits_state];
		} else {
			args[i] = Z3_mk_not(ctx,bitvec[i+obj->m_nbits_state]);
		}
	}

	return Z3_mk_and(ctx,obj->nbits(obj),args);
}
Z3_ast          idkripke_z3_atomicprop(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int label) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);

	int si, cnt;
	idlist_t *h_states;
	idrb_node_t *it, *h_lst;
	h_states = obj->mp_state_label_map->get_values1_at(obj->mp_state_label_map,&label);

	if ((h_states==NULL)||(h_states->length(h_states)==0)) {
		return Z3_mk_true(ctx);
	}
	Z3_ast *args, out;

	h_lst = h_states->mp_head;
	assert(h_lst!=NULL);

	args = malloc(h_lst->length(h_lst)*sizeof(Z3_ast));
	assert(args!=NULL);
	cnt = 0;
	idrb_traverse(it,h_lst) {
		si = *(int *)it->value(it);
		args[cnt] = obj->z3_state(obj,ctx,bitvec,si);
		cnt++;
	}

	if (cnt>1) {
		out = Z3_mk_or(ctx,cnt,args);
	} else if (cnt==1) {
		out = args[0];
	} else {
		out = Z3_mk_true(ctx);
	}
	free(args);

	return out;
}
Z3_ast          idkripke_z3_edges(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus) {
	assert(obj!=NULL);
	assert(bitvecK!=NULL);
	assert(bitvecKplus!=NULL);

	idrb_node_t *it1, *it2;
	idlist_t *h_to_lst;
	int to, from, *p_ind, numnz;
	idarray_t *p_ast_array = idarray_byvalue(sizeof(Z3_ast));
	Z3_ast args2[2], arg, *p_args;

	// assuming non-direcional edges
	idrb_traverse(it1,obj->mp_edges_map->mp_values2_at) {
		from = *(int *)it1->keyg(it1);
		h_to_lst = (idlist_t *)it1->value(it1);
		args2[0] = obj->z3_state(obj,ctx,bitvecK,from);
		idrb_traverse(it2,h_to_lst->mp_head) {
			to = *(int *)it2->value(it2);
			args2[1] = obj->z3_state(obj,ctx,bitvecKplus,to);
			arg = Z3_mk_and(ctx,2,args2);
			p_ast_array->append(p_ast_array,&arg);
		}
	}
	p_ast_array->to_sparse(p_ast_array,&numnz,&p_ind,&p_args);
	arg = Z3_mk_or(ctx,numnz,p_args);
	free(p_ind);
	free(p_args);
	p_ast_array->destroy(p_ast_array);

	return arg;
}
Z3_ast          idkripke_z3_transitions(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus) {
	assert(obj!=NULL);
	assert(bitvecK!=NULL);
	assert(bitvecKplus!=NULL);

	int nmodes = obj->mp_mode_sys->length(obj->mp_mode_sys);
	assert(nmodes>0);
	if ((nmodes==1)||(obj->mp_transitions_map->m_nvalues1==0)) {
		return  Z3_mk_true(ctx);
	}

	idrb_node_t *it1, *it2;
	idlist_t *h_to_lst;
	int to, from, *p_ind, numnz;
	idarray_t *p_ast_array = idarray_byvalue(sizeof(Z3_ast));
	Z3_ast args2[2], arg, *p_args;

	for (from=0;from<nmodes;from++) {
		args2[0] = obj->z3_mode(obj,ctx,bitvecK,from);
		for (to=0;to<nmodes;to++) {
			if (!obj->mp_transitions_map->exists(obj->mp_transitions_map,&from,&to)) {
				args2[1] = obj->z3_mode(obj,ctx,bitvecKplus,to);
				arg = Z3_mk_and(ctx,2,args2);
				p_ast_array->append(p_ast_array,&arg);
			}
		}
	}
	p_ast_array->to_sparse(p_ast_array,&numnz,&p_ind,&p_args);
	arg = Z3_mk_or(ctx,numnz,p_args);
	free(p_ind);
	free(p_args);
	p_ast_array->destroy(p_ast_array);

	return arg;
}

Z3_ast          idkripke_z3_unmarked(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec) {
	assert(obj!=NULL);
	assert(bitvec!=NULL);

	if (idrb_empty(obj->mp_unmarked->mp_head)) {
		return Z3_mk_true(ctx);
	}

	int si, cnt;
	idrb_node_t *it;
	Z3_ast args[obj->mp_unmarked->mp_head->length(obj->mp_unmarked->mp_head)];

	cnt = 0;
	idrb_traverse(it,obj->mp_unmarked->mp_head) {
		si = *(int *)it->value(it);
		args[cnt] = Z3_mk_not(ctx,obj->z3_state(obj,ctx,bitvec,si));
		cnt++;
	}

	return Z3_mk_or(ctx,cnt,args);
}
Z3_ast         *idkripke_z3_bitvecE(idkripke_t *obj, Z3_context ctx) {
	assert(obj!=NULL);

	Z3_ast *bitvec;
	int i;
	char str[80];

	bitvec = malloc(obj->nbits(obj)*sizeof(Z3_ast));
	assert(bitvec!=NULL);

	for(i=0;i<obj->nbits(obj);i++) {
		sprintf(str,"b%d_E",i);
		bitvec[i] = mk_bool_var(ctx,str);
	}

	return bitvec;
}

int				idkripke_get_bits_from_state(idkripke_t *obj, int state) {
	assert(obj!=NULL);

	if (obj->mp_state_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}

	int *h_bits = idintmap_get_first_value2_from_value1(obj->mp_state_bit_map,state);
	if (h_bits==NULL) {
		return -1;
	}
	return *h_bits;
}
int				idkripke_get_bits_from_mode(idkripke_t *obj, int mode) {
	assert(obj!=NULL);

	if (obj->mp_mode_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}

	int *h_bits = idintmap_get_first_value2_from_value1(obj->mp_mode_bit_map,mode);
	if (h_bits==NULL) {
		return -1;
	}
	return *h_bits;
}
idmap_t *		idkripke_get_state_bit_map(idkripke_t *obj) {
	assert(obj!=NULL);

	if (obj->mp_state_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}

	return obj->mp_state_bit_map;
}
int				idkripke_get_state_from_bits(idkripke_t *obj, int bits) {
	assert(obj!=NULL);

	if (obj->mp_state_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}


	int mask = 1;
	for(int i=1;i<obj->m_nbits_state;i++) {
		mask = (mask<<1) + 1;
	}
	bits = bits & mask;

	int *h_state = idintmap_get_first_value1_from_value2(obj->mp_state_bit_map,bits);
	assert(h_state!=NULL);
	return *h_state;
}
int				idkripke_get_mode_from_bits(idkripke_t *obj, int bits) {
	assert(obj!=NULL);

	if (obj->mp_state_bit_map == NULL) {
		idkripke_init_state_mode_bit_map(obj);
	}


	int mask = 1;
	for(int i=0;i<obj->m_nbits_state;i++) {
		mask = (mask<<1);
	}
	for(int i=1;i<obj->m_nbits_mode;i++) {
		mask = (mask<<1)+1;
	}
	bits = (bits & mask)>>obj->m_nbits_state;

	if (obj->m_nbits_mode>0) {
		int *h_mode = idintmap_get_first_value1_from_value2(obj->mp_mode_bit_map,bits);
		assert(h_mode!=NULL);
		return *h_mode;
	}
	assert(bits==0);
	return 0;
}
void             idkripke_print(idkripke_t *obj, FILE *out) {
	assert(obj!=NULL);
	idrb_node_t *it;
	int si, qi;
	idlist_t *h_label_i, *h_modes_i;
	char str[80];


	printf("modes = %d\n",obj->m_nmodes);
	idrb_traverse(it,obj->mp_mode_sys->mp_head)  {
		qi = it->keyi(it);
		printf("mode q_%d = \n",qi);
		idlinear_sys_print((idlinear_sys_t *)it->value(it),stdout);
		printf("X = \n");
		idpolytope_print((idpolytope_t *)obj->mp_mode_statedom->get_at(obj->mp_mode_statedom,qi),stdout);
		printf("U = \n");
		idpolytope_print((idpolytope_t *)obj->mp_mode_inputdom->get_at(obj->mp_mode_inputdom,qi),stdout);
	}
	printf("nstates = %d\n",obj->m_nstates);
	idrb_traverse(it,obj->mp_states->mp_head)  {
		si = it->keyi(it);
		h_label_i = obj->mp_state_label_map->get_values2_at(obj->mp_state_label_map,&si);
		h_modes_i = obj->mp_state_mode_map->get_values2_at(obj->mp_state_mode_map,&si);
		printf("state s_%d, label = {%s}",si,(h_label_i==NULL?"":idintlist_to_str(h_label_i,str)));
		printf(", modes = {%s}, polytope = \n",(h_modes_i==NULL?"":idintlist_to_str(h_modes_i,str)));
		idpolytope_print((idpolytope_t *)it->value(it),stdout);
	}
	printf("edges = \n");
	idinttable_print(obj->mp_edges,stdout);
	printf("s0 = %d \n", obj->m_initial);

}
