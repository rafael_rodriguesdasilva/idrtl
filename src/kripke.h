 /*
 * kripke.h
 *
 *  Created on: Mar 13, 2019
 *      Author: rafael
 */

#ifndef KRIPKE_H_
#define KRIPKE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


/* public functions */
extern idkripke_t *     idkripke_single_mode(iddblmatrix_t *p_x0, idbool_t isfair, idpolytope_t *p_workspace,
		idlinear_sys_t *p_sys, idpolytope_t *p_U);
extern idkripke_t *     idkripke(iddblmatrix_t *p_x0, int q0, idbool_t isfair);
extern idkripke_t * idkripke_empty(iddblmatrix_t *p_x0, int q0, idbool_t isfair);
extern void             idkripke_destroy(idkripke_t *obj);
extern idkripke_t * 	idkripke_cpy(idkripke_t *obj);

/* set functions */
extern void             idkripke_add_polytope(idkripke_t *obj, idpolytope_t *p_poly, int symbol);
extern void             idkripke_add_mode(idkripke_t *obj,
		int q, idlinear_sys_t *p_sys, idpolytope_t *p_statedom, idpolytope_t *p_inputdom);
extern void             idkripke_rem_transition_between_modes(idkripke_t *obj, int q0, int q1);

/* get functions */
extern int             idkripke_dim(idkripke_t *obj);
extern int             idkripke_nbits(idkripke_t *obj);
extern int             idkripke_nstates(idkripke_t *obj);
extern idbool_t        idkripke_is_fair(idkripke_t *obj);
extern idmap_t *		idkripke_get_state_bit_map(idkripke_t *obj);
extern int				idkripke_get_bits_from_state(idkripke_t *obj, int state);
extern int				idkripke_get_bits_from_mode(idkripke_t *obj, int mode);
extern int				idkripke_get_state_from_bits(idkripke_t *obj, int bits);
extern int				idkripke_get_mode_from_bits(idkripke_t *obj, int bits);
extern void             idkripke_print(idkripke_t *obj, FILE *out);

/* static operations */
extern void 			idkripke_append_mode(idkripke_t *obj,
							int q, idlinear_sys_t *p_sys, idpolytope_t *p_statedom, idpolytope_t *p_inputdom);
extern int 				idkripke_append_state(idkripke_t *obj, int q, idpolytope_t *p_state, idlist_t *p_label);
extern void 			idkripke_insert_edge(idkripke_t *obj, int from, int to, int facet);

/* SMT operations */
extern Z3_ast         *idkripke_z3_bitvecE(idkripke_t *obj, Z3_context ctx);
extern Z3_ast         *idkripke_z3_bitvec_at(idkripke_t *obj, Z3_context ctx, int K);
extern Z3_ast          idkripke_z3_state(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int state);
extern Z3_ast          idkripke_z3_atomicprop(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int label);
extern Z3_ast          idkripke_z3_edges(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus);
extern Z3_ast          idkripke_z3_transitions(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus);
extern Z3_ast          idkripke_z3_unmarked(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec);
extern Z3_ast          idkripke_z3_mode(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int mode);
#ifdef IDRTL_DEBUG_MEMLEAK_KRIPKE
	static int		    idkripke_destroy_cnt = 0;
#endif


struct idkripke_s{
	/* capsulated member functions */
	void             (*destroy)(idkripke_t *obj);
	idkripke_t * 	 (*cpy)(idkripke_t *obj);
	void             (*check_all_adjacencies)(idkripke_t *obj);

	/* set functions */
	void 			 (*add_polytope)(idkripke_t *obj, idpolytope_t *p_poly, int symbol);
	void             (*add_mode)(idkripke_t *obj,
			int q, idlinear_sys_t *p_sys, idpolytope_t *p_statedom, idpolytope_t *p_inputdom);
	void             (*rem_transition_between_modes)(idkripke_t *obj, int q0, int q1);

	/* get functions */
	int 			 (*dim)(idkripke_t *obj);
	int 			 (*nbits)(idkripke_t *obj);
	int 			 (*nstates)(idkripke_t *obj);
	idbool_t		 (*is_fair)(idkripke_t *obj);
	idmap_t *		 (*get_state_bit_map)(idkripke_t *obj);
	int				 (*get_bits_from_state)(idkripke_t *obj, int state);
	int				 (*get_bits_from_mode)(idkripke_t *obj, int mode);
	int				 (*get_state_from_bits)(idkripke_t *obj, int bits);
	int				 (*get_mode_from_bits)(idkripke_t *obj, int bits);
	void             (*print)(idkripke_t *obj, FILE *out);

	/* SMT operations */
	Z3_ast         *(*z3_bitvecE)(idkripke_t *obj, Z3_context ctx);
	Z3_ast         *(*z3_bitvec_at)(idkripke_t *obj, Z3_context ctx, int K);
	Z3_ast          (*z3_state)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int state);
	Z3_ast          (*z3_atomicprop)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int label);
	Z3_ast          (*z3_edges)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus);
	Z3_ast          (*z3_transitions)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvecK, Z3_ast *bitvecKplus);
	Z3_ast          (*z3_unmarked)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec);
	Z3_ast          (*z3_mode)(idkripke_t *obj, Z3_context ctx, Z3_ast *bitvec, int mode);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_KRIPKE
	int		      m_id;
#endif
	idtable_t		*mp_edges;
	idmap_t			*mp_edges_map,
					*mp_state_label_map,
					*mp_state_mode_map,
					*mp_state_bit_map,
					*mp_mode_bit_map,
					*mp_transitions_map;
	idarray_t  		*mp_states,
					*mp_mode_sys,
					*mp_mode_inputdom,
					*mp_mode_statedom;
	idlist_t     	*mp_unmarked;
	idcsys_t 		*mp_x0_constr;
	iddblmatrix_t	*mp_x0;
	int			      m_initial,
					  m_nstates,
					  m_nmodes,
					  m_nbits_state,
					  m_nbits_mode,
					  m_q0;
	idbool_t          m_isfair;
};


#ifdef __cplusplus
}
#endif



#endif /* KRIPKE_H_ */
