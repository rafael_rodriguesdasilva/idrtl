/*
 * linear_dynamical_system.c
 *
 *  Created on: Jul 13, 2019
 *      Author: rafael
 */

#include "linear_dynamical_system.h"

/* public functions */
idlinear_sys_t * 	idlinear_sys(iddblmatrix_t *p_A, iddblmatrix_t *p_B, iddblmatrix_t *p_c) {
	assert(p_A!=NULL);
	assert(p_B!=NULL);
	assert(p_c!=NULL);
	assert(p_A->rows(p_A)==p_A->cols(p_A));
	assert(p_A->rows(p_A)==p_B->rows(p_B));
	assert(p_A->rows(p_A)==p_c->rows(p_c));
	assert(p_c->cols(p_c)==1);

	idlinear_sys_t *obj = malloc(sizeof(idlinear_sys_t));
	assert(obj!=NULL);

	obj->destroy = idlinear_sys_destroy;
	obj->cpy = idlinear_sys_cpy;
	obj->A = idlinear_sys_A;
	obj->B = idlinear_sys_B;
	obj->c = idlinear_sys_c;
	obj->n = idlinear_sys_n;
	obj->m = idlinear_sys_m;
	obj->print = idlinear_sys_print;
	obj->infnorm = idlinear_sys_infnorm;

	obj->mp_A = p_A;
	obj->mp_B = p_B;
	obj->mp_c = p_c;
	obj->m_n = p_A->rows(p_A);
	obj->m_m = p_B->cols(p_B);

#ifdef IDRTL_DEBUG_MEMLEAK_LINEAR_SYS
	static int idlinear_sys_last_id = 0;
	obj->m_id = idlinear_sys_last_id;
	printf("debug memleak construct idlinear_sys %d\n", obj->m_id);
	idlinear_sys_last_id++;
	idlinear_sys_destroy_cnt++;
#endif

	return obj;
}
void      			idlinear_sys_destroy(idlinear_sys_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_LINEAR_SYS
		idlinear_sys_destroy_cnt--;
		printf("debug memleak destroy idlinear_sys %d still remains %d to destroy\n", obj->m_id, idlinear_sys_destroy_cnt);
		if (idlinear_sys_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		iddblmatrix_destroy(obj->mp_A);
		iddblmatrix_destroy(obj->mp_B);
		iddblmatrix_destroy(obj->mp_c);
		free(obj);
	}
}
idlinear_sys_t * 	idlinear_sys_cpy(idlinear_sys_t *obj) {
	assert(obj!=NULL);

	idlinear_sys_t *sys = idlinear_sys(	obj->mp_A->cpy(obj->mp_A),
										obj->mp_B->cpy(obj->mp_B),
										obj->mp_c->cpy(obj->mp_c));
	return sys;
}

/* get functions */
iddblmatrix_t *	idlinear_sys_A(idlinear_sys_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_A!=NULL);
	return obj->mp_A;
}
iddblmatrix_t *	idlinear_sys_B(idlinear_sys_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_B!=NULL);
	return obj->mp_B;
}
iddblmatrix_t *	idlinear_sys_c(idlinear_sys_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_c!=NULL);
	return obj->mp_c;
}
int						idlinear_sys_n(idlinear_sys_t *obj) {
	assert(obj!=NULL);
	return obj->m_n;
}
int						idlinear_sys_m(idlinear_sys_t *obj) {
	assert(obj!=NULL);
	return obj->m_m;
}
void						idlinear_sys_print(idlinear_sys_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);

	int i, j, n = obj->n(obj), m = obj->m(obj);
	iddblmatrix_t *h_A = obj->A(obj), *h_B = obj->B(obj), *h_c = obj->c(obj);
	double v;
	idbool_t is_first;

	for(i=0;i<n;i++) {
		is_first = TRUE;
		fprintf(out,"x_%d \t= ", i);
		for(j=0;j<n;j++) {
			v = h_A->get(h_A,i,j);
			if (fabs(v)>=ID_ABSTOL) {
				if (is_first) {
					fprintf(out,"%.3f*x_%d", v, j);
					is_first = FALSE;
				} else if (v > 0.0) {
					fprintf(out,"\t+ %.3f*x_%d", fabs(v), j);
				} else {
					fprintf(out,"\t- %.3f*x_%d", fabs(v), j);
				}
			}
		}

		for(j=0;j<m;j++) {
			v = h_B->get(h_B,i,j);
			if (fabs(v)>=ID_ABSTOL) {
				if (is_first) {
					fprintf(out,"%.3f*u_%d", v, j);
					is_first = FALSE;
				} else if (v > 0.0) {
					fprintf(out,"\t+ %.3f*u_%d", fabs(v), j);
				} else {
					fprintf(out,"\t- %.3f*u_%d", fabs(v), j);
				}
			}
		}

		v = h_c->get(h_c,i,0);
		if (fabs(v)>=ID_ABSTOL) {
			if (is_first) {
				fprintf(out,"%.3f", v);
				is_first = FALSE;
			} else if (v > 0.0) {
				fprintf(out,"\t+ %.3f", fabs(v));
			} else {
				fprintf(out,"\t- %.3f", fabs(v));
			}
		}
		fprintf(out,"\n");
	}

}
double 			idlinear_sys_infnorm(idlinear_sys_t *obj, double *h_x0, double *h_u0, double *h_x1) {
	assert(obj!=NULL);
	assert(h_x0!=NULL);
	assert(h_u0!=NULL);
	assert(h_x1!=NULL);

	int i, j;
	double v, vmax = -GRB_INFINITY;

	for(i=0;i<obj->m_n;i++) {
		v = h_x1[i];
		for(j=0;j<obj->m_n;j++) {
			v -= h_x0[j]*obj->mp_A->get(obj->mp_A,i,j);
		}
		for(j=0;j<obj->m_m;j++) {
			v -= h_u0[j]*obj->mp_B->get(obj->mp_B,i,j);
		}
		v -= obj->mp_c->get(obj->mp_c,i,0);
		if (v>vmax) {
			vmax = v;
		}
	}
	return vmax;
}
