/*
 * linear_dynamical_system.h
 *
 *  Created on: Jul 13, 2019
 *      Author: rafael
 */

#ifndef SOLVER_LINEAR_DYNAMICAL_SYSTEM_H_
#define SOLVER_LINEAR_DYNAMICAL_SYSTEM_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idlinear_sys_t * 	idlinear_sys(iddblmatrix_t *p_A, iddblmatrix_t *p_B, iddblmatrix_t *p_c);
extern void      			idlinear_sys_destroy(idlinear_sys_t *obj);
extern idlinear_sys_t * 	idlinear_sys_cpy(idlinear_sys_t *obj);

/* get functions */
extern iddblmatrix_t *	idlinear_sys_A(idlinear_sys_t *obj);
extern iddblmatrix_t *	idlinear_sys_B(idlinear_sys_t *obj);
extern iddblmatrix_t *	idlinear_sys_c(idlinear_sys_t *obj);
extern int				idlinear_sys_n(idlinear_sys_t *obj);
extern int				idlinear_sys_m(idlinear_sys_t *obj);
extern void				idlinear_sys_print(idlinear_sys_t *obj, FILE *out);
extern double 			idlinear_sys_infnorm(idlinear_sys_t *obj, double *h_x0, double *h_u0, double *h_x1);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_LINEAR_SYS
	static int		    idlinear_sys_destroy_cnt = 0;
#endif


struct idlinear_sys_s{
	/* capsulated member functions */
	void           		(*destroy)(idlinear_sys_t *obj);
	idlinear_sys_t *	(*cpy)(idlinear_sys_t *obj);

	/* get functions */
	iddblmatrix_t *	(*A)(idlinear_sys_t *obj);
	iddblmatrix_t *	(*B)(idlinear_sys_t *obj);
	iddblmatrix_t *	(*c)(idlinear_sys_t *obj);
	int						(*n)(idlinear_sys_t *obj);
	int						(*m)(idlinear_sys_t *obj);
	void					(*print)(idlinear_sys_t *obj, FILE *out);
	double 					(*infnorm)(idlinear_sys_t *obj, double *h_x0, double *h_u0, double *h_x1);

	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_LINEAR_SYS
	int		      m_id;
#endif
	int 		m_n,
				m_m;
	iddblmatrix_t 	*mp_A,
					*mp_B,
					*mp_c;
};

#ifdef __cplusplus
}
#endif

#endif /* SOLVER_LINEAR_DYNAMICAL_SYSTEM_H_ */
