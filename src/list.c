/*
 * list.c
 *
 *  Created on: Jul 2, 2019
 *      Author: rafael
 */

#include "list.h"
static idlist_t *     	idlist_default_constructor(void) {
	idlist_t *obj = malloc(sizeof(idlist_t));
	assert(obj!=NULL);

	obj->destroy = idlist_destroy;
	obj->cpy = idlist_cpy;
	obj->push = NULL;
	obj->push_from_list = idlist_push_from_list;
	obj->pop = idlist_pop;
	obj->length = idlist_length;
	obj->remove = idlist_remove;
	obj->exists = idlist_exists;
	obj->find = idlist_find;
	obj->is_equal_to = idlist_is_equal_to;

#ifdef IDRTL_DEBUG_MEMLEAK_LIST
	static int idlist_last_id = 0;
	obj->m_id = idlist_last_id;
	printf("debug memleak construct list %d\n", obj->m_id);
	idlist_last_id++;
	idlist_destroy_cnt++;
#endif
	obj->m_length = 0;
	obj->m_sizeof_value = 0;
	obj->mh_cpy = NULL;
	obj->mp_head = NULL;

	return obj;
}

/* public functions */
idlist_t * 	idlist_byvalue(size_t sizeof_value, int (*cmp_values)(void *ptr1, void *ptr2)) {
	idlist_t *obj = idlist_default_constructor();

	obj->push = idlist_push_value;
	obj->m_sizeof_value = sizeof_value;
	obj->mh_cpy = NULL;
	obj->mp_head = idrb_with_custom_key(cmp_values,NULL,free);

	return obj;
}
idlist_t * 	idlist_byptr(int (*cmp_values)(void *ptr1, void *ptr2),
								void (*destroy_value)(void *value), void* (*cpy_value)(void *value)){
	idlist_t *obj = idlist_default_constructor();

	obj->push = idlist_push_ptr;
	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy = cpy_value;
	if (destroy_value == NULL) {
		obj->mp_head = idrb_with_custom_key(cmp_values,NULL,free);
	} else {
		obj->mp_head = idrb_with_custom_key(cmp_values,NULL,destroy_value);
	}

	return obj;
}
idlist_t * 	idlist_byref(int (*cmp_values)(void *ptr1, void *ptr2)){
	idlist_t *obj = idlist_default_constructor();

	obj->push = idlist_push_ptr;
	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy = idref_cpy;
	obj->mp_head = idrb_with_custom_key(cmp_values,NULL,NULL);

	return obj;
}

void             idlist_destroy(idlist_t *obj) {
	int i;
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_LIST
		idlist_destroy_cnt--;
		printf("debug memleak destroy list %d still remains %d to destroy\n", obj->m_id, idlist_destroy_cnt);
		if (idlist_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		obj->mp_head->destroy(obj->mp_head);
		free(obj);
	}
}
idlist_t *     	idlist_cpy(idlist_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it;
	idlist_t *lst = idlist_default_constructor();
	void *p_value, *h_value;

	lst->push = obj->push;
	lst->m_sizeof_value = obj->m_sizeof_value;
	lst->mh_cpy = obj->mh_cpy;
	lst->mp_head = idrb_with_custom_key(obj->mp_head->k.cmp,NULL,obj->mp_head->v.destroy);

	idrb_traverse(it, obj->mp_head) {
		h_value = it->value(it);
		if (lst->mh_cpy!=NULL) {
			p_value = lst->mh_cpy(h_value);
		} else {
			p_value = malloc(lst->m_sizeof_value);
			assert(p_value!=NULL);
			memcpy(p_value,h_value,lst->m_sizeof_value);
		}
		lst->mp_head->insert(lst->mp_head,p_value,p_value);
		lst->m_length++;
	}
	assert(lst->m_length==obj->m_length);

	return lst;
}
/* set functions */
void            idlist_push_ptr(idlist_t *obj, void *p_value) {
	assert(obj!=NULL);

	if (p_value==NULL)
		return;

	obj->remove(obj,p_value);
	obj->mp_head->insert(obj->mp_head,p_value,p_value);
	obj->m_length++;
}
void            idlist_push_value(idlist_t *obj, void *h_value) {
	assert(obj!=NULL);

	if ((h_value==NULL)||(obj->exists(obj,h_value)))
		return;
	void *p_value;

	if (obj->mh_cpy==NULL) {
		p_value = malloc(obj->m_sizeof_value);
		assert(p_value!=NULL);
		memcpy(p_value,h_value,obj->m_sizeof_value);
	} else {
		p_value = obj->mh_cpy(h_value);
	}

	obj->mp_head->insert(obj->mp_head,p_value,p_value);
	obj->m_length++;
}

/* get functions */
int           idlist_length(idlist_t *obj) {
	assert(obj!=NULL);
	return obj->m_length;
}
void *		idlist_pop(idlist_t *obj) {
	assert(obj!=NULL);

	if (obj->length(obj)==0) {
		return NULL;
	}

	void *p_value;
	idrb_node_t *h_node = idrb_last(obj->mp_head);
	assert(h_node!=NULL);

	if (idrb_nil(h_node)) {
		return NULL;
	}

	if (obj->mp_head->v.destroy==NULL){
		p_value = h_node->value(h_node);
	} else if (obj->mh_cpy==NULL) {
		p_value = malloc(obj->m_sizeof_value);
		assert(p_value!=NULL);
		memcpy(p_value,h_node->value(h_node),obj->m_sizeof_value);
	} else {
		p_value = obj->mh_cpy(h_node->value(h_node));
	}
	obj->mp_head->delete_node(h_node);
	obj->m_length--;
	return p_value;
}
void       	idlist_remove(idlist_t *obj, void *h_value) {
	assert(obj!=NULL);
	assert(h_value!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,h_value,&fnd);

	if (fnd) {
		obj->mp_head->delete_node(h_node);
		obj->m_length--;
	}
}
idbool_t   	idlist_exists(idlist_t *obj, void *h_value) {
	assert(obj!=NULL);
	assert(h_value!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,h_value,&fnd);

	return (fnd?TRUE:FALSE);
}
void *   	idlist_find(idlist_t *obj, void *h_value) {
	assert(obj!=NULL);
	assert(h_value!=NULL);

	int fnd;
	idrb_node_t *h_node = obj->mp_head->find_key_n(obj->mp_head,h_value,&fnd);

	if (fnd) {
		return h_node->value(h_node);
	}
	return NULL;
}
void      	idlist_push_from_list(idlist_t *obj, idlist_t *h_lst) {
	assert(obj!=NULL);
	assert(h_lst!=NULL);

	idrb_node_t *it;

	idrb_traverse(it, h_lst->mp_head) {
		obj->push(obj,it->value(it));
	}
}
void idintlist_print(idlist_t *obj, FILE* out) {
	assert(obj!=NULL);

	idrb_node_t *it;

	idrb_traverse(it,obj->mp_head) {
		fprintf(out,"%d ",*(int *)it->value(it));
	}
	fprintf(out,"\n");
}
char *idintlist_to_str(idlist_t *obj, char *h_str) {
	assert(obj!=NULL);

	idrb_node_t *it;

	h_str[0] = '\0';
	idrb_traverse(it,obj->mp_head) {
		if (it!=idrb_first(obj->mp_head)) {
			sprintf(h_str,"%s,%d",h_str,*(int *)it->value(it));
		} else {
			sprintf(h_str,"%d",*(int *)it->value(it));
		}
	}

	return h_str;
}
idbool_t   	idlist_is_equal_to(idlist_t *obj, idlist_t *h_another_list) {
	assert(obj!=NULL);
	assert(h_another_list!=NULL);

	if (	(obj->m_length != h_another_list->m_length) ||
			(obj->m_sizeof_value != h_another_list->m_sizeof_value) ||
			(obj->mp_head->k.cmp != h_another_list->mp_head->k.cmp)) {
		return FALSE;
	}

	idrb_node_t *it;
	idrb_traverse(it,h_another_list->mp_head) {
		if (!obj->exists(obj,it->value(it))) {
			return FALSE;
		}
	}

	return TRUE;
}
void *idlist_first(idlist_t *obj) {

	if ((obj==NULL)||(idrb_first(obj->mp_head)==idrb_nil(obj->mp_head))) {
		return NULL;
	}
	idrb_node_t *h_node = idrb_first(obj->mp_head);
	return h_node->value(h_node);
}
