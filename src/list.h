/*
 * list.h
 *
 *  Created on: Jul 2, 2019
 *      Author: rafael
 */

#ifndef SOLVER_LIST_H_
#define SOLVER_LIST_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


/* public functions */
extern idlist_t * 	idlist_byvalue(size_t sizeof_value, int (*cmp_values)(void *ptr1, void *ptr2));
extern idlist_t * 	idlist_byptr(int (*cmp_values)(void *ptr1, void *ptr2),
								void (*destroy_value)(void *value), void* (*cpy_value)(void *value));
extern idlist_t * 	idlist_byref(int (*cmp_values)(void *ptr1, void *ptr2));
extern void      	idlist_destroy(idlist_t *obj);
extern idlist_t * 	idlist_cpy(idlist_t *obj);

/* set functions */
extern void      	idlist_push_ptr(idlist_t *obj, void *p_value);
extern void        	idlist_push_value(idlist_t *obj, void *h_value);
extern void        	idlist_push_from_list(idlist_t *obj, idlist_t *h_lst);

/* get functions */
extern void *		idlist_pop(idlist_t *obj);
extern int 			idlist_length(idlist_t *obj);
extern void       	idlist_remove(idlist_t *obj, void *h_value);
extern idbool_t   	idlist_exists(idlist_t *obj, void *h_value);
extern void *   	idlist_find(idlist_t *obj, void *h_value);

/* operation */
extern idbool_t   	idlist_is_equal_to(idlist_t *obj, idlist_t *h_another_list);

#ifdef IDRTL_DEBUG_MEMLEAK_LIST
	static int		    idlist_destroy_cnt = 0;
#endif

/* static operation */
extern void *idlist_first(idlist_t *obj);
#define idcharlist(void) idlist_byvalue(sizeof(char),idcharcmpfunc_);
#define idintlist(void) idlist_byvalue(sizeof(int),idintcmpfunc_);
extern void idintlist_print(idlist_t *obj, FILE* out);
extern char *idintlist_to_str(idlist_t *obj, char *h_str);

struct idlist_s{
	/* capsulated member functions */
	void           	(*destroy)(idlist_t *obj);
	idlist_t *		(*cpy)(idlist_t *obj);

	/* get functions */
	void          	(*push)(idlist_t *obj, void *value);
	void          	(*push_from_list)(idlist_t *obj, idlist_t *h_lst);

	/* get functions */
	void *			(*pop)(idlist_t *obj);
	int          	(*length)(idlist_t *obj);
	void          	(*remove)(idlist_t *obj, void *h_value);
	idbool_t   		(*exists)(idlist_t *obj, void *h_value);
	void *			(*find)(idlist_t *obj, void *h_value);

	/* operation */
	idbool_t   	(*is_equal_to)(idlist_t *obj, idlist_t *h_another_list);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_LIST
	int		      m_id;
#endif
	idrb_node_t *mp_head;
	int 		m_length;
	size_t		m_sizeof_value;
	void* 		(*mh_cpy)(void *value);
};



#ifdef __cplusplus
}
#endif

#endif /* SOLVER_LIST_H_ */
