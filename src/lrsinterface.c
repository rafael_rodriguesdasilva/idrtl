/*
 * lrsinterface.c
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      Interfaces for lrs from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#include "lrsinterface.h"


size_t pN = 20; /* Number of rows/vertices/rays found between printing out info */

static long lrs_checkpoint_seconds = 0;

static long lrs_global_count = 0;

#ifdef SIGNALS
static void checkpoint ();
static void die_gracefully ();
static void setup_signals ();
static void timecheck ();
static void lrs_dump_state () {};
#endif


int my_lrs_init()
{
	assert ( lrs_mp_init (ZERO, stdin, stdout) );
	lrs_global_count = 0;
	lrs_checkpoint_seconds = 0;
#ifdef SIGNALS
	setup_signals ();
#endif
  return TRUE;
}


idmatrix_t *H2V(idmatrix_t *inp) {
	lrs_dic *Pv;
	lrs_dat *Qv;
	lrs_mp_vector output;
	lrs_mp_matrix Lin;

	size_t i;
	long col;

	assert( my_lrs_init () == TRUE );

	Qv = lrs_alloc_dat ("LRS globals");
	assert( Qv!= NULL );

	Qv->m = inp->rows(inp);
	Qv->n = inp->cols(inp);

	output = lrs_alloc_mp_vector (Qv->n);

	struct GMPlist* retVal = GMPlist_create();

	lrs_mp_vector num, den;
	num = lrs_alloc_mp_vector(inp->cols(inp));
	den = lrs_alloc_mp_vector(inp->cols(inp));

	Pv = lrs_alloc_dic (Qv);
	assert( Pv != NULL );

	idmatrix_t *out;

	mpq_t *curRow;

	for (i = 1; i <= inp->rows(inp); ++i) {
		inp->getRow(inp,num, den,i-1);
		lrs_set_row_mp(Pv,Qv,i,num,den,GE);
	}

	if( lrs_getfirstbasis (&Pv, Qv, &Lin, TRUE) ) {

		// RRS: Removed, it just print
		//for (col = 0L; col < Qv->nredundcol; col++)
		//  lrs_printoutput (Qv, Lin[col]);
		do {
			for (col = 0L; col <= Pv->d; col++) {
				if (lrs_getsolution (Pv, Qv, output, col)) {
					curRow = mpz_to_mpq_vec( output, inp->cols(inp));
					retVal = retVal->push(retVal, curRow);
				}
			}
		} while (lrs_getnextbasis (&Pv, Qv, FALSE));

		lrs_clear_mp_vector (output, Qv->n);
		lrs_clear_mp_vector (num, Qv->n);
		lrs_clear_mp_vector (den, Qv->n);
		lrs_free_dic (Pv,Qv);
		lrs_free_dat (Qv);

		out = retVal->to_matrix(retVal, inp->cols(inp));
		inp->destroy(inp);

		return out;

	} else {
		// RRS: Removed, it just print
		//printf("Empty set.\n");
		lrs_clear_mp_vector (output, Qv->n);
		lrs_clear_mp_vector (num, Qv->n);
		lrs_clear_mp_vector (den, Qv->n);
		lrs_free_dic (Pv,Qv);
		lrs_free_dat (Qv);
		out = id_zeros(0, inp->cols(inp));

		inp->destroy(inp);

		return out;
	}
}

idmatrix_t *V2H(idmatrix_t *inp) {
	lrs_dic *P;
	lrs_dat *Q;
	lrs_mp_vector output;
	lrs_mp_matrix Lin;

	long i;
	long col;

	assert( my_lrs_init () == TRUE );

	Q = lrs_alloc_dat ("LRS globals");
	assert ( Q != NULL );
	Q->m = inp->rows(inp);
	Q->n = inp->cols(inp);
	Q->hull = TRUE;
	Q->polytope = TRUE;

	output = lrs_alloc_mp_vector (Q->n);

	struct GMPlist* retVal = GMPlist_create();

	mpz_t *num, *den;
	num = mpz_row_init_one(inp->cols(inp));
	den = mpz_row_init_one(inp->cols(inp));

	P = lrs_alloc_dic (Q);
	assert ( P != NULL );

	idmatrix_t *retMat;


	mpq_t *curRow;

	lrs_set_row_mp(P ,Q ,0 ,num ,den , GE);
	mpz_set_si( P->A[0][0], 1L );

	for (i = 1; i <= inp->rows(inp); ++i) {
		inp->getRow(inp,num, den,i-1);
		if ( zero(num[0]) ) {
			Q->polytope = FALSE;
			Q->homogeneous = FALSE;
		}
		lrs_set_row_mp(P ,Q ,i ,num ,den , GE);
	}

	if ( lrs_getfirstbasis (&P, Q, &Lin, TRUE) ) {
		// RRS: removed since just pring
		//for (col = 0L; col < Q->nredundcol; col++)
		//  lrs_printoutput (Q, Lin[col]);

		do {
			for (col = 0; col <= P->d; col++) {
				if (lrs_getsolution (P, Q, output, col)) {
					curRow = mpz_to_mpq_vec( output, inp->cols(inp));
					retVal = retVal->push(retVal, curRow);
				}
			}
		} while (lrs_getnextbasis (&P, Q, FALSE));

		lrs_clear_mp_vector ( output, Q->n);
		mpz_row_clean ( num, inp->cols(inp));
		mpz_row_clean ( den, inp->cols(inp));
		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );

		retMat = retVal->to_matrix(retVal, inp->cols(inp));

		inp->destroy(inp);

	return retMat;

	} else {

		//printf("Empty set.\n");
		lrs_clear_mp_vector ( output, Q->n);
		mpz_row_clean ( num, inp->cols(inp));
		mpz_row_clean ( den, inp->cols(inp));
		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );

		retMat = id_zeros(0, inp->cols(inp));
		retMat->empty = 1;
		inp->destroy(inp);

		return retMat;

	}

}


idmatrix_t *reducemat(idmatrix_t *inp) {
	lrs_dic *P;
	lrs_dat *Q;
	lrs_mp_matrix Lin;

	long i;


	size_t m = inp->rows(inp);

	assert( my_lrs_init () == TRUE );

	Q = lrs_alloc_dat ("LRS globals");
	assert ( Q != NULL );
	Q->m = m;
	Q->n = inp->cols(inp);

	lrs_mp_vector num, den;
	num = lrs_alloc_mp_vector(inp->cols(inp));
	den = lrs_alloc_mp_vector(inp->cols(inp));

	P = lrs_alloc_dic (Q);
	assert ( P != NULL );

	idmatrix_t *retMat;

	struct GMPlist* retVal;


	for (i = 1; i <= m; ++i) {
		inp->getRow(inp,num, den,i-1);

		lrs_set_row_mp(P ,Q ,i ,num ,den , GE);
	}


	if ( lrs_getfirstbasis (&P, Q, &Lin, TRUE) ) {
		size_t lastdv = Q->lastdv;
		size_t d = P->d;
		size_t ineq;
		m = P->m_A;

		retVal = GMPlist_create();
		for (i = lastdv + 1; i <= m + d; ++i) {
			ineq = Q->inequality[i - lastdv] - 1;
			if (!checkindex(P, Q, i)) {
				retVal = retVal->push(retVal, inp->mpq_row_extract(inp, ineq));
			}
		}

		retMat = retVal->to_matrix(retVal, inp->cols(inp));

		lrs_clear_mp_vector(num,inp->cols(inp));
		lrs_clear_mp_vector(den,inp->cols(inp));
		  if(Q->nredundcol > 0)
		     lrs_clear_mp_matrix(Lin,Q->nredundcol,Q->n);
		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );
		inp->destroy(inp);


		return retMat;

	} else {

		//printf("Empty set.\n");
		retMat = id_zeros(0, inp->cols(inp));
		retMat->empty = 1;
		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );

		lrs_clear_mp_vector(num,inp->cols(inp));
		lrs_clear_mp_vector(den,inp->cols(inp));
		inp->destroy(inp);


		return retMat;
	}
}

idmatrix_t *reducevertices(idmatrix_t *inp) {
	lrs_dic *P;
	lrs_dat *Q;
	lrs_mp_matrix Lin;

	long i;

	size_t m = inp->rows(inp);

	assert( my_lrs_init () == TRUE );

	Q = lrs_alloc_dat ("LRS globals");
	assert ( Q != NULL );
	Q->m = m;
	Q->n = inp->cols(inp);
	Q->hull = TRUE;

	lrs_mp_vector num, den;
	num = lrs_alloc_mp_vector(inp->cols(inp));
	den = lrs_alloc_mp_vector(inp->cols(inp));

	P = lrs_alloc_dic (Q);
	assert ( P != NULL );

	idmatrix_t *retMat;

	struct GMPlist* retVal = GMPlist_create();
	inp->print(inp,stdout);
	for (i = 1; i <= m; ++i) {
		inp->getRow(inp,num, den,i-1);
		lrs_set_row_mp(P ,Q ,i ,num ,den , GE);
	}

	if ( lrs_getfirstbasis (&P, Q, &Lin, TRUE) ) {

		size_t lastdv = Q->lastdv;
		size_t d = P->d;
		size_t ineq;
		m = P->m_A;

		for (i = lastdv + 1; i <= m + d; ++i) {
			ineq = Q->inequality[i - lastdv] - 1;
			if (!checkindex(P, Q, i)) {
				retVal = retVal->push(retVal, inp->mpq_row_extract(inp, ineq));
			}
		}

		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );
		retMat = retVal->to_matrix(retVal, inp->cols(inp));
		inp->destroy(inp);

		return retMat;

	} else {

		//printf("Empty set.\n");
		retMat = id_zeros(0, inp->cols(inp));
		retMat->empty = 1;
		lrs_free_dic ( P , Q);
		lrs_free_dat ( Q );

		inp->destroy(inp);

		return retMat;

	}
}


#ifdef SIGNALS

/*
   If given a signal
   USR1            print current cobasis and continue
   TERM            print current cobasis and terminate
   INT (ctrl-C) ditto
   HUP                     ditto
 */
static void
setup_signals ()
{
  errcheck ("signal", signal (SIGTERM, die_gracefully));
  errcheck ("signal", signal (SIGALRM, timecheck));
  errcheck ("signal", signal (SIGHUP, die_gracefully));
  errcheck ("signal", signal (SIGINT, die_gracefully));
  errcheck ("signal", signal (SIGUSR1, checkpoint));
}

static void
timecheck ()
{
  lrs_dump_state ();
  errcheck ("signal", signal (SIGALRM, timecheck));
  alarm (lrs_checkpoint_seconds);
}

static void
checkpoint ()
{
  lrs_dump_state ();
  errcheck ("signal", signal (SIGUSR1, checkpoint));
}

static void
die_gracefully ()
{
  lrs_dump_state ();

  exit (1);
}

#endif

