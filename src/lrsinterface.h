/*
 * lrsinterface.h
 *
 *  Created on: Jan 18, 2019
 *      Author: rafael
 *
 *      Interfaces for lrs from Geometric calculation library,
 *      found in http://worc4021.github.io/
 */

#ifndef LRSINTERFACE_H_
#define LRSINTERFACE_H_

#include "gmplist.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Interface to the LRS library */

int my_lrs_init();
idmatrix_t *H2V(idmatrix_t *inp);
idmatrix_t *V2H(idmatrix_t *inp);
idmatrix_t *reducemat(idmatrix_t *inp);
idmatrix_t *reducevertices(idmatrix_t *inp);

#ifdef __cplusplus
}
#endif

#endif /* LRSINTERFACE_H_ */
