/*
 * map.c
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#include "map.h"

static idbool_t idmap_find(idrb_node_t *h_list,  void *h_key, idrb_node_t **hh_node_out) {
	int fnd;
	*hh_node_out = h_list->find_key_n(h_list,h_key,&fnd);
	return (fnd?TRUE:FALSE);
}

static void idmap_push(idrb_node_t *h_values_node,  void *p_value) {
	idlist_t *h_values = (idlist_t *)h_values_node->value(h_values_node);
	h_values->push(h_values,p_value);
}

static idrb_node_t *idmap_mk_node(idrb_node_t *h_values_at,  void *p_key, int (*cmp_value)(void *key1, void *key2)) {
	idlist_t *p_values = idlist_byref(cmp_value);
	return h_values_at->insert(h_values_at,p_key,p_values);
}
static idmap_t *idmap_default_constructor(void) {
	idmap_t *obj = malloc(sizeof(idmap_t));
	assert(obj!=NULL);

	obj->destroy = idmap_destroy;
	obj->cpy = idmap_cpy;
	obj->set = NULL;
	obj->set_values1 = NULL;
	obj->set_values2 = NULL;
	obj->remove_at = idmap_remove_at;
	obj->remove_value1 = idmap_remove_value1;
	obj->remove_value2 = idmap_remove_value2;
	obj->nvalues1 = idmap_nvalues1;
	obj->nvalues2 = idmap_nvalues2;
	obj->get_values1_at = idmap_get_values1_at;
	obj->get_values2_at = idmap_get_values2_at;
	obj->exists			= idmap_exists;

#ifdef IDRTL_DEBUG_MEMLEAK_MAP
	static int idmap_last_id = 0;
	obj->m_id = idmap_last_id;
	printf("debug memleak construct map %d\n", obj->m_id);
	idmap_last_id++;
	idmap_destroy_cnt++;
#endif
	obj->m_nvalues1 = 0;
	obj->m_nvalues2 = 0;
	obj->m_sizeof_value1 = 0;
	obj->m_sizeof_value2 = 0;
	obj->mh_cpy_value1 = NULL;
	obj->mh_cpy_value2 = NULL;
	obj->mp_values1_at = NULL;
	obj->mp_values2_at = NULL;

	return obj;
}


/* public functions */
idmap_t *     	idmap_byvalues(size_t sizeof_value1, int (*cmp_value1)(void *key1, void *key2),
		size_t sizeof_value2, int (*cmp_value2)(void *key1, void *key2)) {
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof_value1;
	obj->m_sizeof_value2 = sizeof_value2;
	obj->mh_cpy_value1 = NULL;
	obj->mh_cpy_value2 = NULL;
	obj->set = idmap_set_byvalues;
	obj->set_values1 = idmap_set_values1_byvalues;
	obj->set_values2 = idmap_set_values2_byvalues;
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,free,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,free,idlist_destroy);

	return obj;
}
idmap_t *     	idmap_byptrs(void (*destroy_value1)(void *value), int (*cmp_value1)(void *key1, void *key2),
		void* (*cpy_value1)(void *value), void (*destroy_value2)(void *value),
		int (*cmp_value2)(void *key1, void *key2), void* (*cpy_value2)(void *value)) {
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);
	assert(cpy_value1!=NULL);
	assert(cpy_value2!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof(void *);
	obj->m_sizeof_value2 = sizeof(void *);
	obj->mh_cpy_value1 = cpy_value1;
	obj->mh_cpy_value2 = cpy_value2;
	obj->set = idmap_set_byrefs;
	obj->set_values1 = idmap_set_values1_byrefs;
	obj->set_values2 = idmap_set_values2_byrefs;
	if (destroy_value1==NULL) {
		destroy_value1 = free;
	}
	if (destroy_value2==NULL) {
		destroy_value2 = free;
	}
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,destroy_value2,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,destroy_value1,idlist_destroy);

	return obj;
}
idmap_t *     	idmap_byrefs(int (*cmp_value1)(void *key1, void *key2), int (*cmp_value2)(void *key1, void *key2)) {
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof(void *);
	obj->m_sizeof_value2 = sizeof(void *);
	obj->mh_cpy_value1 = idref_cpy;
	obj->mh_cpy_value2 = idref_cpy;
	obj->set = idmap_set_byrefs;
	obj->set_values1 = idmap_set_values1_byrefs;
	obj->set_values2 = idmap_set_values2_byrefs;
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,NULL,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,NULL,idlist_destroy);

	return obj;
}
idmap_t *     	idmap_byvalue_and_ptr(size_t sizeof_value1, int (*cmp_value1)(void *key1, void *key2),
		void (*destroy_value2)(void *value), int (*cmp_value2)(void *key1, void *key2), void* (*cpy_value2)(void *value)){
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);
	assert(cpy_value2!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof_value1;
	obj->m_sizeof_value2 = sizeof(void *);
	obj->mh_cpy_value1 = NULL;
	obj->mh_cpy_value2 = cpy_value2;
	obj->set = idmap_set_byvalue_and_ref;
	obj->set_values1 = idmap_set_values1_byvalue_and_ref;
	obj->set_values2 = idmap_set_values2_byvalue_and_ref;
	if (destroy_value2==NULL) {
		destroy_value2 = free;
	}
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,destroy_value2,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,free,idlist_destroy);

	return obj;
}
idmap_t *     	idmap_byvalue_and_ref(size_t sizeof_value1,int (*cmp_value1)(void *key1, void *key2),
		int (*cmp_value2)(void *key1, void *key2)){
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof_value1;
	obj->m_sizeof_value2 = sizeof(void *);
	obj->mh_cpy_value1 = NULL;
	obj->mh_cpy_value2 = idref_cpy;
	obj->set = idmap_set_byvalue_and_ref;
	obj->set_values1 = idmap_set_values1_byvalue_and_ref;
	obj->set_values2 = idmap_set_values2_byvalue_and_ref;
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,NULL,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,free,idlist_destroy);

	return obj;
}
idmap_t *     	idmap_byptr_and_ref(void (*destroy_value1)(void *value), int (*cmp_value1)(void *key1, void *key2),
		void* (*cpy_value1)(void *value), int (*cmp_value2)(void *key1, void *key2)){
	assert(cmp_value1!=NULL);
	assert(cmp_value2!=NULL);
	assert(cpy_value1!=NULL);

	idmap_t *obj = idmap_default_constructor();

	obj->m_sizeof_value1 = sizeof(void *);
	obj->m_sizeof_value2 = sizeof(void *);
	obj->mh_cpy_value1 = cpy_value1;
	obj->mh_cpy_value2 = idref_cpy;
	obj->set = idmap_set_byrefs;
	obj->set_values1 = idmap_set_values1_byrefs;
	obj->set_values2 = idmap_set_values2_byrefs;
	if (destroy_value1==NULL) {
		destroy_value1 = free;
	}
	obj->mp_values1_at = idrb_with_custom_key(cmp_value2,NULL,idlist_destroy);
	obj->mp_values2_at = idrb_with_custom_key(cmp_value1,destroy_value1,idlist_destroy);

	return obj;
}
void             idmap_destroy(idmap_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_MAP
		idmap_destroy_cnt--;
		printf("debug memleak destroy map %d still remains %d to destroy\n", obj->m_id, idmap_destroy_cnt);
		if (idmap_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idrb_destroy(obj->mp_values1_at);
		idrb_destroy(obj->mp_values2_at);
		free(obj);
	}
}
idmap_t *     	idmap_cpy(idmap_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it, *it2, *h_values1_node, *h_values2_node;
	idlist_t *h_values1;
	idmap_t *p_map = idmap_default_constructor();
	void *p_value1, *h_value1, *p_value2, *h_value2;
	int fnd;

	p_map->m_sizeof_value1 = obj->m_sizeof_value1;
	p_map->m_sizeof_value2 = obj->m_sizeof_value2;
	p_map->mh_cpy_value1 = obj->mh_cpy_value1;
	p_map->mh_cpy_value2 = obj->mh_cpy_value2;
	p_map->set = obj->set;
	p_map->mp_values1_at = idrb_with_custom_key(obj->mp_values1_at->k.cmp,obj->mp_values1_at->k.destroy,idlist_destroy);
	p_map->mp_values2_at = idrb_with_custom_key(obj->mp_values2_at->k.cmp,obj->mp_values2_at->k.destroy,idlist_destroy);

	idrb_traverse(it, obj->mp_values1_at) {
		h_value2 = it->keyg(it);
		if (p_map->mh_cpy_value2!=NULL) {
			p_value2 = p_map->mh_cpy_value2(h_value2);
		} else {
			p_value2 = idmk_value(h_value2,p_map->m_sizeof_value2);
		}
		idmap_mk_node(p_map->mp_values1_at,p_value2,p_map->mp_values2_at->k.cmp);
	}
	idrb_traverse(it, obj->mp_values2_at) {
		h_value1 = it->keyg(it);
		if (p_map->mh_cpy_value1!=NULL) {
			p_value1 = p_map->mh_cpy_value1(h_value1);
		} else {
			p_value1 = idmk_value(h_value1,p_map->m_sizeof_value1);
		}
		idmap_mk_node(p_map->mp_values2_at,p_value1,p_map->mp_values1_at->k.cmp);
	}
	idrb_traverse(it, obj->mp_values1_at) {
		h_value2 = it->keyg(it);
		assert(idmap_find(p_map->mp_values1_at,h_value2,&h_values1_node));
		h_values1 = (idlist_t *)it->value(it);
		idrb_traverse(it2, h_values1->mp_head) {
			h_value1 = it2->value(it2);
			assert(idmap_find(p_map->mp_values2_at,h_value1,&h_values2_node));
			idmap_push(h_values1_node,h_values2_node->keyg(h_values2_node));
			idmap_push(h_values2_node,h_values1_node->keyg(h_values1_node));
		}
	}
	return p_map;
}

/* set functions */
static void idmap_set(idmap_t *obj, void *h_value1, void *h_value2, idbool_t is_value1_byref, idbool_t is_value2_byref) {
	assert(obj!=NULL);
	assert(obj->mp_values1_at!=NULL);
	assert(obj->mp_values2_at!=NULL);

	idrb_node_t *h_values1_node, *h_values2_node;
	void *p_value1, *p_value2;

	if (idmap_find(obj->mp_values1_at,h_value2,&h_values1_node)) {
		p_value2 = h_values1_node->keyg(h_values1_node);
		if (idmap_find(obj->mp_values2_at,h_value1,&h_values2_node)) {
			p_value1 = h_values2_node->keyg(h_values2_node);
			idmap_push(h_values1_node,p_value1);
			idmap_push(h_values2_node,p_value2);
		} else {
			obj->m_nvalues1++;
			if (is_value1_byref) {
				p_value1 = h_value1;
			} else {
				p_value1 = idmk_value(h_value1,obj->m_sizeof_value1);
			}
			h_values2_node = idmap_mk_node(obj->mp_values2_at,p_value1,obj->mp_values1_at->k.cmp);
			idmap_push(h_values1_node,p_value1);
			idmap_push(h_values2_node,p_value2);
		}
	} else {
		obj->m_nvalues2++;
		if (is_value2_byref) {
			p_value2 = h_value2;
		} else {
			p_value2 = idmk_value(h_value2,obj->m_sizeof_value2);
		}
		h_values1_node = idmap_mk_node(obj->mp_values1_at,p_value2,obj->mp_values2_at->k.cmp);
		if (idmap_find(obj->mp_values2_at,h_value1,&h_values2_node)) {
			p_value1 = h_values2_node->keyg(h_values2_node);
			idmap_push(h_values1_node,p_value1);
			idmap_push(h_values2_node,p_value2);
		} else {
			obj->m_nvalues1++;
			if (is_value1_byref) {
				p_value1 = h_value1;
			} else {
				p_value1 = idmk_value(h_value1,obj->m_sizeof_value1);
			}
			h_values2_node = idmap_mk_node(obj->mp_values2_at,p_value1,obj->mp_values1_at->k.cmp);
			idmap_push(h_values1_node,p_value1);
			idmap_push(h_values2_node,p_value2);
		}
	}
}


void           	idmap_set_byrefs(idmap_t *obj, 			void *h_value1, void *h_value2) {
	assert(obj!=NULL);
	idmap_set(obj,h_value1,h_value2,TRUE,TRUE);
}
void           	idmap_set_byvalues(idmap_t *obj, 		void *h_value1, void *h_value2){
	assert(obj!=NULL);
	idmap_set(obj,h_value1,h_value2,FALSE,FALSE);
}
void           	idmap_set_byvalue_and_ref(idmap_t *obj,	void *h_value1, void *h_value2){
	assert(obj!=NULL);
	idmap_set(obj,h_value1,h_value2,FALSE,TRUE);
}

static void idmap_set_values(idrb_node_t *h_keys_at, idrb_node_t *h_values_at, int *h_nkeys, int *h_nvalues,
		idbool_t is_key_byref, void *h_key, size_t sizeof_key,
		idbool_t is_value_byref, idlist_t *p_values, size_t sizeof_value) {

	assert(h_keys_at!=NULL);
	assert(h_values_at!=NULL);
	assert(h_nkeys!=NULL);
	assert(h_nvalues!=NULL);
	assert(h_key!=NULL);
	assert(p_values!=NULL);


	idrb_node_t *it, *h_values_node, *h_keys_node;
	void *p_key, *p_value, *h_value;

	if (idmap_find(h_values_at,h_key,&h_values_node)) {
		p_key = h_values_node->keyg(h_values_node);
		idrb_traverse(it,p_values->mp_head) {
			h_value = it->value(it);
			if (idmap_find(h_keys_at,h_value,&h_keys_node)) {
				p_value = h_keys_node->keyg(h_keys_node);
				idmap_push(h_values_node,p_value);
				idmap_push(h_keys_node,p_key);
			} else {
				(*h_nvalues)++;
				if (is_value_byref) {
					p_value = h_value;
				} else {
					p_value = idmk_value(h_value,sizeof_value);
				}
				h_keys_node = idmap_mk_node(h_keys_at,p_value,h_values_at->k.cmp);
				idmap_push(h_values_node,p_value);
				idmap_push(h_keys_node,p_key);
			}
		}
		p_values->destroy(p_values);
	} else {
		(*h_nkeys)++;
		if (is_key_byref) {
			p_key = h_key;
		} else {
			p_key = idmk_value(h_key,sizeof_key);
		}
		h_values_node = h_values_at->insert(h_values_at,p_key,p_values);
		idrb_traverse(it,p_values->mp_head) {
			h_value = it->value(it);
			if (idmap_find(h_keys_at,h_value,&h_keys_node)) {
				p_value = h_keys_node->keyg(h_keys_node);
				idmap_push(h_keys_node,p_key);
			} else {
				(*h_nvalues)++;
				if (is_value_byref) {
					p_value = h_value;
				} else {
					p_value = idmk_value(h_value,sizeof_value);
				}
				h_keys_node = idmap_mk_node(h_keys_at,p_value,h_values_at->k.cmp);
				idmap_push(h_keys_node,p_key);
			}
		}
	}
}
void           	idmap_set_values1_byrefs(idmap_t *obj, 			idlist_t *p_values1, void *h_value2) {
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values2_at, obj->mp_values1_at, &obj->m_nvalues2, &obj->m_nvalues1,
			TRUE, h_value2, obj->m_sizeof_value2,
			TRUE, p_values1, obj->m_sizeof_value1
			);
}

void           	idmap_set_values1_byvalues(idmap_t *obj, 		idlist_t *p_values1, void *h_value2){
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values2_at, obj->mp_values1_at, &obj->m_nvalues2, &obj->m_nvalues1,
			FALSE, h_value2, obj->m_sizeof_value2,
			FALSE, p_values1, obj->m_sizeof_value1
			);
}
void           	idmap_set_values1_byvalue_and_ref(idmap_t *obj,	idlist_t *p_values1, void *h_value2) {
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values2_at, obj->mp_values1_at, &obj->m_nvalues2, &obj->m_nvalues1,
			TRUE, h_value2, obj->m_sizeof_value2,
			FALSE, p_values1, obj->m_sizeof_value1
			);
}
void           	idmap_set_values2_byrefs(idmap_t *obj, 			void *h_value1, idlist_t *p_values2) {
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values1_at, obj->mp_values2_at, &obj->m_nvalues1, &obj->m_nvalues2,
			TRUE, h_value1, obj->m_sizeof_value1,
			TRUE, p_values2, obj->m_sizeof_value2
			);
}

void           	idmap_set_values2_byvalues(idmap_t *obj, 		void *h_value1, idlist_t *p_values2){
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values1_at, obj->mp_values2_at, &obj->m_nvalues1, &obj->m_nvalues2,
			FALSE, h_value1, obj->m_sizeof_value1,
			FALSE, p_values2, obj->m_sizeof_value2
			);
}
void           	idmap_set_values2_byvalue_and_ref(idmap_t *obj,	void *h_value1, idlist_t *p_values2) {
	assert(obj!=NULL);

	idmap_set_values(
			obj->mp_values1_at, obj->mp_values2_at, &obj->m_nvalues1, &obj->m_nvalues2,
			FALSE, h_value1, obj->m_sizeof_value1,
			TRUE, p_values2, obj->m_sizeof_value2
			);
}

void         	idmap_remove_at(idmap_t *obj, 			void *h_value1, void *h_value2) {
	assert(obj!=NULL);
	assert(obj->mp_values1_at!=NULL);
	assert(obj->mp_values2_at!=NULL);

	int fnd1, fnd2;
	idrb_node_t	*h_node1 = obj->mp_values1_at->find_key_n(obj->mp_values1_at,h_value2,&fnd1),
				*h_node2 = obj->mp_values2_at->find_key_n(obj->mp_values2_at,h_value1,&fnd2);
	idlist_t *h_values1,*h_values2;

	if (fnd1) {
		h_values1 = (idlist_t *)h_node1->value(h_node1);
		h_values1->remove(h_values1,h_value1);
	}
	if (fnd2) {
		h_values1 = (idlist_t *)h_node1->value(h_node1);
		h_values1->remove(h_values1,h_value2);
	}
}
void         	idmap_remove_value1(idmap_t *obj, 				void *h_value1) {
	assert(obj!=NULL);
	assert(obj->mp_values1_at!=NULL);
	assert(obj->mp_values2_at!=NULL);

	int fnd;
	idrb_node_t	*it, *h_node1,
				*h_node2 = obj->mp_values2_at->find_key_n(obj->mp_values2_at,h_value1,&fnd);
	idlist_t *h_values1, *h_values2;
	void *h_value2;

	if (fnd) {
		h_values2 = (idlist_t *)h_node2->value(h_node2);
		idrb_traverse(it,h_values2->mp_head) {
			h_value2 = it->value(it);
			h_node1 = obj->mp_values1_at->find_key_n(obj->mp_values1_at,h_value2,&fnd);
			h_values1 = (idlist_t *)h_node1->value(h_node1);
			h_values1->remove(h_values1,h_value1);
		}
		idrb_delete_node(h_node2);
	}
}
void         	idmap_remove_value2(idmap_t *obj, 				void *h_value2) {
	assert(obj!=NULL);
	assert(obj->mp_values1_at!=NULL);
	assert(obj->mp_values2_at!=NULL);

	int fnd;
	idrb_node_t	*it, *h_node2,
				*h_node1 = obj->mp_values1_at->find_key_n(obj->mp_values1_at,h_value2,&fnd);
	idlist_t *h_values1, *h_values2;
	void *h_value1;

	if (fnd) {
		h_values1 = (idlist_t *)h_node1->value(h_node1);
		idrb_traverse(it,h_values1->mp_head) {
			h_value1 = it->value(it);
			h_node2 = obj->mp_values2_at->find_key_n(obj->mp_values2_at,h_value1,&fnd);
			h_values2 = (idlist_t *)h_node2->value(h_node2);
			h_values2->remove(h_values2,h_value2);
		}
		idrb_delete_node(h_node1);
	}
}

int           	idmap_nvalues1(idmap_t *obj) {
	assert(obj!=NULL);
	return obj->m_nvalues1;
}
int           	idmap_nvalues2(idmap_t *obj){
	assert(obj!=NULL);
	return obj->m_nvalues2;
}
idlist_t *    idmap_get_values1_at(idmap_t *obj, void *h_value2) {
	assert(obj!=NULL);
	assert(obj->mp_values1_at!=NULL);

	int fnd;
	idrb_node_t	*h_node = obj->mp_values1_at->find_key_n(obj->mp_values1_at,h_value2,&fnd);
	idlist_t *h_values1;

	if (fnd) {
		h_values1 = (idlist_t *)h_node->value(h_node);
		return h_values1;
	}

	return NULL;
}
idlist_t *    idmap_get_values2_at(idmap_t *obj, void *h_value1){
	assert(obj!=NULL);
	assert(obj->mp_values2_at!=NULL);

	int fnd;
	idrb_node_t	*h_node = obj->mp_values2_at->find_key_n(obj->mp_values2_at,h_value1,&fnd);
	idlist_t *h_values2;

	if (fnd) {
		h_values2 = (idlist_t *)h_node->value(h_node);
		return h_values2;
	}

	return NULL;
}
idbool_t		idmap_exists(idmap_t *obj, void *p_value1, void *p_value2) {
	assert(obj!=NULL);

	idlist_t *h_values2 = obj->get_values2_at(obj,p_value1);

	if (h_values2!=NULL) {
		return h_values2->exists(h_values2,p_value2);
	}
	return FALSE;
}
void		idintmap_print(idmap_t *obj, FILE* out) {
	assert(obj!=NULL);

	idrb_node_t *it;
	idlist_t *h_values2;
	int value1;
	char str[80];


	idrb_traverse(it,obj->mp_values2_at) {
		h_values2 = (idlist_t *)it->value(it);
		value1 = *(int *)it->keyg(it);
		fprintf(out,"[%d] = %s\n", value1, idintlist_to_str(h_values2,str));
	}
}
extern int *	idintmap_get_first_value1_from_value2(idmap_t *obj, int value2) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t	*h_node = obj->mp_values1_at->find_key_n(obj->mp_values1_at,&value2,&fnd);
	if (!fnd) {
		return NULL;
	}
	idlist_t *h_bits = (idlist_t *)h_node->value(h_node);
	idrb_node_t *h_bit = idrb_first(h_bits->mp_head);
	if (h_bit==idrb_nil(h_bits->mp_head)) {
		return NULL;
	}
	return (int *)h_bit->value(h_bit);
}
extern int	*	idintmap_get_first_value2_from_value1(idmap_t *obj, int value1) {
	assert(obj!=NULL);

	int fnd;
	idrb_node_t	*h_node = obj->mp_values2_at->find_key_n(obj->mp_values2_at,&value1,&fnd);
	if (!fnd) {
		return NULL;
	}
	idlist_t *h_bits = (idlist_t *)h_node->value(h_node);
	idrb_node_t *h_bit = idrb_first(h_bits->mp_head);
	if (h_bit==idrb_nil(h_bits->mp_head)) {
		return NULL;
	}
	return (int *)h_bit->value(h_bit);
}
