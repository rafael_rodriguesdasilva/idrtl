/*
 * map.h
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#ifndef SOLVER_MAP_H_
#define SOLVER_MAP_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idmap_t *     	idmap_byvalues(size_t sizeof_value1, int (*cmp_value1)(void *key1, void *key2),
		size_t sizeof_value2, int (*cmp_value2)(void *key1, void *key2));
extern idmap_t *     	idmap_byptrs(void (*destroy_value1)(void *value), int (*cmp_value1)(void *key1, void *key2),
		void* (*cpy_value1)(void *value), void (*destroy_value2)(void *value),
		int (*cmp_value2)(void *key1, void *key2), void* (*cpy_value2)(void *value));
extern idmap_t *     	idmap_byrefs(int (*cmp_value1)(void *key1, void *key2), int (*cmp_value2)(void *key1, void *key2));
extern idmap_t *     	idmap_byvalue_and_ptr(size_t sizeof_value1, int (*cmp_value1)(void *key1, void *key2),
		void (*destroy_value2)(void *value), int (*cmp_value2)(void *key1, void *key2), void* (*cpy_value2)(void *value));
extern idmap_t *     	idmap_byvalue_and_ref(size_t sizeof_value1,int (*cmp_value1)(void *key1, void *key2),
		int (*cmp_value2)(void *key1, void *key2));
extern idmap_t *     	idmap_byptr_and_ref(void (*destroy_value1)(void *value), int (*cmp_value1)(void *key1, void *key2),
		void* (*cpy_value1)(void *value), int (*cmp_value2)(void *key1, void *key2));
extern void             idmap_destroy(idmap_t *obj);
extern idmap_t *     	idmap_cpy(idmap_t *obj);

/* set functions */
extern void            	idmap_set_byrefs(idmap_t *obj, 			void *p_value1, void *p_value2);
extern void            	idmap_set_byvalues(idmap_t *obj, 		void *h_value1, void *h_value2);
extern void            	idmap_set_byvalue_and_ref(idmap_t *obj,	void *h_value1, void *p_value2);
extern void            	idmap_set_values1_byrefs(idmap_t *obj, 			idlist_t *p_values1, void *p_value2);
extern void            	idmap_set_values1_byvalues(idmap_t *obj, 		idlist_t *h_values1, void *p_value2);
extern void            	idmap_set_values1_byvalue_and_ref(idmap_t *obj,	idlist_t *h_values1, void *p_value2);
extern void            	idmap_set_values2_byrefs(idmap_t *obj, 			void *p_value1, idlist_t *p_values2);
extern void            	idmap_set_values2_byvalues(idmap_t *obj, 		void *h_value1, idlist_t *p_values2);
extern void            	idmap_set_values2_byvalue_and_ref(idmap_t *obj,	void *h_value1, idlist_t *p_values2);
extern void         	idmap_remove_at(idmap_t *obj, 			void *h_value1, void *h_value2);
extern void         	idmap_remove_value1(idmap_t *obj, 				void *h_value1);
extern void         	idmap_remove_value2(idmap_t *obj, 				void *h_value2);

/* get functions */
extern int           	idmap_nvalues1(idmap_t *obj);
extern int           	idmap_nvalues2(idmap_t *obj);
extern idlist_t *    idmap_get_values1_at(idmap_t *obj, void *h_value2);
extern idlist_t *    idmap_get_values2_at(idmap_t *obj, void *h_value1);
extern idbool_t		idmap_exists(idmap_t *obj, void *p_value1, void *p_value2);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_MAP
	static int		    idmap_destroy_cnt = 0;
#endif

#define idintmap(void) idmap_byvalues(sizeof(int),idintcmpfunc_,sizeof(int),idintcmpfunc_);
extern void		idintmap_print(idmap_t *obj, FILE* out);
extern int	*	idintmap_get_first_value1_from_value2(idmap_t *obj, int value2);
extern int	*	idintmap_get_first_value2_from_value1(idmap_t *obj, int value1);

struct idmap_s{
	/* capsulated member functions */
	void       	(*destroy)(idmap_t *obj);
	idmap_t *	(*cpy)(idmap_t *obj);

	/* set functions */
	void       	(*set)(idmap_t *obj, void *p_value1, void *p_value2);
	void       	(*set_values1)(idmap_t *obj, idlist_t *p_values1, void *p_value2);
	void       	(*set_values2)(idmap_t *obj, void *p_value1, idlist_t *p_values2);
	void  		(*remove_at)(idmap_t *obj, void *p_value1, void *p_value2);
	void  		(*remove_value1)(idmap_t *obj, void *p_value1);
	void  		(*remove_value2)(idmap_t *obj, void *p_value2);

	/* get functions */
	int      	(*nvalues1)(idmap_t *obj);
	int      	(*nvalues2)(idmap_t *obj);
	idlist_t * 		(*get_values1_at)(idmap_t *obj, void *h_value2);
	idlist_t *	   	(*get_values2_at)(idmap_t *obj, void *h_value1);
	idbool_t		(*exists)(idmap_t *obj, void *p_value1, void *p_value2);
	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_MAP
	int		      m_id;
#endif
	idrb_node_t 	*mp_values1_at,
					*mp_values2_at;
	int    			m_nvalues1,
					m_nvalues2;
	size_t			m_sizeof_value1,
					m_sizeof_value2;
	void          	*(*mh_cpy_value1)(void *value),
					*(*mh_cpy_value2)(void *value);
};


#ifdef __cplusplus
}
#endif


#endif /* SOLVER_MAP_H_ */
