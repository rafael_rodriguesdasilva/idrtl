/*
 * polytope.c
 *
 *  Created on: Feb 11, 2019
 *      Author: rafael
 */


#include "idrtl.h"

//#define IDPOLYTOPE_DEBUG_DIFF
//#define IDPOLYTOPE_DEBUG_DIFF_L2
//#define IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
//#define IDPOLYTOPE_DEBUG_CONV
//#define IDPOLYTOPE_DEBUG_CONV_L2


/* private functions */
static idpolytope_t *  idpolytope(int dim) {
	idpolytope_t *obj = malloc(sizeof(idpolytope_t));
	assert(obj!=NULL);

	static int my_last_id = 0;

	/* capsulated member functions */
	/* operations which preserves inputs */
	obj->cpy = idpolytope_cpy;
	obj->chebyradius = idpolytope_chebyradius;
	obj->is_adjacent_to = idpolytope_is_adjacent_to;
	obj->isinside = idpolytope_isinside;
	obj->isinside_dblcolvector = idpolytope_isinside_dblcolvector;
	obj->isfulldim = idpolytope_isfulldim;
	obj->isempty = idpolytope_isempty;
	obj->and_with_predicate = idpolytope_and_with_predicate;
	obj->and_with_polytope = idpolytope_and_with_polytope;
	obj->or_with_predicate = idpolytope_or_with_predicate;
	obj->or_with_polytope = idpolytope_or_with_polytope;
	obj->difference = idpolytope_difference;
	obj->convhull_with_polytope = idpolytope_convhull_with_polytope;
	obj->get_halfspace = idpolytope_get_halfspace;
	obj->is_equal_to = idpolytope_is_equal_to;
	obj->get_vertices = idpolytope_get_vertices;
	obj->get_x_constr = idpolytope_get_x_constr;


	/* operations which destroys inputs */
	obj->destroy = idpolytope_destroy;

	/* get functions */
	obj->print = idpolytope_print;
	obj->find_predicate_index = idpolytope_find_predicate_index;


	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_POLYTOPE
	static int idpolytope_last_id = 0;
	obj->m_id = idpolytope_last_id;
	printf("debug memleak construct polytope %d\n", obj->m_id);
	idpolytope_destroy_cnt++;
	idpolytope_last_id++;
#endif
	obj->m_dim = dim;
	obj->m_rows = 0;
	obj->mp_A = NULL;
	obj->mp_b = NULL;
	obj->m_rcheby = 0.0;
	obj->mp_V = NULL;
	obj->m_rcheby_isinitialized = FALSE;
	obj->mp_x_constr = NULL;

	return obj;
}

double idpolytope_evaluate_rcheby(idmatrix_t *h_A, idmatrix_t *h_b) {
	assert(h_A!=NULL);
	assert(h_b!=NULL);

	int n, m, r, c, optimstatus;
	double rcheby;

	n = h_A->cols(h_A);
	m = h_A->rows(h_A);
	assert(h_b->rows(h_b)==m);
	assert(h_b->cols(h_b)==1);

	int *p_ind, ind[n+1], numnz;
	double *p_val, val[n+1], rhs;

	GRBmodel 	*p_model = grb_mk_model();
	for(c=0;c<n;c++) {
		assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);
	}
	assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, -1.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);

	for(r=0;r<m;r++) {
		h_A->to_sparse_rowvector(h_A,r,&numnz,&p_ind,&p_val);
		memcpy(ind,p_ind,numnz*sizeof(int));
		memcpy(val,p_val,numnz*sizeof(double));
		ind[numnz] = n;
		val[numnz] = 1.0;
		numnz++;
		rhs = h_b->getat(h_b,r,0);
		assert(idGRB_NoError(GRBaddconstr(p_model, numnz, ind, val, GRB_LESS_EQUAL, rhs, NULL))==0);
		free(p_ind);
		free(p_val);
	}

	optimstatus = grb_optimize(p_model, &rcheby);
	assert(optimstatus == GRB_OPTIMAL);

	GRBfreemodel(p_model);
	return -rcheby;
}

static idmatrix_t *idpolytope_compute_vertices(idpolytope_t *obj) {
	idmatrix_t *p_H = obj->mp_b->cpy(obj->mp_b);
	p_H = p_H->stackHoriz(p_H,obj->mp_A->cpy(obj->mp_A));

	idmatrix_t *p_V = H2V(p_H);

	idmatrix_t *p_i = p_V->VertBreakdown(p_V);
	p_i->destroy(p_i);

	return p_V;
}

/* constructors */
idpolytope_t *  idpolytope_from_cs(idcsys_t *h_constr) {
	assert(h_constr!=NULL);

	idmatrix_t *p_A, *p_b;
	h_constr->get_halfspace(h_constr,&p_A,&p_b);

	return H_idpolytope(p_A,p_b);
}

idpolytope_t *  H_idpolytope(idmatrix_t *p_A, idmatrix_t *p_b) {
	assert(p_A!=NULL);
	assert(p_b!=NULL);
	assert(p_A->rows(p_A)==p_b->rows(p_b));

	idpolytope_t *obj = idpolytope(p_A->cols(p_A));
	idmatrix_t *p_Ar = reducemat(p_b->stackHoriz(p_b,p_A));
	idmatrix_t *p_br = p_Ar->VertBreakdown(p_Ar);
	obj->mp_x_constr = id_mk_csys_from_inequalities(p_Ar,p_br);
	obj->mp_x_constr->get_halfspace(obj->mp_x_constr,&obj->mp_A,&obj->mp_b);
	obj->m_rows = obj->mp_x_constr->m_nconstrs;
	p_Ar->destroy(p_Ar);
	p_br->destroy(p_br);

	return obj;
}
idpolytope_t *  V_idpolytope(idmatrix_t *V) {
	assert(V!=NULL);
	idpolytope_t *obj = idpolytope(V->cols(V));

	double *ind, *val, v;
	int i, j, numnz;
	idmatrix_t *p_A, *p_b;

	obj->mp_V = reducevertices(V); // V is destroyed here

	obj->mp_A = reducemat(V2H(obj->mp_V->cpy(obj->mp_V)));
	obj->mp_b = obj->mp_A->VertBreakdown(obj->mp_A);
	obj->m_rows = obj->mp_A->rows(obj->mp_A);
	obj->mp_x_constr = id_mk_csys_from_inequalities(obj->mp_A,obj->mp_b);

	return obj;
}

/* operations which preserves inputs */
idpolytope_t *  idpolytope_cpy(idpolytope_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_A!=NULL);
	assert(obj->mp_b!=NULL);

	int i;
	idpolytope_t *P = idpolytope(obj->m_dim);
	idrb_node_t *it;
	idpredicate_t *p_pi_new, *h_pi_old;

	P->m_rows = obj->m_rows;
	P->mp_A = obj->mp_A->cpy(obj->mp_A);
	P->mp_b = obj->mp_b->cpy(obj->mp_b);
	P->mp_x_constr = obj->mp_x_constr->cpy(obj->mp_x_constr);
	if (obj->mp_V!=NULL) {
		P->mp_V = obj->mp_V->cpy(obj->mp_V);
	}
	if (obj->m_rcheby_isinitialized == TRUE) {
		P->m_rcheby = obj->m_rcheby;
		P->m_rcheby_isinitialized = TRUE;
	}

	return P;
}
double          idpolytope_chebyradius(idpolytope_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_A!=NULL);
	assert(obj->mp_b!=NULL);


	if (obj->m_rcheby_isinitialized == FALSE) {
		obj->m_rcheby_isinitialized = TRUE;
		obj->m_rcheby = idpolytope_evaluate_rcheby(obj->mp_A, obj->mp_b);
	}

	return obj->m_rcheby;
}

static idbool_t idpolytope_get_adjacent_facets(idpolytope_t *h_poly1, idpolytope_t *h_poly2,
		int *h_adjacent_facet_poly1, int *h_adjacent_facet_poly2) {
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);
	assert(h_adjacent_facet_poly1!=NULL);
	assert(h_adjacent_facet_poly2!=NULL);

	int pi1_idx, pi2_idx,adjacent_facet_poly1, adjacent_facet_poly2;
	idpredicate_t *h_pi1, *h_pi2, *p_pi;
	idcsys_t *h_cs1, *h_cs2;
	idrb_node_t *it;

	adjacent_facet_poly1 = -1;
	adjacent_facet_poly2 = -1;

#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug idpolytope_get_adjacent_facets: intersection is NOT full dimensional\n");
#endif
	h_cs1 = h_poly1->mp_x_constr;
	h_cs2 = h_poly2->mp_x_constr;
	idrb_traverse(it,h_cs1->mp_row_pi->mp_values1_at) {
		h_pi1 = (idpredicate_t *)it->keyg(it);
		pi1_idx = *(int *)idlist_first((idlist_t *)it->value(it));
		p_pi = h_pi1->cpy(h_pi1);
		p_pi->neg(p_pi);
		h_pi2 = h_cs2->find(h_cs2,p_pi,&pi2_idx);
		if ((h_pi2!=NULL)&&(adjacent_facet_poly1<0)) {
			adjacent_facet_poly1 = pi1_idx;
			adjacent_facet_poly2 = pi2_idx;
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
			printf("debug idpolytope_get_adjacent_facets: found opposing facets %d and %d\n", adjacent_facet_poly1,
																				   adjacent_facet_poly2);
			printf("debug is_adjacent_to: found opposing facet %d =\n", adjacent_facet_poly1);
				h_pi1->print(h_pi1,stdout);
			printf("debug is_adjacent_to: found opposing facet %d =\n", adjacent_facet_poly2);
				h_pi2->print(h_pi2,stdout);
			printf("debug is_adjacent_to: which is equal to \n");
				p_pi->print(p_pi,stdout);
#endif
			p_pi->destroy(p_pi);
		} else if (h_pi2!=NULL) {
			p_pi->destroy(p_pi);
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
				printf("debug idpolytope_get_adjacent_facets: found additional opposing facets %d and %d; thus, they are not adjacent.\n",
							pi1_idx, pi2_idx);
#endif
			return FALSE;
		}
	}

	if (adjacent_facet_poly1<0) {
		assert(adjacent_facet_poly2<0);
		return FALSE;
	}
	*h_adjacent_facet_poly1 = adjacent_facet_poly1;
	*h_adjacent_facet_poly2 = adjacent_facet_poly2;
	return TRUE;

}

static idpolytope_t *idpolytope_mk_intersection_without_adjacent_facets(idpolytope_t *h_poly1, int adjacent_facet1,
		idpolytope_t *h_poly2, int adjacent_facet2) {
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);

	int facet, offset, varidx;
	idmatrix_t *p_A, *p_b;
	idpolytope_t *p_inter;
	p_A = id_zeros(h_poly1->m_rows+h_poly2->m_rows-2,h_poly1->m_dim);
	p_b = id_zeros(h_poly1->m_rows+h_poly2->m_rows-2,1);

#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug mk_intersection_without_adjacent_facets: adding poly1 predicates except at %d\n", adjacent_facet1);
#endif
	for(facet=0;facet<h_poly1->m_rows;facet++) {
		if (facet<adjacent_facet1) {
			p_b->setat(p_b,facet,0,h_poly1->mp_b->getat(h_poly1->mp_b,facet,0));
			for(varidx=0;varidx<h_poly1->m_dim;varidx++) {
				p_A->setat(p_A,facet,varidx,h_poly1->mp_A->getat(h_poly1->mp_A,facet,varidx));
			}
		} else if (adjacent_facet1<facet) {
			p_b->setat(p_b,facet-1,0,h_poly1->mp_b->getat(h_poly1->mp_b,facet,0));
			for(varidx=0;varidx<h_poly1->m_dim;varidx++) {
				p_A->setat(p_A,facet-1,varidx,h_poly1->mp_A->getat(h_poly1->mp_A,facet,varidx));
			}
		}
	}
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug mk_intersection_without_adjacent_facets: adding poly2 predicates except at %d\n", adjacent_facet2);
#endif
	offset = h_poly1->m_rows-1;
	for(facet=0;facet<h_poly2->m_rows;facet++) {
		if (facet<adjacent_facet2) {
			p_b->setat(p_b,facet+offset,0,h_poly2->mp_b->getat(h_poly2->mp_b,facet,0));
			for(varidx=0;varidx<h_poly2->m_dim;varidx++) {
				p_A->setat(p_A,facet+offset,varidx,h_poly2->mp_A->getat(h_poly2->mp_A,facet,varidx));
			}
		} else if (adjacent_facet2<facet) {
			p_b->setat(p_b,facet+offset-1,0,h_poly2->mp_b->getat(h_poly2->mp_b,facet,0));
			for(varidx=0;varidx<h_poly2->m_dim;varidx++) {
				p_A->setat(p_A,facet+offset-1,varidx,h_poly2->mp_A->getat(h_poly2->mp_A,facet,varidx));
			}
		}
	}

#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug mk_intersection_without_adjacent_facets: h_poly1 = \n");
	h_poly1->print(h_poly1,stdout);
	printf("debug mk_intersection_without_adjacent_facets: h_poly2 = \n");
	h_poly2->print(h_poly2,stdout);
	printf("debug mk_intersection_without_adjacent_facets: A = \n");
	p_A->print(p_A,stdout);
	printf("debug mk_intersection_without_adjacent_facets: b = \n");
	p_b->print(p_b,stdout);
#endif

	p_inter = H_idpolytope(p_A,p_b); // h_poly1/adjacent_facet_poly1 cap h_poly2/adjacent_facet_poly2
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug mk_intersection_without_adjacent_facets: h_poly1/adjacent_facet_poly1 cap h_poly2/adjacent_facet_poly2 = \n");
	p_inter->print(p_inter,stdout);
#endif

	return p_inter;
}

// return NULL if does not exists a convex union
static idpolytope_t *idpolytope_convex_union(idpolytope_t *h_poly1, idpolytope_t *h_poly2) {
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	idrb_node_t *it;
	printf("debug idpolytope_convex_union: h_poly1 = \n");
	h_poly1->print(h_poly1,stdout);
	printf("debug idpolytope_convex_union: h_poly2 = \n");
	h_poly2->print(h_poly2,stdout);
#endif

	idpolytope_t *p_conv, *p_union;
	idarray_t *p_diff;
	// Proposition: a disjunction of two polytopes P1 and P2 is a single polytope conv(P1 cup P2)
	// (i.e., convex disjunction) if and only if conv(P1 cup P2) - P1 = P2.

	p_union = NULL;
	p_conv = h_poly1->convhull_with_polytope(h_poly1,h_poly2);
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug idpolytope_convex_union: conv(P1 cup P2) = \n");
	p_conv->print(p_conv,stdout);
#endif


	p_diff = p_conv->difference(p_conv,h_poly1); // conv(P1 cup P2) - P1
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug idpolytope_convex_union: conv(P1 cup P2) - h_poly1 = \n");
	idrb_traverse(it,p_diff->mp_head) {
		printf("debug idpolytope_convex_union: P[%d] = \n", it->keyi(it));
		idpolytope_print((idpolytope_t *)it->value(it),stdout);
	}
#endif
	if ((p_diff->length(p_diff)==1)&&
			(h_poly2->is_equal_to(h_poly2,(idpolytope_t *)idrb_value(idrb_first(p_diff->mp_head))))){
		p_union = p_conv;
	} else {
		p_conv->destroy(p_conv);
	}
	p_diff->destroy(p_diff);
	return p_union;
}

idbool_t        idpolytope_is_adjacent_to(idpolytope_t *h_poly1, idpolytope_t *h_poly2,
		idpolytope_t *h_intersection, int *h_adjacent_facet_1, int *h_adjacent_facet_2,
		idpolytope_t **hp_convex_disjunction) {
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
	printf("debug is_adjacent_to: starting...\n");
	printf("debug is_adjacent_to: poly1 = \n");
	h_poly1->print(h_poly1,stdout);
	printf("debug is_adjacent_to: poly2 = \n");
	h_poly2->print(h_poly2,stdout);
	printf("debug is_adjacent_to: intersection = \n");
	h_intersection->print(h_intersection,stdout);
#endif


	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);

	// Definition: Two polytopes P1, P2 are adjacent if and only if
	// 1) the intersection between P1 and P2 is NOT full dimensional,
	// 2) there exists a single adjacent facet adj1 for P1 and adj2 for P2,
	// 3) the intersection P1/adj1 and P2/adj2 is full dimensional.

	idarray_t *p_diff;
	idpredicate_t *h_pi, *h_pi1, *h_pi2, *p_pi, *h_pi_adj1, *h_pi_adj2;
	int facet, adjacent_facet_poly1, adjacent_facet_poly2,
			varidx, offset, pi1_idx, pi2_idx;
	idmatrix_t *p_A, *p_b;
	idpolytope_t *p_poly1, *p_poly2, *p_inter, *p_conv;



	if ((h_adjacent_facet_1!=NULL)&&(h_adjacent_facet_2!=NULL)) {
		(*h_adjacent_facet_1) = -1;
		(*h_adjacent_facet_2) = -1;
	}
	if (hp_convex_disjunction!=NULL) {
		(*hp_convex_disjunction) = NULL;
	}
	adjacent_facet_poly1 = -1;
	adjacent_facet_poly2 = -1;

	if ((!h_intersection->isfulldim(h_intersection))) { // condition (1)
		// Lemma: A (dim-1) dimensional polytope has reduced H-representation with 1 hyperplane.
		// Proof: This trivially follows from the projection of convex polytopes.
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
		printf("debug is_adjacent_to: intersection is NOT full dimensional\n");
#endif

		if (idpolytope_get_adjacent_facets(h_poly1,h_poly2,&adjacent_facet_poly1,&adjacent_facet_poly2)) { // condition (2)
			assert(adjacent_facet_poly1>=0);
			assert(adjacent_facet_poly2>=0);

			p_inter = idpolytope_mk_intersection_without_adjacent_facets(h_poly1,adjacent_facet_poly1,
					h_poly2,adjacent_facet_poly2);

			if (p_inter->isfulldim(p_inter)) { // condition (3)
				if (hp_convex_disjunction!=NULL) {
	#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
					printf("debug is_adjacent_to: check if exists a convex disjunction\n");
	#endif
					(*hp_convex_disjunction) = idpolytope_convex_union(h_poly1,h_poly2);
				}
				if (h_adjacent_facet_1!=NULL) {
					(*h_adjacent_facet_1) = adjacent_facet_poly1;
				}
				if (h_adjacent_facet_2!=NULL) {
					(*h_adjacent_facet_2) = adjacent_facet_poly2;
				}
#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
				printf("debug is_adjacent_to: they are adjacent. END\n");
#endif
				p_inter->destroy(p_inter);
				return TRUE;
			} // end condition (3)
		} // end condition (2)
	} // end condition (1)

#ifdef IDPOLYTOPE_DEBUG_IS_ADJACENT_TO
		printf("debug is_adjacent_to: they are NOT adjacent. END\n");
#endif
	return FALSE;
}
idbool_t        idpolytope_is_equal_to(idpolytope_t *h_poly1, idpolytope_t *h_poly2) {
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);


	if (h_poly1->m_rows != h_poly2->m_rows) {
		// if both full-dimensional, the minimal representation must have the same number of
		// halfspaces if they are equal.
		return FALSE;
	}

	int i, fnd;
	idpredicate_t *h_pi1;
	idrb_node_t 	*h_lst1 = h_poly1->mp_x_constr->mp_row_pi->mp_values1_at,
					*h_lst2 = h_poly2->mp_x_constr->mp_row_pi->mp_values1_at;
	idrb_node_t *it;

	idrb_traverse(it, h_lst1) {
		h_pi1 = (idpredicate_t *)it->keyg(it);
		h_lst2->find_key_n(h_lst2,h_pi1,&fnd);
		if (!fnd) {
			return FALSE;
		}
	}
	return TRUE;
}
idbool_t        idpolytope_isinside(idpolytope_t *obj, idsparse_t *h_x) {
	assert(obj!=NULL);
	assert(h_x->nvars(h_x)==obj->m_dim);

	idpredicate_t *h_pi;
	idlist_t 	*h_lst = obj->mp_x_constr;
	idrb_node_t *it;

	idrb_traverse(it, h_lst->mp_head) {
		h_pi = (idpredicate_t *)it->keyg(it);
		if (h_pi->eval(h_pi,h_x) == FALSE) {
			return FALSE;
		}
	}
	return TRUE;
}
idbool_t        	idpolytope_isinside_dblcolvector(idpolytope_t *obj, iddblmatrix_t *h_x) {
	assert(obj!=NULL);
	assert((h_x->rows(h_x)==obj->m_dim)&&(h_x->cols(h_x)==1));

	idpredicate_t *h_pi;
	idrb_node_t 	*h_lst = obj->mp_x_constr->mp_row_pi->mp_values1_at;
	idrb_node_t *it;

	idrb_traverse(it, h_lst) {
		h_pi = (idpredicate_t *)it->keyg(it);
		if (h_pi->eval_from_dblcolvector(h_pi,h_x) == FALSE) {
			return FALSE;
		}
	}
	return TRUE;

}
idbool_t        idpolytope_isfulldim(idpolytope_t *obj) {
	if (obj->m_rows<=obj->m_dim) {
		return FALSE;
	} else {
		//polytope is fully dimensional if chebyshev's radius is positive
		return (obj->chebyradius(obj)>=0.001?TRUE:FALSE);
	}
}

idpolytope_t *  	idpolytope_and_with_predicate(idpolytope_t *obj, idpredicate_t *h_pi) {
	assert(obj!=NULL);
	assert(h_pi!=NULL);
	assert(h_pi->nvars(h_pi)==obj->m_dim);

	int i,j,
		rows = obj->m_rows,
	    dim = obj->m_dim;
	idmatrix_t *p_A = obj->mp_A->cpy(obj->mp_A), *p_b = obj->mp_b->cpy(obj->mp_b);
	p_A = p_A->liftVert(p_A); // add a new line with zeros
	p_b = p_b->liftVert(p_b); // add a new line with zeros
	int numnz, *h_ind;
	double *h_val, rhs;
	char sense;

	h_pi->get(h_pi,&numnz,&h_ind,&h_val,&sense,&rhs);
	assert(sense==GRB_LESS_EQUAL);

	p_b->setat(p_b,rows,0,rhs);
	for(j=0;j<numnz;j++) {
		p_A->setat(p_A,rows,h_ind[j],h_val[j]);
	}


	return H_idpolytope(p_A,p_b);
}
idpolytope_t *   idpolytope_and_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2) {
	assert(obj!=NULL);
	assert(h_poly2!=NULL);
	assert(h_poly2->m_dim==obj->m_dim);

	int dim = obj->m_dim;
	idmatrix_t *p_A, *p_b;
	assert(dim == h_poly2->m_dim);

	p_A = obj->mp_A->cpy(obj->mp_A);
	p_A = p_A->stackVert(p_A,h_poly2->mp_A->cpy(h_poly2->mp_A));
	p_b = obj->mp_b->cpy(obj->mp_b);
	p_b = p_b->stackVert(p_b,h_poly2->mp_b->cpy(h_poly2->mp_b));

	return H_idpolytope(p_A,p_b);
}
idpolytope_t *  	idpolytope_or_with_predicate(idpolytope_t *obj, idpredicate_t *h_pi) {
	assert(obj!=NULL);
	assert(h_pi!=NULL);
	assert(h_pi->nvars(h_pi)==obj->m_dim);

	// Let P be the polytope and H = {a'*x <= b} the halfspace representation of the predicate,
	// then the or (union) operation: P or H = H + (H^c and P) = (H^c and P),
	// because H is not full-dimensional and, therefore, is not a polytope.
	// Thus, the output is H^c*P, where H^c = {-a'*x <= -b}.

	idpredicate_t *p_pi_not = h_pi->cpy(h_pi);
	p_pi_not->neg(p_pi_not);
	idpolytope_t *Pout = obj->and_with_predicate(obj, p_pi_not);
	p_pi_not->destroy(p_pi_not);

	return Pout;
}

idarray_t *idpolytope_or_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2) {
	assert(obj!=NULL);
	assert(h_poly2!=NULL);
	assert(h_poly2->m_dim==obj->m_dim);

	// Let Pi and Pj be two polytopes, the or operation is Pi or Pj = Plargest + (Psmallest - Plargest)

	idarray_t *p_array;
	idpolytope_t *h_largest, *h_smallest;

	if (obj->chebyradius(obj)>=h_poly2->chebyradius(h_poly2)) {
		h_largest = obj;
		h_smallest = h_poly2;
	} else {
		h_largest = h_poly2;
		h_smallest = obj;
	}

	p_array = h_largest->difference(h_largest,h_smallest);
	p_array->append(p_array,h_largest->cpy(h_largest));

	return p_array;
}
int idpolytope_cmp(idpolytope_t *h_poly1, idpolytope_t *h_poly2) {
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);

	if (h_poly1->chebyradius(h_poly1) > h_poly1->chebyradius(h_poly2)) {
		return 1;
	} else if (h_poly1->chebyradius(h_poly1) > h_poly1->chebyradius(h_poly2)) {
		return -1;
	} else {
		return 0;
	}
}
idarray_t *idpolytope_difference(idpolytope_t *obj, idpolytope_t *h_poly2) {
#ifdef IDPOLYTOPE_DEBUG_DIFF
	printf("debug idpolytope_difference: starting...\n");
#endif
	assert(obj!=NULL);
	assert(h_poly2!=NULL);
	assert(h_poly2->m_dim==obj->m_dim);

	int n, r, c, optimstatus;
	double rcheby[h_poly2->m_rows];
	idarray_t *p_parray = idarray_byref();

	n = obj->m_dim;

	int *h_ind = NULL, numnz;
	double *h_val = NULL, rhs;
	GRBmodel 	*p_model = grb_mk_model();
	char sense;

	idrb_node_t *p_piset, *it;
	idpredicate_t *h_pi, *p_pi;
	idpolytope_t *p_poly, *p_poly1;
	idsparse_t *p_lhs, *h_lhs;
	double lhs = 1.0;

	for(c=0;c<n;c++) {
		assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);
	}
	assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, -1.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);

	idrb_traverse(it, obj->mp_x_constr->mp_row_pi->mp_values1_at) {
		h_pi = (idpredicate_t *)it->keyg(it);
		h_lhs = h_pi->lhs_get(h_pi);
		p_lhs = h_lhs->cpy(h_lhs);
		p_lhs->upd_nvars(p_lhs,p_lhs->nvars(p_lhs)+1);
		p_lhs->upd_numnz(p_lhs,p_lhs->numnz(p_lhs)+1);
		numnz = p_lhs->numnz(p_lhs);
		p_lhs->set_at(p_lhs,numnz-1,n,&lhs);
		h_ind = p_lhs->ind(p_lhs);
		h_val = (double *)p_lhs->values(p_lhs);
		rhs = h_pi->rhs_get(h_pi);
		assert(idGRB_NoError(GRBaddconstr(p_model, numnz, h_ind, h_val, GRB_LESS_EQUAL, rhs, NULL))==0);
		r++;
		p_lhs->destroy(p_lhs);
	}


	// Let P1 = {Ai*x<=bi}  and P2 = {Aj*x<=bj} be two polytopes, the difference between two polytopes is:
	// P1-P2 = full-dimensional polytopes in the set Pdiff = {P_1,...,P_rows2},
	// where P_k = {[Ai;aj_k]*x<=[bi;bj_k]}
#ifdef IDPOLYTOPE_DEBUG_DIFF
	printf("debug idpolytope_difference: ====================================\n");
	printf("debug idpolytope_difference: computing Pdiff set                 \n");
#endif

	p_piset = idrb_with_double_key(NULL);
	r = 0;
	idrb_traverse(it, h_poly2->mp_x_constr->mp_row_pi->mp_values1_at) {
		h_pi = (idpredicate_t *)it->keyg(it);
		h_lhs = h_pi->lhs_get(h_pi);
		p_lhs = h_lhs->cpy(h_lhs);
		numnz = p_lhs->numnz(p_lhs);
		h_val = p_lhs->values(p_lhs);
		for(c=0;c<numnz;c++) {
			h_val[c] = -h_val[c];
		}
		p_lhs->upd_nvars(p_lhs,p_lhs->nvars(p_lhs)+1);
		p_lhs->upd_numnz(p_lhs,p_lhs->numnz(p_lhs)+1);
		numnz = p_lhs->numnz(p_lhs);
		p_lhs->set_at(p_lhs,numnz-1,n,&lhs);
		h_ind = p_lhs->ind(p_lhs);
		h_val = (double *)p_lhs->values(p_lhs);
		rhs = h_pi->rhs_get(h_pi);
		assert(idGRB_NoError(GRBaddconstr(p_model, numnz, h_ind, h_val, GRB_LESS_EQUAL, -rhs, NULL))==0);

#ifdef IDPOLYTOPE_DEBUG_DIFF_L2
		int logid = rand();
		printf("debug idpolytope_difference: row = %d, logid = %d, pi=\n", r, logid);
		h_pi->print(h_pi,stdout);
		grb_log_lp_problem(p_model,"diff",logid);
#endif
		optimstatus = grb_optimize(p_model, &rcheby[r]);
		assert(optimstatus == GRB_OPTIMAL);
		rcheby[r] = -rcheby[r];
#ifdef IDPOLYTOPE_DEBUG_DIFF_L2
		printf("debug idpolytope_difference: rcheby = %.3f\n", rcheby[r]);
		grb_log_solution(p_model,"diff",logid);
#endif




		if (rcheby[r]>ID_ABSTOL) {
			rcheby[r] = 1/rcheby[r];
			p_piset->insert(p_piset,&rcheby[r],h_pi);
#ifdef IDPOLYTOPE_DEBUG_DIFF
			printf("debug idpolytope_difference: added predicate (rcheb = %.3f):\n", rcheby[r]);
			h_pi->print(h_pi,stdout);
#endif

		}

		assert(idGRB_NoError(GRBdelconstrs(p_model, 1, &obj->m_rows))==0);
		r++;
		p_lhs->destroy(p_lhs);
	}

#ifdef IDPOLYTOPE_DEBUG_DIFF
	printf("debug idpolytope_difference: computing Pdiff set END             \n");
	printf("debug idpolytope_difference: ====================================\n");
#endif
	GRBfreemodel(p_model);

	if (idrb_empty(p_piset)) {
		// P2 does not intersect with P1; thus, the difference is empty.
#ifdef IDPOLYTOPE_DEBUG_DIFF
		printf("debug idpolytope_difference: P2^c does not intersect with P1; thus, the difference is empty. END.\n");
#endif
		p_piset->destroy(p_piset);
		return p_parray;
	}

	p_poly1 = obj->cpy(obj);
	idrb_traverse(it, p_piset) {
		// for each halfspace in Pj (sorted by largest intersection)
		h_pi = (idpredicate_t *)it->value(it);
		p_pi = h_pi->cpy(h_pi);
		p_pi->neg(p_pi);
#ifdef IDPOLYTOPE_DEBUG_DIFF
		printf("debug idpolytope_difference: rcheb = %d, pi = \n", 1/it->keyd(it));
		p_pi->print(p_pi,stdout);
#endif
		p_poly = p_poly1->and_with_predicate(p_poly1,p_pi);
		p_pi->destroy(p_pi);
#ifdef IDPOLYTOPE_DEBUG_DIFF
		printf("debug idpolytope_difference: intersection = \n");
		p_poly->print(p_poly,stdout);
#endif
		if ((!p_poly->isempty(p_poly))&&(p_poly->chebyradius(p_poly)>ID_ABSTOL)) {
#ifdef IDPOLYTOPE_DEBUG_DIFF
			printf("debug idpolytope_difference: added region[%d] = \n", p_parray->length(p_parray));
			p_poly->print(p_poly,stdout);
#endif
			p_parray->append(p_parray,p_poly);
			p_poly = NULL;
		} else {
			p_poly->destroy(p_poly);
			p_poly = NULL;
		}

		// Now, we remove the halfspace from Pi to intersect with other halfspaces
		p_poly = p_poly1->and_with_predicate(p_poly1,h_pi);
		p_poly1->destroy(p_poly1);
		p_poly1 = p_poly;
		p_poly = NULL;
#ifdef IDPOLYTOPE_DEBUG_DIFF
		printf("debug idpolytope_difference: poly1 = \n", p_parray->length(p_parray));
		p_poly1->print(p_poly1,stdout);
#endif
		// if  Pi is not full-dimensional, we consider that is empty. Thus, we break the loop.
		if (!p_poly1->chebyradius(p_poly1)>ID_ABSTOL) {
			break;
		}
	}
	p_poly1->destroy(p_poly1);
	p_piset->destroy(p_piset);

#ifdef IDPOLYTOPE_DEBUG_DIFF
	idpolytope_t *h_poly;
	idrb_traverse(it, p_parray->mp_head) {
		printf("debug idpolytope_difference: region[%d] = \n", it->keyi(it));
		h_poly = (idpolytope_t *)it->value(it);
		h_poly->print(h_poly,stdout);
	}

	printf("debug idpolytope_difference: END.\n");
#endif
	return p_parray;
}

/* operations which destroys inputs */
void             idpolytope_destroy(idpolytope_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_POLYTOPE
		idpolytope_destroy_cnt--;
		printf("debug memleak destroy polytope %d still remains %d to destroy\n", obj->m_id, idpolytope_destroy_cnt);
		if (idpolytope_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		if (obj->mp_A!=NULL) {
			obj->mp_A->destroy(obj->mp_A);
		}
		if (obj->mp_b!=NULL) {
			obj->mp_b->destroy(obj->mp_b);
		}
		if (obj->mp_V!=NULL) {
			obj->mp_V->destroy(obj->mp_V);
		}
		idcsys_destroy(obj->mp_x_constr);
		free(obj);
	}
}

/* get functions */
void            idpolytope_print(idpolytope_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(out!=NULL);

	if (obj->m_rows==0) {
		fprintf(out,"Empty set.\n");
	} else {
		obj->mp_x_constr->print(obj->mp_x_constr,out);
	}
}

void idpolytope_get_halfspace(idpolytope_t *obj, idmatrix_t **A, idmatrix_t **b) {
	assert(obj!=NULL);
	assert(A!=NULL);
	assert(b!=NULL);

	(*A) = obj->mp_A->cpy(obj->mp_A);
	(*b) = obj->mp_b->cpy(obj->mp_b);
}

void idpolytope_get_vertices(idpolytope_t *obj, idmatrix_t **V) {
	assert(obj!=NULL);
	assert(V!=NULL);

	if (obj->mp_V == NULL) {
		obj->mp_V = idpolytope_compute_vertices(obj);
	}
	(*V) = obj->mp_V->cpy(obj->mp_V);
}

int				idpolytope_find_predicate_index(idpolytope_t *obj, idpredicate_t *h_pi1) {
	assert(obj!=NULL);
	assert(h_pi1!=NULL);
	assert(h_pi1->nvars(h_pi1)==obj->m_dim);
	assert(obj->mp_x_constr!=NULL);

	int idx;

	idpredicate_t *h_pi2 = obj->mp_x_constr->find(obj->mp_x_constr,h_pi1,&idx);

	if (h_pi2==NULL) {
		return -1;
	}

	return idx;
}
idcsys_t *idpolytope_get_x_constr(idpolytope_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_x_constr!=NULL);

	return obj->mp_x_constr;
}

static void idpolytope_collect_outter_hyperspaces(idmap_t *p_piset, int *h_npi, idpolytope_t *h_poly1, idpolytope_t *h_poly2) {
	assert(p_piset!=NULL);
	assert(h_poly1!=NULL);
	assert(h_poly2!=NULL);
	assert(h_poly2->m_dim==h_poly1->m_dim);

	int r, c, optimstatus,
		n = h_poly1->m_dim;
	double 	rcheby,
			lhs = 1.0;

	idrb_node_t *it;
	idpredicate_t *h_pi;
	idsparse_t *p_lhs, *h_lhs;

	int *h_ind = NULL, numnz, fnd;
	double *h_val = NULL, rhs;
	GRBmodel 	*p_model = grb_mk_model();
	char sense;

	for(c=0;c<n;c++) {
		assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);
	}
	assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, -1.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);

	r=0;

	idrb_traverse(it, h_poly1->mp_x_constr->mp_row_pi->mp_values1_at) {
		h_pi = (idpredicate_t *)it->keyg(it);
#ifdef IDPOLYTOPE_DEBUG_CONV
		printf("debug idpolytope_convhull_with_polytope: added predicate %d =\n", r);
		h_pi->print(h_pi,stdout);
#endif
		h_lhs = h_pi->lhs_get(h_pi);
		p_lhs = h_lhs->cpy(h_lhs);
		p_lhs->upd_nvars(p_lhs,p_lhs->nvars(p_lhs)+1);
		p_lhs->upd_numnz(p_lhs,p_lhs->numnz(p_lhs)+1);
		numnz = p_lhs->numnz(p_lhs);
		p_lhs->set_at(p_lhs,numnz-1,n,&lhs);
		h_ind = p_lhs->ind(p_lhs);
		h_val = (double *)p_lhs->values(p_lhs);
		rhs = h_pi->rhs_get(h_pi);
		assert(idGRB_NoError(GRBaddconstr(p_model, numnz, h_ind, h_val, GRB_LESS_EQUAL, rhs, NULL))==0);
		r++;
		p_lhs->destroy(p_lhs);
	}
#ifdef IDPOLYTOPE_DEBUG_CONV
	printf("debug idpolytope_convhull_with_polytope: ====================================\n");
	printf("debug idpolytope_convhull_with_polytope: computing outter hyperspaces set\n");
#endif

	r = *h_npi;
	idrb_traverse(it, h_poly2->mp_x_constr->mp_row_pi->mp_values1_at) {
		h_pi = (idpredicate_t *)it->keyg(it);
		idrb_find_key_n(p_piset->mp_values1_at,h_pi,&fnd);
		if (fnd) {
			// do nothing
			continue;
		}
		idrb_find_key_n(h_poly1->mp_x_constr->mp_row_pi->mp_values1_at,h_pi,&fnd);
		if (fnd) {
			p_piset->set(p_piset,&r,h_pi->cpy(h_pi));
			r++;
#ifdef IDPOLYTOPE_DEBUG_CONV
			printf("debug idpolytope_convhull_with_polytope: added predicate:\n", rcheby);
			h_pi->print(h_pi,stdout);
#endif
			continue;
		}
		h_lhs = h_pi->lhs_get(h_pi);
		p_lhs = h_lhs->cpy(h_lhs);
		numnz = p_lhs->numnz(p_lhs);
		h_val = p_lhs->values(p_lhs);
		for(c=0;c<numnz;c++) {
			h_val[c] = -h_val[c];
		}
		p_lhs->upd_nvars(p_lhs,p_lhs->nvars(p_lhs)+1);
		p_lhs->upd_numnz(p_lhs,p_lhs->numnz(p_lhs)+1);
		numnz = p_lhs->numnz(p_lhs);
		p_lhs->set_at(p_lhs,numnz-1,n,&lhs);
		h_ind = p_lhs->ind(p_lhs);
		h_val = (double *)p_lhs->values(p_lhs);
		rhs = h_pi->rhs_get(h_pi);
		assert(idGRB_NoError(GRBaddconstr(p_model, numnz, h_ind, h_val, GRB_LESS_EQUAL, -rhs, NULL))==0);

		optimstatus = grb_optimize(p_model, &rcheby);
		assert(optimstatus == GRB_OPTIMAL);
		rcheby = -rcheby;
#ifdef IDPOLYTOPE_DEBUG_CONV
		printf("debug idpolytope_convhull_with_polytope: rcheb = %.3f\n", rcheby);
#endif
#ifdef IDPOLYTOPE_DEBUG_CONV_L2
		int logid = rand();
		printf("debug idpolytope_convhull_with_polytope: row = %d, logid = %d, pi=\n", r, logid);
		h_pi->print(h_pi,stdout);
		grb_log_lp_problem(p_model,"conv",logid);
#endif
#ifdef IDPOLYTOPE_DEBUG_CONV_L2
		printf("debug idpolytope_convhull_with_polytope: rcheby = %.3f\n", rcheby);
		grb_log_solution(p_model,"conv",logid);
#endif

		if (rcheby<=ID_ABSTOL) {
			p_piset->set(p_piset,&r,h_pi->cpy(h_pi));
			r++;
#ifdef IDPOLYTOPE_DEBUG_CONV
			printf("debug idpolytope_convhull_with_polytope: added predicate:\n", rcheby);
			h_pi->print(h_pi,stdout);
#endif
		}

		assert(idGRB_NoError(GRBdelconstrs(p_model, 1, &h_poly1->m_rows))==0);
		p_lhs->destroy(p_lhs);
	}
	*h_npi = r;
	GRBfreemodel(p_model);
}
idpolytope_t *  	idpolytope_convhull_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2) {
#ifdef IDPOLYTOPE_DEBUG_CONV
	printf("debug idpolytope_convhull_with_polytope: starting...\n");
#endif
	assert(obj!=NULL);
	assert(h_poly2!=NULL);
	assert(h_poly2->m_dim==obj->m_dim);


	idmap_t *p_piset = idmap_byvalue_and_ptr(sizeof(int),idintcmpfunc_,idpredicate_destroy,idpredicate_cmp,idpredicate_cpy);
	int nconstrs = 0;
	idpolytope_collect_outter_hyperspaces(p_piset,&nconstrs,obj,h_poly2);
	idpolytope_collect_outter_hyperspaces(p_piset,&nconstrs,h_poly2,obj);
	assert(nconstrs>0);

	idpolytope_t *p_conv = idpolytope(obj->m_dim);
	p_conv->mp_x_constr = id_mk_csys_empty(nconstrs);
	p_conv->mp_x_constr->mp_row_pi = p_piset;
	p_conv->mp_x_constr->m_nvars = obj->m_dim;
	p_conv->mp_A = GMPmat_create_copy(p_conv->mp_x_constr->get_A(p_conv->mp_x_constr));
	p_conv->mp_b = GMPmat_create_copy(p_conv->mp_x_constr->get_b(p_conv->mp_x_constr));
	p_conv->m_rows = nconstrs;

#ifdef IDPOLYTOPE_DEBUG_CONV
	printf("debug idpolytope_convhull_with_polytope: p_conv =\n");
	p_conv->print(p_conv,stdout);
	printf("debug idpolytope_convhull_with_polytope: END\n");
#endif
	return p_conv;
}
idbool_t        	idpolytope_isempty(idpolytope_t *obj) {
	assert(obj!=NULL);
	return obj->m_rows <= 0;
}
