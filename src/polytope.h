/*
 * polytope.h
 *
 *  Created on: Feb 11, 2019
 *      Author: rafael
 */

#ifndef POLYTOPE_H_
#define POLYTOPE_H_

#include "idrtl.h"


#ifdef __cplusplus
extern "C" {
#endif


/* types */


/* constructors */
extern idpolytope_t *  idpolytope_from_cs(idcsys_t *h_constr);
extern idpolytope_t *  H_idpolytope(idmatrix_t *p_A, idmatrix_t *p_b);
extern idpolytope_t *  V_idpolytope(idmatrix_t *p_V);

/* operations which preserves inputs */
extern idpolytope_t *  idpolytope_cpy(idpolytope_t *obj);
extern double          idpolytope_chebyradius(idpolytope_t *obj);
extern idbool_t        idpolytope_is_adjacent_to(idpolytope_t *h_poly1, idpolytope_t *h_poly2,
													idpolytope_t *h_intersection, int *h_adjacent_facet_1, int *h_adjacent_facet_2,
													idpolytope_t **hp_convex_disjunction);
extern idbool_t        	idpolytope_is_equal_to(idpolytope_t *h_poly1, idpolytope_t *h_poly2);
extern idbool_t        	idpolytope_isinside(idpolytope_t *obj, idsparse_t *h_x);
extern idbool_t        	idpolytope_isinside_dblcolvector(idpolytope_t *obj, iddblmatrix_t *h_x);
extern idbool_t        	idpolytope_isfulldim(idpolytope_t *obj);
extern idbool_t        	idpolytope_isempty(idpolytope_t *obj);
extern idpolytope_t *  	idpolytope_and_with_predicate(idpolytope_t *obj, idpredicate_t *h_pi);
extern idpolytope_t *  	idpolytope_and_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2);
extern idpolytope_t *  	idpolytope_or_with_predicate(idpolytope_t *obj, idpredicate_t *h_pi);
extern idarray_t * 		idpolytope_or_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2);
extern idarray_t * 		idpolytope_difference(idpolytope_t *obj, idpolytope_t *h_poly2);
extern idpolytope_t *  	idpolytope_convhull_with_polytope(idpolytope_t *obj, idpolytope_t *h_poly2);

/* operations which destroys inputs */
extern void             idpolytope_destroy(idpolytope_t *obj);

/* get functions */
extern void            	idpolytope_print(idpolytope_t *obj, FILE *out);
extern void            	idpolytope_get_halfspace(idpolytope_t *obj, idmatrix_t **hp_A, idmatrix_t **hp_b);
extern void            	idpolytope_get_vertices(idpolytope_t *obj, idmatrix_t **hp_V);
extern int				idpolytope_find_predicate_index(idpolytope_t *obj, idpredicate_t *h_pi1);
extern idcsys_t *		idpolytope_get_x_constr(idpolytope_t *obj);

/* static operations */
#ifdef IDRTL_DEBUG_MEMLEAK_POLYTOPE
	static int		    idpolytope_destroy_cnt = 0;
#endif

#define idpolyarray_byptr(void) idarray_byptr(idpolytope_destroy,idpolytope_cpy);
#define idpolyarray_byref(void) idarray_byref();

struct idpolytope_s {
	/* capsulated member functions */
	/* operations which preserves inputs */
	idpolytope_t *  (*cpy)(idpolytope_t *obj);
	double          (*chebyradius)(idpolytope_t *obj);
	idbool_t        (*is_adjacent_to)(idpolytope_t *h_poly1, idpolytope_t *h_poly2,
										idpolytope_t *h_intersection, int *h_adjacent_facet_1, int *h_adjacent_facet_2,
										idpolytope_t **hp_convex_disjunction);
	idbool_t        (*is_equal_to)(idpolytope_t *h_poly1, idpolytope_t *h_poly2);
	idbool_t        (*isinside)(idpolytope_t *obj, idsparse_t *h_x);
	idbool_t        (*isinside_dblcolvector)(idpolytope_t *obj, iddblmatrix_t *h_x);
	idbool_t        (*isfulldim)(idpolytope_t *obj);
	idbool_t        (*isempty)(idpolytope_t *obj);
	idpolytope_t *  (*and_with_predicate)(idpolytope_t *obj, idpredicate_t *h_pi);
	idpolytope_t *  (*and_with_polytope)(idpolytope_t *obj, idpolytope_t *h_poly2);
	idpolytope_t *  (*or_with_predicate)(idpolytope_t *obj, idpredicate_t *h_pi);
	idarray_t * 	(*or_with_polytope)(idpolytope_t *obj, idpolytope_t *h_poly2);
	idarray_t *	 	(*difference)(idpolytope_t *obj, idpolytope_t *h_poly2);
	idpolytope_t *  (*convhull_with_polytope)(idpolytope_t *obj, idpolytope_t *h_poly2);

	/* operations which destroys inputs */
	void             (*destroy)(idpolytope_t *obj);

	/* get functions */
	void            (*print)(idpolytope_t *obj, FILE *out);
	void            (*get_halfspace)(idpolytope_t *obj, idmatrix_t **hp_A, idmatrix_t **hp_b);
	void            (*get_vertices)(idpolytope_t *obj, idmatrix_t **hp_V);
	int				(*find_predicate_index)(idpolytope_t *obj, idpredicate_t *h_pi1);
	idcsys_t *		(*get_x_constr)(idpolytope_t *obj);


	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_POLYTOPE
	int		      m_id;
#endif
	idcsys_t 	*mp_x_constr;
	double		m_rcheby;
	idmatrix_t	*mp_A,
				*mp_b,
				*mp_V;
	int			m_dim,
				m_rows;
	idbool_t    m_rcheby_isinitialized;
};



#ifdef __cplusplus
}
#endif

#endif /* POLYTOPE_H_ */
