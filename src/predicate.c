/*
 * predicate.c
 *
 *  Created on: Jan 29, 2019
 *      Author: rafael
 */

#include "predicate.h"

//#define IDPREDICATE_DEBUG_CMP

static void  idpredicate_normalize(idpredicate_t *obj) {
	assert(obj!=NULL);

	double anorm;
	int i;
	int 	numnz = obj->mp_lhs->numnz(obj->mp_lhs);
	double *h_val = (double *)obj->mp_lhs->values(obj->mp_lhs);

	anorm = 0.0;
	for(i=0;i<numnz;i++) {
		anorm += (h_val[i])*(h_val[i]);
	}
	anorm = sqrt(anorm);

	for(i=0;i<numnz;i++) {
		h_val[i] = (h_val[i])/anorm;
	}
	obj->m_rhs = obj->m_rhs/anorm;
}

static idpredicate_t *idpredicate(idsparse_t *p_lhs, char sense, double rhs) {
	idpredicate_t *obj = malloc(sizeof(idpredicate_t));
	assert(obj!=NULL);

	/* operators */
	obj->cpy         = idpredicate_cpy;
	obj->destroy     = idpredicate_destroy;
	obj->neg         = idpredicate_neg;
	obj->cmp         = idpredicate_cmp;
	obj->print       = idpredicate_print;
	obj->eval        = idpredicate_eval;
	obj->eval_from_dblcolvector = idpredicate_eval_from_dblcolvector;


	/* update functions */
	obj->factor_at   = idpredicate_factor_at;

	/* get functions */
	obj->isEqual     = idpredicate_isEqual;
	obj->isLessEqual = idpredicate_isLessEqual;
	obj->get			= idpredicate_get;
	obj->nvars      = idpredicate_nvars;
	obj->lhs_get	= idpredicate_lhs_get;
	obj->sense_get	= idpredicate_sense_get;
	obj->rhs_get	= idpredicate_rhs_get;

#ifdef IDRTL_DEBUG_MEMLEAK_PREDICATE
	static int idpredicate_last_id = 0;
	obj->m_id = idpredicate_last_id;
	printf("debug memleak construct predicate %d\n", obj->m_id);
	idpredicate_last_id++;
	idpredicate_destroy_cnt++;
#endif
	obj->m_rhs		= rhs;
	obj->mp_lhs		= p_lhs;
	obj->m_sense	= sense;

	idpredicate_normalize(obj);

	return obj;
}

idpredicate_t *id_mkLe(idsparse_t *p_lhs, double rhs) {
	idpredicate_t *obj = idpredicate(p_lhs, GRB_LESS_EQUAL, rhs);

	return obj;
}

idpredicate_t *id_mkEq(idsparse_t *p_lhs, double rhs) {
	idpredicate_t *obj = idpredicate(p_lhs, GRB_EQUAL, rhs);

	return obj;
}

idpredicate_t *idpredicate_cpy(idpredicate_t *obj) {
	assert(obj!=NULL);

	idpredicate_t *pi = idpredicate(obj->mp_lhs->cpy(obj->mp_lhs), obj->m_sense, obj->m_rhs);

	return pi;
}
void idpredicate_destroy(idpredicate_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_PREDICATE
		idpredicate_destroy_cnt--;
		printf("debug memleak destroy predicate %d still remains %d to destroy\n", obj->m_id, idpredicate_destroy_cnt);
		if (idpredicate_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idsparse_destroy(obj->mp_lhs);
		free(obj);
	}
}

void idpredicate_neg(idpredicate_t *obj) {
	assert(obj!=NULL);

	int 	numnz = obj->mp_lhs->numnz(obj->mp_lhs);
	double *h_val = (double *)obj->mp_lhs->values(obj->mp_lhs);

	for(int i=0;i<numnz;i++) {
		h_val[i] = -h_val[i];
	}

	obj->m_rhs = -obj->m_rhs;
}

int idpredicate_cmp(idpredicate_t *obj, idpredicate_t *pi) {
#ifdef IDPREDICATE_DEBUG_CMP
	printf("debug idpredicate_cmp: starting...\n");
#endif
	assert(obj!=NULL);
	assert(pi!=NULL);
	assert(pi->nvars(pi)==obj->nvars(obj));
#ifdef IDPREDICATE_DEBUG_CMP
	printf("debug idpredicate_cmp: comparing\n");
	obj->print(obj,stdout,NULL);
	printf("debug idpredicate_cmp: with\n");
	pi->print(pi,stdout,NULL);
#endif

	// Do they have same memory address?
	if (obj==pi) {
		// If so, they must be equal
#ifdef IDPREDICATE_DEBUG_CMP
		printf("debug idpredicate_cmp: they have same memory address; thus, the must be equal.\n");
		printf("debug idpredicate_cmp: END.\n");
#endif
		return 0;
	}

	// Don't they have same sense?
	if (obj->m_sense!=pi->m_sense) {
#ifdef IDPREDICATE_DEBUG_CMP
		printf("debug idpredicate_cmp: different inequalities; thus, they are different.\n");
		printf("debug idpredicate_cmp: END.\n");
#endif
		// If don't, they must be different.
		if (obj->isEqual(obj)) return  1; // equal is always bigger than lessequal
		else                   return -1;
	}

	if (obj->m_rhs-pi->m_rhs > ID_ABSTOL) {
#ifdef IDPREDICATE_DEBUG_CMP
		printf("debug idpredicate_cmp: different rhs (%.3f > %.3f); thus, they are different.\n", obj->m_rhs, pi->m_rhs);
		printf("debug idpredicate_cmp: END.\n");
#endif
		return  1;
	} else if (pi->m_rhs-obj->m_rhs > ID_ABSTOL) {
#ifdef IDPREDICATE_DEBUG_CMP
		printf("debug idpredicate_cmp: different rhs (%.3f > %.3f); thus, they are different.\n", pi->m_rhs, obj->m_rhs);
		printf("debug idpredicate_cmp: END.\n");
#endif
		return -1;
	}

	double val1,val2;

	for(int i=0;i<obj->nvars(obj);i++) {
		val1 = obj->factor_at(obj,i);
		val2 = pi->factor_at(pi,i);
		if (val1-val2 > ID_ABSTOL) {
#ifdef IDPREDICATE_DEBUG_CMP
			printf("debug idpredicate_cmp: different factor of element %d (%.3f > %.3f); thus, they are different.\n", i, val1, val2);
			printf("debug idpredicate_cmp: END.\n");
#endif
			return  1;
		}
		else if (val2-val1 > ID_ABSTOL) {
#ifdef IDPREDICATE_DEBUG_CMP
			printf("debug idpredicate_cmp: different factor of element %d (%.3f > %.3f); thus, they are different.\n", i, val2, val1);
			printf("debug idpredicate_cmp: END.\n");
#endif
			return -1;
		}

	}

#ifdef IDPREDICATE_DEBUG_CMP
	printf("debug idpredicate_cmp: they are equal.\n");
	printf("debug idpredicate_cmp: END.\n");
#endif
	return 0;
}

void idpredicate_print(idpredicate_t *obj, FILE *h_out) {
	assert(obj!=NULL);

	int i;
	idbool_t isfirst = TRUE;
	idrb_node_t *it, *lst;
	int 	numnz = obj->mp_lhs->numnz(obj->mp_lhs),
			*h_ind = obj->mp_lhs->ind(obj->mp_lhs);
	double *h_val = (double *)obj->mp_lhs->values(obj->mp_lhs);


	if (numnz == 0) {
		if (obj->isLessEqual(obj)) {
			if (ID_ABSTOL < obj->m_rhs) {
				fprintf(h_out,"TRUE\n");
			} else {
				fprintf(h_out,"FALSE\n");
			}
		} else {
			if (fabs(obj->m_rhs) <= ID_ABSTOL) {
				fprintf(h_out,"TRUE\n");
			} else {
				fprintf(h_out,"FALSE\n");
			}
		}
	} else {
		for(i=0;i<numnz;i++) {
			if (!isfirst) fprintf(h_out," ");
			if (h_val[i]>0) fprintf(h_out,"+");
			fprintf(h_out,"%.3f*x_%d", h_val[i],h_ind[i]);
		}

		if (obj->isLessEqual(obj)) {
			fprintf(h_out," <= ");
		} else {
			fprintf(h_out," == ");
		}
		fprintf(h_out,"%.3f\n", obj->m_rhs);
	}
}



idbool_t       idpredicate_eval(idpredicate_t *obj, idsparse_t *h_x) {
	assert(obj!=NULL);
	assert(h_x->nvars(h_x)==obj->nvars(obj));

	int i;

	int 	numnz = h_x->numnz(h_x),
			*h_ind = h_x->ind(h_x);
	double *h_val = (double *)h_x->values(h_x);


	double lhs = 0.0;
	for(i=0;i<numnz;i++) {
		lhs += obj->factor_at(obj,h_ind[i])*h_val[i];
	}

	if (obj->isLessEqual(obj)) {
		return (lhs-obj->m_rhs <= ID_ABSTOL?TRUE:FALSE);
	} else {
		return (fabs(lhs-obj->m_rhs) <= ID_ABSTOL?TRUE:FALSE);
	}
}
idbool_t       idpredicate_eval_from_dblcolvector(idpredicate_t *obj, iddblmatrix_t *h_x) {
	assert(obj!=NULL);
	assert((h_x->rows(h_x)==obj->nvars(obj))&&(h_x->cols(h_x)==1));

	int i;

	double lhs = 0.0;
	for(i=0;i<obj->nvars(obj);i++) {
		lhs += obj->factor_at(obj,i)*h_x->get(h_x,i,0);
	}

	if (obj->isLessEqual(obj)) {
		return (lhs-obj->m_rhs <= ID_ABSTOL?TRUE:FALSE);
	} else {
		return (fabs(lhs-obj->m_rhs) <= ID_ABSTOL?TRUE:FALSE);
	}

}
double         idpredicate_factor_at(idpredicate_t *obj, int index) {
	assert(obj!=NULL);
	assert((0<=index)&&(index<obj->nvars(obj)));

	int 	numnz = obj->mp_lhs->numnz(obj->mp_lhs),
			*h_ind = obj->mp_lhs->ind(obj->mp_lhs);
	double *h_val = (double *)obj->mp_lhs->values(obj->mp_lhs);

	int i = (int)idbinary_search_int(h_ind,numnz,index);

	if (i<0) {
		return 0.0;
	}
	return h_val[i];
}

idbool_t idpredicate_isLessEqual(idpredicate_t *obj) {
	assert(obj!=NULL);

	return obj->m_sense==GRB_LESS_EQUAL;
}
idbool_t idpredicate_isEqual(idpredicate_t *obj) {
	assert(obj!=NULL);

	return obj->m_sense==GRB_EQUAL;
}
void idpredicate_get(idpredicate_t *obj, int *h_numnz, int **hh_ind, double **hh_val, char *h_sense, double *h_rhs) {
	assert(obj!=NULL);
	assert(hh_ind!=NULL);
	assert(hh_val!=NULL);
	assert(h_numnz!=NULL);
	assert(h_sense!=NULL);
	assert(h_rhs!=NULL);


	int 	numnz = obj->mp_lhs->numnz(obj->mp_lhs),
			*h_ind = obj->mp_lhs->ind(obj->mp_lhs);
	double *h_val = (double *)obj->mp_lhs->values(obj->mp_lhs);
	*hh_ind = h_ind;
	*hh_val = h_val;
	*h_numnz = numnz;
	*h_sense = obj->m_sense;
	*h_rhs   = obj->m_rhs;

}
const int      idpredicate_nvars(idpredicate_t *obj) {
	assert(obj!=NULL);
	return idsparse_nvars(obj->mp_lhs);
}

idsparse_t *	  idpredicate_lhs_get(idpredicate_t *obj) {
	assert(obj!=NULL);
	return obj->mp_lhs;
}
const char	  idpredicate_sense_get(idpredicate_t *obj) {
	assert(obj!=NULL);
	return obj->m_sense;
}
const double	  idpredicate_rhs_get(idpredicate_t *obj) {
	assert(obj!=NULL);
	return obj->m_rhs;
}
