/*
 * predicate.h
 *
 *  Created on: Jan 29, 2019
 *      Author: rafael
 *
 *      predicates assumed to be linear
 */

#ifndef PREDICATE_H_
#define PREDICATE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif


/* types */

/* constructors */
#define idpredicatelist(void) idlist_byptr(idpredicate_cmp,idpredicate_destroy,idpredicate_cpy);
extern idpredicate_t *id_mkLe(idsparse_t *p_lhs, double rhs);
extern idpredicate_t *id_mkEq(idsparse_t *p_lhs, double rhs);

/* operators */
extern idpredicate_t *idpredicate_cpy(idpredicate_t *obj);
extern void           idpredicate_destroy(idpredicate_t *obj);
extern void           idpredicate_neg(idpredicate_t *obj);
extern int            idpredicate_cmp(idpredicate_t *obj, idpredicate_t *h_pi);
extern void           idpredicate_print(idpredicate_t *obj, FILE *h_out);
extern idbool_t       idpredicate_eval(idpredicate_t *obj, idsparse_t *h_x);
extern idbool_t       idpredicate_eval_from_dblcolvector(idpredicate_t *obj, iddblmatrix_t *h_x);

/* update functions */
extern double         idpredicate_factor_at(idpredicate_t *obj, int ind);

/* get functions */
extern const int      idpredicate_nvars(idpredicate_t *obj);
extern idbool_t       idpredicate_isLessEqual(idpredicate_t *obj);
extern idbool_t       idpredicate_isEqual(idpredicate_t *obj);
extern void           idpredicate_get(idpredicate_t *obj, int *h_numnz, int **hh_ind, double **hh_val, char *h_sense, double *h_rhs);
extern idsparse_t *	  idpredicate_lhs_get(idpredicate_t *obj);
extern const char	  idpredicate_sense_get(idpredicate_t *obj);
extern const double	  idpredicate_rhs_get(idpredicate_t *obj);

/* static operations */
#ifdef IDRTL_DEBUG_MEMLEAK_PREDICATE
	static int		  idpredicate_destroy_cnt = 0;
#endif

struct idpredicate_s {
	/* capsulated member functions */
	idpredicate_t *(*cpy)(idpredicate_t *obj);
	void           (*destroy)(idpredicate_t *obj);
	void           (*neg)(idpredicate_t *obj);
	int            (*cmp)(idpredicate_t *obj, idpredicate_t *h_pi);
	void           (*print)(idpredicate_t *obj, FILE *h_out);
	idbool_t       (*eval)(idpredicate_t *obj, idsparse_t *h_x);
	idbool_t       (*eval_from_dblcolvector)(idpredicate_t *obj, iddblmatrix_t *h_x);

	double 		   (*factor_at)(idpredicate_t *obj, int ind);

	const int 	   (*nvars)(idpredicate_t *obj);
	idbool_t 	   (*isLessEqual)(idpredicate_t *obj);
	idbool_t 	   (*isEqual)(idpredicate_t *obj);
	void           (*get)(idpredicate_t *obj, int *h_numnz, int **hh_ind, double **hh_val, char *h_sense, double *h_rhs);
	idsparse_t *	(*lhs_get)(idpredicate_t *obj);
	const char	  	(*sense_get)(idpredicate_t *obj);
	const double	(*rhs_get)(idpredicate_t *obj);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_PREDICATE
	int		      m_id;
#endif
	idsparse_t	*mp_lhs; // left-hand system
	char	      m_sense; // LessEq or Eq
	double        m_rhs;  // right-hand system
};

#ifdef __cplusplus
}
#endif

#endif /* PREDICATE_H_ */
