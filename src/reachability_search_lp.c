/*
 * reachability_search_lp.c
 *
 *  Created on: Jul 11, 2019
 *      Author: rafael
 */

#include "reachability_search_lp.h"

//#define IDREACH_DEBUG_L1
//#define IDREACH_DEBUG_L2
//#define IDREACH_DEBUG_L3

idbool_t   	idreach_lp_is_reachable(idreach_lp_t *obj) {
	assert(obj!=NULL);
	return (obj->m_objval <= obj->m_delta?TRUE:FALSE);
}
idreach_lp_t * 	idreach_lp(double delta, double delta_progress, idlinear_sys_t *h_sys,	idcsys_t *h_u_constr,
		idcsys_t *h_x0_constr, idcsys_t *h_xe_constr) {
	malloc(h_sys!=NULL);
	malloc(h_u_constr!=NULL);
	malloc(h_x0_constr!=NULL);
	malloc(h_xe_constr!=NULL);

	idreach_lp_t *obj = malloc(sizeof(idreach_lp_t));
	malloc(obj!=NULL);

	obj->cpy = idreach_lp_cpy;
	obj->destroy = idreach_lp_destroy;
	obj->is_reachable = idreach_lp_is_reachable;
	obj->go_on = idreach_lp_go_on;

#ifdef IDRTL_DEBUG_MEMLEAK_REACH_LP
	static int idreach_lp_last_id = 0;
	obj->m_id = idreach_lp_last_id;
	printf("debug memleak construct reach_lp %d\n", obj->m_id);
	idreach_lp_last_id++;
	idreach_lp_destroy_cnt++;
#endif
	obj->mh_sys = h_sys;
	obj->mp_A = h_sys->mp_A->cpy(h_sys->mp_A);
	obj->mp_B = h_sys->mp_B->cpy(h_sys->mp_B);
	obj->mh_c = h_sys->mp_c;
	obj->m_nsample = 0;
	obj->m_nvars = 0;
	obj->mp_model = NULL;
	obj->m_n = obj->mp_B->rows(obj->mp_B);
	obj->m_m = obj->mp_B->cols(obj->mp_B);
	obj->m_delta = delta;
	obj->m_delta_progress = delta_progress;
	obj->m_objval = GRB_INFINITY;
	obj->mh_u_constr = h_u_constr;
	obj->mh_x0_constr = h_x0_constr;
	obj->mh_xe_constr = h_xe_constr;
	obj->mp_model = grb_mk_model();

	return obj;
}
void      	idreach_lp_destroy(idreach_lp_t *obj) {
	int i;
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_REACH_LP
		idreach_lp_destroy_cnt--;
		printf("debug memleak destroy idreach_lp %d still remains %d to destroy\n", obj->m_id, idreach_lp_destroy_cnt);
		if (idreach_lp_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		obj->mp_A->destroy(obj->mp_A);
		obj->mp_B->destroy(obj->mp_B);
		GRBfreemodel(obj->mp_model);
		free(obj);
	}
}

idreach_lp_t * 	idreach_lp_cpy(idreach_lp_t *obj) {
	assert(obj!=NULL);
	idreach_lp_t *reach = idreach_lp(obj->m_delta,obj->m_delta_progress,obj->mh_sys,
			obj->mh_u_constr, obj->mh_x0_constr, obj->mh_xe_constr);

	return reach;
}

static int idreach_lp_mk_dynamical_constr(idreach_lp_t *obj) {
	int ind[obj->m_nvars], numnz;
	double val[obj->m_nvars];
	int row, col, constr_cnt,
		nsample = obj->m_nsample,
		n = obj->m_n,
		m = obj->m_m;
	double v;

	constr_cnt = 0;
	for(row=0;row<n;row++) {
		numnz = 0;
		for(col=0;col<n;col++) {
			v = obj->mp_A->get(obj->mp_A,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = obj->m_x0_offset + col;
				val[numnz] = v;
				numnz++;
			}
		}
		ind[numnz] = obj->m_xe_offset + row;
		val[numnz] = -1.0;
		numnz++;
		for(col=0;col<m*nsample;col++) {
			v = obj->mp_B->get(obj->mp_B,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = obj->m_u_offset+col;
				val[numnz] = v;
				numnz++;
			}
		}

		grb_add_constr(obj->mp_model,"D",nsample,0,row,numnz,ind,val,GRB_EQUAL,-obj->mh_c->get(obj->mh_c,row,0));
		constr_cnt++;
	}

	return constr_cnt;
}

idbool_t   	idreach_lp_go_on(idreach_lp_t *obj, int *h_nsamples_out, int nsamples_max) {
#ifdef IDREACH_DEBUG_L1
	printf("debug reachability go on the search : last nsample = %d\n", obj->m_nsample);
#endif
	assert(obj!=NULL);
	assert(h_nsamples_out!=NULL);


	int i,
		n = obj->m_n,
		m = obj->m_m;
	iddblmatrix_t *p_An;
	double objval;
	int numnz, *h_ind;
	double rhs, *h_val;
	char sense;

	if ((obj->m_nsample>n)&&(obj->m_nsample>nsamples_max)) {
#ifdef IDREACH_DEBUG_L1
		printf("debug reachability go on the search : STOP at nsample = %d because is is greater than the maximum %d END.\n", obj->m_nsample, nsamples_max);
#endif
		return FALSE;
	}

	if ( obj->m_nsample==0) {
#ifdef IDREACH_DEBUG_L1
		printf("debug reachability go on the search : initialize\n");
#endif


		// adds a slack variable which must be positive and our objective is to minimize it
		obj->m_nvars = 0;
		grb_mk_positive_slack_variable(obj->mp_model,0,1.0);
		obj->m_nvars++;
		obj->m_x0_offset = obj->m_nvars;
		grb_mk_state_variable_array(obj->mp_model,0,0,n);
		obj->m_nvars += n;
		obj->m_xe_offset = obj->m_nvars;
		grb_mk_state_variable_array(obj->mp_model,1,0,n);
		obj->m_nvars += n;
		obj->m_u_offset = obj->m_nvars;
		obj->m_constr_offset = 0;
		for(i=0;i<obj->mh_x0_constr->nconstrs(obj->mh_x0_constr);i++) {
			obj->mh_x0_constr->get_at(obj->mh_x0_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			grb_add_constr_with_ind_starting_at(obj->mp_model,"X",0,0,i,obj->m_x0_offset,numnz,h_ind,h_val,sense,rhs);
			obj->m_constr_offset++;
		}

		for(i=0;i<obj->mh_xe_constr->nconstrs(obj->mh_xe_constr);i++) {
			obj->mh_xe_constr->get_at(obj->mh_xe_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			grb_add_constr_with_ind_starting_at(obj->mp_model,"X",1,0,i,obj->m_xe_offset,numnz,h_ind,h_val,sense,rhs);
			obj->m_constr_offset++;
		}
	} else {
#ifdef IDREACH_DEBUG_L1
		printf("debug reachability go on the search : update dynamic constraint parameters\n");
#endif

#ifdef IDREACH_DEBUG_L2
		printf("debug reachability go on the search : B = \n");
		obj->mp_B->print(obj->mp_B,stdout);
#endif
		obj->mp_B = iddblmatrix_right_concatenate(iddblmatrix_multiply(obj->mp_A,obj->mh_sys->B(obj->mh_sys)),obj->mp_B);
#ifdef IDREACH_DEBUG_L2
		printf("debug reachability go on the search : B = \n");
		obj->mp_B->print(obj->mp_B,stdout);
		printf("debug reachability go on the search : A = \n");
		obj->mp_A->print(obj->mp_A,stdout);
#endif
		p_An = iddblmatrix_multiply_and_return_A(obj->mp_A,obj->mp_A);
#ifdef IDREACH_DEBUG_L2
		printf("debug reachability go on the search : A = \n");
		obj->mp_A->print(obj->mp_A,stdout);
#endif

		// remove previous dynamical constraints
		grb_del_constr_array(obj->mp_model,obj->m_constr_offset,n);
	}

	// Add dynamic constraints


	grb_mk_input_variable_array(obj->mp_model,obj->m_nsample,0,m);
	for(i=0;i<obj->mh_u_constr->nconstrs(obj->mh_u_constr);i++) {
		obj->mh_u_constr->get_at(obj->mh_u_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
		grb_add_ubconstr_with_slack_and_ind_starting_at(obj->mp_model,
				"U",obj->m_nsample,0,i,
				obj->m_u_offset,0,
				numnz,h_ind,h_val,rhs);
		obj->m_constr_offset++;
	}
	obj->m_nvars += m;
	obj->m_nsample++;
	*h_nsamples_out = obj->m_nsample;

	idreach_lp_mk_dynamical_constr(obj);

	if (grb_optimize(obj->mp_model,&objval)==GRB_OPTIMAL) {
#ifdef IDREACH_DEBUG_L3
		int logid = rand();
		printf("debug reachability go on the search : logid = %d END.\n", logid);
		grb_log_lp_problem(obj->mp_model,"reach",logid);
		grb_log_solution(obj->mp_model,"reach",logid);
#endif
#ifdef IDREACH_DEBUG_L1
		printf("debug reachability go on the search : objval = %.3f; thus, the progress is %.6f.\n", objval, obj->m_objval-objval);
#endif

		if (objval<=obj->m_delta) {
#ifdef IDREACH_DEBUG_L1
			printf("debug reachability go on the search : objval = %.3f <= %.3f = delta.\n", objval, obj->m_delta);
			printf("debug reachability go on the search : STOP at nsample = %d because it is feasible END.\n", obj->m_nsample);
#endif
			obj->m_objval = objval;
			return FALSE;
		} else if ((obj->m_nsample>n)&&(obj->m_objval-objval<obj->m_delta_progress)) {
#ifdef IDREACH_DEBUG_L1
			printf("debug reachability go on the search : progress = %.6f < %.6f delta_progress.\n", obj->m_objval-objval, obj->m_delta_progress);
			printf("debug reachability go on the search : STOP at nsample = %d because it of no progress END.\n", obj->m_nsample);
#endif
			obj->m_objval = objval;
			return FALSE;
		}
#ifdef IDREACH_DEBUG_L1
			printf("debug reachability go on the search : CONTINUE at nsample = %d END.\n", obj->m_nsample);
#endif
		obj->m_objval = objval;
		return TRUE;
	}

	assert(obj->m_nsample<=n);

#ifdef IDREACH_DEBUG_L3
	int logid = rand();
	printf("debug reachability go on the search : logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"reach",logid);
#endif

#ifdef IDREACH_DEBUG_L1
			printf("debug reachability go on the search : CONTINUE at nsample = %d END.\n", obj->m_nsample);
#endif
	return TRUE;
}
