/*
 * reachability_search_lp.h
 *
 *  Created on: Jul 11, 2019
 *      Author: rafael
 */

#ifndef SOLVER_REACHABILITY_SEARCH_LP_H_
#define SOLVER_REACHABILITY_SEARCH_LP_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idreach_lp_t * 	idreach_lp(double delta, double delta_progress, idlinear_sys_t *h_sys,	idcsys_t *h_u_constr, idcsys_t *h_x0_constr, idcsys_t *h_xe_constr);
extern void      	idreach_lp_destroy(idreach_lp_t *obj);
extern idreach_lp_t * 	idreach_lp_cpy(idreach_lp_t *obj);

extern idbool_t   	idreach_lp_go_on(idreach_lp_t *obj, int *h_nsamples_out, int nsamples_max);
extern idbool_t   	idreach_lp_is_reachable(idreach_lp_t *obj);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_REACH_LP
	static int		    idreach_lp_destroy_cnt = 0;
#endif

	struct idreach_lp_s{
		/* capsulated member functions */
		void           	(*destroy)(idreach_lp_t *obj);
		idreach_lp_t *		(*cpy)(idreach_lp_t *obj);

		idbool_t   		(*go_on)(idreach_lp_t *obj, int *h_nsamples_out, int nsamples_max);
		idbool_t   		(*is_reachable)(idreach_lp_t *obj);

		/* operation */

		/* private variables - do not access directly */
	#ifdef IDRTL_DEBUG_MEMLEAK_REACH_LP
		int		      m_id;
	#endif
		iddblmatrix_t 	*mp_A,
						*mp_B,
						*mh_c;
		idlinear_sys_t	*mh_sys;
		int				m_nsample,
						m_nvars,
						m_constr_offset,
						m_x0_offset,
						m_xe_offset,
						m_u_offset,
						m_n,
						m_m;
		double 			m_delta,
						m_delta_progress,
						m_objval;

		GRBmodel 		*mp_model;
		idcsys_t 	*mh_u_constr,
							*mh_x0_constr,
							*mh_xe_constr;
	};

#ifdef __cplusplus
}
#endif

#endif /* SOLVER_REACHABILITY_SEARCH_LP_H_ */
