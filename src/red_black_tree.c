/*
Generic C code for red-black trees.
Copyright (C) 2000 James S. Plank

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/* Revision 1.2.  Jim Plank */

/* Original code by Jim Plank (plank@cs.utk.edu) */
/* modified for THINK C 6.0 for Macintosh by Chris Bartley */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>
#include "red_black_tree.h"

static void mk_new_int(idrb_node_t * l, idrb_node_t * r, idrb_node_t * p, int il);
static idrb_node_t * lprev(idrb_node_t * n);
static idrb_node_t * rprev(idrb_node_t * n);
static void recolor(idrb_node_t * n);
static void single_rotate(idrb_node_t * y, int l);


#define isred(n) (n->s.red)
#define isblack(n) (!isred(n))
#define isleft(n) (n->s.left)
#define isright(n) (!isleft(n))
#define isint(n) (n->s.internal)
#define isext(n) (!isint(n))
#define ishead(n) (n->s.head)
#define isroot(n) (n->s.root)
#define setred(n) n->s.red = 1
#define setblack(n) n->s.red = 0
#define setleft(n) n->s.left = 1
#define setright(n) n->s.left = 0
#define sethead(n) n->s.head = 1
#define setroot(n) n->s.root = 1
#define setint(n) n->s.internal = 1
#define setext(n) n->s.internal = 0
#define setnormal(n) { n->s.root = 0; n ->s.head = 0; }
#define sibling(n) ((isleft(n)) ? n->p.parent->c.child.right \
                                : n->p.parent->c.child.left)

static int cmp_int(void *h_ptr1, void *h_ptr2) {
	assert(h_ptr1!=NULL);
	assert(h_ptr2!=NULL);

	int key1 = *((int *)h_ptr1);
	int key2 = *((int *)h_ptr2);

	if (key1>key2) {
		return 1;
	} else if(key1<key2) {
		return -1;
	} else {
		return 0;
	}
}
static int cmp_dbl(void *h_ptr1, void *h_ptr2) {
	assert(h_ptr1!=NULL);
	assert(h_ptr2!=NULL);

	double key1 = *((double *)h_ptr1);
	double key2 = *((double *)h_ptr2);

	if (key1>key2) {
		return 1;
	} else if(key1<key2) {
		return -1;
	} else {
		return 0;
	}
}

static void insert(idrb_node_t * item, idrb_node_t * list)	{ /* Inserts to the end of a list */
	assert(item!=NULL);
	assert(list!=NULL);

	idrb_node_t * last_node;

	last_node = list->c.list.blink;

	list->c.list.blink = item;
	last_node->c.list.flink = item;
	item->c.list.blink = last_node;
	item->c.list.flink = list;
}

static void delete_item(idrb_node_t * item)	{	/* Deletes an arbitrary iterm */
	assert(item!=NULL);
	if (item->v.destroy!=NULL) {
		item->v.destroy(item->v.value.val);
	}
	if ((item->k.destroy!=NULL)&&(item->k.type == IDKEY_CUSTOM)) {
		item->k.destroy(item->k.key.keyg);
	}
	item->c.list.flink->c.list.blink = item->c.list.blink;
	item->c.list.blink->c.list.flink = item->c.list.flink;
}

static void init_key(idrb_node_t * n, idkeytype_t keytype,int (*cmp)(void *key1, void *key2), void (*destroy_key)(void *key)) {
	assert(n!=NULL);

	if (keytype==IDKEY_INT) {
		assert(cmp==NULL);
		n->k.type = IDKEY_INT;
		n->k.key.keyi = 0;
		n->k.cmp = cmp_int;
		n->k.destroy = NULL;
	} else if (keytype==IDKEY_DOUBLE) {
		assert(cmp==NULL);
		n->k.type = IDKEY_DOUBLE;
		n->k.key.keyd = 0.0;
		n->k.cmp = cmp_dbl;
		n->k.destroy = NULL;
	} else if (keytype==IDKEY_CUSTOM) {
		assert(cmp!=NULL);
		n->k.type = IDKEY_CUSTOM;
		n->k.key.keyg = NULL;
		n->k.cmp = cmp;
		n->k.destroy = destroy_key;
	}
}

static void setkey(idrb_node_t * n, void *key) {
	assert(n!=NULL);
	assert(key!=NULL);

	if (n->k.type==IDKEY_INT) {
		n->k.key.keyi = *((int *)key);
		n->k.h_key = &n->k.key.keyi;
	} else if (n->k.type==IDKEY_DOUBLE) {
		n->k.key.keyd = *((double *)key);
		n->k.h_key = &n->k.key.keyd;
	} else if (n->k.type==IDKEY_CUSTOM) {
		n->k.key.keyg = key;
		n->k.h_key = key;
	}
}

static void setfunc(idrb_node_t * n) {
	assert(n!=NULL);

#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	static int idrb_last_id = 0;
	n->m_id = idrb_last_id;
	printf("debug memleak construct rb node %d\n", n->m_id);
	idrb_last_id++;
	idrb_destroy_cnt++;
#endif
	n->insert = idrb_insert;
	n->find_key = idrb_find_key;
	n->find_key_n = idrb_find_key_n;
	n->delete_node = idrb_delete_node;
	n->destroy = idrb_destroy;
	n->value = idrb_value;
	n->keyi = idrb_keyi;
	n->keyd = idrb_keyd;
	n->keyg = idrb_keyg;
	n->length = idrb_length;
}

#define mk_new_ext(n, new, kkkey, vvval) {\
  new = (idrb_node_t *) malloc(sizeof(struct idrb_node_s));\
  new->v.value.val = vvval;\
  new->v.destroy = n->v.destroy;\
  new->k.type = n->k.type;\
  new->k.destroy = n->k.destroy;\
  new->k.cmp = n->k.cmp;\
  setkey(new,kkkey);\
  setfunc(new);\
  setext(new);\
  setblack(new);\
  setnormal(new);\
}
static void mk_new_int(idrb_node_t * l, idrb_node_t * r, idrb_node_t * p, int il)
{
  idrb_node_t * newnode;

  newnode = (idrb_node_t *) malloc(sizeof(struct idrb_node_s));
  setint(newnode);
  setred(newnode);
  setnormal(newnode);
  setfunc(newnode);
  newnode->c.child.left = l;
  newnode->c.child.right = r;
  newnode->p.parent = p;
  newnode->k.key.lext = l;
  newnode->v.value.rext = r;
  l->p.parent = newnode;
  r->p.parent = newnode;
  setleft(l);
  setright(r);
  if (ishead(p)) {
    p->p.root = newnode;
    setroot(newnode);
  } else if (il) {
    setleft(newnode);
    p->c.child.left = newnode;
  } else {
    setright(newnode);
    p->c.child.right = newnode;
  }
  recolor(newnode);
}




idrb_node_t * lprev(idrb_node_t * n)
{
  if (ishead(n)) return n;
  while (!isroot(n)) {
    if (isright(n)) return n->p.parent;
    n = n->p.parent;
  }
  return n->p.parent;
}

idrb_node_t * rprev(idrb_node_t * n)
{
  if (ishead(n)) return n;
  while (!isroot(n)) {
    if (isleft(n)) return n->p.parent;
    n = n->p.parent;
  }
  return n->p.parent;
}

idrb_node_t * idrb_with_integer_key(void (*destroy_value)(void *value)) {
	idrb_node_t * head;
	head = (idrb_node_t *) malloc (sizeof(idrb_node_t));
	assert(head!=NULL);

	head->c.list.flink = head;
	head->c.list.blink = head;
	head->p.root = head;
	init_key(head,IDKEY_INT,NULL,NULL);
	setfunc(head);
	head->v.destroy = destroy_value;
	sethead(head);
	return head;
}
idrb_node_t * idrb_with_double_key(void (*destroy_value)(void *value)) {
	idrb_node_t * head;
	head = (idrb_node_t *) malloc (sizeof(idrb_node_t));
	assert(head!=NULL);

	head->c.list.flink = head;
	head->c.list.blink = head;
	head->p.root = head;
	init_key(head,IDKEY_DOUBLE,NULL,NULL);
	setfunc(head);
	head->v.destroy = destroy_value;
	sethead(head);
	return head;
}
idrb_node_t * idrb_with_custom_key(int (*cmp)(void *key1, void *key2), void (*destroy_key)(void *key), void (*destroy_value)(void *value)) {
	assert(cmp!=NULL);

	idrb_node_t * head;
	head = (idrb_node_t *) malloc (sizeof(idrb_node_t));
	assert(head!=NULL);

	head->c.list.flink = head;
	head->c.list.blink = head;
	head->p.root = head;
	init_key(head,IDKEY_CUSTOM,cmp,destroy_key);
	setfunc(head);
	head->v.destroy = destroy_value;
	sethead(head);
	return head;
}

idrb_node_t * idrb_find_key_n(idrb_node_t * n, void *key, int *fnd)
{
  int cmp;
  idrb_node_t * head = n;


  *fnd = 0;
  if (!ishead(n)) {
    fprintf(stderr, "rb_find_key_n called on non-head 0x%x\n", n);
    exit(1);
  }
  if (n->p.root == n) return n;
  cmp = head->k.cmp(key, n->c.list.blink->k.h_key);
  if (cmp == 0) {
    *fnd = 1;
    return n->c.list.blink;
  }
  if (cmp > 0) return n;
  else n = n->p.root;
  while (1) {
    if (isext(n)) return n;
    cmp = head->k.cmp(key, n->k.key.lext->k.h_key);
    if (cmp == 0) {
      *fnd = 1;
      return n->k.key.lext;
    }
    if (cmp < 0) n = n->c.child.left ; else n = n->c.child.right;
  }
}

idrb_node_t * idrb_find_key(idrb_node_t * n, void *key)
{
  int fnd;
  return idrb_find_key_n(n, key, &fnd);
}

idrb_node_t * idrb_insert_b(idrb_node_t * n, void *h_key, void *p_val)
{
  idrb_node_t * newleft, * newright, * newnode, * p;

  if (ishead(n)) {
    if (n->p.root == n) {         /* Tree is empty */
      mk_new_ext(n, newnode, h_key, p_val);
      insert(newnode, n);
      n->p.root = newnode;
      newnode->p.parent = n;
      setroot(newnode);
      return newnode;
    } else {
      mk_new_ext(n, newright, h_key, p_val);
      insert(newright, n);
      newleft = newright->c.list.blink;
      setnormal(newleft);
      mk_new_int(newleft, newright, newleft->p.parent, isleft(newleft));
      p = rprev(newright);
      if (!ishead(p)) p->k.key.lext = newright;
      return newright;
    }
  } else {
    mk_new_ext(n, newleft, h_key, p_val);
    insert(newleft, n);
    setnormal(n);
    mk_new_int(newleft, n, n->p.parent, isleft(n));
    p = lprev(newleft);
    if (!ishead(p)) p->v.value.rext = newleft;
    return newleft;
  }
}

static void recolor(idrb_node_t * n)
{
  idrb_node_t * p, * gp, * s;
  int done = 0;

  while(!done) {
    if (isroot(n)) {
      setblack(n);
      return;
    }

    p = n->p.parent;

    if (isblack(p)) return;

    if (isroot(p)) {
      setblack(p);
      return;
    }

    gp = p->p.parent;
    s = sibling(p);
    if (isred(s)) {
      setblack(p);
      setred(gp);
      setblack(s);
      n = gp;
    } else {
      done = 1;
    }
  }
  /* p's sibling is black, p is red, gp is black */

  if ((isleft(n) == 0) == (isleft(p) == 0)) {
    single_rotate(gp, isleft(n));
    setblack(p);
    setred(gp);
  } else {
    single_rotate(p, isleft(n));
    single_rotate(gp, isleft(n));
    setblack(n);
    setred(gp);
  }
}

static void single_rotate(idrb_node_t * y, int l)
{
  int rl, ir;
  idrb_node_t * x, * yp;
  char *tmp;

  ir = isroot(y);
  yp = y->p.parent;
  if (!ir) {
    rl = isleft(y);
  }

  if (l) {
    x = y->c.child.left;
    y->c.child.left = x->c.child.right;
    setleft(y->c.child.left);
    y->c.child.left->p.parent = y;
    x->c.child.right = y;
    setright(y);
  } else {
    x = y->c.child.right;
    y->c.child.right = x->c.child.left;
    setright(y->c.child.right);
    y->c.child.right->p.parent = y;
    x->c.child.left = y;
    setleft(y);
  }

  x->p.parent = yp;
  y->p.parent = x;
  if (ir) {
    yp->p.root = x;
    setnormal(y);
    setroot(x);
  } else {
    if (rl) {
      yp->c.child.left = x;
      setleft(x);
    } else {
      yp->c.child.right = x;
      setright(x);
    }
  }
}

void idrb_delete_node(idrb_node_t * n)
{
  idrb_node_t * s, * p, * gp;
  char ir;

  if (isint(n)) {
    fprintf(stderr, "Cannot delete an internal node: 0x%x\n", n);
    exit(1);
  }
  if (ishead(n)) {
    fprintf(stderr, "Cannot delete the head of an rb_tree: 0x%x\n", n);
    exit(1);
  }
  delete_item(n); /* Delete it from the list */
  p = n->p.parent;  /* The only node */
  if (isroot(n)) {
    p->p.root = p;
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", n->m_id, idrb_destroy_cnt);
#endif
    free(n);
    return;
  }
  s = sibling(n);    /* The only node after deletion */
  if (isroot(p)) {
    s->p.parent = p->p.parent;
    s->p.parent->p.root = s;
    setroot(s);
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", p->m_id, idrb_destroy_cnt);
#endif
    free(p);
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", n->m_id, idrb_destroy_cnt);
#endif
    free(n);
    return;
  }
  gp = p->p.parent;  /* Set parent to sibling */
  s->p.parent = gp;
  if (isleft(p)) {
    gp->c.child.left = s;
    setleft(s);
  } else {
    gp->c.child.right = s;
    setright(s);
  }
  ir = isred(p);
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", p->m_id, idrb_destroy_cnt);
#endif
    free(p);
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", n->m_id, idrb_destroy_cnt);
#endif
    free(n);

  if (isext(s)) {      /* Update proper rext and lext values */
    p = lprev(s);
    if (!ishead(p)) p->v.value.rext = s;
    p = rprev(s);
    if (!ishead(p)) p->k.key.lext = s;
  } else if (isblack(s)) {
    fprintf(stderr, "DELETION PROB -- sib is black, internal\n");
    exit(1);
  } else {
    p = lprev(s);
    if (!ishead(p)) p->v.value.rext = s->c.child.left;
    p = rprev(s);
    if (!ishead(p)) p->k.key.lext = s->c.child.right;
    setblack(s);
    return;
  }

  if (ir) return;

  /* Recolor */

  n = s;
  p = n->p.parent;
  s = sibling(n);
  while(isblack(p) && isblack(s) && isint(s) &&
        isblack(s->c.child.left) && isblack(s->c.child.right)) {
    setred(s);
    n = p;
    if (isroot(n)) return;
    p = n->p.parent;
    s = sibling(n);
  }

  if (isblack(p) && isred(s)) {  /* Rotation 2.3b */
    single_rotate(p, isright(n));
    setred(p);
    setblack(s);
    s = sibling(n);
  }

  { idrb_node_t * x, * z; char il;

    if (isext(s)) {
      fprintf(stderr, "DELETION ERROR: sibling not internal\n");
      exit(1);
    }

    il = isleft(n);
    x = il ? s->c.child.left : s->c.child.right ;
    z = sibling(x);

    if (isred(z)) {  /* Rotation 2.3f */
      single_rotate(p, !il);
      setblack(z);
      if (isred(p)) setred(s); else setblack(s);
      setblack(p);
    } else if (isblack(x)) {   /* Recoloring only (2.3c) */
      if (isred(s) || isblack(p)) {
        fprintf(stderr, "DELETION ERROR: 2.3c not quite right\n");
        exit(1);
      }
      setblack(p);
      setred(s);
      return;
    } else if (isred(p)) { /* 2.3d */
      single_rotate(s, il);
      single_rotate(p, !il);
      setblack(x);
      setred(s);
      return;
    } else {  /* 2.3e */
      single_rotate(s, il);
      single_rotate(p, !il);
      setblack(x);
      return;
    }
  }
}


int idrb_nblack(idrb_node_t * n)
{
  int nb;
  if (ishead(n) || isint(n)) {
    fprintf(stderr, "ERROR: rb_nblack called on a non-external node 0x%x\n",
            n);
    exit(1);
  }
  nb = 0;
  while(!ishead(n)) {
    if (isblack(n)) nb++;
    n = n->p.parent;
  }
  return nb;
}

int idrb_plength(idrb_node_t * n)
{
  int pl;
  if (ishead(n) || isint(n)) {
    fprintf(stderr, "ERROR: rb_plength called on a non-external node 0x%x\n",
            n);
    exit(1);
  }
  pl = 0;
  while(!ishead(n)) {
    pl++;
    n = n->p.parent;
  }
  return pl;
}

int idrb_length(idrb_node_t * n)
{
  int pl = 0;

  idrb_node_t *it;
  idrb_traverse(it,n) {
	  pl++;
  }


  return pl;
}

void idrb_destroy(idrb_node_t * n)
{
  if (!ishead(n)) {
    fprintf(stderr, "ERROR: Rb_free_tree called on a non-head node\n");
    exit(1);
  }

  while(idrb_first(n) != idrb_nil(n)) {
    idrb_delete_node(idrb_first(n));
  }
#ifdef IDRTL_DEBUG_MEMLEAK_RBNODE
	idrb_destroy_cnt--;
	printf("debug memleak destroy rb node %d still remains %d to destroy\n", n->m_id, idrb_destroy_cnt);
	if (idrb_destroy_cnt==0) {
		printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
	}
#endif
  free(n);
}

void *idrb_value(idrb_node_t * n)
{
  return n->v.value.val;
}

int idrb_keyi(idrb_node_t * node) {
	return node->k.key.keyi;
}
double idrb_keyd(idrb_node_t * node) {
	return node->k.key.keyd;
}
void *idrb_keyg(idrb_node_t * node) {
	return node->k.key.keyg;
}

idrb_node_t * idrb_insert_a(idrb_node_t * nd, void *key, void *p_val)
{
  return idrb_insert_b(nd->c.list.flink, key, p_val);
}

idrb_node_t * idrb_insert(idrb_node_t * tree, void *key, void *p_val)
{

	return idrb_insert_b(idrb_find_key(tree, key), key, p_val);
}

