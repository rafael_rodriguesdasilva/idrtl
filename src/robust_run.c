/*
 * robust_run.c
 *
 *  Created on: Jul 24, 2019
 *      Author: rafael
 */


#include "robust_run.h"

//#define ROBRUN_DEBUG_L1
//#define ROBRUN_DEBUG_L2

static int idrobrun_mk_dynamical_constr(GRBmodel *h_model, int step, int loop_id, double delta, idlinear_sys_t *h_sys,
		int x0_offset, int u0_offset, int xe_offset) {
#ifdef ROBRUN_DEBUG_L2
	printf("debug idrobrun_mk_dynamical_constr: starting... step=%d, loop_id=%d\n");
#endif
	int row, col, constr_cnt,
		n = h_sys->n(h_sys),
		m = h_sys->m(h_sys);
	double v;
	int ind[n+m], numnz;
	double val[n+m];

	constr_cnt = 0;
	for(row=0;row<n;row++) {
		numnz = 0;
		for(col=0;col<n;col++) {
			v = h_sys->mp_A->get(h_sys->mp_A,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = x0_offset + col;
				val[numnz] = v;
#ifdef ROBRUN_DEBUG_L2
				printf("debug idrobrun_mk_dynamical_constr: x0[%d] =  x[%d] = %.3f\n", col, ind[numnz], val[numnz]);
#endif
				numnz++;
			}
		}
		ind[numnz] = xe_offset + row;
		val[numnz] = -1.0;
#ifdef ROBRUN_DEBUG_L2
		printf("debug idrobrun_mk_dynamical_constr: x1[%d] =  x[%d] = %.3f\n", row, ind[numnz], val[numnz]);
#endif
		numnz++;
		for(col=0;col<m;col++) {
			v = h_sys->mp_B->get(h_sys->mp_B,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = u0_offset+col;
				val[numnz] = v;
#ifdef ROBRUN_DEBUG_L2
				printf("debug idrobrun_mk_dynamical_constr: u0[%d] =  x[%d] = %.3f\n", col, ind[numnz], val[numnz]);
#endif
				numnz++;
			}
		}
#ifdef ROBRUN_DEBUG_L2
		printf("debug idrobrun_mk_dynamical_constr: numnz = %d\n", numnz);
#endif

		grb_add_inftynorm_constr_with_ind_starting_at(h_model,"D",step,loop_id,row,0,
				numnz,ind,val,-h_sys->mp_c->get(h_sys->mp_c,row,0),delta);
		constr_cnt+=2;
	}

	return constr_cnt;
}

static int idrobrun_add_at(idrobrun_t *obj, int step1, int loop_id1,
		int step0, int loop_id0, int x0_offset, int u0_offset, int x1_offset,
		idlinear_sys_t *h_sys, idcsys_t *h_u0_constr, idcsys_t *h_x1_constr) {
#ifdef ROBRUN_DEBUG_L2
		printf("debug idrobrun_add_at: step1 = %d, loop_id1=%d, step0 = %d, loop_id0 = %d, x0_offset = %d, u0_offset = %d, x1_offset = %d\n",
				step1, loop_id1, step0,loop_id0,x0_offset,u0_offset,x1_offset);
#endif

	int i, n = obj->m_n, m = obj->m_m;

	int numnz, *h_ind, s_offset;
	double rhs, *h_val;
	char sense;

	if (x1_offset < 0) {
		x1_offset = obj->m_grb_nvars;
		grb_mk_state_variable_array(obj->mp_model,step1,loop_id1,n);
		obj->m_grb_nvars+=n;
		if (step1>0) {
			s_offset = obj->m_grb_nvars;
			grb_mk_positive_slack_variable_with_step(obj->mp_model,step1,loop_id1,-1.0);
			obj->m_grb_nvars+=1;
		}

		for(i=0;i<h_x1_constr->nconstrs(h_x1_constr);i++) {
			h_x1_constr->get_at(h_x1_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			if ((step1==0)||(sense==GRB_EQUAL)) {
				grb_add_constr_with_ind_starting_at(obj->mp_model,"X",step1,loop_id1,i,
					x1_offset,numnz,h_ind,h_val,sense,rhs);
			} else {
				grb_add_lbconstr_with_slack_and_ind_starting_at(obj->mp_model,"X",step1,loop_id1,i,
					x1_offset,s_offset,numnz,h_ind,h_val,rhs);
			}
		}
	}

	if (((loop_id1==0)&&(step1>0))||(loop_id1 > 0)) {
		if (u0_offset<0) {
			u0_offset = obj->m_grb_nvars;
			grb_mk_input_variable_array(obj->mp_model,step0,loop_id0,m);
			obj->m_grb_nvars+=m;
			for(i=0;i<h_u0_constr->nconstrs(h_u0_constr);i++) {
				h_u0_constr->get_at(h_u0_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
				grb_add_constr_with_ind_starting_at(obj->mp_model,"U",step0,loop_id0,i,
						u0_offset,numnz,h_ind,h_val,sense,rhs);
			}
		}

		idrobrun_mk_dynamical_constr(obj->mp_model,step0,loop_id0,obj->m_delta,h_sys,
				x0_offset,u0_offset,x1_offset);
	}

	return x1_offset;
}

static idrobrun_t *     	idrobrun_default_constructor(double delta, idkripke_t *h_M) {
	idrobrun_t *obj = malloc(sizeof(idrobrun_t));
	assert(obj!=NULL);

	obj->destroy 		= idrobrun_destroy;
	obj->cpy			= idrobrun_cpy;
	obj->append_step	= idrobrun_append_step;
	obj->increment_at	= idrobrun_increment_at;
	obj->state			= idrobrun_state;
	obj->label			= idrobrun_label;
	obj->x				= idrobrun_x;
	obj->u				= idrobrun_u;
	obj->slack			= idrobrun_slack;
	obj->length			= idrobrun_length;
	obj->n				= idrobrun_n;
	obj->m				= idrobrun_m;
	obj->nsamples		= idrobrun_nsamples;
	obj->nsteps			= idrobrun_nsteps;
	obj->print			= idrobrun_print;
	obj->compute		= idrobrun_compute;

#ifdef IDRTL_DEBUG_MEMLEAK_ROBRUN
	static int idrobrun_last_id = 0;
	obj->m_id = idrobrun_last_id;
	printf("debug memleak construct idrobrun %d\n", obj->m_id);
	idrobrun_last_id++;
	idrobrun_destroy_cnt++;
#endif

	obj->mp_model = NULL;
	obj->mp_h_label_array = NULL;
	obj->mp_h_sys_array = NULL;
	obj->mp_h_u_constr_array = NULL;
	obj->mp_h_x_constr_array = NULL;
	obj->mp_nsample_intarray = NULL;
	obj->mp_states_intarray = NULL;
	obj->mp_x = NULL;
	obj->mp_u = NULL;
	obj->m_length = 0;
	obj->m_m = 0;
	obj->m_n = 0;
	obj->m_slack = GRB_INFINITY;
	obj->m_grb_nvars = 0;
	obj->m_nsteps = 0;
	obj->m_length_aux = 0;
	obj->m_delta = delta;
	obj->mh_M = h_M;

	return obj;
}


/* public functions */
idrobrun_t *     	idrobrun(double delta, idkripke_t *h_M) {
	idrobrun_t *obj = idrobrun_default_constructor(delta, h_M);

	obj->mp_h_label_array = idarray_byref();
	obj->mp_h_sys_array = idarray_byref();
	obj->mp_h_u_constr_array = idarray_byref();
	obj->mp_h_x_constr_array = idarray_byref();
	obj->mp_nsample_intarray = idintarray();
	obj->mp_states_intarray = idintarray();
	obj->mp_model = grb_mk_model();

	return obj;
}

idrobrun_t *     	idrobrun_cpy(idrobrun_t *obj) {
	assert(obj!=NULL);

	idrobrun_t *p_run = idrobrun_default_constructor(obj->m_delta, obj->mh_M);

	p_run->mp_model = grb_copy_model(obj->mp_model);
	p_run->mp_h_label_array = idarray_cpy(obj->mp_h_label_array);
	p_run->mp_h_sys_array = idarray_cpy(obj->mp_h_sys_array);
	p_run->mp_h_u_constr_array = idarray_cpy(obj->mp_h_u_constr_array);
	p_run->mp_h_x_constr_array = idarray_cpy(obj->mp_h_x_constr_array);
	p_run->mp_nsample_intarray = idarray_cpy(obj->mp_nsample_intarray);
	p_run->mp_states_intarray = idarray_cpy(obj->mp_states_intarray);
	p_run->m_grb_nvars = obj->m_grb_nvars;
	p_run->m_nsteps = obj->m_nsteps;
	p_run->m_n = obj->m_n;
	p_run->m_m = obj->m_m;
	p_run->m_length = 0;
	p_run->m_length_aux = obj->m_length_aux;

	return p_run;
}
void          	idrobrun_destroy(idrobrun_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_ROBRUN
		idrobrun_destroy_cnt--;
		printf("debug memleak destroy idrobrun %d still remains %d to destroy\n", obj->m_id, idrobrun_destroy_cnt);
		if (idrobrun_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_states_intarray);
		idarray_destroy(obj->mp_h_label_array);
		idarray_destroy(obj->mp_h_sys_array);
		idarray_destroy(obj->mp_h_u_constr_array);
		idarray_destroy(obj->mp_h_x_constr_array);
		idarray_destroy(obj->mp_nsample_intarray);
		GRBfreemodel(obj->mp_model);
		if (obj->mp_x!=NULL) {
			free(obj->mp_x);
		}
		if (obj->mp_u!=NULL) {
			free(obj->mp_u);
		}
		free(obj);
	}
}
/* set functions */
static int idrobrun_get_facet(idrobrun_t *obj, int s0, int s1) {
	assert(obj!=NULL);

	int *h_facet0 = (int *)obj->mh_M->mp_edges->get_at(obj->mh_M->mp_edges,s0,s1);
	assert(h_facet0!=NULL);

	return *h_facet0;
}
static void idrobrun_add_switching_constr(idrobrun_t *obj, idcsys_t *h_x_constr,
		int step, int loop_id, int facet) {
	assert(obj!=NULL);
	assert(h_x_constr!=NULL);
	int numnz, *h_ind, s_offset, x_offset;
	double *h_val, rhs;
	char sense;

	h_x_constr->get_at(h_x_constr,facet,&numnz, &h_ind, &h_val, &sense, &rhs);
	grb_del_constr_byname(obj->mp_model,"X",step,loop_id,facet);
	s_offset = obj->m_grb_nvars;
	grb_mk_positive_slack_variable_array(obj->mp_model,step,loop_id,facet,-1.0);
	obj->m_grb_nvars+=1;
	x_offset = grb_get_varnum_byname(obj->mp_model,"x",step,loop_id,0);
	grb_add_lbconstr_with_slack_and_ind_starting_at(obj->mp_model,"X",step,loop_id,facet,
			x_offset,s_offset,numnz,h_ind,h_val,rhs);
}

static void idrobrun_switching_facet_update(idrobrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_nsteps>2);


	int step1, s0, facet0, s1, loop_id1, nsamples1, x1_offset, s2, facet2, s_offset;
	idcsys_t *h_x1_constr;

	idrb_node_t *it_s, *it_nsmp, *it_xcs;

	// first step
	it_s = idrb_first(obj->mp_states_intarray->mp_head);
	it_nsmp = idrb_first(obj->mp_nsample_intarray->mp_head);
	it_xcs = idrb_first(obj->mp_h_x_constr_array->mp_head);
	step1 = 0;

	// ignore

	facet0 = -1;
	facet2 = -1;

	// next step
	it_s = idrb_next(it_s);
	it_nsmp = idrb_next(it_nsmp);
	it_xcs = idrb_next(it_xcs);
	step1++;

	s1 = *(int *)it_s->value(it_s);
	nsamples1 = *(int *)it_nsmp->value(it_nsmp);
	loop_id1 = nsamples1 - 1;
	h_x1_constr = (idcsys_t *)it_xcs->value(it_xcs);

	// next step
	it_s = idrb_next(it_s);
	it_nsmp = idrb_next(it_nsmp);
	it_xcs = idrb_next(it_xcs);

	while(it_s!=idrb_nil(obj->mp_states_intarray->mp_head)&&
			it_nsmp!=idrb_nil(obj->mp_nsample_intarray->mp_head)&&
				it_xcs!=idrb_nil(obj->mp_h_x_constr_array->mp_head)) {

		s2 = *(int *)it_s->value(it_s);

		facet2 = idrobrun_get_facet(obj,s1,s2);
		if (facet2!=facet0) {
			idrobrun_add_switching_constr(obj,h_x1_constr,step1,loop_id1,facet2);
		}

		step1++;
		s0 = s1;
		s1 = s2;
		h_x1_constr = (idcsys_t *)it_xcs->value(it_xcs);
		nsamples1 = *(int *)it_nsmp->value(it_nsmp);
		loop_id1 = 0;
		facet0 = idrobrun_get_facet(obj,s1,s0);
		idrobrun_add_switching_constr(obj,h_x1_constr,step1,loop_id1,facet0);
		loop_id1 = nsamples1 - 1;

		// next step
		it_s = idrb_next(it_s);
		it_nsmp = idrb_next(it_nsmp);
		it_xcs = idrb_next(it_xcs);
	}
}



void          	idrobrun_append_step(idrobrun_t *obj, int nsamples, int si, idlist_t *h_label,
		idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr){
#ifdef ROBRUN_DEBUG_L1
	printf("debug idrobrun_append_step: starting... nsteps = %d, add %d samples\n", obj->m_nsteps, nsamples);
	obj->print(obj,stdout);
#endif
	assert(obj!=NULL);
	assert(h_label!=NULL);
	assert(h_sys!=NULL);
	assert(h_u_constr!=NULL);
	assert(h_x_constr!=NULL);
	assert(nsamples>0);
	assert(si>=0);

	int id, x0_offset, u0_offset, x1_offset, step0, loop_id0, step1, loop_id1;
	idlinear_sys_t *h_sys0;
	idcsys_t *h_u0_constr;
	idarray_t *h_from_s0_to;
	idrb_node_t *it;
	int numnz, *h_ind;
	double *h_val, rhs;
	char sense;


	if (obj->mp_x!=NULL) {
		free(obj->mp_x);
	}
	obj->mp_x = NULL;
	if (obj->mp_u!=NULL) {
		free(obj->mp_u);
	}
	obj->mp_u = NULL;
	obj->m_length = 0;

	obj->m_length_aux += nsamples;


	if (obj->m_nsteps==0) {
		obj->m_n = h_sys->n(h_sys);
		obj->m_m = h_sys->m(h_sys);

		step1 = 0;
		loop_id1 = 0;
		h_sys0 = h_sys;
		h_u0_constr = h_u_constr;
	} else {
		assert(obj->m_n == h_sys->n(h_sys));
		assert(obj->m_m == h_sys->m(h_sys));
		step1 = obj->m_nsteps;
		loop_id1 = 0;
		step0 = obj->m_nsteps-1;
		loop_id0 = *(int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step0) - 1;
		h_sys0 = (idlinear_sys_t *)obj->mp_h_sys_array->get_at(obj->mp_h_sys_array,step0);
		h_u0_constr = (idcsys_t *)obj->mp_h_u_constr_array->get_at(obj->mp_h_u_constr_array,step0);
		x0_offset = grb_get_varnum_byname(obj->mp_model,"x",step0,loop_id0,0);
		u0_offset = grb_get_varnum_byname(obj->mp_model,"u",step0,loop_id0,0);
	}

	obj->mp_states_intarray->append(obj->mp_states_intarray, &si);
	obj->mp_h_label_array->append(obj->mp_h_label_array, h_label);
	obj->mp_h_sys_array->append(obj->mp_h_sys_array, h_sys);
	obj->mp_h_u_constr_array->append(obj->mp_h_u_constr_array, h_u_constr);
	obj->mp_h_x_constr_array->append(obj->mp_h_x_constr_array, h_x_constr);
	obj->mp_nsample_intarray->append(obj->mp_nsample_intarray, &nsamples);


	x0_offset = idrobrun_add_at(obj,step1,loop_id1,step0,loop_id0,x0_offset,u0_offset,-1,h_sys0,h_u0_constr,h_x_constr);
	for(id=1;id<nsamples;id++) {
		x0_offset = idrobrun_add_at(obj,step1,loop_id1+id,step1,loop_id1+id-1,x0_offset,-1,-1,h_sys,h_u_constr,h_x_constr);
	}
	obj->m_nsteps++;
#ifdef ROBRUN_DEBUG_L2
	int logid = rand();
	printf("debug idrobrun_append_step: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"robrun_append_step",logid);
#endif

#ifdef ROBRUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug idrobrun_append_step: END. nsteps = %d\n", obj->m_nsteps);
#endif
}
void        	idrobrun_increment_at(idrobrun_t *obj, int step, int nsamples) {
#ifdef ROBRUN_DEBUG_L1
	printf("debug idrobrun_increment_at: starting... nsteps = %d, add %d samples at step %d\n", obj->m_nsteps, nsamples, step);
#endif
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->m_nsteps));
	assert(nsamples>=0);
#ifdef ROBRUN_DEBUG_L1
	obj->print(obj,stdout);
#endif

	if (nsamples==0) {
#ifdef ROBRUN_DEBUG_L1
		printf("debug idrobrun_increment_at: END. nsteps = %d\n", obj->m_nsteps);
#endif
		return;
	}

	if (obj->mp_x!=NULL) {
		free(obj->mp_x);
	}
	obj->mp_x = NULL;
	if (obj->mp_u!=NULL) {
		free(obj->mp_u);
	}
	obj->mp_u = NULL;
	obj->m_length = 0;

	obj->m_length_aux += nsamples;

	idlinear_sys_t *h_sys;
	idcsys_t *h_u0_constr, *h_x1_constr;
	int id, *h_nsample, x0_offset, u0_offset, x1_offset, nsample;
	idarray_t *h_from_s0_to;

	h_sys = (idlinear_sys_t *)obj->mp_h_sys_array->get_at(obj->mp_h_sys_array,step);
	h_nsample = (int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step);
	nsample = *h_nsample - 1;
	h_x1_constr = (idcsys_t *)obj->mp_h_x_constr_array->get_at(obj->mp_h_x_constr_array,step);
	h_u0_constr = (idcsys_t *)obj->mp_h_u_constr_array->get_at(obj->mp_h_u_constr_array,step);
#ifdef ROBRUN_DEBUG_L1
	printf("debug idrobrun_increment_at: nsamples = %d at step %d\n", *h_nsample, step);
#endif	assert(obj!=NULL);

	x0_offset = grb_get_varnum_byname(obj->mp_model,"x",step,nsample,0);
	if (step==obj->m_nsteps-1) {
		u0_offset = -1;
	} else {
		u0_offset = grb_get_varnum_byname(obj->mp_model,"u",step,nsample,0);
		grb_del_constrs_byprefix(obj->mp_model,"D",step,nsample,2*obj->n(obj));
	}
	for(id=0;id<nsamples;id++) {
		x0_offset = idrobrun_add_at(obj,step,nsample+id+1,step,nsample+id,x0_offset,u0_offset,-1,
				h_sys,h_u0_constr,h_x1_constr);
		u0_offset = -1;
	}
	if (step<obj->m_nsteps-1) {
		x1_offset = grb_get_varnum_byname(obj->mp_model,"x",step+1,0,0);
		idrobrun_add_at(obj,step,nsample+id+1,step,nsample+id,x0_offset,u0_offset,x1_offset,
						h_sys,h_u0_constr,h_x1_constr);
	}

	(*h_nsample)+=nsamples;
#ifdef ROBRUN_DEBUG_L2
	int logid = rand();
	printf("debug idrobrun_increment_at: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"robrun_increment_at",logid);
#endif
#ifdef ROBRUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug idrobrun_increment_at: END. nsteps = %d\n", obj->m_nsteps);
#endif
}
/* get functions */
int			idrobrun_state(idrobrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return *(int *)obj->mp_states_intarray->get_at(obj->mp_states_intarray,step);
}
idlist_t *	idrobrun_label(idrobrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return (idlist_t *)obj->mp_h_label_array->get_at(obj->mp_h_label_array,step);
}
int			idrobrun_nsamples(idrobrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return *(int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step);
}
double *		idrobrun_x(idrobrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->mp_x;
}
double *		idrobrun_u(idrobrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->mp_u;
}
double 		idrobrun_slack(idrobrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->m_slack;
}
int			idrobrun_length(idrobrun_t *obj) {
	assert(obj!=NULL);

	return obj->m_length;
}
int			idrobrun_nsteps(idrobrun_t *obj){
	assert(obj!=NULL);

	return obj->m_nsteps;
}
int			idrobrun_n(idrobrun_t *obj) {
	assert(obj!=NULL);

	return obj->m_n;
}
int		idrobrun_m(idrobrun_t *obj){
	assert(obj!=NULL);

	return obj->m_m;
}
void         idrobrun_print(idrobrun_t *obj, FILE *out) {
	assert(obj!=NULL);


	idlist_t *h_label;
	idrb_node_t *it;
	int i, j, si, t, n = obj->n(obj), m = obj->m(obj), cnt, step;
	double *h_x, *h_u, v;
	char str[80];
	fprintf(out,"Path = ");
	for(i=0;i<obj->m_nsteps;i++) {
		h_label = obj->label(obj,i);
		t = obj->nsamples(obj,i);
		fprintf(out,"\t(%s)^%d", idintlist_to_str(h_label,str),t);
		if (i+1%10==0) {
			fprintf(out,"\n       ");
		}
	}
	fprintf(out,"\n");

	fprintf(out,"Run  = ");
	for(i=0;i<obj->m_nsteps;i++) {
		si = obj->state(obj,i);
		t = obj->nsamples(obj,i);
		fprintf(out,"\t(s_%d)^%d", si,t);
		if (i+1%10==0) {
			fprintf(out,"\n       ");
		}
	}
	fprintf(out,"\n");

	if (obj->m_length==0) {
		fprintf(out,"No trajectory.\n");
	} else {
		fprintf(out,"Trajectoy  = \n");
		h_x = obj->x(obj);
		h_u = obj->u(obj);
		for(i=0;i<n;i++) {
			fprintf(out,"x_%d\t", i);
		}
		for(i=0;i<m;i++) {
			fprintf(out,"u_%d\t", i);
		}
		fprintf(out,"slack\n");
		it = idrb_first(obj->mp_nsample_intarray->mp_head);
		cnt = 0;
		step = 0;
		for(i=0;i<obj->length(obj);i++) {
			if (cnt>=*(int *)it->value(it)) {
				it = idrb_next(it);
				cnt = 0;
				step++;
			}
			cnt++;
			for(j=0;j<n;j++) {
				fprintf(out,"%.3f\t", h_x[i*n+j]);
			}
			if (i<obj->length(obj)-1) {
				for(j=0;j<m;j++) {
					fprintf(out,"%.3f", h_u[i*m+j]);
					if (j<m-1) {
						fprintf(out,"\t");
					} else {
						fprintf(out,"\n");
					}
				}
			} else {
				for(j=0;j<m;j++) {
					fprintf(out,"0.000");
					if (j<m-1) {
						fprintf(out,"\t");
					} else {
						fprintf(out,"\n");
					}
				}
			}
		}
		fprintf(out,"sum(slack) = %.3f\n", obj->m_slack);
	}

}


/* operations */
void					idrobrun_compute(idrobrun_t *obj) {
#ifdef ROBRUN_DEBUG_L1
	printf("debug compute robust run: starting...\n");
	obj->print(obj,stdout);
#endif
	assert(obj!=NULL);
	assert(obj->m_nsteps>0);

	if (obj->m_length>0) {
#ifdef ROBRUN_DEBUG_L1
		obj->print(obj,stdout);
		printf("debug compute robust run: END.\n");
#endif
		return;
	}

	idrobrun_switching_facet_update(obj);

#ifdef ROBRUN_DEBUG_L2
	int logid = rand();
	printf("debug debug compute robust run: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"robrun",logid);
#endif
	assert(grb_optimize(obj->mp_model,&obj->m_slack)==GRB_OPTIMAL);
#ifdef ROBRUN_DEBUG_L2
	grb_log_solution(obj->mp_model,"robrun",logid);
#endif

	int i, j, n = obj->n(obj), m = obj->m(obj), nsteps = obj->nsteps(obj), nsamples;
	double *h_x_it, *h_u_it;

	obj->m_length = obj->m_length_aux;

#ifdef ROBRUN_DEBUG_L1
	printf("debug compute robust run: length = %d\n", obj->m_length);
	int nvars = 0, nx = 0, nu = 0;
#endif
	obj->mp_x = (double *)malloc(obj->m_length*n*sizeof(double));
	assert(obj->mp_x!=NULL);
	obj->mp_u = (double *)malloc((obj->m_length-1)*m*sizeof(double));
	assert(obj->mp_u!=NULL);
	h_x_it = obj->mp_x;
	h_u_it = obj->mp_u;

	for(i=0;i<obj->nsteps(obj);i++) {
		nsamples = obj->nsamples(obj,i);
		for(j=0;j<nsamples;j++) {
#ifdef ROBRUN_DEBUG_L1
			printf("debug compute robust run: get x_%d[%d]\n", i, j);
			nvars+=n;
			nx+=n;
			printf("debug compute robust run: num of states = %d <= %d\n", nx, obj->m_length*n);
			assert(nx<=obj->m_length*n);
#endif
			h_x_it = ((i==0)&&(j==0)?obj->mp_x:h_x_it+n);
			grb_get_state_variable_array(obj->mp_model,i,j,n,h_x_it);
			if ((i!=obj->nsteps(obj)-1)||(j!=nsamples-1)) {
#ifdef ROBRUN_DEBUG_L1
				printf("debug compute robust run: get u_%d[%d]\n", i, j);
				nvars+=m;
				nu+=m;
				printf("debug compute robust run: num of inputs = %d <= %d\n", nu, (obj->m_length-1)*m);
				assert(nu<=(obj->m_length-1)*m);
#endif
				h_u_it = ((i==0)&&(j==0)?obj->mp_u:h_u_it+m);
				grb_get_input_variable_array(obj->mp_model,i,j,m,h_u_it);
			}
		}
#ifdef ROBRUN_DEBUG_L1
		printf("debug compute robust run: nvars = %d\n", nvars);
#endif
	}
#ifdef ROBRUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug compute robust run: END.\n");
#endif

}
