/*
 * robust_run.h
 *
 *  Created on: Jul 24, 2019
 *      Author: rafael
 */

#ifndef SOLVER_ROBUST_RUN_H_
#define SOLVER_ROBUST_RUN_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idrobrun_t *    	idrobrun(double delta, idkripke_t *h_M);
extern void         	idrobrun_destroy(idrobrun_t *obj);
extern idrobrun_t *   	idrobrun_cpy(idrobrun_t *obj);

/* set functions */
extern void        		idrobrun_append_step(idrobrun_t *obj, int nselfloop, int si, idlist_t *h_label,
		idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);
extern void        		idrobrun_increment_at(idrobrun_t *obj, int step, int nsamples);

/* get functions */
extern int			idrobrun_state(idrobrun_t *obj, int step);
extern idlist_t *	idrobrun_label(idrobrun_t *obj, int step);
extern int			idrobrun_nsamples(idrobrun_t *obj, int step);
extern double *		idrobrun_x(idrobrun_t *obj);
extern double *		idrobrun_u(idrobrun_t *obj);
extern double 		idrobrun_slack(idrobrun_t *obj);
extern int			idrobrun_length(idrobrun_t *obj);
extern int			idrobrun_nsteps(idrobrun_t *obj);
extern int			idrobrun_n(idrobrun_t *obj);
extern int			idrobrun_m(idrobrun_t *obj);
extern void         idrobrun_print(idrobrun_t *obj, FILE *out);

/* operations */
extern void			idrobrun_compute(idrobrun_t *obj);

#ifdef IDRTL_DEBUG_MEMLEAK_ROBRUN
	static int		    idrobrun_destroy_cnt = 0;
#endif

struct idrobrun_s{
	/* capsulated member functions */
	void             (*destroy)(idrobrun_t *obj);
	idrobrun_t *        (*cpy)(idrobrun_t *obj);

	/* set functions */
	void          	(*append_step)(idrobrun_t *obj, int nselfloop, int si, idlist_t *h_label,
			idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);
	void			(*increment_at)(idrobrun_t *obj, int step, int nsamples);

	/* get functions */
	int					(*state)(idrobrun_t *obj, int step);
	idlist_t *			(*label)(idrobrun_t *obj, int step);
	int					(*nsamples)(idrobrun_t *obj, int step);
	double *			(*x)(idrobrun_t *obj);
	double *			(*u)(idrobrun_t *obj);
	double 				(*slack)(idrobrun_t *obj);
	int					(*length)(idrobrun_t *obj);
	int					(*nsteps)(idrobrun_t *obj);
	int					(*n)(idrobrun_t *obj);
	int					(*m)(idrobrun_t *obj);
	void				(*print)(idrobrun_t *obj, FILE *out);

	/* operations */
	void				(*compute)(idrobrun_t *obj);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_ROBRUN
	int		      m_id;
#endif
	idarray_t 		*mp_states_intarray,
					*mp_h_label_array,
					*mp_h_sys_array,
					*mp_h_u_constr_array,
					*mp_h_x_constr_array,
					*mp_nsample_intarray;
	double	*mp_x,
			*mp_u,
			m_slack,
			m_delta;
	int		m_length,
			m_length_aux,
			m_nsteps,
			m_n,
			m_m,
			m_grb_nvars;
	GRBmodel 		*mp_model;
	idkripke_t *mh_M;
};


#ifdef __cplusplus
}
#endif



#endif /* SOLVER_ROBUST_RUN_H_ */
