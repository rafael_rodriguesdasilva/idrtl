/*
 * run.c
 *
 *  Created on: Jun 6, 2019
 *      Author: rafael
 */

#include "run.h"

//#define RUN_DEBUG_L1
//#define RUN_DEBUG_L2

static int idrun_mk_dynamical_constr(GRBmodel *h_model, int step, int loop_id, int slack_id, idlinear_sys_t *h_sys,
		int x0_offset, int u0_offset, int xe_offset) {
#ifdef RUN_DEBUG_L2
	printf("debug mk_dynamical_constr: starting... step=%d, loop_id=%d\n");
#endif
	int row, col, constr_cnt,
		n = h_sys->n(h_sys),
		m = h_sys->m(h_sys);
	double v;
	int ind[n+m], numnz;
	double val[n+m];

	constr_cnt = 0;
	for(row=0;row<n;row++) {
		numnz = 0;
		for(col=0;col<n;col++) {
			v = h_sys->mp_A->get(h_sys->mp_A,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = x0_offset + col;
				val[numnz] = v;
#ifdef RUN_DEBUG_L2
				printf("debug mk_dynamical_constr: x0[%d] =  x[%d] = %.3f\n", col, ind[numnz], val[numnz]);
#endif
				numnz++;
			}
		}
		ind[numnz] = xe_offset + row;
		val[numnz] = -1.0;
#ifdef RUN_DEBUG_L2
		printf("debug mk_dynamical_constr: x1[%d] =  x[%d] = %.3f\n", row, ind[numnz], val[numnz]);
#endif
		numnz++;
		for(col=0;col<m;col++) {
			v = h_sys->mp_B->get(h_sys->mp_B,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = u0_offset+col;
				val[numnz] = v;
#ifdef RUN_DEBUG_L2
				printf("debug mk_dynamical_constr: u0[%d] =  x[%d] = %.3f\n", col, ind[numnz], val[numnz]);
#endif
				numnz++;
			}
		}
#ifdef RUN_DEBUG_L2
		printf("debug mk_dynamical_constr: numnz = %d\n", numnz);
#endif

		grb_add_inftynorm_constr_with_slack_and_ind_starting_at(h_model,"D",step,loop_id,row,0,slack_id,
				numnz,ind,val,-h_sys->mp_c->get(h_sys->mp_c,row,0));
		constr_cnt+=2;
	}

	return constr_cnt;
}

static int idrun_add_at(idrun_t *obj, int slack_id, int step1, int loop_id1,
		int step0, int loop_id0, int x0_offset, int u0_offset, int x1_offset,
		idlinear_sys_t *h_sys, idcsys_t *h_u0_constr, idcsys_t *h_x1_constr) {
#ifdef RUN_DEBUG_L2
		printf("debug idrun_add_at: step1 = %d, loop_id1=%d, step0 = %d, loop_id0 = %d, x0_offset = %d, u0_offset = %d, x1_offset = %d\n",
				step1, loop_id1, step0,loop_id0,x0_offset,u0_offset,x1_offset);
#endif

	int i, n = obj->m_n, m = obj->m_m;

	int numnz, *h_ind;
	double rhs, *h_val;
	char sense;

	if (x1_offset < 0) {
		x1_offset = obj->m_grb_nvars;
		grb_mk_state_variable_array(obj->mp_model,step1,loop_id1,n);
		obj->m_grb_nvars+=n;

		for(i=0;i<h_x1_constr->nconstrs(h_x1_constr);i++) {
			h_x1_constr->get_at(h_x1_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			grb_add_constr_with_ind_starting_at(obj->mp_model,"X",step1,loop_id1,i,
					x1_offset,numnz,h_ind,h_val,sense,rhs);
		}
	}

	if (((loop_id1==0)&&(step1>0))||(loop_id1 > 0)) {
		if (u0_offset<0) {
			u0_offset = obj->m_grb_nvars;
			grb_mk_input_variable_array(obj->mp_model,step0,loop_id0,m);
			obj->m_grb_nvars+=m;
			for(i=0;i<h_u0_constr->nconstrs(h_u0_constr);i++) {
				h_u0_constr->get_at(h_u0_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
				grb_add_constr_with_ind_starting_at(obj->mp_model,"U",step0,loop_id0,i,
						u0_offset,numnz,h_ind,h_val,sense,rhs);
			}
		}

		idrun_mk_dynamical_constr(obj->mp_model,step0,loop_id0,slack_id,h_sys,
				x0_offset,u0_offset,x1_offset);
	}

	return x1_offset;
}

static idrun_t *     	idrun_default_constructor(void) {
	idrun_t *obj = malloc(sizeof(idrun_t));
	assert(obj!=NULL);

	obj->destroy 		= idrun_destroy;
	obj->cpy			= idrun_cpy;
	obj->append_step	= idrun_append_step;
	obj->increment_at	= idrun_increment_at;
	obj->state			= idrun_state;
	obj->label			= idrun_label;
	obj->x				= idrun_x;
	obj->u				= idrun_u;
	obj->slack			= idrun_slack;
	obj->length			= idrun_length;
	obj->n				= idrun_n;
	obj->m				= idrun_m;
	obj->nsamples		= idrun_nsamples;
	obj->nsteps			= idrun_nsteps;
	obj->print			= idrun_print;
	obj->compute		= idrun_compute;

#ifdef IDRTL_DEBUG_MEMLEAK_RUN
	static int idrun_last_id = 0;
	obj->m_id = idrun_last_id;
	printf("debug memleak construct idrun %d\n", obj->m_id);
	idrun_last_id++;
	idrun_destroy_cnt++;
#endif

	obj->mp_model = NULL;
	obj->mp_h_label_array = NULL;
	obj->mp_h_sys_array = NULL;
	obj->mp_h_u_constr_array = NULL;
	obj->mp_h_x_constr_array = NULL;
	obj->mp_nsample_intarray = NULL;
	obj->mp_states_intarray = NULL;
	obj->mp_x = NULL;
	obj->mp_u = NULL;
	obj->m_length = 0;
	obj->m_m = 0;
	obj->m_n = 0;
	obj->m_slack = GRB_INFINITY;
	obj->m_grb_nvars = 0;
	obj->m_nsteps = 0;
	obj->m_length_aux = 0;

	return obj;
}


/* public functions */
idrun_t *     	idrun(void) {
	idrun_t *obj = idrun_default_constructor();

	obj->mp_h_label_array = idarray_byref();
	obj->mp_h_sys_array = idarray_byref();
	obj->mp_h_u_constr_array = idarray_byref();
	obj->mp_h_x_constr_array = idarray_byref();
	obj->mp_nsample_intarray = idintarray();
	obj->mp_states_intarray = idintarray();
	obj->mp_model = grb_mk_model();

	return obj;
}

idrun_t *     	idrun_cpy(idrun_t *obj) {
	assert(obj!=NULL);

	idrun_t *p_run = idrun_default_constructor();

	p_run->mp_model = grb_copy_model(obj->mp_model);
	p_run->mp_h_label_array = idarray_cpy(obj->mp_h_label_array);
	p_run->mp_h_sys_array = idarray_cpy(obj->mp_h_sys_array);
	p_run->mp_h_u_constr_array = idarray_cpy(obj->mp_h_u_constr_array);
	p_run->mp_h_x_constr_array = idarray_cpy(obj->mp_h_x_constr_array);
	p_run->mp_nsample_intarray = idarray_cpy(obj->mp_nsample_intarray);
	p_run->mp_states_intarray = idarray_cpy(obj->mp_states_intarray);
	p_run->m_grb_nvars = obj->m_grb_nvars;
	p_run->m_nsteps = obj->m_nsteps;
	p_run->m_n = obj->m_n;
	p_run->m_m = obj->m_m;
	p_run->m_length = 0;
	p_run->m_length_aux = obj->m_length_aux;

	return p_run;
}
void          	idrun_destroy(idrun_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_RUN
		idrun_destroy_cnt--;
		printf("debug memleak destroy idrun %d still remains %d to destroy\n", obj->m_id, idrun_destroy_cnt);
		if (idrun_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_states_intarray);
		idarray_destroy(obj->mp_h_label_array);
		idarray_destroy(obj->mp_h_sys_array);
		idarray_destroy(obj->mp_h_u_constr_array);
		idarray_destroy(obj->mp_h_x_constr_array);
		idarray_destroy(obj->mp_nsample_intarray);
		GRBfreemodel(obj->mp_model);
		if (obj->mp_x!=NULL) {
			free(obj->mp_x);
		}
		if (obj->mp_u!=NULL) {
			free(obj->mp_u);
		}
		free(obj);
	}
}
/* set functions */
void          	idrun_append_step(idrun_t *obj, int nsamples, int si, idlist_t *h_label,
		idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr){
#ifdef RUN_DEBUG_L1
	printf("debug run:append_step: starting... nsteps = %d, add %d samples\n", obj->m_nsteps, nsamples);
	obj->print(obj,stdout);
#endif
	assert(obj!=NULL);
	assert(h_label!=NULL);
	assert(h_sys!=NULL);
	assert(h_u_constr!=NULL);
	assert(h_x_constr!=NULL);
	assert(nsamples>0);
	assert(si>=0);

	int id, x0_offset, u0_offset, step0, loop_id0, step1, loop_id1;
	idlinear_sys_t *h_sys0;
	idcsys_t *h_u0_constr;

	if (obj->mp_x!=NULL) {
		free(obj->mp_x);
	}
	obj->mp_x = NULL;
	if (obj->mp_u!=NULL) {
		free(obj->mp_u);
	}
	obj->mp_u = NULL;
	obj->m_length = 0;

	obj->m_length_aux += nsamples;


	if (obj->m_nsteps==0) {
		obj->m_n = h_sys->n(h_sys);
		obj->m_m = h_sys->m(h_sys);

		grb_mk_positive_slack_variable(obj->mp_model,0,1.0);
		obj->m_grb_nvars++;
		step1 = 0;
		loop_id1 = 0;
		h_sys0 = h_sys;
		h_u0_constr = h_u_constr;
	} else {
		assert(obj->m_n == h_sys->n(h_sys));
		assert(obj->m_m == h_sys->m(h_sys));

		step1 = obj->m_nsteps;
		loop_id1 = 0;
		step0 = obj->m_nsteps-1;
		loop_id0 = *(int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step0) - 1;
		h_sys0 = (idlinear_sys_t *)obj->mp_h_sys_array->get_at(obj->mp_h_sys_array,step0);
		h_u0_constr = (idcsys_t *)obj->mp_h_u_constr_array->get_at(obj->mp_h_u_constr_array,step0);
		x0_offset = grb_get_varnum_byname(obj->mp_model,"x",step0,loop_id0,0);
		u0_offset = grb_get_varnum_byname(obj->mp_model,"u",step0,loop_id0,0);
	}


	obj->mp_states_intarray->append(obj->mp_states_intarray, &si);
	obj->mp_h_label_array->append(obj->mp_h_label_array, h_label);
	obj->mp_h_sys_array->append(obj->mp_h_sys_array, h_sys);
	obj->mp_h_u_constr_array->append(obj->mp_h_u_constr_array, h_u_constr);
	obj->mp_h_x_constr_array->append(obj->mp_h_x_constr_array, h_x_constr);
	obj->mp_nsample_intarray->append(obj->mp_nsample_intarray, &nsamples);


	x0_offset = idrun_add_at(obj,0,step1,loop_id1,step0,loop_id0,x0_offset,u0_offset,-1,h_sys0,h_u0_constr,h_x_constr);
	for(id=1;id<nsamples;id++) {
		x0_offset = idrun_add_at(obj,0,step1,loop_id1+id,step1,loop_id1+id-1,x0_offset,-1,-1,h_sys,h_u_constr,h_x_constr);
	}
	obj->m_nsteps++;
#ifdef RUN_DEBUG_L2
	int logid = rand();
	printf("debug run:append_step: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"run_append_step",logid);
#endif
#ifdef RUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug run:append_step: END. nsteps = %d\n", obj->m_nsteps);
#endif
}
void        	idrun_increment_at(idrun_t *obj, int step, int nsamples) {
#ifdef RUN_DEBUG_L1
	printf("debug run:increment_at: starting... nsteps = %d, add %d samples at step %d\n", obj->m_nsteps, nsamples, step);
#endif
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->m_nsteps));
	assert(nsamples>=0);
#ifdef RUN_DEBUG_L1
	obj->print(obj,stdout);
#endif

	if (nsamples==0) {
#ifdef RUN_DEBUG_L1
		printf("debug run:increment_at: END. nsteps = %d\n", obj->m_nsteps);
#endif
		return;
	}

	if (obj->mp_x!=NULL) {
		free(obj->mp_x);
	}
	obj->mp_x = NULL;
	if (obj->mp_u!=NULL) {
		free(obj->mp_u);
	}
	obj->mp_u = NULL;
	obj->m_length = 0;

	obj->m_length_aux += nsamples;

	idlinear_sys_t *h_sys;
	idcsys_t *h_u0_constr, *h_x1_constr;
	int id, *h_nsample, x0_offset, u0_offset, x1_offset, nsample;

	h_sys = (idlinear_sys_t *)obj->mp_h_sys_array->get_at(obj->mp_h_sys_array,step);
	h_nsample = (int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step);
	nsample = *h_nsample - 1;
	h_x1_constr = (idcsys_t *)obj->mp_h_x_constr_array->get_at(obj->mp_h_x_constr_array,step);
	h_u0_constr = (idcsys_t *)obj->mp_h_u_constr_array->get_at(obj->mp_h_u_constr_array,step);
#ifdef RUN_DEBUG_L1
	printf("debug run:increment_at: nsamples = %d at step %d\n", *h_nsample, step);
#endif	assert(obj!=NULL);

	x0_offset = grb_get_varnum_byname(obj->mp_model,"x",step,nsample,0);
	if (step==obj->m_nsteps-1) {
		u0_offset = -1;
	} else {
		u0_offset = grb_get_varnum_byname(obj->mp_model,"u",step,nsample,0);
		grb_del_constrs_byprefix(obj->mp_model,"D",step,nsample,2*obj->n(obj));
	}
	for(id=0;id<nsamples;id++) {
		x0_offset = idrun_add_at(obj,0,step,nsample+id+1,step,nsample+id,x0_offset,u0_offset,-1,
				h_sys,h_u0_constr,h_x1_constr);
		u0_offset = -1;
	}
	if (step<obj->m_nsteps-1) {
		x1_offset = grb_get_varnum_byname(obj->mp_model,"x",step+1,0,0);
		idrun_add_at(obj,0,step,nsample+id+1,step,nsample+id,x0_offset,u0_offset,x1_offset,
						h_sys,h_u0_constr,h_x1_constr);
	}

	(*h_nsample)+=nsamples;
#ifdef RUN_DEBUG_L2
	int logid = rand();
	printf("debug run:increment_at: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"run_increment_at",logid);
#endif
#ifdef RUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug run:increment_at: END. nsteps = %d\n", obj->m_nsteps);
#endif
}
/* get functions */
int			idrun_state(idrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return *(int *)obj->mp_states_intarray->get_at(obj->mp_states_intarray,step);
}
idlist_t *	idrun_label(idrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return (idlist_t *)obj->mp_h_label_array->get_at(obj->mp_h_label_array,step);
}
int			idrun_nsamples(idrun_t *obj, int step) {
	assert(obj!=NULL);
	assert((0<=step)&&(step<obj->nsteps(obj)));

	return *(int *)obj->mp_nsample_intarray->get_at(obj->mp_nsample_intarray,step);
}
double *		idrun_x(idrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->mp_x;
}
double *		idrun_u(idrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->mp_u;
}
double 		idrun_slack(idrun_t *obj) {
	assert(obj!=NULL);
	assert(obj->m_length>0);

	return obj->m_slack;
}
int			idrun_length(idrun_t *obj) {
	assert(obj!=NULL);

	return obj->m_length;
}
int			idrun_nsteps(idrun_t *obj){
	assert(obj!=NULL);

	return obj->m_nsteps;
}
int			idrun_n(idrun_t *obj) {
	assert(obj!=NULL);

	return obj->m_n;
}
int		idrun_m(idrun_t *obj){
	assert(obj!=NULL);

	return obj->m_m;
}
void         idrun_print(idrun_t *obj, FILE *out) {
	assert(obj!=NULL);


	idlist_t *h_label;
	idlinear_sys_t *h_sys;
	idrb_node_t *it;
	int i, j, si, t, n = obj->n(obj), m = obj->m(obj), cnt, step;
	double *h_x, *h_u, v;
	char str[80];
	fprintf(out,"Path = ");
	for(i=0;i<obj->m_nsteps;i++) {
		h_label = obj->label(obj,i);
		t = obj->nsamples(obj,i);
		fprintf(out,"\t(%s)^%d", idintlist_to_str(h_label,str),t);
		if (i+1%10==0) {
			fprintf(out,"\n       ");
		}
	}
	fprintf(out,"\n");

	fprintf(out,"Run  = ");
	for(i=0;i<obj->m_nsteps;i++) {
		si = obj->state(obj,i);
		t = obj->nsamples(obj,i);
		fprintf(out,"\t(s_%d)^%d", si,t);
		if (i+1%10==0) {
			fprintf(out,"\n       ");
		}
	}
	fprintf(out,"\n");

	if (obj->m_length==0) {
		fprintf(out,"No trajectory.\n");
	} else {
		fprintf(out,"Trajectoy  = \n");
		h_x = obj->x(obj);
		h_u = obj->u(obj);
		for(i=0;i<n;i++) {
			fprintf(out,"x_%d\t", i);
		}
		for(i=0;i<m;i++) {
			fprintf(out,"u_%d\t", i);
		}
		fprintf(out,"slack\n");
		it = idrb_first(obj->mp_nsample_intarray->mp_head);
		cnt = 0;
		step = 0;
		for(i=0;i<obj->length(obj);i++) {
			if (cnt>=*(int *)it->value(it)) {
				it = idrb_next(it);
				cnt = 0;
				step++;
			}
			cnt++;
			h_sys = (idlinear_sys_t *)obj->mp_h_sys_array->get_at(obj->mp_h_sys_array,step);
			for(j=0;j<n;j++) {
				fprintf(out,"%.3f\t", h_x[i*n+j]);
			}
			if (i<obj->length(obj)-1) {
				for(j=0;j<m;j++) {
					fprintf(out,"%.3f\t", h_u[i*m+j]);
				}
				v = h_sys->infnorm(h_sys,h_x + i*n,h_u + i*m,h_x + (i+1)*n);
				fprintf(out,"%.3f\n", v);
			} else {
				for(j=0;j<m;j++) {
					fprintf(out,"0.000\t0.000\n");
				}
			}
		}
		fprintf(out,"slack = %.3f\n", obj->m_slack);
	}

}


/* operations */
void					idrun_compute(idrun_t *obj) {
#ifdef RUN_DEBUG_L1
	printf("debug compute run: starting...\n");
	obj->print(obj,stdout);
#endif
	assert(obj!=NULL);
	assert(obj->m_nsteps>0);

	if (obj->m_length>0) {
#ifdef RUN_DEBUG_L1
		obj->print(obj,stdout);
		printf("debug compute run: END.\n");
#endif
		return;
	}

	assert(grb_optimize(obj->mp_model,&obj->m_slack)==GRB_OPTIMAL);
#ifdef RUN_DEBUG_L2
	int logid = rand();
	printf("debug debug compute run: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"run",logid);
	grb_log_solution(obj->mp_model,"run",logid);
#endif

	int i, j, n = obj->n(obj), m = obj->m(obj), nsteps = obj->nsteps(obj), nsamples;
	double *h_x_it, *h_u_it;

	obj->m_length = obj->m_length_aux;

#ifdef RUN_DEBUG_L1
	printf("debug compute run: length = %d\n", obj->m_length);
	int nvars = 0, nx = 0, nu = 0;
#endif
	obj->mp_x = (double *)malloc(obj->m_length*n*sizeof(double));
	assert(obj->mp_x!=NULL);
	obj->mp_u = (double *)malloc((obj->m_length-1)*m*sizeof(double));
	assert(obj->mp_u!=NULL);
	h_x_it = obj->mp_x;
	h_u_it = obj->mp_u;

	for(i=0;i<obj->nsteps(obj);i++) {
		nsamples = obj->nsamples(obj,i);
		for(j=0;j<nsamples;j++) {
#ifdef RUN_DEBUG_L1
			printf("debug compute run: get x_%d[%d]\n", i, j);
			nvars+=n;
			nx+=n;
			printf("debug compute run: num of states = %d <= %d\n", nx, obj->m_length*n);
			assert(nx<=obj->m_length*n);
#endif
			h_x_it = ((i==0)&&(j==0)?obj->mp_x:h_x_it+n);
			grb_get_state_variable_array(obj->mp_model,i,j,n,h_x_it);
			if ((i!=obj->nsteps(obj)-1)||(j!=nsamples-1)) {
#ifdef RUN_DEBUG_L1
				printf("debug compute run: get u_%d[%d]\n", i, j);
				nvars+=m;
				nu+=m;
				printf("debug compute run: num of inputs = %d <= %d\n", nu, (obj->m_length-1)*m);
				assert(nu<=(obj->m_length-1)*m);
#endif
				h_u_it = ((i==0)&&(j==0)?obj->mp_u:h_u_it+m);
				grb_get_input_variable_array(obj->mp_model,i,j,m,h_u_it);
			}
		}
#ifdef RUN_DEBUG_L1
		printf("debug compute run: nvars = %d\n", nvars);
#endif
	}
#ifdef RUN_DEBUG_L1
	obj->print(obj,stdout);
	printf("debug compute run: END.\n");
#endif

}
