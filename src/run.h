/*
 * run.h
 *
 *  Created on: Jun 6, 2019
 *      Author: rafael
 */

#ifndef RUN_H_
#define RUN_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idrun_t *    idrun(void);
extern void         idrun_destroy(idrun_t *obj);
extern idrun_t *    idrun_cpy(idrun_t *obj);

/* set functions */
extern void        	idrun_append_step(idrun_t *obj, int nselfloop, int si, idlist_t *h_label,
		idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);
extern void        	idrun_increment_at(idrun_t *obj, int step, int nsamples);

/* get functions */
extern int			idrun_state(idrun_t *obj, int step);
extern idlist_t *	idrun_label(idrun_t *obj, int step);
extern int			idrun_nsamples(idrun_t *obj, int step);
extern double *		idrun_x(idrun_t *obj);
extern double *		idrun_u(idrun_t *obj);
extern double 		idrun_slack(idrun_t *obj);
extern int			idrun_length(idrun_t *obj);
extern int			idrun_nsteps(idrun_t *obj);
extern int			idrun_n(idrun_t *obj);
extern int			idrun_m(idrun_t *obj);
extern void         idrun_print(idrun_t *obj, FILE *out);

/* operations */
extern void			idrun_compute(idrun_t *obj);

#ifdef IDRTL_DEBUG_MEMLEAK_RUN
	static int		    idrun_destroy_cnt = 0;
#endif

struct idrun_s{
	/* capsulated member functions */
	void             (*destroy)(idrun_t *obj);
	idrun_t *        (*cpy)(idrun_t *obj);

	/* set functions */
	void          	(*append_step)(idrun_t *obj, int nselfloop, int si, idlist_t *h_label,
			idlinear_sys_t *h_sys, idcsys_t *h_u_constr, idcsys_t *h_x_constr);
	void			(*increment_at)(idrun_t *obj, int step, int nsamples);

	/* get functions */
	int					(*state)(idrun_t *obj, int step);
	idlist_t *			(*label)(idrun_t *obj, int step);
	int					(*nsamples)(idrun_t *obj, int step);
	double *			(*x)(idrun_t *obj);
	double *			(*u)(idrun_t *obj);
	double 				(*slack)(idrun_t *obj);
	int					(*length)(idrun_t *obj);
	int					(*nsteps)(idrun_t *obj);
	int					(*n)(idrun_t *obj);
	int					(*m)(idrun_t *obj);
	void				(*print)(idrun_t *obj, FILE *out);

	/* operations */
	void				(*compute)(idrun_t *obj);

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_RUN
	int		      m_id;
#endif
	idarray_t 		*mp_states_intarray,
					*mp_h_label_array,
					*mp_h_sys_array,
					*mp_h_u_constr_array,
					*mp_h_x_constr_array,
					*mp_nsample_intarray;
	double	*mp_x,
			*mp_u,
			m_slack;
	int		m_length,
			m_length_aux,
			m_nsteps,
			m_n,
			m_m,
			m_grb_nvars;
	GRBmodel 		*mp_model;
};


#ifdef __cplusplus
}
#endif


#endif /* RUN_H_ */
