/*
 * selfloop_feasibility_search.c
 *
 *  Created on: Jul 12, 2019
 *      Author: rafael
 */

#include "selfloop_feasibility_search.h"

//#define IDSLFEAS_DEBUG_L1
//#define IDSLFEAS_DEBUG_L2
//#define IDSLFEAS_DEBUG_L3

idslfeas_lp_t * 	idslfeas_lp(double delta, double delta_progress,
		idlinear_sys_t *h_sys,
		idcsys_t *h_u_constr, idcsys_t *h_x0_constr,
		idcsys_t *h_x_constr, idcsys_t *h_xe_constr) {

	malloc(h_sys!=NULL);
	malloc(h_u_constr!=NULL);
	malloc(h_x0_constr!=NULL);
	malloc(h_x_constr!=NULL);
	malloc(h_xe_constr!=NULL);

	idslfeas_lp_t *obj = malloc(sizeof(idslfeas_lp_t));
	malloc(obj!=NULL);

	obj->cpy = idslfeas_lp_cpy;
	obj->destroy = idslfeas_lp_destroy;
	obj->go_on = idslfeas_lp_go_on;
	obj->is_feasible = idslfeas_lp_is_feasible;

	#ifdef IDRTL_DEBUG_MEMLEAK_SLFEAS_LP
	static int idslfeas_lp_last_id = 0;
	obj->m_id = idslfeas_lp_last_id;
	printf("debug memleak construct feas_lp %d\n", obj->m_id);
	idslfeas_lp_last_id++;
	idslfeas_lp_destroy_cnt++;
	#endif
	obj->mh_sys = h_sys;
	obj->m_nsample = 0;
	obj->m_nvars = 0;
	obj->mp_model = NULL;
	obj->m_delta = delta;
	obj->m_delta_progress = delta_progress;
	obj->m_objval = GRB_INFINITY;
	obj->mh_u_constr = h_u_constr;
	obj->mh_x0_constr = h_x0_constr;
	obj->mh_x_constr = h_x_constr;
	obj->mh_xe_constr = h_xe_constr;
	obj->mp_model = grb_mk_model();

	return obj;
}
void      	idslfeas_lp_destroy(idslfeas_lp_t *obj) {
	int i;
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_SLFEAS_LP
		idslfeas_lp_destroy_cnt--;
		printf("debug memleak destroy idslfeas_lp %d still remains %d to destroy\n", obj->m_id, idslfeas_lp_destroy_cnt);
		if (idslfeas_lp_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		GRBfreemodel(obj->mp_model);
		free(obj);
	}
}
idslfeas_lp_t * 	idslfeas_lp_cpy(idslfeas_lp_t *obj) {
	assert(obj!=NULL);
	idslfeas_lp_t *feas = idslfeas_lp(obj->m_delta,obj->m_delta_progress,obj->mh_sys,
			obj->mh_u_constr,obj->mh_x0_constr,obj->mh_x_constr,obj->mh_xe_constr);

	return feas;
}

static int idslfeas_lp_mk_dynamical_constr(idslfeas_lp_t *obj, int slack_id) {
	int row, col, constr_cnt,
		nsample = obj->m_nsample,
		n = obj->mh_sys->n(obj->mh_sys),
		m = obj->mh_sys->m(obj->mh_sys);
	double v;
	int ind[n+m], numnz;
	double val[n+m];

	constr_cnt = 0;
	for(row=0;row<n;row++) {
		numnz = 0;
		for(col=0;col<n;col++) {
			v = obj->mh_sys->mp_A->get(obj->mh_sys->mp_A,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = obj->m_x_offset + col;
				val[numnz] = v;
				numnz++;
			}
		}
		ind[numnz] = obj->m_xe_offset + row;
		val[numnz] = -1.0;
		numnz++;
		for(col=0;col<m;col++) {
			v = obj->mh_sys->mp_B->get(obj->mh_sys->mp_B,row,col);
			if (fabs(v)>=ID_ABSTOL) {
				ind[numnz] = obj->m_u_offset+col;
				val[numnz] = v;
				numnz++;
			}
		}
		grb_add_inftynorm_constr_with_slack_and_ind_starting_at(obj->mp_model,"D",nsample,0,row,0,slack_id,numnz,ind,val,
				-obj->mh_sys->mp_c->get(obj->mh_sys->mp_c,row,0));
		constr_cnt+=2;
	}

	return constr_cnt;
}
idbool_t   	idslfeas_lp_is_feasible(idslfeas_lp_t *obj) {
	assert(obj!=NULL);
	return (obj->m_objval <= obj->m_delta?TRUE:FALSE);
}
idbool_t   	idslfeas_lp_go_on(idslfeas_lp_t *obj, int *h_nsamples_inout, int nsamples_max) {
#ifdef IDSLFEAS_DEBUG_L1
	printf("debug self-loop of linear system go on the search: last nsample = %d\n", obj->m_nsample);
#endif
	assert(obj!=NULL);
	assert(h_nsamples_inout!=NULL);

	int i, j,
		n = obj->mh_sys->m_n,
		m = obj->mh_sys->m_m;
	iddblmatrix_t *p_An;
	double objval;
	int numnz, *h_ind;
	double rhs, *h_val;
	char sense;

	if (obj->m_nsample==0) {
#ifdef IDSLFEAS_DEBUG_L1
		printf("debug self-loop of linear system go on the search: initialize\n");
#endif

		// adds a slack variable which must be positive and our objective is to minimize it
		obj->m_nvars = 0;
		grb_mk_positive_slack_variable(obj->mp_model,0,1.0);
		obj->m_nvars++;
		obj->m_x0_offset = obj->m_nvars;
		obj->m_x_offset = obj->m_nvars;
		obj->m_xe_offset = obj->m_nvars;
		grb_mk_state_variable_array(obj->mp_model,0,0,n);
		obj->m_nvars += n;
		obj->m_constr_offset = 0;

		for(i=0;i<obj->mh_x0_constr->nconstrs(obj->mh_x0_constr);i++) {
			obj->mh_x0_constr->get_at(obj->mh_x0_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			grb_add_constr_with_ind_starting_at(obj->mp_model,"X",0,0,i,obj->m_x0_offset,numnz,h_ind,h_val,sense,rhs);
			obj->m_constr_offset++;
		}

		for(j=0;j<*h_nsamples_inout-1;j++) {
			obj->m_u_offset = obj->m_nvars;
			grb_mk_input_variable_array(obj->mp_model,j,0,m);
			for(i=0;i<obj->mh_u_constr->nconstrs(obj->mh_u_constr);i++) {
				obj->mh_u_constr->get_at(obj->mh_u_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
				grb_add_constr_with_ind_starting_at(obj->mp_model,"U",obj->m_nsample,0,i,obj->m_u_offset,numnz,h_ind,h_val,sense,rhs);
				obj->m_constr_offset++;
			}
			obj->m_nvars += m;

			obj->m_nsample++;

			obj->m_xe_offset = obj->m_nvars;
			grb_mk_state_variable_array(obj->mp_model,j+1,0,n);
			for(i=0;i<obj->mh_x_constr->nconstrs(obj->mh_x_constr);i++) {
				obj->mh_x_constr->get_at(obj->mh_x_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
				grb_add_constr_with_ind_starting_at(obj->mp_model,"X",obj->m_nsample,0,i,obj->m_xe_offset,numnz,h_ind,h_val,sense,rhs);
				obj->m_constr_offset++;
			}
			obj->m_nvars += n;

			obj->m_constr_offset += idslfeas_lp_mk_dynamical_constr(obj,0);
			obj->m_x_offset = obj->m_xe_offset;
		}

	} else {
#ifdef IDSLFEAS_DEBUG_L1
		printf("debug self-loop of linear system go on the search: update dynamic constraint parameters\n");
#endif

		// remove previous dynamical constraints
		grb_del_constrs_byprefix(obj->mp_model,"X",obj->m_nsample,0,obj->mh_xe_constr->nconstrs(obj->mh_xe_constr));
		for(i=0;i<obj->mh_x_constr->nconstrs(obj->mh_x_constr);i++) {
			obj->mh_x_constr->get_at(obj->mh_x_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
			grb_add_constr_with_ind_starting_at(obj->mp_model,"X",obj->m_nsample,0,i,obj->m_xe_offset,numnz,h_ind,h_val,sense,rhs);
			obj->m_constr_offset++;
		}
		obj->m_x_offset = obj->m_xe_offset;
	}



#ifdef IDSLFEAS_DEBUG_L1
	printf("debug self-loop of linear system go on the search: add dynamic constraints of last state\n");
#endif
	obj->m_u_offset = obj->m_nvars;
	grb_mk_input_variable_array(obj->mp_model,obj->m_nsample,0,m);
	for(i=0;i<obj->mh_u_constr->nconstrs(obj->mh_u_constr);i++) {
		obj->mh_u_constr->get_at(obj->mh_u_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
		grb_add_constr_with_ind_starting_at(obj->mp_model,"U",obj->m_nsample,0,i,obj->m_u_offset,numnz,h_ind,h_val,sense,rhs);
		obj->m_constr_offset++;
	}
	obj->m_nvars += m;

	obj->m_nsample++;
	*h_nsamples_inout = obj->m_nsample;

	obj->m_xe_offset = obj->m_nvars;
	grb_mk_state_variable_array(obj->mp_model,obj->m_nsample,0,n);
	obj->m_nvars += n;
	for(i=0;i<obj->mh_xe_constr->nconstrs(obj->mh_xe_constr);i++) {
		obj->mh_xe_constr->get_at(obj->mh_xe_constr,i,&numnz,&h_ind,&h_val,&sense,&rhs);
		grb_add_constr_with_ind_starting_at(obj->mp_model,"X",obj->m_nsample,0,i,obj->m_xe_offset,numnz,h_ind,h_val,sense,rhs);
		obj->m_constr_offset++;
	}

	idslfeas_lp_mk_dynamical_constr(obj,0);

	assert(grb_optimize(obj->mp_model,&objval)==GRB_OPTIMAL);
#ifdef IDSLFEAS_DEBUG_L1
		printf("debug self-loop of linear system go on the search: objval = %.3f; thus, the progress is %.6f.\n", objval, obj->m_objval-objval);
#endif

#ifdef IDSLFEAS_DEBUG_L3
	int logid = rand();
	printf("debug self-loop of linear system go on the search: logid = %d END.\n", logid);
	grb_log_lp_problem(obj->mp_model,"feas",logid);
	grb_log_solution(obj->mp_model,"feas",logid);
#endif

	if (objval<=obj->m_delta) {
#ifdef IDSLFEAS_DEBUG_L1
		printf("debug self-loop of linear system go on the search : objval = %.3f <= %.3f = delta.\n", objval, obj->m_delta);
		printf("debug self-loop of linear system go on the search : STOP at nsample = %d because it is feasible END.\n", obj->m_nsample);
#endif
		obj->m_objval = objval;
		return FALSE;
	} else if (obj->m_objval-objval<obj->m_delta_progress) {
#ifdef IDSLFEAS_DEBUG_L1
		printf("debug self-loop of linear system go on the search : progress = %.6f < %.6f delta_progress.\n", obj->m_objval-objval, obj->m_delta_progress);
		printf("debug self-loop of linear system go on the search : STOP at nsample = %d because it of no progress END.\n", obj->m_nsample);
#endif
		obj->m_objval = objval;
		return FALSE;
	}
#ifdef IDSLFEAS_DEBUG_L1
	printf("debug self-loop of linear system go on the search : CONTINUE at nsample = %d END.\n", obj->m_nsample);
#endif
	obj->m_objval = objval;
	return TRUE;
}
