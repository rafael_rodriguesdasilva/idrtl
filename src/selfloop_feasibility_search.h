/*
 * selfloop_feasibility_search.h
 *
 *  Created on: Jul 12, 2019
 *      Author: rafael
 */

#ifndef SOLVER_SELFLOOP_FEASIBILITY_SEARCH_H_
#define SOLVER_SELFLOOP_FEASIBILITY_SEARCH_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idslfeas_lp_t * 	idslfeas_lp(double delta, double delta_progress, idlinear_sys_t *h_sys,
		idcsys_t *h_u_constr, idcsys_t *h_x0_constr,
		idcsys_t *h_x_constr, idcsys_t *h_xe_constr);
extern void      		idslfeas_lp_destroy(idslfeas_lp_t *obj);
extern idslfeas_lp_t * 	idslfeas_lp_cpy(idslfeas_lp_t *obj);

extern idbool_t   	idslfeas_lp_go_on(idslfeas_lp_t *obj, int *h_nsamples_inout, int nsamples_max);
extern idbool_t   	idslfeas_lp_is_feasible(idslfeas_lp_t *obj);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_SLFEAS_LP
	static int		    idslfeas_lp_destroy_cnt = 0;
#endif

	struct idslfeas_lp_s{
		/* capsulated member functions */
		void           	(*destroy)(idslfeas_lp_t *obj);
		idslfeas_lp_t *		(*cpy)(idslfeas_lp_t *obj);

		idbool_t   	(*go_on)(idslfeas_lp_t *obj, int *h_nsamples_inout, int nsamples_max);
		idbool_t   		(*is_feasible)(idslfeas_lp_t *obj);

		/* operation */

		/* private variables - do not access directly */
	#ifdef IDRTL_DEBUG_MEMLEAK_SLFEAS_LP
		int		      m_id;
	#endif
		idlinear_sys_t *mh_sys;
		int				m_nsample,
						m_nvars,
						m_constr_offset,
						m_x0_offset,
						m_x_offset,
						m_xe_offset,
						m_u_offset;
		double 			m_delta,
						m_delta_progress,
						m_objval;
		idcsys_t 	*mh_u_constr,
							*mh_x0_constr,
							*mh_x_constr,
							*mh_xe_constr;

		GRBmodel 		*mp_model;
	};

#ifdef __cplusplus
}
#endif

#endif /* SOLVER_SELFLOOP_FEASIBILITY_SEARCH_H_ */
