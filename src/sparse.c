/*
 * sparse.c
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#include "sparse.h"

static void idsparse_initialize(idsparse_t *obj) {
	assert(obj!=NULL);

	for(int i=0; i<obj->numnz(obj); i++) {
		obj->mp_ind[i] = -1;
	}
}

static idbool_t idsparse_exist_value_at(idsparse_t *obj, int i) {
	assert(obj!=NULL);
	assert((0<=i)&&(i<obj->numnz(obj)));

	return (obj->mp_ind[i]<0?FALSE:TRUE);
}

static idsparse_t *idsparse_default_constructor(int nvars, int numnz, size_t sizeof_value) {
	idsparse_t *obj = malloc(sizeof(idsparse_t));
	assert(obj!=NULL);

	obj->destroy = idsparse_destroy;
	obj->cpy 	= idsparse_cpy;
	obj->set_at	= NULL;
	obj->nvars 	= idsparse_nvars;
	obj->numnz 	= idsparse_numnz;
	obj->ind 	= idsparse_ind;
	obj->values 	= idsparse_values;
	obj->pointers 	= idsparse_pointers;
	obj->upd_numnz		= idsparse_upd_numnz;
	obj->upd_nvars		= idsparse_upd_nvars;

#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	static int idsparse_last_id = 0;
	obj->m_id = idsparse_last_id;
	printf("debug memleak construct sparse %d\n", obj->m_id);
	idsparse_last_id++;
	idsparse_destroy_cnt++;
#endif
	obj->m_nvars 		= nvars;
	obj->m_numnz 		= numnz;
	obj->m_sizeof_value	= sizeof_value;
	obj->mp_ind			= malloc(obj->m_numnz*sizeof(int));
	assert(obj->mp_ind!=NULL);
	obj->mp_val			= NULL;
	obj->mpp_val		= NULL;
	obj->mh_cpy_value 	= NULL;
	obj->mh_destroy_value = NULL;

	idsparse_initialize(obj);

	return obj;
}


/* public functions */
idsparse_t *     	idsparse_byvalue(int nvars, int numnz, size_t sizeof_value) {
	idsparse_t *obj = idsparse_default_constructor(nvars,numnz,sizeof_value);

	obj->m_sizeof_value = sizeof_value;
	obj->mh_destroy_value = free;
	obj->set_at = idsparse_set_at_byvalue;
	obj->mp_val			= malloc(obj->m_numnz*obj->m_sizeof_value);
	assert(obj->mp_val!=NULL);

	return obj;
}
idsparse_t *     	idsparse_byptr(int nvars, int numnz, void (*destroy_value)(void *value), void* (*cpy_value)(void *value)) {
	assert(cpy_value!=NULL);
	idsparse_t *obj = idsparse_default_constructor(nvars,numnz,sizeof(void *));

	obj->mh_cpy_value = cpy_value;
	obj->set_at = idsparse_set_at_byref;
	if (destroy_value==NULL) {
		obj->mh_destroy_value = free;
	} else {
		obj->mh_destroy_value = destroy_value;
	}
	obj->mpp_val		= malloc(obj->m_numnz*sizeof(void *));
	assert(obj->mpp_val!=NULL);


	return obj;
}
idsparse_t *     	idsparse_byref(int nvars, int numnz) {
	idsparse_t *obj = idsparse_default_constructor(nvars,numnz,sizeof(void *));

	obj->mh_cpy_value = idref_cpy;
	obj->set_at = idsparse_set_at_byref;
	obj->mpp_val		= malloc(obj->m_numnz*sizeof(void *));
	assert(obj->mpp_val!=NULL);

	return obj;
}
void             idsparse_destroy(idsparse_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
		idsparse_destroy_cnt--;
		printf("debug memleak destroy sparse %d still remains %d to destroy\n", obj->m_id, idsparse_destroy_cnt);
		if (idsparse_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif

		if (obj->mp_val!=NULL) {
			free(obj->mp_val);
		}
		if (obj->mpp_val!=NULL) {
			if (obj->mh_destroy_value!=NULL) {
				for(int i=0;i<obj->m_numnz;i++) {
					obj->mh_destroy_value(obj->mpp_val[i]);
				}
			}
			free(obj->mpp_val);
		}
		free(obj->mp_ind);
		free(obj);
	}
}
idsparse_t *     	idsparse_cpy(idsparse_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it;
	idsparse_t *lst = idsparse_default_constructor(obj->nvars(obj),obj->numnz(obj),obj->m_sizeof_value);

	lst->mh_cpy_value = obj->mh_cpy_value;
	lst->mh_destroy_value = obj->mh_destroy_value;
	lst->set_at = obj->set_at;

	memcpy(lst->mp_ind,obj->mp_ind,obj->m_numnz*sizeof(int));
	if (obj->mp_val!=NULL) {
		lst->mp_val			= malloc(lst->m_numnz*lst->m_sizeof_value);
		assert(lst->mp_val!=NULL);
		memcpy(lst->mp_val,obj->mp_val,obj->m_numnz*obj->m_sizeof_value);
	}
	if (obj->mpp_val!=NULL) {
		lst->mpp_val		= malloc(lst->m_numnz*sizeof(void *));
		assert(lst->mpp_val!=NULL);
		for(int i=0;i<obj->numnz(obj);i++) {
			assert(lst->mh_cpy_value!=NULL);
			lst->mpp_val[i] = obj->mh_cpy_value(obj->mpp_val[i]);
		}
	}

	return lst;
}

/* set functions */
void            idsparse_set_at_byref(idsparse_t *obj, int i, int ind, void *p_value) {
	assert(obj!=NULL);
	assert((0<=i)&&(i<obj->numnz(obj)));
	assert((0<=ind)&&(ind<obj->nvars(obj)));

	if ((idsparse_exist_value_at(obj,i))&&(obj->mh_destroy_value!=NULL)) {
		obj->mh_destroy_value(obj->mpp_val[i]);
	}

	obj->mp_ind[i] = ind;
	obj->mpp_val[i] = p_value;
}
void            idsparse_set_at_byvalue(idsparse_t *obj, int i, int ind, void *h_value) {
	assert(obj!=NULL);
	assert((0<=i)&&(i<obj->numnz(obj)));
	assert((0<=ind)&&(ind<obj->nvars(obj)));

	obj->mp_ind[i] = ind;
	memcpy(obj->mp_val+i*obj->m_sizeof_value,h_value,obj->m_sizeof_value);
}

/* get functions */
int           idsparse_nvars(idsparse_t *obj) {
	assert(obj!=NULL);
	return obj->m_nvars;
}
int           	idsparse_numnz(idsparse_t *obj) {
	assert(obj!=NULL);

	return obj->m_numnz;
}
int *          idsparse_ind(idsparse_t *obj) {
	assert(obj!=NULL);
	return obj->mp_ind;
}
void * idsparse_values(idsparse_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_val!=NULL);
	return obj->mp_val;
}
void ** idsparse_pointers(idsparse_t *obj) {
	assert(obj!=NULL);
	assert(obj->mpp_val!=NULL);
	return obj->mpp_val;
}
void            	idsparse_upd_numnz(idsparse_t *obj, int numnz_new) {
	assert(obj!=NULL);
	assert((0<numnz_new)&&(numnz_new<=obj->nvars(obj)));
	if (obj->mp_val!=NULL) {
		obj->mp_val = realloc(obj->mp_val,numnz_new*obj->m_sizeof_value);
	}
	if (obj->mpp_val!=NULL) {
		if (obj->mh_destroy_value!=NULL) {
			for(int i=numnz_new;i<obj->m_numnz;i++) {
				obj->mh_destroy_value(obj->mpp_val[i]);
			}
		}
		obj->mpp_val = realloc(obj->mp_val,numnz_new*sizeof(void *));
	}
	obj->m_numnz = numnz_new;

}
void            	idsparse_upd_nvars(idsparse_t *obj, int nvars_new) {
	assert(obj!=NULL);
	obj->m_nvars = nvars_new;
}
