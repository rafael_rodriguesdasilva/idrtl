/*
 * sparse.h
 *
 * sparse data type is a sparse vector with nvars dimension and numnz nonzero values.
 *
 * Dependences: none
 *
 *  Created on: Jul 26, 2019
 *      Author: rafael
 */

#ifndef SOLVER_SPARSE_H_
#define SOLVER_SPARSE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
// construct a sparse vector with dimension nvars and numnz length with values passed by reference.
extern idsparse_t *    	idsparse_byvalue(int nvars, int numnz, size_t sizeof_value);
// construct a sparse vector with dimension nvars and numnz length with values are pointers.
extern idsparse_t *    	idsparse_byptr(int nvars, int numnz, void (*destroy_value)(void *value), void* (*cpy_value)(void *value));
// construct a sparse vector with dimension nvars and numnz length with values are reference.
extern idsparse_t *    	idsparse_byref(int nvars, int numnz);
extern void            	idsparse_destroy(idsparse_t *obj);
extern idsparse_t *    	idsparse_cpy(idsparse_t *obj);

/* set functions */
extern void            	idsparse_set_at_byref(idsparse_t *obj, int i, int ind, void *p_value);
extern void            	idsparse_set_at_byvalue(idsparse_t *obj, int i, int ind, void *h_value);
extern void            	idsparse_upd_numnz(idsparse_t *obj, int numnz_new);
extern void            	idsparse_upd_nvars(idsparse_t *obj, int nvars_new);

/* get functions */
extern int           	idsparse_nvars(idsparse_t *obj);
extern int           	idsparse_numnz(idsparse_t *obj);
extern int *           	idsparse_ind(idsparse_t *obj);
extern void *        	idsparse_values(idsparse_t *obj);
extern void **        	idsparse_pointers(idsparse_t *obj);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	static int		    idsparse_destroy_cnt = 0;
#endif
#define iddblsparse(nvars,numnz) idsparse_byvalue(nvars,numnz,sizeof(double));

struct idsparse_s{
	/* capsulated member functions */

	// destroy sparse
	void       	(*destroy)(idsparse_t *obj);
	// copy sparse
	idsparse_t *	(*cpy)(idsparse_t *obj);

	/* set functions */

	// set i-th nonzero value at index ind and values p_value
	void       	(*set_at)(idsparse_t *obj, int i, int ind, void *p_value);
	// update the length (i.e., number of nonzero values, numnz)
	void        (*upd_numnz)(idsparse_t *obj, int numnz_new);
	// update the dimension (i.e., nvars)
	void        (*upd_nvars)(idsparse_t *obj, int nvars_new);

	/* get functions */
	int      	(*nvars)(idsparse_t *obj);
	int      	(*numnz)(idsparse_t *obj);
	int *      	(*ind)(idsparse_t *obj);
	// values vector (only byval sparse)
	void * 		(*values)(idsparse_t *obj);
	// vector of reference to values (byptr or byref sparse)
	void ** 	(*pointers)(idsparse_t *obj);
	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_ARRAY
	int		      m_id;
#endif
	int    	m_nvars,
			m_numnz,
			*mp_ind;
	void	**mpp_val,
			*mp_val;
	size_t	m_sizeof_value;
	void*   (*mh_cpy_value)(void *value);
	void    (*mh_destroy_value)(void *value);
};


#ifdef __cplusplus
}
#endif

#endif /* SOLVER_SPARSE_H_ */
