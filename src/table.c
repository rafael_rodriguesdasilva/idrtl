/*
 * table.c
 *
 *  Created on: Jul 24, 2019
 *      Author: rafael
 */


#include "table.h"

static idtable_t *idtable_default_constructor(void) {
	idtable_t *obj = malloc(sizeof(idtable_t));
	assert(obj!=NULL);

	obj->destroy = idtable_destroy;
	obj->cpy = idtable_cpy;
	obj->set_at = NULL;
	obj->remove_at = idtable_remove_at;
	obj->remove_row = idtable_remove_row;
	obj->remove_col = idtable_remove_col;
	obj->rows = idtable_rows;
	obj->cols = idtable_cols;
	obj->get_at = idtable_get_at;

#ifdef IDRTL_DEBUG_MEMLEAK_TABLE
	static int idtable_last_id = 0;
	obj->m_id = idtable_last_id;
	printf("debug memleak construct table %d\n", obj->m_id);
	idtable_last_id++;
	idtable_destroy_cnt++;
#endif
	obj->m_sizeof_value = NULL;
	obj->mh_cpy_value = NULL;
	obj->mh_destroy_value = NULL;
	obj->mp_rows = idarray_byptr(idarray_destroy,idarray_cpy);
	obj->mp_cols = idarray_byptr(idarray_destroy,idarray_cpy);

	return obj;
}


/* public functions */
idtable_t *     	idtable_byvalue(size_t sizeof_value) {
	idtable_t *obj = idtable_default_constructor();

	obj->m_sizeof_value = sizeof_value;
	obj->mh_cpy_value = NULL;
	obj->mh_destroy_value = free;
	obj->set_at = idtable_set_at_byvalue;

	return obj;
}
idtable_t *     	idtable_byptr(void (*destroy_value)(void *value), void* (*cpy_value)(void *value)) {
	idtable_t *obj = idtable_default_constructor();

	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy_value = cpy_value;
	if (destroy_value==NULL) {
		obj->mh_destroy_value = free;
	} else {
		obj->mh_destroy_value = destroy_value;
	}
	obj->set_at = idtable_set_at_byptr;

	return obj;
}
idtable_t *     	idtable_byref(void) {
	idtable_t *obj = idtable_default_constructor();

	obj->m_sizeof_value = sizeof(void *);
	obj->mh_cpy_value = idref_cpy;
	obj->mh_destroy_value = NULL;
	obj->set_at = idtable_set_at_byref;

	return obj;
}
void             idtable_destroy(idtable_t *obj) {
	if (obj!=NULL) {
#ifdef IDRTL_DEBUG_MEMLEAK_TABLE
		idtable_destroy_cnt--;
		printf("debug memleak destroy table %d still remains %d to destroy\n", obj->m_id, idtable_destroy_cnt);
		if (idtable_destroy_cnt==0) {
			printf("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
		}
#endif
		idarray_destroy(obj->mp_rows);
		idarray_destroy(obj->mp_cols);
		free(obj);
	}
}
idtable_t *     	idtable_cpy(idtable_t *obj) {
	assert(obj!=NULL);

	idrb_node_t *it1, *it2, *row;
	idtable_t *lst = idtable_default_constructor();
	int i, j;
	void *p_value, *h_value;

	lst->m_sizeof_value = obj->m_sizeof_value;
	lst->mh_cpy_value = obj->mh_cpy_value;
	lst->mh_destroy_value = obj->mh_destroy_value;
	lst->set_at = obj->set_at;

	idrb_traverse(it1, obj->mp_rows->mp_head) {
		i = it1->keyi(it1);
		row = (idrb_node_t *)it1->value(it1);
		idrb_traverse(it2, row) {
			j = it2->keyi(it2);
			h_value = it2->value(it2);
			if (lst->mh_cpy_value!=NULL) {
				p_value = lst->mh_cpy_value(h_value);
			} else {
				p_value = malloc(lst->m_sizeof_value);
				assert(p_value!=NULL);
				memcpy(p_value,h_value,lst->m_sizeof_value);
			}
			idtable_set_at_byref(lst,i,j,p_value);
		}
	}

	return lst;
}

/* set functions */
static void idtable_set(idtable_t *obj, int i, int j, void *h_value_new, int type) {
	assert(obj!=NULL);
	assert(h_value_new!=NULL);

	idarray_t *h_row, *h_col, *p_row, *p_col;
	void *p_value;

	h_row = (idarray_t *)obj->mp_rows->get_at(obj->mp_rows,i);
	if (h_row!=NULL) {
		h_row->set_at(h_row,j,h_value_new);
	} else {
		if (type==0) { // by value
			p_row = idarray_byvalue(obj->m_sizeof_value);
		} else if (type == 1) { // by ptr
			p_row = idarray_byptr(obj->mh_destroy_value,obj->mh_cpy_value);
		} else if (type == 2) { // by ref
			p_row = idarray_byref();
		}
		p_row->set_at(p_row,j,h_value_new);
		obj->mp_rows->set_at(obj->mp_rows,i,p_row);
		h_row = p_row;
	}
	p_value = h_row->get_at(h_row,j);
	h_col = (idarray_t *)obj->mp_cols->get_at(obj->mp_cols,j);
	if (h_col!=NULL) {
		h_col->set_at(h_col,i,p_value);
	} else {
		p_col = idarray_byref();
		p_col->set_at(p_col,i,p_value);
		obj->mp_cols->set_at(obj->mp_cols,j,p_col);
	}
}

void            idtable_set_at_byref(idtable_t *obj, int i, int j, void *p_value) {
	assert(obj!=NULL);
	idtable_set(obj,i,j,p_value,2);
}
void            idtable_set_at_byvalue(idtable_t *obj, int i, int j, void *h_value) {
	assert(obj!=NULL);
	idtable_set(obj,i,j,h_value,0);
}
void            idtable_set_at_byptr(idtable_t *obj, int i, int j, void *h_value) {
	assert(obj!=NULL);
	idtable_set(obj,i,j,h_value,1);
}
void idtable_remove_at(idtable_t *obj, int i, int j) {
	assert(obj!=NULL);

	idarray_t *h_row, *h_col;

	h_row = (idarray_t *)obj->mp_rows->get_at(obj->mp_rows,i);
	if (h_row!=NULL) {
		h_row->remove_at(h_row,j);
	}
	h_col = (idarray_t *)obj->mp_cols->get_at(obj->mp_cols,j);
	if (h_col!=NULL) {
		h_row->remove_at(h_row,i);
	}
}

/* get functions */
int           idtable_rows(idtable_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_rows!=NULL);
	return obj->mp_rows->length(obj->mp_rows);
}
int           idtable_cols(idtable_t *obj) {
	assert(obj!=NULL);
	assert(obj->mp_cols!=NULL);
	return obj->mp_cols->length(obj->mp_cols);
}
void *        idtable_get_at(idtable_t *obj, int i, int j) {
	assert(obj!=NULL);

	idarray_t *h_row;

	h_row = (idarray_t *)obj->mp_rows->get_at(obj->mp_rows,i);
	if (h_row!=NULL) {
		return h_row->get_at(h_row,j);
	}
	return NULL;
}
void         	idtable_remove_row(idtable_t *obj, int i) {
	assert(obj!=NULL);
	assert(obj->mp_cols!=NULL);
	assert(obj->mp_rows!=NULL);

	idarray_t *h_row, *h_col;
	idrb_node_t *it;

	h_row = (idarray_t *)obj->mp_rows->get_at(obj->mp_rows,i);
	if (h_row!=NULL) {
		idrb_traverse(it,h_row->mp_head) {
			h_col = (idarray_t *)obj->mp_cols->get_at(obj->mp_cols,it->keyi(it));
			assert(h_col!=NULL);
			h_col->remove_at(h_col,i);
		}
		obj->mp_rows->remove_at(obj->mp_rows,i);
	}
}
void         	idtable_remove_col(idtable_t *obj, int j) {
	assert(obj!=NULL);
	assert(obj->mp_cols!=NULL);
	assert(obj->mp_rows!=NULL);

	idarray_t *h_row, *h_col;
	idrb_node_t *it;

	h_col = (idarray_t *)obj->mp_cols->get_at(obj->mp_cols,j);
	if (h_col!=NULL) {
		idrb_traverse(it,h_col->mp_head) {
			h_row = (idarray_t *)obj->mp_rows->get_at(obj->mp_rows,it->keyi(it));
			assert(h_row!=NULL);
			h_row->remove_at(h_row,j);
		}
		obj->mp_cols->remove_at(obj->mp_cols,j);
	}
}
void            	idinttable_print(idtable_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(obj->mp_cols!=NULL);
	assert(obj->mp_rows!=NULL);

	idrb_node_t *it_row, *it_col;
	idarray_t *h_col;

	idrb_traverse(it_row,obj->mp_rows->mp_head) {
		h_col = (idarray_t *)it_row->value(it_row);
		idrb_traverse(it_col,h_col->mp_head) {
			fprintf(out,"(%d,%d,%d)", it_row->keyi(it_row), it_col->keyi(it_col), *(int *)it_col->value(it_col));
		}
		if (h_col->length(h_col)>0) {
			fprintf(out,"\n");
		}
	}
}
void            	idintlisttable_print(idtable_t *obj, FILE *out) {
	assert(obj!=NULL);
	assert(obj->mp_cols!=NULL);
	assert(obj->mp_rows!=NULL);

	idrb_node_t *it_row, *it_col;
	idarray_t *h_col;
	char *str[80];

	idrb_traverse(it_row,obj->mp_rows->mp_head) {
		h_col = (idarray_t *)it_row->value(it_row);
		idrb_traverse(it_col,h_col->mp_head) {
			fprintf(out,"(%d,%d,{%s})", it_row->keyi(it_row), it_col->keyi(it_col),
					idintlist_to_str((idlist_t *)it_col->value(it_col),str));
		}
		if (h_col->length(h_col)>0) {
			fprintf(out,"\n");
		}
	}
}
