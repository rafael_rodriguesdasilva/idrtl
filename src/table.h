/*
 * table.h
 *
 *  Created on: Jul 24, 2019
 *      Author: rafael
 */

#ifndef SOLVER_TABLE_H_
#define SOLVER_TABLE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

/* public functions */
extern idtable_t *     	idtable_byvalue(size_t sizeof_value);
extern idtable_t *     	idtable_byptr(void (*destroy_value)(void *value), void* (*cpy_value)(void *value));
extern idtable_t *     	idtable_byref();
extern void             idtable_destroy(idtable_t *obj);
extern idtable_t *     	idtable_cpy(idtable_t *obj);

/* set functions */
extern void            	idtable_set_at_byref(idtable_t *obj, int i, int j, void *p_value);
extern void            	idtable_set_at_byvalue(idtable_t *obj, int i, int j, void *h_value);
extern void            idtable_set_at_byptr(idtable_t *obj, int i, int j, void *h_value);
extern void         	idtable_remove_at(idtable_t *obj, int i, int j);
extern void         	idtable_remove_row(idtable_t *obj, int i);
extern void         	idtable_remove_col(idtable_t *obj, int j);

/* get functions */
extern int           	idtable_rows(idtable_t *obj);
extern int           	idtable_cols(idtable_t *obj);
extern void *        	idtable_get_at(idtable_t *obj, int i, int j);

/* operation */
#ifdef IDRTL_DEBUG_MEMLEAK_TABLE
	static int		    idtable_destroy_cnt = 0;
#endif
#define idinttable(void) idtable_byvalue(sizeof(int));
#define idintlisttable(void) idtable_byptr(idlist_destroy,idlist_cpy);
extern void            	idinttable_print(idtable_t *obj, FILE *out);
extern void            	idintlisttable_print(idtable_t *obj, FILE *out);

struct idtable_s{
	/* capsulated member functions */
	void       	(*destroy)(idtable_t *obj);
	idtable_t *	(*cpy)(idtable_t *obj);

	/* set functions */
	void       	(*set_at)(idtable_t *obj, int i, int j, void *value);
	void  		(*remove_at)(idtable_t *obj, int i, int j);
	void  		(*remove_row)(idtable_t *obj, int i);
	void  		(*remove_col)(idtable_t *obj, int j);

	/* get functions */
	int      	(*rows)(idtable_t *obj);
	int      	(*cols)(idtable_t *obj);
	void * 		(*get_at)(idtable_t *obj, int i, int j);
	/* operation */

	/* private variables - do not access directly */
#ifdef IDRTL_DEBUG_MEMLEAK_TABLE
	int		      m_id;
#endif
	idarray_t 		*mp_rows,
					*mp_cols;
	size_t			m_sizeof_value;
	void*          (*mh_cpy_value)(void *value);
	void*		   (*mh_destroy_value)(void *value);
};


#ifdef __cplusplus
}
#endif


#endif /* SOLVER_TABLE_H_ */
