/*
 * utils.c
 *
 *  Created on: Jan 30, 2019
 *      Author: rafael
 */
#include "idrtl.h"

GRBenv   *grbenv_ = NULL;

void idmalloc_sparse(int **hp_ind, double **hp_val, int length) {
	*hp_ind = malloc(length*sizeof(int));
	assert(*hp_ind!=NULL);
	*hp_val = malloc(length*sizeof(double));
	assert(*hp_val!=NULL);
}

char *idprefix(char *str, const char *name, int i) {
	sprintf(str,name, i);
	return str;
}
void id_set_global_constants(void) {
	assert(idGRB_NoError(GRBloadenv(&grbenv_, NULL))==0);
	assert(idGRB_NoError(GRBsetintparam(grbenv_, "OutputFlag", 0))==0);
	srand(time(NULL));

}

void id_destroy_global_constants(void) {
	GRBfreeenv(grbenv_);
}
int idGRB_NoError(int error) {
	if (error) {
		printf("ERROR: %s\n", GRBgeterrormsg(grbenv_));
	}
	return error;
}
void *idref_cpy(void *p_value) {
	return p_value;
}
void *idmk_value(void *h_value, size_t sizeof_value) {
	void *p_value = malloc(sizeof_value);
	assert(p_value!=NULL);
	memcpy(p_value,h_value,sizeof_value);
	return p_value;
}

// linear programming for small and full matrix constraint A
// return true if the following problem is feasible:
// fval = min c'*x s.t. A*x <= b, where A is mxn double matrix, b is m vector, x and c is a n vector.
//idbool_t idlinprog(idmatrix_t *c, idmatrix_t *A, idmatrix_t *b, double *fval, double **x) {
//	assert(c!=NULL);
//	assert(A!=NULL);
//	assert(b!=NULL);
//	assert(fval!=NULL);
//
//	size_t n, m, i, j;
//	idmatrix_t *H, *V, *a, *F;
//	double fmin, fcur, v, *p_val;
//	int *p_ind, nvars, numnz, optimstatus;
//	n = A->cols(A);
//	m = A->rows(A);
//	assert(b->rows(b)==m);
//	assert(b->cols(b)==1);
//	assert(c->rows(c)==n);
//	assert(c->cols(c)==1);
//
//	if ((n>5)||(m>10)) {
//		GRBmodel 	*p_model = grb_mk_model();
//		for(i=0;i<n;i++) {
//			v = c->getat(c,i,0);
//			if (fabs(v) <= ID_ABSTOL) {
//				assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, 0.0, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);
//			} else {
//				assert(idGRB_NoError(GRBaddvar(p_model, 0, NULL, NULL, -v, -GRB_INFINITY, GRB_INFINITY, GRB_CONTINUOUS, NULL))==0);
//			}
//		}
//
//		for(i=0;i<m;i++) {
//			A->to_sparse_rowvector(A,i,&nvars,&numnz,&p_ind,&p_val);
//			v = b->getat(b,i,0);
//			assert(idGRB_NoError(GRBaddconstr(p_model, numnz, p_ind, p_val, GRB_LESS_EQUAL, v, NULL))==0);
//			free(p_ind);
//			free(p_val);
//		}
//
//		optimstatus = grb_optimize(p_model, fval);
//
//		if ((x!=NULL)&&(optimstatus == GRB_OPTIMAL)) {
//			p_ind = malloc(n*sizeof(int));
//			assert(p_ind!=NULL);
//			for(i=0;i<n;i++) {
//				p_ind[i] = i;
//			}
//			assert(idGRB_NoError(GRBgetdblattrarray(p_model, GRB_DBL_ATTR_X, n, p_ind, x))==0);
//			free(p_ind);
//		}
//
//		GRBfreemodel(p_model);
//		return (optimstatus == GRB_OPTIMAL?TRUE:FALSE);
//	}
//
//	H = b->stackHoriz(b,A);
//
//	V = H2V(H);
//
//	if ((V==NULL)||(V->rows(V)==0)) {
//		return FALSE;
//	}
//
//	a = id_zeros(1,1);
//	a = a->stackVert(a,c);
//	F = V->multiply(V->cpy(V),a);
//
//	fmin = ID_INF;
//	for(i=0;i<F->rows(F);i++) {
//		if (V->iszero(V,i,0)==1) {
//			// it means that is not full-dimensional; thus, something went wrong.
//			return FALSE;
//		}
//		fcur = F->getat(F,i,0);
//		if (fcur<fmin) {
//			j = i;
//			fmin = fcur;
//		}
//	}
//
//	(*fval) = fmin;
//	if (x!=NULL) {
//		for(i=0;i<n;i++) {
//			(*x)[i] = V->getat(V,j,i+1);
//		}
//	}
//	return TRUE;
//}

int idintcmpfunc_(const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int idcharcmpfunc_(const void * a, const void * b) {
   return ( *(char*)a - *(char*)b );
}


int idbinary_search_int(int *h_arr, int size, int val) {
	if (size==0) {
		return -1;
	}

	int l, u, m;


	l = 0;
	u = size-1;

	while(l<=u) {
		m = (l+u)/2;

		if (h_arr[m]<val) {
			l=m+1;
		} else if (h_arr[m] == val) {
			break;
		} else {
			u=m-1;
		}
	}

	if (l>u) {
		return -1;
	} else {
		return (int)m;
	}
}

void free_dblarr(double **arr, int len) {
	for(int i=0;i<len;i++) {
		free(arr[i]);
	}
}
