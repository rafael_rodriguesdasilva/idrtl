/*
 * utils.h
 *
 *  Created on: Jan 30, 2019
 *      Author: rafael
 */

#ifndef UTILS_H_
#define UTILS_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "lrsinterface.h"
#include <time.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include<string.h>
#include<setjmp.h>
#include "gurobi_c.h"
#include<z3.h>

extern GRBenv   *grbenv_;

typedef int idbool_t;
#define TRUE 1
#define FALSE 0

#define ID_ABSTOL  	 0.000001 // 1E-6
#define ID_INF GRB_INFINITY
#define ID_STR 80

enum idvalue_type{
    IDNUMBER_CHAR          = (0x01),
	IDNUMBER_UNSIGNED_CHAR = (0x02),
	IDNUMBER_SIGNED_CHAR   = (0x03),
	IDNUMBER_INT           = (0x04),
	IDNUMBER_UNSIGNED_INT  = (0x05),
	IDNUMBER_SHORT         = (0x06),
	IDNUMBER_LONG          = (0x07),
	IDNUMBER_UNSIGNED_LONG = (0x08),
	IDNUMBER_FLOAT         = (0x09),
	IDNUMBER_DOUBLE        = (0x0A),
	IDNUMBER_LONG_DOUBLE   = (0x0B)
};

enum {
	RTL_ATOMICPROP         = (0x01),
    RTL_AND		           = (0x02),
    RTL_OR		           = (0x03),
	RTL_UNTIL              = (0x04),
	RTL_RELEASE            = (0x05),
	RTL_ALWAYS             = (0x06),
	RTL_EVENTUALLY         = (0x07)
};

extern void id_set_global_constants(void);
extern void id_destroy_global_constants(void);
extern int idGRB_NoError(int error);

//extern idbool_t idlinprog(idmatrix_t *c, idmatrix_t *A, idmatrix_t *b, double *fval, double **x);

extern int idintcmpfunc_(const void * a, const void * b);
extern int idcharcmpfunc_(const void * a, const void * b);
extern int idbinary_search_int(int *h_arr, int size, int val);
extern void free_dblarr(double **arr, int len);

extern char *idprefix(char *str, const char *name, int i);
extern void idmalloc_sparse(int **hp_ind, double **hp_val, int length);
extern void *idref_cpy(void *p_value);
extern void *idmk_value(void *h_value, size_t sizeof_value);

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H_ */
