/*
 * z3_interface.h
 *
 *  Created on: May 23, 2019
 *      Author: rafael
 */

#ifndef Z3_INTERFACE_H_
#define Z3_INTERFACE_H_

#include "idrtl.h"

#ifdef __cplusplus
extern "C" {
#endif

extern idbool_t z3_get_bool(Z3_context ctx, Z3_model m, Z3_func_decl cnst);
/**
   \brief Create an AST node representing bitvec1 ~= bitvec2 where bitvec1 and bitvec2 are bitvector with length len..
*/
extern Z3_ast mk_bitvec_neq(Z3_context ctx, Z3_ast *bitvec1, Z3_ast *bitvec2, int len);
/**
   \brief Create an AST node representing t1 and t2..
*/
extern Z3_ast mk_and(Z3_context ctx, Z3_ast t1, Z3_ast t2);

/**
   \brief Create an AST node representing t1 or t2..
*/
extern Z3_ast mk_or(Z3_context ctx, Z3_ast t1, Z3_ast t2);

/**
   \brief exit gracefully in case of error.
*/
extern void exitf(const char* message);

/**
   \brief exit if unreachable code was reached.
*/
extern void unreachable();

/**
   \brief Simpler error handler.
 */
extern void error_handler(Z3_context c, Z3_error_code e);

static jmp_buf g_catch_buffer;
/**
   \brief Low tech exceptions.

   In high-level programming languages, an error handler can throw an exception.
*/
extern void throw_z3_error(Z3_context c, Z3_error_code e);

/**
   \brief Error handling that depends on checking an error code on the context.

*/

extern void nothrow_z3_error(Z3_context c, Z3_error_code e);

/**
   \brief Create a logical context.

   Enable model construction. Other configuration parameters can be passed in the cfg variable.

   Also enable tracing to stderr and register custom error handler.
*/
extern Z3_context mk_context_custom(Z3_config cfg, Z3_error_handler err);

extern Z3_solver mk_solver(Z3_context ctx);

extern void del_solver(Z3_context ctx, Z3_solver s);

/**
   \brief Create a logical context.

   Enable model construction only.

   Also enable tracing to stderr and register standard error handler.
*/
extern Z3_context mk_context();

/**
   \brief Create a logical context.

   Enable fine-grained proof construction.
   Enable model construction.

   Also enable tracing to stderr and register standard error handler.
*/
extern Z3_context mk_proof_context();

/**
   \brief Create a variable using the given name and type.
*/
extern Z3_ast mk_var(Z3_context ctx, const char * name, Z3_sort ty);

/**
   \brief Create a boolean variable using the given name.
*/
extern Z3_ast mk_bool_var(Z3_context ctx, const char * name);

/**
   \brief Create an integer variable using the given name.
*/
extern Z3_ast mk_int_var(Z3_context ctx, const char * name);

/**
   \brief Create a Z3 integer node using a C int.
*/
extern Z3_ast mk_int(Z3_context ctx, int v);

/**
   \brief Create a real variable using the given name.
*/
extern Z3_ast mk_real_var(Z3_context ctx, const char * name);

/**
   \brief Create the unary function application: <tt>(f x)</tt>.
*/
extern Z3_ast mk_unary_app(Z3_context ctx, Z3_func_decl f, Z3_ast x);

/**
   \brief Create the binary function application: <tt>(f x y)</tt>.
*/
extern Z3_ast mk_binary_app(Z3_context ctx, Z3_func_decl f, Z3_ast x, Z3_ast y);

/**
   \brief Check whether the logical context is satisfiable, and compare the result with the expected result.
   If the context is satisfiable, then display the model.
*/
extern void check(Z3_context ctx, Z3_solver s, Z3_lbool expected_result);
/**
   \brief Prove that the constraints already asserted into the logical
   context implies the given formula.  The result of the proof is
   displayed.

   Z3 is a satisfiability checker. So, one can prove \c f by showing
   that <tt>(not f)</tt> is unsatisfiable.

   The context \c ctx is not modified by this function.
*/
extern void prove(Z3_context ctx, Z3_solver s, Z3_ast f, Z3_bool is_valid);

/**
   \brief Assert the axiom: function f is injective in the i-th argument.

   The following axiom is asserted into the logical context:
   \code
   forall (x_0, ..., x_n) finv(f(x_0, ..., x_i, ..., x_{n-1})) = x_i
   \endcode

   Where, \c finv is a fresh function declaration.
*/
extern void assert_inj_axiom(Z3_context ctx, Z3_solver s, Z3_func_decl f, unsigned i);

/**
   \brief Assert the axiom: function f is commutative.

   This example uses the SMT-LIB parser to simplify the axiom construction.
*/
extern void assert_comm_axiom(Z3_context ctx, Z3_solver s, Z3_func_decl f);

/**
   \brief Z3 does not support explicitly tuple updates. They can be easily implemented
   as macros. The argument \c t must have tuple type.
   A tuple update is a new tuple where field \c i has value \c new_val, and all
   other fields have the value of the respective field of \c t.

   <tt>update(t, i, new_val)</tt> is equivalent to
   <tt>mk_tuple(proj_0(t), ..., new_val, ..., proj_n(t))</tt>
*/
extern Z3_ast mk_tuple_update(Z3_context c, Z3_ast t, unsigned i, Z3_ast new_val);

/**
   \brief Display a symbol in the given output stream.
*/
extern void display_symbol(Z3_context c, FILE * out, Z3_symbol s);

/**
   \brief Display the given type.
*/
extern void display_sort(Z3_context c, FILE * out, Z3_sort ty);

/**
   \brief Custom ast pretty printer.

   This function demonstrates how to use the API to navigate terms.
*/
extern void display_ast(Z3_context c, FILE * out, Z3_ast v);

/**
   \brief Custom function interpretations pretty printer.
*/
extern void display_function_interpretations(Z3_context c, FILE * out, Z3_model m);

/**
   \brief Custom model pretty printer.
*/
extern void display_model(Z3_context c, FILE * out, Z3_model m);

/**
   \brief Similar to #check, but uses #display_model instead of #Z3_model_to_string.
*/
extern void check2(Z3_context ctx, Z3_solver s, Z3_lbool expected_result);

/**
   \brief Display Z3 version in the standard output.
*/
extern void display_version();

#ifdef __cplusplus
}
#endif

#endif /* Z3_INTERFACE_H_ */
